# Tango Kernel Follow-up Meeting
Held on 2025/1/9 at 15:00 CET on Zoom.
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Benjamin Bertrand (MAX IV)
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Sergi Rubio (ALBA)
- Yury Matveev (DESY)
- Thomas Juerges (SKAO)
- Thomas Braun (byte physics e.K.)
- Thomas Ives (OSL)

## Status of [Actions defined in the previous meetings](../../2024/2024-12-12/Minutes.md#summary-of-remaining-actions)

All:
- Comments are welcome on issue https://gitlab.com/tango-controls/rfc/-/issues/168
  (Should the device name specification require domain/family/member to start with ALPHA?).
  This issue should be solved at the next kernel meeting.

-> Done

- Contribute to the MR about Telemetry RFC -> https://gitlab.com/tango-controls/rfc/-/merge_requests/178

-> Still open.

Andy (ESRF):
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango
  controls) Andy will check with our Tango-Controls web host if they can provide what we need, how files
  can be uploaded, etc.
- [ ] Move from slack to mattermost (feel free to delegate)

-> not present

Damien (ESRF):
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
  recommended, Windows not supported, or whatever the case.

-> not present

Gwenaëlle (SOLEIL):
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
  version, and you can no longer configure a default log level from code. It is a bad practice to put a
  logging conf file because it risks overloading that of the project which has another one.

-> not present

Lorenzo (Elettra):
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

-> not present

Reynald (ESRF):
- [ ] Ask Webu for a quote about SSO using Gitlab.com for Mattermost.
**Answer from Webu**:
> It's possible to configure Mattermost to be able to connect through Gitlab.com account,
> but we need to have another way of creating an account or login in case users without gitlab.com account want to login.
> It's also possible to add the gitlab plugin to Mattermost : https://docs.mattermost.com/integrate/gitlab-interoperability.html
>
> The setup of gitlab.com SSO with a self-hosted Mattermost instance should be pretty straight forward (compared to the
> data migration), we will try to do it during the installation/config/migration of Mattermost (around 4 days).

So the Mattermost migration would cost:
- Installation / config / production of Mattermost with migration of data from Slack to Mattermost, around 4 days
- Extra VM needed here, for 666€ HT per year (2Go ram, 25Go DD,2CPU), adminsys time is free/shared with your current vm
  In option :
  Webu can synchronize account from Django to Mattermost (automatic creation of Mattermost account when a django account is created),
  It might be useful for newcomers
  => around 3 days

- [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available
  both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
  organization can be killed. Don't remove name, if someone else could use it in the future. All images should
  be deleted and a disclaimer should be available to notify the potential users
  Some images are still pulled. Some images seem to be still used in some projects? See
  https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
  https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
  gitlab-ci-templates!3 (comment 942846713)

-> not present

Sergio (ALBA):
- [ ] Work on cppTango#814, dynamic device attributes (will be done later).
- [ ] Will revise fandango section in new tango-doc

-> no progress

Thomas B. (byte physics):
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
  "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] TangoTickets#103 (comment 1723745893)
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same
  system. TB will create a bug report for Duncan.

-> no progress

Thomas J. (SKAO):
- [x] TJ to make a list with all the java tools that would need to have a release page/or be built by the Tango Source Distribution.
Jars that one has to find in various places:
Astor.jar
DBBench.jar
HDB++.jar
HDBConfigurator.jar
HDBViewer.jar
JSSHTerminal.jar
JTango.jar
Jive.jar
LogViewer.jar
Pogo.jar
RestServer.jar
jcalendar.jar
jhdbviewer.jar
jython.jar
log4j.jar

Tasks (Benjamin):
- [ ] Update java CI template to zip the fat jar (programs) or all jar files (libraries) and attach it to the release page on tagging

Vincent (MAX IV) & Yury (DESY):
- [ ] set up a v10 sync and telemetry RFC meeting
- [x] create MR for telemetry RFC
- [x] Create a MR in tango-doc ( to modify https://tango-controls.readthedocs.io/en/doc-restructure-template/Reference/ecosystem.html) -> https://gitlab.com/tango-controls/tango-doc/-/merge_requests/459
- [ ]  Send the link to this MR to the kernel mailing list/slack asking maintainers to update it
- [ ] Replies from SC for the questions from 2024-11-14:

## Discussion needed issues

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

## High priority issues

## Icalepcs 2025
- Tango Controls workshop
The group shares the opinion that a Tango Controls workshop would be very beneficial for the community.
Task for TJ:
    - [ ] Marco B was mentioned. Get in touch with him and ask him if he can gauge the interest for a workshop in the steering committee.

- Status paper
TJ offered to shepherd the process for the paper. Same people will be working on the same topics.. We want to get started now.
Tasks for TJ:
    - [ ] Create Slack channel
    - [ ] Invite people to Slack channel
    - [ ] Upload old paper to Overleaf
    - [ ] Share link to paper with group on Slack

- Tango stickers
Who pays for the stickers? 150€-200€ for different stickers.
Tasks for Sergi
- [ ] Ask Zibi as a member of the executive what the limit is that we can spend on stickers

## AOB

Happy New Year 2025! \o/

### Next Kernel Follow-up Meeting
https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 23rd January 2025 at 15:00 CET.

## Summary of remaining actions

All:
- Contribute to the MR about Telemetry RFC -> https://gitlab.com/tango-controls/rfc/-/merge_requests/178

Andy (ESRF):
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango
  controls) Andy will check with our Tango-Controls web host if they can provide what we need, how files
  can be uploaded, etc.
- [ ] Move from slack to mattermost (feel free to delegate)

Benjamin (MaxIV):
- [ ] Update java CI template to zip the fat jar (programs) or all jar files (libraries) and attach it to the
  release page on tagging. Ensure that all the following projects use it:
  - Astor.jar
  - DBBench.jar
  - HDB++.jar
  - HDBConfigurator.jar
  - HDBViewer.jar
  - JSSHTerminal.jar
  - JTango.jar
  - Jive.jar
  - LogViewer.jar
  - Pogo.jar
  - RestServer.jar
  - jcalendar.jar
  - jhdbviewer.jar
  - jython.jar
  - log4j.jar

Damien (ESRF):
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
  recommended, Windows not supported, or whatever the case.

Gwenaëlle (SOLEIL):
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
  version, and you can no longer configure a default log level from code. It is a bad practice to put a
  logging conf file because it risks overloading that of the project which has another one.

Lorenzo (Elettra):
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

Reynald (ESRF):
- [ ] Ask Webu for a quote about SSO using Gitlab.com for Mattermost.
**Answer from Webu**:
> It's possible to configure Mattermost to be able to connect through Gitlab.com account,
> but we need to have another way of creating an account or login in case users without gitlab.com account want to login.
> It's also possible to add the gitlab plugin to Mattermost : https://docs.mattermost.com/integrate/gitlab-interoperability.html
>
> The setup of gitlab.com SSO with a self-hosted Mattermost instance should be pretty straight forward (compared to the
> data migration), we will try to do it during the installation/config/migration of Mattermost (around 4 days).

So the Mattermost migration would cost:
- Installation / config / production of Mattermost with migration of data from Slack to Mattermost, around 4 days
- Extra VM needed here, for 666€ HT per year (2Go ram, 25Go DD,2CPU), adminsys time is free/shared with your current vm
  In option :
  Webu can synchronize account from Django to Mattermost (automatic creation of Mattermost account when a django account is created),
  It might be useful for newcomers
  => around 3 days

- [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available
  both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
  organization can be killed. Don't remove name, if someone else could use it in the future. All images should
  be deleted and a disclaimer should be available to notify the potential users
  Some images are still pulled. Some images seem to be still used in some projects? See
  https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
  https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
  gitlab-ci-templates!3 (comment 942846713)

Sergio (ALBA):
- [ ] Work on cppTango#814, dynamic device attributes (will be done later).
- [ ] Will revise fandango section in new tango-doc
- [ ] Ask Zibi as a member of the executive what the limit is that we can spend on stickers (~200€)

Thomas B. (byte physics):
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
  "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] TangoTickets#103 (comment 1723745893)
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same
  system. TB will create a bug report for Duncan.

Thomas J. (SKAO):
Tasks for ICALEPS 2025:
    - [ ] Marco B was mentioned. Get in touch with him and ask him if he can gauge the interest for a workshop in the steering committee.
    - [ ] Create Slack channel
    - [ ] Invite people to Slack channel
    - [ ] Upload old paper to Overleaf
    - [ ] Share link to paper with group on Slack

Vincent (MAX IV) & Yury (DESY):
- [ ] set up a v10 sync and telemetry RFC meeting
- [ ] Send the link to this MR, https://gitlab.com/tango-controls/tango-doc/-/merge_requests/459, to the kernel mailing list/slack asking maintainers to update it
- [ ] Replies from SC for the questions from 2024-11-14

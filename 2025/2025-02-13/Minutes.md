# Tango Kernel Follow-up Meeting
Held on 2025/02/13 at 15:00 CET on Zoom.
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Benjamin Bertrand (MAX IV)
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Sergi Rubio (ALBA)
- Yury Matveev (DESY)
- Thomas Braun (byte physics e.K.)
- Lukasz Zytniak (S2Innovation)
- Stéphane Poirier (SOLEIL)

## Status of [Actions defined in the previous meetings](../../2025/2025-01-23/Minutes.md#summary-of-remaining-actions)

All:
- Contribute to the MR about Telemetry RFC -> https://gitlab.com/tango-controls/rfc/-/merge_requests/178

Andy (ESRF):
- [ ] Find a location were to host the TangoBox files. Andy will check with webu, our Tango-Controls web host
  if they can provide what we need, how files
  can be uploaded, etc.
- [X] Move from slack to mattermost (feel free to delegate) . We will move to HZDR Mattermost

Benjamin (MaxIV):
- [ ] Update java CI template to zip the fat jar (programs) or all jar files (libraries) and attach it to the
  release page on tagging. Ensure that all the following projects use it:
  - Astor.jar
  - DBBench.jar
  - HDB++.jar
  - HDBConfigurator.jar
  - HDBViewer.jar
  - JSSHTerminal.jar
  - JTango.jar
  - Jive.jar
  - LogViewer.jar
  - Pogo.jar
  - RestServer.jar
  - jcalendar.jar
  - jhdbviewer.jar
  - jython.jar
  - log4j.jar
> No news

Damien (ESRF):
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
  recommended, Windows not supported, or whatever the case.

Gwenaëlle (SOLEIL):
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
  version, and you can no longer configure a default log level from code. It is a bad practice to put a
  logging conf file because it risks overloading that of the project which has another one.
- [ ] Adapt HDB++ to RFC naming scheme executable

Lorenzo (Elettra):
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

Reynald (ESRF):
- [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available
  both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
  organization can be killed. Don't remove name, if someone else could use it in the future. All images should
  be deleted and a disclaimer should be available to notify the potential users
  Some images are still pulled. Some images seem to be still used in some projects? See
  https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
  https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
  gitlab-ci-templates!3 (comment 942846713)

Sergio (ALBA):
- [ ] Work on cppTango#814, dynamic device attributes (will be done later).
- [ ] Will revise fandango section in new tango-doc
- [ ] Ask Zibi as a member of the executive what the limit is that we can spend on stickers
- [ ] Replicate tests on set_timeout_millis using Tango v10.0.2, create ticket if the issues persist (with code to replicate)

Thomas B. (byte physics):
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
  "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] TangoTickets#103 ([comment 1723745893](https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893))
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same
  system. TB will create a bug report for Duncan.

> No progress.

Thomas J. (SKAO):

Tasks for ICALEPCS 2025:
- [X] Create Slack channel  => Needs to be created as well on Mattermost
- [ ] Invite people to Slack channel
- [ ] Upload old paper to Overleaf
- [ ] Share link to paper with group on Slack
- [ ] Organize tango workshop

Yury (DESY):
- [ ] set up a v10 sync and telemetry RFC meeting

> No progress.

Thomas Ives (OSL):
- [ ] Revise tango-docs "testing-tango-using-docker" page to point to the ska-tango-images documentation
- [ ] Create issues on cppTango and TangoDatabase to add and use functions for checking if a
  device/executable/server_instance name is valid


## Discussion needed issues

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

https://gitlab.com/tango-controls/TangoTickets/-/issues/119 (Event pushed by code flag in DB) will be implemented in cppTango 11.
This issue was created in TangoTickets because this is some work that could be done as well in JTango and cppTango.

## High priority issues

### PyTango pybind11 test release
Expect to have a test "dev" release available soon.  Need  help testing the big change.

https://gitlab.com/tango-controls/pytango/-/merge_requests/800

### MatterMost

This will replace Slack with this, but cannot import history.  Helmholtz Institute will host this.  New instance has been created.

Using the "Helmholtz ID" option, people can login with their own institute's account from many countries.  Other authentication options include Google and Github.

New action: Yury to create channels from Slack.

### PyTango 10.0.1 release planned

Plan is to deprecate pipes in 10.0.1, with removal in PyTango 10.1.0.  
Are there some LIMA pytango clients using pipes? 

## Icalepcs 2025

### Tango Controls workshop

### Status paper

### Tango stickers

## Tango Community Meeting 2025

Gonna take place on May 21-23 in Italy, Giulianova. Organized by INAF. Details come soon.

## AOB

Anyone who can help with Lima?
https://www.tango-controls.org/community/forum/c/development/python/problem-with-v4l2-and-property-settings/?page=1#post-5446

### Next Kernel Follow-up Meeting
https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place in 2 weeks on Thursday 27th February 2025 at 15:00 CET.

## Summary of remaining actions

All:
- Contribute to the MR about Telemetry RFC -> https://gitlab.com/tango-controls/rfc/-/merge_requests/178

Andy (ESRF):
- [ ] Find a location were to host the TangoBox files. Andy will check with webu, our Tango-Controls web host
  if they can provide what we need, how files
  can be uploaded, etc.

Benjamin (MaxIV):
- [ ] Update java CI template to zip the fat jar (programs) or all jar files (libraries) and attach it to the
  release page on tagging. Ensure that all the following projects use it:
  - Astor.jar
  - DBBench.jar
  - HDB++.jar
  - HDBConfigurator.jar
  - HDBViewer.jar
  - JSSHTerminal.jar
  - JTango.jar
  - Jive.jar
  - LogViewer.jar
  - Pogo.jar
  - RestServer.jar
  - jcalendar.jar
  - jhdbviewer.jar
  - jython.jar
  - log4j.jar

Damien (ESRF):
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
  recommended, Windows not supported, or whatever the case.

Gwenaëlle (SOLEIL):
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
  version, and you can no longer configure a default log level from code. It is a bad practice to put a
  logging conf file because it risks overloading that of the project which has another one.
- [ ] Adapt HDB++ to RFC naming scheme executable

Lorenzo (Elettra):
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

Reynald (ESRF):
- [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available
  both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
  organization can be killed. Don't remove name, if someone else could use it in the future. All images should
  be deleted and a disclaimer should be available to notify the potential users
  Some images are still pulled. Some images seem to be still used in some projects? See
  https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
  https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
  gitlab-ci-templates!3 (comment 942846713)

Sergio (ALBA):
- [ ] Work on cppTango#814, dynamic device attributes (will be done later).
- [ ] Will revise fandango section in new tango-doc
- [ ] Ask Zibi as a member of the executive what the limit is that we can spend on stickers
- [ ] Replicate tests on set_timeout_millis using Tango v10.0.2, create ticket if the issues persist (with code to replicate)

Thomas B. (byte physics):
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
  "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] TangoTickets#103 ([comment 1723745893](https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893))
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same
  system. TB will create a bug report for Duncan.

Thomas J. (SKAO):

Tasks for ICALEPCS 2025:
- [ ] Create Slack channel  => Needs to be created as well on Mattermost
- [ ] Invite people to Slack channel
- [ ] Upload old paper to Overleaf
- [ ] Share link to paper with group on Slack
- [ ] Organize tango workshop

Yury (DESY):
- [ ] set up a v10 sync and telemetry RFC meeting
- [ ] re-create relevant channels from Slack on HZDR Mattermost.

Thomas Ives (OSL):
- [ ] Revise tango-docs "testing-tango-using-docker" page to point to the ska-tango-images documentation
- [ ] Create issues on cppTango and TangoDatabase to add and use functions for checking if a
  device/executable/server_instance name is valid



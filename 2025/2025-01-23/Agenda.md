# Tango Kernel Follow-up Meeting - 2025/01/23

To be held on 2025/01/23 at 15:00 CET on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

# Agenda

## Status of [Actions defined in the previous meetings](../2025-01-09/Minutes.md#summary-of-remaining-actions)
## High priority issues
## Issues requiring discussion
## Icalepcs 2025
## Tango Community Meeting 2025
## AOB

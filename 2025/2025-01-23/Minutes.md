# Tango Kernel Follow-up Meeting
Held on 2025/01/23 at 15:00 CET on Zoom.
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Benjamin Bertrand (MAX IV)
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Sergi Rubio (ALBA)
- Yury Matveev (DESY)
- Thomas Juerges (SKAO)
- Thomas Braun (byte physics e.K.)
- Thomas Ives (OSL)
- Lukasz Zytniak (S2Innovation)

## Status of [Actions defined in the previous meetings](../../2025/2025-01-09/Minutes.md#summary-of-remaining-actions)

All:
- Contribute to the MR about Telemetry RFC -> https://gitlab.com/tango-controls/rfc/-/merge_requests/178

Andy (ESRF):
- [ ] Find a location were to host the TangoBox files. Andy will check with webu, our Tango-Controls web host if they can provide what we need, how files
  can be uploaded, etc.
- [ ] Move from slack to mattermost (feel free to delegate)

-> Not present

Benjamin (MaxIV):
- [ ] Update java CI template to zip the fat jar (programs) or all jar files (libraries) and attach it to the
  release page on tagging. Ensure that all the following projects use it:
  - Astor.jar
  - DBBench.jar
  - HDB++.jar
  - HDBConfigurator.jar
  - HDBViewer.jar
  - JSSHTerminal.jar
  - JTango.jar
  - Jive.jar
  - LogViewer.jar
  - Pogo.jar
  - RestServer.jar
  - jcalendar.jar
  - jhdbviewer.jar
  - jython.jar
  - log4j.jar

-> No update

Damien (ESRF):
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
  recommended, Windows not supported, or whatever the case.

-> Not present

Gwenaëlle (SOLEIL):
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
  version, and you can no longer configure a default log level from code. It is a bad practice to put a
  logging conf file because it risks overloading that of the project which has another one.

-> Not present

Lorenzo (Elettra):
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

-> Not present

Reynald (ESRF):
- [ ] Ask Webu for a quote about SSO using Gitlab.com for Mattermost.
  **Answer from Webu**:
> It's possible to configure Mattermost to be able to connect through Gitlab.com account,
> but we need to have another way of creating an account or login in case users without gitlab.com account want to login.
> It's also possible to add the gitlab plugin to Mattermost : https://docs.mattermost.com/integrate/gitlab-interoperability.html
>
> The setup of gitlab.com SSO with a self-hosted Mattermost instance should be pretty straight forward (compared to the
> data migration), we will try to do it during the installation/config/migration of Mattermost (around 4 days).

So the Mattermost migration would cost:
- Installation / config / production of Mattermost with migration of data from Slack to Mattermost, around 4 days
- Extra VM needed here, for 666€ HT per year (2Go ram, 25Go DD,2CPU), adminsys time is free/shared with your current vm
  In option :
  Webu can synchronize account from Django to Mattermost (automatic creation of Mattermost account when a django account is created),
  It might be useful for newcomers
  => around 3 days

- [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available
  both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
  organization can be killed. Don't remove name, if someone else could use it in the future. All images should
  be deleted and a disclaimer should be available to notify the potential users
  Some images are still pulled. Some images seem to be still used in some projects? See
  https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
  https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
  gitlab-ci-templates!3 (comment 942846713)

-> Not present

Sergio (ALBA):
- [ ] Work on cppTango#814, dynamic device attributes (will be done later).
- [ ] Will revise fandango section in new tango-doc
- [ ] Ask Zibi as a member of the executive what the limit is that we can spend on stickers

Zibi wants to make stickers for sardana and taurus as well.

Thomas B. (byte physics):
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
  "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] TangoTickets#103 ([comment 1723745893](https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893))
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same
  system. TB will create a bug report for Duncan.

-> Not progressed.

Thomas J. (SKAO):
Tasks for ICALEPCS 2025:
- [ ] Marco B was mentioned. Get in touch with him and ask him if he can gauge the interest for a workshop in the steering committee.
- [ ] Create Slack channel
- [ ] Invite people to Slack channel
- [ ] Upload old paper to Overleaf
- [ ] Share link to paper with group on Slack

There shall be a tango workshop at ICALEPS.
-[ ] TJ will organize it

Yury (DESY):
- [ ] set up a v10 sync and telemetry RFC meeting
- [x] Send the link to this MR, https://gitlab.com/tango-controls/tango-doc/-/merge_requests/459, to the kernel mailing list/slack asking maintainers to update it

-> Done right now.

- [x] Replies from SC for the questions from 2024-11-14

-> Already resolved.

## Discussion needed issues

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

## High priority issues

## Icalepcs 2025
### Tango Controls workshop
TJ will do the admin.

### Status paper

### Tango stickers

## Tango Community Meeting 2025

Gonna take place on May 21-23 in Italy, Giulianova. Orginized by INAF. Details come soon.

## AOB

TI:
ska-tango-images has some new documentation: https://developer.skao.int/projects/ska-tango-images/en/latest/.  Can I incorporate this somehow into the Tango documentation?

tango-doc repo, doc-restructure branch: source/How-To/debugging/debugging-and-testing/testing-tango-using-docker.md

RFC: What are the device server naming rules?  The RFCs suggest that they must be a valid "family", i.e. only ALPHA | DIGIT | _, however, HDB++ produces device servers called hdb++cm-srv and hdb++es-srv

Conclusion: HDB++ should change, the RFC is how things should be.

Thomas Ives Actions:
    - [ ] Revise tango-docs "testing-tango-using-docker" page to point to the ska-tango-images documentation
    - [ ] Create issues on cppTango and TangoDatabase to add and use functions for checking if a device/executable/server_instance name is valid

cpptango 10 AttributeProxy issue: https://gitlab.com/tango-controls/cppTango/-/issues/1370
Do we need a patch release? cpptango devs will decide if it's possible.

-> yes there will be a patch release

pytango documentation got revised: https://tango-controls.readthedocs.io/projects/pytango/en/latest/index.html

### Next Kernel Follow-up Meeting
https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place **in 3 weeks** on Thursday 13th February 2025 at 15:00 CET.

## Summary of remaining actions

All:
- Contribute to the MR about Telemetry RFC -> https://gitlab.com/tango-controls/rfc/-/merge_requests/178

Andy (ESRF):
- [ ] Find a location were to host the TangoBox files. Andy will check with webu, our Tango-Controls web host
  if they can provide what we need, how files
  can be uploaded, etc.
- [ ] Move from slack to mattermost (feel free to delegate)

Benjamin (MaxIV):
- [ ] Update java CI template to zip the fat jar (programs) or all jar files (libraries) and attach it to the
  release page on tagging. Ensure that all the following projects use it:
  - Astor.jar
  - DBBench.jar
  - HDB++.jar
  - HDBConfigurator.jar
  - HDBViewer.jar
  - JSSHTerminal.jar
  - JTango.jar
  - Jive.jar
  - LogViewer.jar
  - Pogo.jar
  - RestServer.jar
  - jcalendar.jar
  - jhdbviewer.jar
  - jython.jar
  - log4j.jar

Damien (ESRF):
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
  recommended, Windows not supported, or whatever the case.

Gwenaëlle (SOLEIL):
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
  version, and you can no longer configure a default log level from code. It is a bad practice to put a
  logging conf file because it risks overloading that of the project which has another one.
- [ ] Adapt HDB++ to RFC naming scheme executable

Lorenzo (Elettra):
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

Reynald (ESRF):
- [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available
  both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
  organization can be killed. Don't remove name, if someone else could use it in the future. All images should
  be deleted and a disclaimer should be available to notify the potential users
  Some images are still pulled. Some images seem to be still used in some projects? See
  https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
  https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
  gitlab-ci-templates!3 (comment 942846713)

Sergio (ALBA):
- [ ] Work on cppTango#814, dynamic device attributes (will be done later).
- [ ] Will revise fandango section in new tango-doc
- [ ] Ask Zibi as a member of the executive what the limit is that we can spend on stickers

Thomas B. (byte physics):
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
  "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] TangoTickets#103 ([comment 1723745893](https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893))
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same
  system. TB will create a bug report for Duncan.

Thomas J. (SKAO):

Tasks for ICALEPCS 2025:
- [ ] Create Slack channel
- [ ] Invite people to Slack channel
- [ ] Upload old paper to Overleaf
- [ ] Share link to paper with group on Slack
- [ ] Organize tango workshop

Yury (DESY):
- [ ] set up a v10 sync and telemetry RFC meeting

Thomas Ives (OSL):
- [ ] Revise tango-docs "testing-tango-using-docker" page to point to the ska-tango-images documentation
- [ ] Create issues on cppTango and TangoDatabase to add and use functions for checking if a
  device/executable/server_instance name is valid

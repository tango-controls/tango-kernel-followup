# Tango Kernel Follow-up Meeting - 2020/11/12

To be held on 2020/11/12 at 15:00 CET on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-10-22/Minutes.md#summary-of-remaining-actions)
 2. [Tango RFCs](https://github.com/tango-controls/rfc/wiki/Meeting-2020-11-12)
 3. [Tango Collaboration Status Meeting](https://indico.esrf.fr/indico/event/49/)
 4. Tango Kernel Webinars
 5. PyTango News
 6. JTango News
 7. cppTango News
 8. Tango Source Distribution News
 9. High priority issues
10. AOB

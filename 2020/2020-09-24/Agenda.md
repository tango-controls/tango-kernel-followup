# Tango Kernel Follow-up Meeting - 2020/09/24

To be held on 2020/09/24 at 15:00 CEST on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-09-10/Minutes.md#summary-of-remaining-actions)
 2. Tango RFCs, with [a sub-agenda](https://github.com/tango-controls/rfc/wiki/Meeting-2020-09-24)
 3. cppTango News
 4. JTango News
 5. PyTango News
 6. Tango Source Distribution News
 7. High priority issues
 8. AOB
     - Tango kernel webinar
     - Tango Meeting
     - Conda packages

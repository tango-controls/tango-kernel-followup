# Tango Kernel Follow-up Meeting - 2020/10/22

To be held on 2020/10/22 at 15:00 CEST on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-10-08/Minutes.md#summary-of-remaining-actions)
 2. [Tango RFCs](https://github.com/tango-controls/rfc/wiki/Meeting-2020-10-22)
 3. cppTango News
 4. JTango News
 5. PyTango News
 6. Tango Source Distribution News
 7. High priority issues
 8. Tango Kernel Webinar
 9. AOB
     - Tango Collaboration Meeting
     - Conda packages

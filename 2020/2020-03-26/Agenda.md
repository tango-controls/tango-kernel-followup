# Tango Kernel Follow-up Meeting - 2020/03/26

To be held on 2020/03/26 at 15:00 CET on https://esrf.zoom.us/j/936186697

# Agenda
 
 1. Tango Source Distribution News
 2. cppTango News
 3. JTango News
 4. PyTango News
 5. Tango Meeting 2020 ([Contest](https://github.com/tango-controls/meeting-2020-contest))
 6. High priority issues
 7. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-03-12/Minutes.md#summary-of-remaining-actions)
 8. AOB
     - Conda packages
     - HDB++
 
# Tango Kernel Follow-up Meeting - 2020/09/10

To be held on 2020/09/10 at 15:00 CEST on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-08-13/Minutes.md#summary-of-remaining-actions)
 2. Tango Kernel training
 3. Tango RFCs, with [a sub-agenda](https://github.com/tango-controls/rfc/wiki/Meeting-2020-09-10)
 4. cppTango News
 5. JTango News
 6. PyTango News
 7. Tango Source Distribution News
 8. Conda packages
 9. High priority issues
10. AOB

Tango Kernel Follow-up Meeting  
Held on 2022/04/07 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/kernel-meeting-2022-04-07-9ti9?lang=en

Participants:
- Benjamin Bertrand (Max IV)
- Reynald Bourtembourg (ESRF)
- Jack Yates (OSL)
- Becky Williams (OSL)
- Aya Yoshimura (OSL)
- Jairo Moldes (ALBA)
- Anton Joubert (MAX IV)
- Lorenzo Pivetta (Elettra)
- Damien Lacoste (ESRF)
- Grzegorz Kowalski (S2Innovation)
- Nicolas Leclercq (ESRF)
- Thomas Braun (byte physics e. K.)
- Thomas Juerges (SKAO)
- Yury Matveev (DESY)
- Andy Gotz (ESRF)
- Gwenaëlle Abeillé (SOLEIL)
- Lukasz Zytniak (S2Innovation)

## RFC workshop

Most institutes are allowed to travel internationally again.  Possibly in May 2022.  Lukasz will make a Doodle poll.

## TangoDatabase and TangoTest contributing rules

We need to define clear contributing rules for TangoDatabase and TangoTest device servers.  
We are looking for volunteers to become code owners and/or reviewers on these 2 repositories.

Thomas B offers to be code owner and reviewer.  Jean Luc Pons (ESRF) may also be interested, according to Reynald.

Andy to provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.

Proposal for TangoDatabase:  2 approvals - one code owner, and one other.  We need 2 code owners.  Thomas B and ??.  Lorenzo to check with Graziano.

TangoTest:  Thomas B and Nicolas L will be code owners.  Only 1 approval required.

## Status of [Actions defined in the previous meetings](../2022-03-24/Minutes.md#summary-of-remaining-actions)

**All institutes**:

- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
  > Thomas B tested, but there was an issue.  Damien has since fixed it.  Thomas B to test again.

Other issue:  can copyright in the Pogo source be changed from ESRF to Tango Controls Collaboration?  It isn't a legal entity, so that would be a problem.

- Test cppTango 9.3.5 and report encountered issues
  > ESRF using 9.3.5-rc1 in production on accelerator.  No problems so far.
  Please test and report, if you find issues.

- PyTango on Windows
  > Some issues that need fixing in cppTango and OmniORB source.  Thomas B will make fixes in cppTango.

- For TangoDatabase, would be nice to review https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/29 to enable easier Windows build
  > Thomas B has reviewed, but some issues.  Will follow up.

- Comment [cppTango#897](https://gitlab.com/tango-controls/cppTango/-/issues/897) about more frequent cppTango releases.
  > Agreed.  Item can be removed from these meetings.  Aim for 2nd October 2022 - version 9.4.0.
  Thomas J. added a comment and closed the issue on Gitlab.

- Andy reported (via Slack) that he had been given access to Ubuntu launchpad by Emmanuel Taurel. 
 > If anyone else wants access, then ask Andy.

**Anton, Lorenzo, Thomas J and Thomas B**:

- Make a proposal to streamline access - this to be sent to steering committee:  
  > Ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/63  
  Anton no longer part of this.  
  Thomas B will arrange a meeting with Thomas J and Lorenzo. Invite Andy as well.

**Andy (ESRF)**:

- Update the description of issue cppTango#822 (CORBA transient timeout message).  
  > No update.  This action will be removed from kernel meetings list of actions since there is already an open issue.

**Anton (Max IV)**:

- Make a poll about required Python versions and architectures for PyTango binary wheels.  
  > No progress.
- In PyTango, can we disable Tango state switching automatically to ALARM when state is ON and an attribute is in ALARM? Investigate and report to Thomas J.
  > Tested something today.  Can override dev_state method in a PyTango device, but check that the cppTango dev_state method isn't doing something else you need!  
  dev_state(self):
  return self.get_state()

https://gitlab.com/tango-controls/cppTango/-/blob/9d0acf65b8ee0456b71d5c6562db2333b64b4026/cppapi/server/device.cpp

**Reynald (ESRF)**:
- Ping Jean-Luc Pons to review and merge tango-controls/device-servers/DeviceClasses/communication/Socket!1
> Reviewed and merged
- Create an issue or MR in tango-doc to improve documentation on whether the DevEnum in pipes are really not supported.
> Still to be done

**Sergi (ALBA)**
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26
> No progress.

- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
 > No progress.

- Close all tickets and pull requests on archive github repos, see https://gitlab.com/tango-controls/TangoTickets/-/issues/62
 > Done.

- cppTango as DLL on Windows, see https://gitlab.com/tango-controls/pytango/-/issues/440

**Thomas J. (SKAO)**:
- Decide if joining the ADASS conference will be done now that it's virtual.

  > Talked to Jan David Mol (ASTRON). We will try to make it happen.

- Another option is SPIE? in July 2022, in Montreal, Canada.

  > SKAO supports Thomas J. in doing this. Thomas J. will send a proposal this or next week.

- Organize a Tango Workshop at the next ADASS conference.

>  Will send proposal to ADASS this week.

- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).

 >  As soon as Thomas J. receives positive news from SPIE/ADASS he will get in touch.

- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
 > Looking at batch files.  log4j?  Most tools don't need it.  LogViewer does. See the forum: https://www.tango-controls.org/community/forum/c/platforms/gnu-linux/the-logviewer-program-does-not-start-log4j/

**Jack Y. (OSL)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
 > Need to test on 9.3.5, will remove apt instructions, will recommend installation of libzmq3 (cppTango#899)

## High priority issues

## Issues requiring discussion

### TangoDatabase DS and C++ 11

We would like to impose a C++11 compiler to be able to compile recent versions of the TangoDatabase DS.  
Any objection?

TB: No objections.

We will also require cmake 3.7 as cppTango/main.

## AOB

### Taurus Webinar

Carlos Pascual is leaving Alba and the Tango-Controls and Taurus community.   
He kindly accepted to prepare and present a Tango Kernel Webinar dedicated to Taurus to share his precious knowledge on Taurus.  
This webinar will take place on Wednesday 27th April 2022.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday **28th** April 2022 at 15:00 **CEST**?

## Summary of remaining actions

**All institutes**:

- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
- PyTango on Windows

**Lorenzo, Thomas J and Thomas B (and Andy?)**:

- Make a proposal to streamline access - this to be sent to steering committee:

**Andy (ESRF)**: 
- Provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.

**Anton (Max IV)**:

- Make a poll about required Python versions and architectures for PyTango binary wheels.

https://gitlab.com/tango-controls/cppTango/-/blob/9d0acf65b8ee0456b71d5c6562db2333b64b4026/cppapi/server/device.cpp

**Lorenzo (ELETTRA)**:
- See with Graziano whether Graziano could become a Code Owner of TangoDatabase repository.

**Reynald (ESRF)**:
- Create an issue or MR in tango-doc to improve documentation on whether the DevEnum in pipes are really not supported.

**Sergi (ALBA)**
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26
- Review https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/29
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- cppTango as DLL on Windows, see https://gitlab.com/tango-controls/pytango/-/issues/440
- Review Damien's proposal on Pogo: pogo!126

**Thomas J. (SKAO)**:
- Send a proposal to organize a Tango workshop during SPIE, in July 2022, in Montreal, Canada.
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution

**Jack Y. (OSL)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.

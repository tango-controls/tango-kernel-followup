# Tango Kernel Follow-up Meeting - 2022/04/07

To be held on 2022/04/07 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/kernel-meeting-2022-04-07-9ti9?lang=en

# Agenda

1. Looking for Code Owners and Reviewers for TangoDatabase and TangoTest. Need to define contribution rules.
2. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2022/2022-03-24/Minutes.md#summary-of-remaining-actions)
3. High priority issues
4. Issues requiring discussion
5. AOB  
5.1 Taurus Webinar - 27th April 2022  
5.2 Next Tango Kernel Teleconf Meeting - Thursday 28th April 2022 @ 15:00 CEST? 



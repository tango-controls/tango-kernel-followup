# Tango Kernel Follow-up Meeting
Held on 2022/08/25 at 15:00 CEST on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-08-25-9w3n?lang=en

**Participants**:
 - Andrew Goetz (ESRF)    
 - Anton Joubert (MAX IV)
 - Aya Yoshimura (OSL)
 - Becky Auger-Williams (OSL)
 - Lorenzo Pivetta (Elettra)
 - Reynald Bourtembourg (ESRF)
 - Thomas Braun (byte physics)
 - Thomas Juerges (SKAO)
 - Yury Matveev (DESY)
 
## Status of [Actions defined in the previous meetings](../2022-08-11/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ping Thomas J. if you're interested in attending (in remote or not) the meeting organized at SKAO on "JupyTango as an engineering toolkit, Tango Controls future development & admin". Here is the [agenda](https://confluence.skatelescope.org/x/kq0_Cw)  
> Thomas J explained the plan for the meeting - no changes to previous.  

- Need new version of Pogo to go with cppTango 9.4.  Damien needs help reviewing: 
  https://gitlab.com/tango-controls/pogo/-/merge_requests  
  For python  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/130  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/109  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
  and java  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/129  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549
> Aya has tested Tango Access Control on Windows with read-only and writable user.  Then try sending commands via Jive.  Worked as expected.
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/tango-9.3.4-beta_win64/README.txt. FIX LINK
> New link is https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/README.txt

**Thomas J. (SKAO) and Benjamin (MaxIV)**: 
- Comment https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15  
> Thomas (SKAO): No progress. Benjamin commented.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))
> StarterService.h and .cpp files missing from Source Distribution - needed for Windows compilation (if you want to build a Windows Service), but not Linux.
> Thomas B and Aya:  Change Source Distribution cleanup script so that these files are not deleted. 
Already done at the time of the writing of these minutes.

- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354) into TangoDatabase repository
> For now, keep bat files in TangoSourceDistribution.

- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files.
  > For now, keep CMake files in TangoSourceDistribution.  Later can consider moving to TangoDatabase repo.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review
> No progress

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- make an issue and README update with details about how the Tango Source Distribution update process is expected to work.

> Thomas (byte physics): No progress

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after I am back from India (September/October) where I will gain some experience helping new colleagues getting their feet wet with TC.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
> No progress
- Try and find an equivalent of tango-cs in the SKAO images.
> No progress
- Invite to final meeting to discuss remaining point regarding "Make a proposal to streamline Gitlab access"
  - Aim for 2nd half of August.
>  Thomas J: Forgot to schedule a meeting. Will do asap.

**Yury (DESY)**: Arrange Zoom room for PyTango regular meetings
> Will be done before 1st September. The first PyTango regular meeting will take place on Thursday 1st September at 15:00 CEST.

## High priority issues

No new high priority issue reported.

## Issues requiring discussion

Duncan Grisby, the OmniORB author, replied to a request from Andy Goetz who asked how can we run some tests on omniORB software. Duncan gave some instructions. We should try to run the tests and come back to Duncan in case something is not clear. http://svn.code.sf.net/p/omniorb/svn/cost/tests
> **Action - Thomas B**: Create an issue on TangoTickets/TangoIDL to try to run the omniORB tests

Andy has gotten good feedback from many sites about their Tango installations.  It would be useful to publish some of this.  E.g., on TangoDocs like https://gitlab.com/tango-controls/tango-doc/-/blob/main/source/administration/deployment/versions-in-use.rst. 
> **Action - Andy (ESRF):** Andy must first ask those that submitted info for permission to publish.

Thomas J will be discussing Geoff Mant's time related to Pybind11 PyTango work.
Anton is still needing good motivation for this as the review and validation effort is very significant.  Will it make future PyTango maintenance that much easier?  Examples of the impact of cppTango kernel changes on the boost vs. pybind11 extension layer would be nice to see. 

## AOB

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 8th September 2022 at 15:00 CEST.

## Summary of remaining actions

**All institutes**:
- Ping Thomas J. if you're interested in attending (in remote or not) the meeting organized at SKAO on "JupyTango as an engineering toolkit, Tango Controls future development & admin". Here is the [agenda](https://confluence.skatelescope.org/x/kq0_Cw)  

- Need new version of Pogo to go with cppTango 9.4.  Damien needs help reviewing: 
  https://gitlab.com/tango-controls/pogo/-/merge_requests  
  For python  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/130  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/109  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
  and java  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/129  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/README.txt

**Andy (ESRF)**: Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))

- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files.
  > For now, keep CMake files in TangoSourceDistribution.  Later can consider moving to TangoDatabase repo.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- make an issue and README update with details about how the Tango Source Distribution update process is expected to work.
- Create an issue on TangoTickets/TangoIDL to try to run the omniORB tests

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after I am back from India (September/October) where I will gain some experience helping new colleagues getting their feet wet with TC.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images.
- Invite to final meeting to discuss remaining point regarding "Make a proposal to streamline Gitlab access"
- Comment https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15  

**Yury (DESY)**: Arrange Zoom room for PyTango regular meetings before 1st September


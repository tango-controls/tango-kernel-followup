# Tango Kernel Follow-up Meeting

Held on 2022/01/13 at 15:00 CET on Zoom.

**Participants**:

- Gwenaëlle Abeillé (Soleil)
- Benjamin Bertrand (MAX IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (Byte Physics)
- Piotr Goryl (S2Innovation)
- Vincent Hardion (MAX IV)
- Anton Joubert (MAX IV)
- Thomas Juerges (SKAO)
- Damien Lacoste (ESRF)
- Katleho Madisa (SARAO)
- Lorenzo Pivetta (ELETTRA)
- Stéphane Poirier (Soleil)
- Sergi Rubio (ALBA)
- Łukasz Żytniak (S2Innovation)

## Memorized attribute access from PyTango?  

Is that part of cppTango API private or public?    
See https://gitlab.com/tango-controls/pytango/-/merge_requests/435#note_802977077 

We decided that exposing get_mem_value() in PyTango is a bad idea.  
The special cases of "Set" and "Not used yet" are internal details of cppTango, and PyTango users must not depend on them.  
Anton should make an issue on cppTango to request a new method that is easier to use, and fits the use case.  
That would only become available in cppTango 9.4.x.  

The is_memorized() method  might be kept in PyTango, if deemed useful but there could be a 
misleading use case (See https://gitlab.com/tango-controls/pytango/-/merge_requests/435#note_810001836).
A request to have it publicly documented in cppTango still needs to be done.

## RFC Workshop Organization

Due to uncertainties linked to COVID-19 pandemic, it has been decided to postpone the organization of a face to face RFC 
workshop.  

Piotr proposed to organize a 4 hours long RFC remote workshop on Zoom so we can move on with the RFC, without having to wait 
for the end of the pandemic.

If someone can only attend 2 hours, this is also fine. It will be like the previous organized remote RFC workshops. 
On a best effort basis.

**Action - Piotr(S2Innovation)**: Send a Framadate poll to find the best time slot for the remote RFC workshop. 

## Status of [Actions defined in the previous meetings](../2021-12-16/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- if you're willing to help in any way for the Tango workshop at the next [ADASS conference](https://www.adass.org) which
  will take place in Victoria, Canada from 11-15 September 2022, please contact Thomas Juerges (SKAO) before end of January 2022.
- Ask java developers to update their projects and remove any reference to bintray.
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126. **Damien has some work to do to improve the MR**.

**Tango Java experts**:
- Send an e-mail on the Tango Mailing list informing users about the potential
  impact of the log4j vulnerability and the recommendations from the experts.  
**Andy created:**  
  - **https://gitlab.com/tango-controls/TangoTickets/-/issues/56**
  - **A new [General/Security section](https://www.tango-controls.org/community/forum/c/general/security) on the Tango forum.
    (Please think about subscribing to this new section to be notified about new posts. This is not done automatically)**
  - **https://www.tango-controls.org/community/forum/c/general/security/log4shell-or-apaches-log4j-cve-2021-44228-vulnerability-and-tango/**
- Organize a teleconf meeting to discuss the strategy to make the jar files available to the community. 
**Done. Teleconf meeting on this topic on Friday 14th January at 11:00 CET**

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?

**Damien (ESRF)**:
- Prepare a new Pogo release

**Łukasz (S2Innovation)**:
- Follow up with IK the removal of https://tango-rest-api.readthedocs.io. **Done**
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration. 
**Migration should be completed during this week#2.** 
- Check the IDL versions used by the device servers migrated/to be migrated from tango-ds Sourceforge project (are there any IDL v1 or v2 servers?).**WIP**

**Piotr (S2Innovation)**: 
- Send a Framadate poll to find the best time slot for the remote RFC workshop.

**Reynald (ESRF)**:
- Clarify whether the DevEnum in pipes are really not supported. Verify that this limitation is well documented
  (It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of
  DevEnum in pipes if it's a real limitation.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.
**pytangoarchiving has been migrated to gitlab.com/tango-controls/hdbpp/libhdbpp-pytangoarchiving**

**Thomas (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26

**Thomas (SKAO)**:
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).
- Prepare a short description of the workshop and share it with the kernel team (slack and/or kernel mailing list).
**Thomas will meet Jan David Mol from Astron on Friday 14th and they will prepare a proposal which will be shared with the kernel team.
At the moment of publishing these minutes, the proposal is available at the following link:
  https://docs.google.com/document/d/1RYrj4tzjF3ZjYpuLZpWDQhMhFHYcaIl6rDdpN63Y1nM/edit?usp=sharing  
We welcome all of your suggestions and comments. 
The suggestion mode can be enabled in the top right corner.
Unfortunately the document is restricted, because it is hosted on SKAO's "premises". 
This means that you need to log into your Google account and then request access**

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124


## High Priority Issues


## Issues requiring discussion

**Thomas Braun (Byte Physics)**: Review [TangoDatabase!28](https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/28)

Lorenzo reported a display update problem when moving a DS from one host to another one. 
Reynald suggested trying with the latest Astor/Starter version because this bug might already be fixed. 

Anton mentioned an issue on the forum which was not answered.

**Reynald (ESRF)**: Answer to the following forum post: https://www.tango-controls.org/community/forum/c/development/c/tangodevenum-and-push_change_event/

If this makes sense, it would be good to consider adding the support for DevEnum in the push_xxx_event() methods.

**Reynald (ESRF)**: Ping N. Leclercq to see whether he could provide some help on the Labview section of the forum (https://www.tango-controls.org/community/forum/c/development/labview/development-of-imaqdx-ds-some-questions-about-best-practice-eg-how-publish-of-a-cluster-array/)

## AOB

Benjamin mentioned that new versions of omniORB have been officially released (4.3.0 and 4.2.5).    
Reynald assigned https://gitlab.com/tango-controls/cppTango/-/issues/760 (Addd support for omniORB 4.3.0) to Thomas Braun 
to test whether cppTango can compile and work as expected with omniORB 4.3.0.    
It would be good to start adding some CI jobs using the 2 new omniORB versions.  

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 27th January 2022 at 15:00 CET.

## Summary of remaining actions

**All institutes**:
- if you're willing to help in any way for the Tango workshop at the next [ADASS conference](https://www.adass.org) which
  will take place in Victoria, Canada from 11-15 September 2022, please contact Thomas Juerges (SKAO) before end of January 2022.
- Ask java developers to update their projects and remove any reference to bintray.

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?

**Damien (ESRF)**:
- Prepare a new Pogo release
- Improve https://gitlab.com/tango-controls/pogo/-/merge_requests/126

**Łukasz (S2Innovation)**:
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration.
- Check the IDL versions used by the device servers migrated/to be migrated from tango-ds Sourceforge project (are there any IDL v1 or v2 servers?).

**Reynald (ESRF)**:
- Clarify whether the DevEnum in pipes are really not supported. Verify that this limitation is well documented
  (It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of
  DevEnum in pipes if it's a real limitation.
- Answer to the following forum post: https://www.tango-controls.org/community/forum/c/development/c/tangodevenum-and-push_change_event/
- Ping N. Leclercq to see whether he could provide some help on the Labview section of the forum (https://www.tango-controls.org/community/forum/c/development/labview/development-of-imaqdx-ds-some-questions-about-best-practice-eg-how-publish-of-a-cluster-array/)

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).

**Thomas (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26
- Review [TangoDatabase!28](https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/28)

**Thomas (SKAO)**:
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124
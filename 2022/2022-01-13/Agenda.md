# Tango Kernel Follow-up Meeting - 2022/01/13

To be held on 2022/01/13 at 15:00 CET on Zoom.

# Agenda

1. Memorized attribute access from PyTango?  Is that part of cppTango API private or public?    (https://gitlab.com/tango-controls/pytango/-/merge_requests/435#note_802977077)
2. RFC workshop organization
3. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-12-16/Minutes.md#summary-of-remaining-actions)
4. High priority issues
5. Issues requiring discussion
6. AOB

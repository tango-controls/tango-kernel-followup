Tango Kernel Follow-up Meeting
Held on 2022/03/10 at 15:00 CET on Zoom.

Participants:
- Thomas Juerges (SKAO)
- Benjamin Bertrand (Max IV)
- Reynald Bourtembourg (ESRF)
- Jack Yates (OSL)
- Becky Williams (OSL)
- Aya Yoshimura (OSL)
- Philip Taylor (OSL)
- Gwenaëlle Abeillé (Synchrotron SOLEIL)
- Jairo Moldes (ALBA)
- Anton Joubert (MAX IV)
- Vincent Hardion (MAX IV)
- Jan David Mol (LOFAR/ASTRON)
- Lorenzo Pivetta (Elettra)
- Yury Matveev (DESY)
- Damien Lacoste (ESRF)
- Łukasz Żytniak (S2Innovation)
- Michal Liszcz (S2Innovation)

## Status of [Actions defined in the previous meetings](../2022-02-24/Minutes.md#summary-of-remaining-actions)

All institutes:

    Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
    
    Test cppTango 9.3.5-rc1 and report back, please
    >> SKAO has tested and is happy so far.
    >> ALBA busy testing (Debian packaging).
    >> MAX IV tried some Windows Conda packaging, and found issues with duplicate include files.  Benjamin will make a cppTango issue: https://gitlab.com/tango-controls/cppTango/-/issues/900
    >> SKAO request a deadline of 1 week (Deadline = Thursday 17th March - 13:00 CET), after which the release goes ahead, unless new issues with the release are found.

Anton, Lorenzo, Thomas J and Thomas B:

    Make a proposal to streamline access - this to be sent to steering committee.
    >> Anton has not started this yet.

        Ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/63

Andy (ESRF):

    Update the description of issue cppTango#822 (CORBA transient timeout message).

    Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu. Delegate task to one of the new companies.
    Proposal that Observatory Sciences try on Ubuntu and then document the installation on Ubuntu (TBD)
    >> Jack Y (OSL) tested install on Ubuntu: https://www.tango-controls.org/community/forum/c/general/installation/installing-tango-934-on-ubuntu-lts-2004/

    Ask  Alessio / Emmanuel to get access for at least one kernel member to https://launchpad.net/~tango-controls/+archive/ubuntu/core

Anton (Max IV):

    Make a poll about required Python versions and architectures for PyTango binary wheels.
    >> No progress.


    In PyTango, can we disable Tango state switching automatically to ALARM when state is ON and an attribute is in ALARM? Investigate and report to Thomas J.
    >> No progress.

Damien (ESRF):

    Prepare a new Pogo release
    >> No progress.

Elettra:

    Address review comments on TangoDatabase!28
    >> No progress.

Jean-Luc Pons (ESRF):

    Review and merge tango-controls/device-servers/DeviceClasses/communication/Socket!1
    >> Reynald will ping Jean-Luc. Gwenaelle confirmed that the MR looks OK for Soleil.

Piotr (S2Innovation):

    Create a doodle/framadate to arrange a meeting to discuss alarms.
    >> Lukasz will send the doodle/framadate: https://framadate.org/xrZqIHSrjR1vNPRj

Reynald (ESRF):

    Clarify whether the DevEnum in pipes are really not supported. Verify that
    this limitation is well documented (It seems, according to the user who
    discussed with Andy) and ask the user to create an issue to ask for the support
    of  DevEnum in pipes if it's a real limitation. RFC does not list DevEnum as
    supported for pipes. See
    https://tango-controls.readthedocs.io/projects/rfc/en/latest/9/DataTypes.html?#pipe
    >> Reynald will create an issue or MR in tango-doc to improve the documentation on this topic

Sergi (ALBA):

    Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).

Thomas B. (byte physics):

    Look at TangoDatabase#26

    2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

    Close all tickets and pull requests on archive github repos, see https://gitlab.com/tango-controls/TangoTickets/-/issues/62
    >> There are still 16 repositories to do. The list is on the issue: https://gitlab.com/tango-controls/TangoTickets/-/issues/62
    >> If you have some admin power on these repositories, please help

Thomas J. (SKAO):

    Decide if joining the ADASS conference will be done now that it's virtual.
        >> Another option is SPIE? in July 2022, in Montreal, Canada.

    Organize a Tango Workshop at the next ADASS conference.

    Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).

    Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904


Aya (OSL):

    Work on TangoTickets#61.: Automate the Packaging of the Windows Distribution
    https://gitlab.com/tango-controls/TangoTickets/-/issues/61
    >> List of files from Sebastien - which need to be downloaded/built.
    TangoSourceDistribution includes some jar files - can they be used as-is on Windows machines?  Yes, 
    Java version 1.8.
    Which database system?  MariaDB, MySQL?  MySQL version 5.7 will work, v8 has some issues.  MariaDB 5.5 works (on CentOS 7). 
    Next is getting the .exe files built, e.g., Starter, TangoTest, etc.

## High priority issues

No critical issue reported.

## Issues requiring discussion

### Can we remove TANGO_LONG32 and TANGO_LONG64 definitions  (cppTango>=9.4)?

See https://gitlab.com/tango-controls/cppTango/-/merge_requests/792#note_868699440  
Deadline for institutes to check their code and report back:  Sunday 13 March 23:59 UTC.  
After this, code will be merged, unless there is a blocking reason.

### Frequent cppTango releases (Thomas Juerges SKAO)

See https://gitlab.com/tango-controls/cppTango/-/issues/897

Many benefits are detailed in the ticket.  
In the meeting, the benefit of improved planning and better targeting of contract developers.

We need to consider the affects on downstream projects.  
This may be difficult initially, but better once the projects are working smoothly.

Will quality be maintained?  Yes, may even be better.

Improvements to release infrastructure will mean that we can easily release patches, if necessary.

SKAO, Elettra, and MAX IV are onboard.

All: please comment on the ticket, especially if you have any reasons not to do it this way.

It would be useful to have feedback from releases that help to improve the process for the next release.
This can be a cppTango issue. 

## AOB

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 24th March 2022 at 15:00 CET.

### Next HDB++ Teleconf Meeting
The next HDB++ teleconf meeting is foreseen on Wednesday 16th March 2022 at 10:00 CET. 
Zoom link will be adverstised on hdbpp tango-controls slack channel.

### 6th Remote WtRFC workshop
The next remote WtRFC (Write the RFC) workshop will take place on Friday 18th March from 10:00 to 14:00 CET.  
Łukasz will share the zoom link on slack and on the tango kernel mailing list.

## Summary of remaining actions

**All institutes**:

- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
- Test cppTango 9.3.5-rc1 and report back before Thursday 17th March 2022 - 13:00 CET, please.
- Answer to doodle/framadate: https://framadate.org/xrZqIHSrjR1vNPRj to organize a meeting to discuss alarms
- Close all tickets and pull requests on archive github repos, see https://gitlab.com/tango-controls/TangoTickets/-/issues/62
- Give feedback about removal of TANGO_LONG32 and TANGO_LONG64 definitions: See https://gitlab.com/tango-controls/cppTango/-/merge_requests/792#note_868699440
  Deadline for institutes to check their code and report back:  Sunday 13 March 23:59 UTC.  
  After this, code will be merged, unless there is a blocking reason.
- Comment [cppTango#897](https://gitlab.com/tango-controls/cppTango/-/issues/897) about more frequent cppTango releases. 

**Anton, Lorenzo, Thomas J and Thomas B**:

- Make a proposal to streamline access - this to be sent to steering committee:
        Ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/63

**Andy (ESRF)**:

- Update the description of issue cppTango#822 (CORBA transient timeout message).

- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu. Delegate task to one of the new companies.
    Proposal that Observatory Sciences try on Ubuntu and then document the installation on Ubuntu (TBD)
    > Jack Y (OSL) tested install on Ubuntu: https://www.tango-controls.org/community/forum/c/general/installation/installing-tango-934-on-ubuntu-lts-2004/

- Ask  Alessio / Emmanuel to get access for at least one kernel member to https://launchpad.net/~tango-controls/+archive/ubuntu/core

**Anton (Max IV)**:

- Make a poll about required Python versions and architectures for PyTango binary wheels.
 
- In PyTango, can we disable Tango state switching automatically to ALARM when state is ON and an attribute is in ALARM? Investigate and report to Thomas J.

**Damien (ESRF)**:
- Prepare a new Pogo release

**Elettra**:
- Address review comments on TangoDatabase!28

**Piotr (S2Innovation)**:
- Arrange a meeting to discuss alarms.

**Reynald (ESRF)**:
- Ping Jean-Luc Pons to review and merge tango-controls/device-servers/DeviceClasses/communication/Socket!1
- Create an issue or MR in tango-doc to improve documentation on whether the DevEnum in pipes are really not supported.

**Sergi (ALBA)**
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

**Thomas J. (SKAO)**:
- Decide if joining the ADASS conference will be done now that it's virtual.
    > Another option is SPIE? in July 2022, in Montreal, Canada.
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution

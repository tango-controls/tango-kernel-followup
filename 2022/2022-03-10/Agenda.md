# Tango Kernel Follow-up Meeting - 2022/03/10

To be held on 2022/03/10 at 15:00 CET on Zoom.

# Agenda

1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2022/2022-02-24/Minutes.md#summary-of-remaining-actions)
2. High priority issues
3. Issues requiring discussion
4. AOB

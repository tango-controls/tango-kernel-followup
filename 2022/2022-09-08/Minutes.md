# Tango Kernel Follow-up Meeting
Held on 2022/09/08 at 15:00 CEST on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-09-08-9wbq?lang=en

**Participants**:
 - Andrew Goetz (ESRF)    
 - Anton Joubert (MAX IV)
 - Aya Yoshimura (OSL)
 - Becky Auger-Williams (OSL)
 - Benjamin Bertrand (MAX IV)
 - Lorenzo Pivetta (Elettra)
 - Lukasz Zytniak (S2Innovation)
 - Nicolas Leclercq (ESRF)
 - Reynald Bourtembourg (ESRF)
 - Sergi Rubio (ALBA)
 - Thomas Braun (byte physics)
 - Thomas Juerges (SKAO)
 - Yury Matveev (DESY)

 
## Tango-Controls group members grooming (https://gitlab.com/tango-controls/TangoTickets/-/issues/63)
**Action - All institutes**: Read it before the next kernel meeting - we will vote on it at the next kernel meeting.  Then it goes to the Steering Committee for approval/disapproval.

## Status of [Actions defined in the previous meetings](../2022-08-25/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ping Thomas J. if you're interested in attending (in remote or not) the meeting organized at SKAO on "JupyTango as an engineering toolkit, Tango Controls future development & admin". Here is the [agenda](https://confluence.skatelescope.org/x/kq0_Cw)  

- Need new version of Pogo to go with cppTango 9.4.  Damien needs help reviewing: 
  https://gitlab.com/tango-controls/pogo/-/merge_requests  
  For python  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/130  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/109  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
  and java  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/129  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/README.txt
> Reynald has started reviewing.  Others can help too. Use merge request below for commenting: https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107

**Andy (ESRF)**: Ask those that submitted feedback about their Tango installations for permission to publish some information.
> On hold.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))
> Has built Starter from files in the v9.3.5 TangoSourceDistribution.  Now working on Access Control, but some issues.

- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files.
  > For now, keep CMake files in TangoSourceDistribution.  Later can consider moving to TangoDatabase repo.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review
https://gitlab.com/tango-controls/pogo/-/merge_requests/126 
> No news

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- make an issue and README update with details about how the Tango Source Distribution update process is expected to work.
> Info has been updated and distributed.  https://gitlab.com/tango-controls/TangoSourceDistribution#information-for-maintainers
- Create an issue on TangoTickets/TangoIDL to try to run the omniORB tests

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after I am back from India (September/October) where I will gain some experience helping new colleagues getting their feet wet with TC.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images.
> SKAO will keep all old images, since people may be using them.  For future, SKAO need to decide which images will be the "official" ones.
> for tango-cs, there is no equivalent, but they intend to make a Docker Compose file with the same functionality.
- Invite to final meeting to discuss remaining point regarding "Make a proposal to streamline Gitlab access"
> Done
- Comment https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15  

**Yury (DESY)**: Arrange Zoom room for PyTango regular meetings before 1st September
> Done.  First meeting was successful.  We will continue with this every 2 weeks (1st and 3rd Thursdays of the month)

## High priority issues

## Issues requiring discussion

Tango Forum:  Unable to load C++ Class with multiclass Python HL
https://www.tango-controls.org/community/forum/c/development/python/unable-to-load-c-class-with-multiclass-python-hl/?page=1#post-4997
Anton will have a look.

**Action - Anton**: Have a look at https://www.tango-controls.org/community/forum/c/development/python/unable-to-load-c-class-with-multiclass-python-hl/?page=1#post-4997

Some general discussion about multiclass devices.  ESRF use this (C++), and working on Pogo changes to better handle shared libraries.  Device code lives in shared library.  Benefit is that many devices that use the same code don't all need to be recompiled - they all use the same library, so it is recompiled, and then devices  servers just need to be restarted.
The archiver also uses a shared library approach to load the correct backend.

## AOB

### fandango

fandango now has Python 3 support!  For about 1 month.  ALBA already using in production.

### cppTango
cppTango 9.4.0rc releases every friday in September.  Please test, if you can. 
There is a cppTango 9.4.0-rc1 release on conda-forge. 
There will be an rc version of the Tango SOurce Distribution too released on Friday 9th September 2022.

You can test the cppTango rc releases from conda, using cpptango-rc conda packages:  
`conda create -y -n cpptango-rc -c conda-forge/label/cpptango_rc -c conda-forge cpptango=9.4.0rc1`

### PyTango
PyTango 9.3.5 release in progress.  Source available on pypi.org.  Trying to build Windows binary wheels with cppTango 9.3.5 embedded instead of cppTango 9.3.4.
Conda packaging in progress, but also dealing with a Windows packaging bug.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 22nd September 2022 at 15:00 CEST.

## Summary of remaining actions

**All institutes**:

- Read Tango-Controls gitlab group members grooming proposal (https://gitlab.com/tango-controls/TangoTickets/-/issues/63) before the next kernel meeting - we will vote on it at the next kernel meeting.  Then it goes to the Steering Committee for approval/disapproval.
- Ping Thomas J. if you're interested in attending (in remote or not) the meeting organized at SKAO on "JupyTango as an engineering toolkit, Tango Controls future development & admin". Here is the [agenda](https://confluence.skatelescope.org/x/kq0_Cw)  

- Need new version of Pogo to go with cppTango 9.4.  Damien needs help reviewing: 
  https://gitlab.com/tango-controls/pogo/-/merge_requests  
  For python  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/130  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/109  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
  and java  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/129  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/README.txt  
Use merge request below for commenting: https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107

**Andy (ESRF)**: Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Anton (Max IV)**: Have a look at https://www.tango-controls.org/community/forum/c/development/python/unable-to-load-c-class-with-multiclass-python-hl/?page=1#post-4997


**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))

- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files. For now, keep CMake files in TangoSourceDistribution.  Later can consider moving to TangoDatabase repo.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review
https://gitlab.com/tango-controls/pogo/-/merge_requests/126 

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- Create an issue on TangoTickets/TangoIDL to try to run the omniORB tests

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after I am back from India (September/October) where I will gain some experience helping new colleagues getting their feet wet with TC.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker Compose file with the same functionality. For future, SKAO need to decide which images will be the "official" ones.
- Comment https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15  


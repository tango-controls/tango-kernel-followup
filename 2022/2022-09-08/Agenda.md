# Tango Kernel Follow-up Meeting - 2022/09/08

To be held on 2022/09/08 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-09-08-9wbq?lang=en

# Agenda

1. Tango-Controls group members grooming (https://gitlab.com/tango-controls/TangoTickets/-/issues/63)
1. Status of [Actions defined in the previous meetings](../2022-08-25/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. AOB

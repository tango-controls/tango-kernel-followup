# Tango Kernel Follow-up Meeting
Held on 2022/05/12 at 15:00 CEST on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-05-12-9u5n?lang=en

**Participants**:
- Gwenaëlle Abeillé (Synchrotron SOLEIL)
- Benjamin Bertrand (MAX IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics e. K.)
- Anton Joubert (MAX IV)
- Thomas Juerges (SKAO)
- Grzegor Kowalski (S2Innovation) 
- Damien Lacoste (ESRF)
- Lorenzo Pivetta (Elettra)
- Sergi Rubio (ALBA)
- Becky Williams (OSL)
- Jack Yates (OSL)
- Aya Yoshimura (OSL)
- Łukasz Żytniak (S2Innovation)
 
## Next Tango Collaboration meeting

To be held on 29th and 30th of June 2022 at Maxlab in Lund (Sweden).  
Vincent Hardion needs to confirm the venue and then messages will be sent through our many channels (expect doves!).  

The meeting will be held from noon to noon, 2 sessions the first afternoon and 2 sessions the morning on the second day.  
It will be available remotely, and in the flesh.

## RFC Workshop
Will be held in Castle residence in Korzkiew (https://www.booking.com/hotel/pl/zamekkorzkiewprzybyslawice.pl.html) from 21st to 23rd June 2022:

    Four bedrooms (each with a bathroom), up to 10 persons, 

    Fully equipped kitchen, 

    Two rooms for work:   

    One with a table for 10 person,  

    Second: sofas and armchairs,  

    Costs   

    Residence for Monday – Friday: 7 596,00PLN gross, ~ 1651EUR,     

    If 10 persons it is 42 EUR / person / night   

    If 5 persons 84 / person / night    

    Meals (Options):   

    Self-supply, self-cooking – costs of supplies,  

    Breakfast:     

    Fully served 20EUR/person/day with dedicated chef,  

    Pre-shopped breakfast baskets: 15EUR/person/day    

    Lunch / Dinner:     

    Delivery from a restaurant – 8 – 18EUR/person/meal   

    Walk to a restaurant    

    Grill     

    There is a grill place, which we can use i.e. Thursday evening,    

    Coffee:     

    Self-served   

    Cookies - S2Innovation delivery 😊    

    WiFi over fibre, sufficient for work and zoom streaming, 

    Photos:  https://zamek.com.pl/en/chambers/     (there is no snow at this time 😉).


Reservation terms:

    To keep the reservation till the end of May, we have to pay 450EUR till Monday 16th of May, 

    Then, beginning of June, pay at least 50%,


## Taranta meeting
The first Taranta (previously named webjive) face to face meeting will take place on 3rd and 4th of June 2022 in Lecce (Italy).  
Taranta is a software for creating web-user interfaces for Tango.  

More details here: https://www.tango-controls.org/community/events/1st-face-face-taranta-meeting-lecce-italy-3-4-june-2022

Details and registration on https://indico.tango-controls.org/event/52/

## Steering Committee News

Lorenzo reporting:  
SC news will be published on tango-control.org website.
- Budget confirmed, ongoing tasks will continue (cppTango, jTango, Docs). Budget extended for cpptango and java projects.  
- Issue: Igor's & Olga's company is registered in Russia. Unclear how this can be resolved. Work has stopped. ESRF is looking into this.
- Collaboration meeting at MAX IV in Lund, Sweden: 2022-06-29 - 2022-06-30, noon to noon

  Likely small fee for in person attendance at Collaboration meeting at MAX IV in Lund.

- RFC workshop at S2Innovation, Krakow, Poland: 2022-06-21 - 2022-06-23
- Taranta meeting in Lecce, Italy: 2022-06-03 - 2022-06-04

  https://indico.tango-controls.org/event/52/

## Status of [Actions defined in the previous meetings](../2022-04-28/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz

**Lorenzo, Thomas J and Thomas B (and Andy?)**:
- Make a proposal to streamline Gitlab access - this to be sent to steering committee
- One decision from steering committee already:  change role of people who leave the community: either "developer", or remove completely.

**Vincent, Piotr and Lorenzo**:
- Discuss the organization of the next Tango-Controls collaboration Meeting and Tango RFC workshop

**Andy (ESRF)**:
- Provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.

**Anton (Max IV)**:
- Make a poll about required Python versions and architectures for PyTango binary wheels.
  Poll is up, until 20 May 2022.   Advertised on Slack, and email to the tango-controls info mailing list.  Will do. https://docs.google.com/forms/d/e/1FAIpQLSfCx5yrqT5nRsRwSPfHfwbzq39_SRr9-Z5st6wczEO5TBul0A/viewform
  arm v7 32-bit is missing (e.g., Beaglebone boards)

**Lorenzo (ELETTRA)**:
- Ask Graziano to create a MR in TangoDatabase to update the Code Owners files
  > Done

**Reynald (ESRF)**:
- Create an issue or MR in tango-doc to improve documentation on whether the DevEnum in pipes are really not supported.
- Create an issue on Taurus repository to provide the links to the videos so these links could be added in the Taurus wiki.
  > Done: https://gitlab.com/taurus-org/taurus/-/issues/1266

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
  > Not yet
- Review https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/29
  CI has been improved by Thomas B.  Will help with detecting issues between cppTango and TangoDatabase.
  Now require C++11 and cmake 3.7.
  > Done
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
  > Not yet
- cppTango as DLL on Windows, see https://gitlab.com/tango-controls/pytango/-/issues/440
  > Open MR: https://gitlab.com/tango-controls/cppTango/-/merge_requests/944
- Review Damien's proposal on Pogo: pogo!126
  > Will remove this action from here.

**Thomas J. (SKAO)**:
- Send a proposal to organize a Tango workshop during SPIE, in July 2022, in Montreal, Canada.

  > Done. Came too late. :-( Marco B. of SKAO is looking into it if he can convince the POC to still get us in. Not looking promising though.

- Organize a Tango Workshop at the next ADASS conference.

  > Done. Received POC's decision that they do not accept tutorials of that length and therefore reject our application.

New idea: Tutorial independent of conferences?

    -> Will gather ideas in Slack. Then decide on the format, the date, etc.

- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).

  > Done. Not necessary any more. :

- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904

  > No action.


**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354)
  into TangoDatabase repository
  > Created yml file on GitLab CI, which download files to build cpp server applications (starter, database, access control, tangotest) and build, using Sebastian's Windows CMake files. It works OK.
  Will make a small change to TangoDatabase Windows CMake files. Fork Sebastian's repository, then make a merge request to the tango-controls TangoDatabase main branch.


**Jack Y. (OSL)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.

    > Done - needs review/merge https://gitlab.com/tango-controls/tango-doc/-/merge_requests/383


## High priority issues


## Issues requiring discussion

## AOB

### Migration to maven central
To deploy the first project - ATK some variables have to be added to the tango-controls group or to a specific project if they do not exist.
Variables to add on group level:

    GITLAB_TOKEN - GitLab Token with access to push a     commit with the next snapshot version after deployment

**Action - Benjamin**: Will create a bot user and give that user access to the java projects.

### Slack

We have reached our 10k message limit, so older messages not searchable.  Is that OK, or do we want to upgrade?  Pro subscription costs "€6.25 per person, per month, when billed yearly".  
Not sure what the non-profits discount is, or if we might qualify:
https://slack.com/help/articles/204368833-Apply-for-the-Slack-for-Nonprofits-discount
> Opinions:   we don't need access to the old messages.

### PyTango on Windows in Conda
Windows - conda package can be built, but need fixes from cppTango.  
Either cppTango 9.3.6-rc1, or patch the cppTango code.  
Rather patch, so conda work decoupled from cppTango releases.

### Testing on Windows
cppTango is not tested on Windows in the existing CI.  Shouldn't it be?  Thomas J would like this.  
PyTango is tested on Windows, so the subset of cppTango that it uses is also tested.  
Damien will try to run the tests suite on Windows.

**Action - Damien**: Try to run cppTango tests suite on Windows

### HDB++ Teleconf Meeting
The next HDB++ Teleconf Meeting will take place on 18th May at 10:00 CEST.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place exceptionally on Wednesday 1st June 2022 at 15:00 CEST.

## Summary of remaining actions

**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
- Review/merge https://gitlab.com/tango-controls/tango-doc/-/merge_requests/383 Done.

**Lorenzo, Thomas J and Thomas B (and Andy?)**:
- Make a proposal to streamline Gitlab access - this to be sent to steering committee

**Andy (ESRF)**:
- Provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354)
  into TangoDatabase repository

**Benjamin (MaxIV)**:
- Create a bot user and give that user access to the java projects (or find an alternative solution).

**Damien (ESRF)**:
- Try to run cppTango test suite on Windows

**Reynald (ESRF)**:
- Create an issue or MR in tango-doc to improve documentation on whether the DevEnum in pipes are really not supported.

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904

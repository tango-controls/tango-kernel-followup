# Tango Kernel Follow-up Meeting - 2022/05/12

To be held on 2022/05/12 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-05-12-9u5n?lang=en

# Agenda

1. Next Tango Collaboration meeting  
2. RFC Workshop  
3. Tarenta Meeting  
4. Status of [Actions defined in the previous meetings](../2022-04-28/Minutes.md#summary-of-remaining-actions)  
5. High priority issues  
6. Issues requiring discussion  
7. AOB  
    7.1 HDB++ Teleconf Meeting  
    7.2 Next Kernel Teleconf Meeting

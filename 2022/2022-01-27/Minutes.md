# Tango Kernel Follow-up Meeting

Held on 2022/01/27 at 15:00 CET on Zoom.

**Participants**:

- Benjamin Bertrand (MAX IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics)
- Anton Joubert (MAX IV)
- Thomas Juerges (SKAO)
- Igor Khokhriakov (IK)
- Damien Lacoste (ESRF)
- Katleho Madisa (SARAO)
- Olga Merkulova (IK)
- Jan David Mol (ASTRON)
- Lorenzo Pivetta (ELETTRA)
- Jairo Moldes (ALBA)
- Łukasz Żytniak (S2Innovation)

## RFC Workshop Organization

Due to uncertainties linked to COVID-19 pandemic, it has been decided to postpone the organization of a face to face RFC workshop.

Piotr proposed to organize a 4 hours long RFC remote workshop on Zoom so we can move on with the RFC, without having to wait for the end of the pandemic.

If someone can only attend 2 hours, this is also fine. It will be like the previous organized remote RFC workshops.
On a best effort basis.

  >  The current best options:
  >- 1st:  Thursday, February 3, 2022 - 13:00 - 17:00 with 4 votes.
  >- 2nd:  Friday, February 4, 2022 - 10:00 - 14:00 with 3 votes (and 3 maybes) 

Łukasz says we will choose the date after closing the poll on Monday 31st January.

## Status of [Actions defined in the previous meetings](../2021-01/13/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- if you're willing to help in any way for the Tango workshop at the next [ADASS conference](https://www.adass.org) which
  will take place in Victoria, Canada from 11-15 September 2022, please contact Thomas Juerges (SKAO) before end of January 2022.
> Link to the proposal: https://docs.google.com/document/d/1RYrj4tzjF3ZjYpuLZpWDQhMhFHYcaIl6rDdpN63Y1nM/edit?usp=sharing (Sorry, but you need to request access permission.)
- Ask java developers to update their projects and remove any reference to bintray.
> During the dedicated meeting, it has been decided to ask Krystian Kedron to help doing the migration. Reynald needs to clarify with Andy how to request officially this work.

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.
  > Answer from Freexian:  unable to do any work on Ubuntu.  
  > Suggest more people upvote/comment on the launchpad issue [https://bugs.launchpad.net/ubuntu/+source/tango/+bug/1932290], which may prompt the Ubuntu maintainers to do the work.
- What about having a tango-controls ppa (personal package archive)?

**Damien (ESRF)**:
- Prepare a new Pogo release
- Improve https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  >  nothing new

**Łukasz (S2Innovation)**:
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration to https://gitlab.com/tango-controls/device-servers
  > S2Innovation has migrated the device servers that they received requests for.
- Check the IDL versions used by the device servers migrated/to be migrated from tango-ds Sourceforge project (are there any IDL v1 or v2 servers?).
  > No v1 or v2 servers.

**Reynald (ESRF)**:
- Clarify whether the DevEnum in pipes are really not supported. Verify that this limitation is well documented
  (It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of  DevEnum in pipes if it's a real limitation.
  > RFC does not list DevEnum as supported for pipes. See https://tango-controls.readthedocs.io/projects/rfc/en/latest/9/DataTypes.html?#pipe
- Answer to the following forum post: https://www.tango-controls.org/community/forum/c/development/c/tangodevenum-and-push_change_event/  
  > Done
- Ping N. Leclercq to see whether he could provide some help on the Labview section of the forum (https://www.tango-controls.org/community/forum/c/development/labview/development-of-imaqdx-ds-some-questions-about-best-practice-eg-how-publish-of-a-cluster-array/)
  > Done

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
  > No update

**Thomas (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26
  > Thomas starting looking at this.  Which MySQL and MariaDB versions should we support?  Could follow supported Debian versions.  
  > Also need to figure out what the data fields are supposed to store.   
  > Are they even written to?  
  > Expect to be changed when properties are modified.
- Review [TangoDatabase!28](https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/28)
  > Awaiting response/update from Elettra (Alessio)

**Thomas (SKAO)**:
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).
  > Not yet

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124
  > No update.

## High Priority Issues


## Issues requiring discussion

Why don't commands support 2D arrays?  See https://gitlab.com/tango-controls/pytango/-/issues/434
- ASTRON and SKA considering using this.
- Lorenzo said device object model designers didn't expect "complex" data for commands, when Tango was originally developed.
- ASTRON use-case:  use a command to point all antennas at these coordinates (2D coords).
- Thomas J: If commands did support same data formats as attributes, the "symmetry" would be better.  Allows - Tango device author to choose.
- Why not pipes?  Some issues with testing.
- One negative to 1D and 2D arrays as commands is for the client to know what size arrays to send - not easy to determine programmatically.  
More flexibility, could result in reduced consistency of API.
- 2D array for command is a possibility, but where does it stop? N-D arrays?
- How is synchronisation ensured when using attribute for complex data, and the command to trigger action?

  - Device can be locked, so that multiple client don't affect each other.

  - Pipes allow for atomic actions.

- Originally 2D arrays called "images" because they were for image data (pictures).
- If Tango was extended for 2D arrays in commands, this requires work in cppTango, JTango, PyTango, and all the tools (Jive, Pogo, ATK, Sardana,...).
- Thomas J requests an estimate of the man hours to implement this in the kernel (cppTango, JTango, PyTango, Jive, pogo).  Thomas B will look at this for cppTango.
- Jan David Mol says he could help the PyTango.

Reynald has issue related to Socket device. He created a MR so the Device server no longer exits if DNS issue.  
Requests review, or comments.  Using "OFF" state to be similar to other cases with no connection.
https://gitlab.com/tango-controls/device-servers/DeviceClasses/communication/Socket/-/merge_requests/1

## AOB

DESY have a new developer with time to contribute to PyTango, up to 50%.  Yury Matveyev.  
Available from 1st March 2022.   
He will be studying the code base and chatting with Anton.

Igor has done some work on a Tango Database Server with pluggable back-end.  
It's a proof of concept in Java.  
The code is available at https://github.com/waltz-controls/virtual-tango-host

Someone will need to take over Henrik's cppTango MR (he is leaving MAX IV).  https://gitlab.com/tango-controls/cppTango/-/merge_requests/870.  
Benjamin to ask Henrik to update the MR with status, and plans/ideas to take it forward.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 10th February 2022 at 15:00 CET.

## Summary of remaining actions

**All institutes**:
- if you're willing to help in any way for the Tango workshop at the next [ADASS conference](https://www.adass.org) which
  will take place in Victoria, Canada from 11-15 September 2022, please contact Thomas Juerges (SKAO) before end of January 2022.  
  Link to the proposal: https://docs.google.com/document/d/1RYrj4tzjF3ZjYpuLZpWDQhMhFHYcaIl6rDdpN63Y1nM/edit?usp=sharing (Sorry, but you need to request access permission.)
- Ask java developers to update their projects and remove any reference to bintray.
- Review https://gitlab.com/tango-controls/device-servers/DeviceClasses/communication/Socket/-/merge_requests/1 and shout if you don't want this proposed change.
- Answer to the RFC framadate poll before Monday 31st January: https://framadate.org/4AZ1zUzjO5IdLcQI
- Upvote/comment on the launchpad issue [https://bugs.launchpad.net/ubuntu/+source/tango/+bug/1932290], which may prompt the Ubuntu maintainers to do the work.

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Freexian: What about having a tango-controls ppa (personal package archive)?

**Benjamin (Max IV)**:
- Ask Henrik to update the MR [cpptango!870](https://gitlab.com/tango-controls/cppTango/-/merge_requests/870) with status, and plans/ideas to take it forward.

**Damien (ESRF)**:
- Prepare a new Pogo release
- Improve https://gitlab.com/tango-controls/pogo/-/merge_requests/126

**Elettra**:
- Address review comments on [TangoDatabase!28](https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/28)

**Reynald (ESRF)**:
- Clarify whether the DevEnum in pipes are really not supported. Verify that this limitation is well documented
  (It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of  DevEnum in pipes if it's a real limitation.
  RFC does not list DevEnum as supported for pipes. See https://tango-controls.readthedocs.io/projects/rfc/en/latest/9/DataTypes.html?#pipe  
  Is it really not supported?
- Clarify with Andy how to request officially work from Krystian Kedron on the Maven migration

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).

**Thomas B. (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

**Thomas J. (SKAO)**:
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).
  
**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124



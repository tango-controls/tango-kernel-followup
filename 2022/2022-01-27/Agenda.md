# Tango Kernel Follow-up Meeting - 2022/01/27

To be held on 2022/01/27 at 15:00 CET on Zoom.

# Agenda

1. RFC remote workshop organization
2. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2022/2022-01-13/Minutes.md#summary-of-remaining-actions)
3. High priority issues
4. Issues requiring discussion
5. AOB

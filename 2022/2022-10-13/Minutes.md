# Tango Kernel Follow-up Meeting
Held on 2022/10/13 at 15:00 CEST on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-10-13-9x07?lang=en

**Participants**: 
 - Anton Joubert (MAX IV)
 - Benjamin Bertrand (MAX IV)
 - Becky Auger-Williams (OSL)
 - Damien Lacoste (ESRF)
 - Kieran Mulholland (OSL)
 - Mateusz Celary (S2Innovation/MAX IV)
 - Reynald Bourtembourg (ESRF)
 - Sergi Rubio (ALBA)
 - Stephane Poirier (Soleil)
 - Thomas Braun (byte physics)
 - Thomas Ives (OSL)
 - Thomas Juerges (SKAO)
 - Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../2022-09-22/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Need new version of Pogo to go with cppTango 9.4.  Damien needs help reviewing: 
  https://gitlab.com/tango-controls/pogo/-/merge_requests  
  For python  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/130  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/109  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
  and java  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/129  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106
 TangoSourceDistribution rc release will include new Pogo (excluding above MRs).    
> Pogo 9.8.0 released without these MRs merged.
https://repo1.maven.org/maven2/org/tango-controls/Pogo/9.8.0/Pogo-9.8.0.jar  
If you want all these MRs merged to get them in the next Pogo version, please review them.

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/README.txt  
Use merge request below for commenting: https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107  
- Test cppTango and Tango Source Distribution release candidates versions
> *Done*  
Now we need PyTango 9.4.0.  Plan is still to release some time in November 2022.  
Sergi is interested in Python 3.11 support as there are supposed to be performance improvements.  
Sergi mentioned an interesting presentation on Python 3.11.  
Benjamin shared also the link of this interesting podcast:  https://talkpython.fm/episodes/show/381/python-perf-specializing-adaptive-interpreter  

**Andy (ESRF)**: Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))

- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files. For now, keep CMake files in TangoSourceDistribution.  Later can consider moving to TangoDatabase repo.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review
https://gitlab.com/tango-controls/pogo/-/merge_requests/126 
- Tango-Controls gitlab group members grooming proposal to be proposed to the steering committee

**Reynald (ESRF)**: Ping the maintainers of Astor and Jive to review the MR to move to Maven Central.

> Done.  Now have a version of these in Maven Central.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
> Not yet.

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after Thomas J. will be back from India where he will gain some experience helping new colleagues getting their feet wet with Tango-Controls.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker Compose file with the same functionality. For future, SKAO need to decide which images will be the "official" ones.
- Comment https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15  
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  
> India trip will be done by 9 Dec 2022.  
Going forward, SKAO Docker images will be based on Ubuntu 22.04 LTS, which uses Python 3.10.  No more Alpine.


## High priority issues

Fix the tango debian package to avoid it being removed:
From https://tracker.debian.org/pkg/tango:
> Version 9.3.4+dfsg1-1 of tango is marked for autoremoval from testing on Mon 17 Oct 2022. It is 
> affected by #1020056. You should try to prevent the removal by fixing these RC bugs.
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1020056
**Action - Reynald and Stephane:** to follow up with Frederic Picca (Debian maintainer) as soon as possible.

## Issues requiring discussion

Thomas J: Tango Controls documentation is not getting good feedback from new users.  Difficult to use, contradictory, scattered over many places, things missing, etc.  Can we do another documentation camp?  Start with cppTango Read The Docs.  
Would be nice to have one website with docs for multiple languages - e.g., examples can switch between C++ and Python.  Like zmq guidelines: https://zguide.zeromq.org  
For a documentation camp:  need a clear plan.
**Action - Thomas J**:  arrange a brainstorming session for week #42.

Need to find a maintainer for the tango docs repository.  There are still many open issues, and merge requests from the last Tango Kernel Doc Camp in 2019.

## AOB

### Java apps on conda-forge

AtkPanel and AtkTuning were added to conda-forge:
https://github.com/conda-forge/tango-atk-panel-feedstock
https://github.com/conda-forge/tango-atk-tuning-feedstock
 LogViewer still depends on log4j1, so we cannot host it on conda-forge.
log4j 1.x has reached end of life in 2015. There are many CVEs that won't be fixed. We should migrate to log4j 2:
https://gitlab.com/tango-controls/LogViewer/-/issues/9

**Action - Mateusz C.**: Follow up with S2Innovation on the LogViewer issues and merge requests.

E.g., Pogo is a "fat" jar on Maven Central: https://repo1.maven.org/maven2/org/tango-controls/Pogo/9.8.0/Pogo-9.8.0.jar

### PyTango 9.4.
Did anyone get the Apple Silicon native compilation working?  Was there an issue with double point floats?
The x86_64 compilation via Rosetta2 on Apple Silicon works fine.
**Action - Anton**:  try Thomas J's magic build scripts on M1. (https://gitlab.com/tjuerges/build_tango.git  Please have a quick look at the README.md, especially the chapters "Requirements" for building and "Required manual preparation" for installation and running pyTango.)

### cppTango

cppTango 9.4.0 and Tango Source Distribution 9.4.0 have been released on September 30th 2022.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 27th October 2022 at 15:00 CEST.  
The next PyTango Teleconf meeting will take place on Thursday 20th October 2022 at 15:00 CEST.

## Summary of remaining actions

- Need new version of Pogo to go with cppTango 9.4.  Damien needs help reviewing: 
  https://gitlab.com/tango-controls/pogo/-/merge_requests  
  For python  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/130  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/109  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
  and java  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/129  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
If you want all these MRs merged to get them in the next Pogo version, please review them.

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/README.txt  
Use merge request below for commenting: https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107  

**Reynald (ESRF) and Stephane (SOLEIL):** to follow up with Frederic Picca (Debian maintainer) as soon as possible.

**Andy (ESRF)**: Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Anton (MaxIV)**:  try Thomas J's magic build scripts on M1. (https://gitlab.com/tjuerges/build_tango.git  Please have a quick look at the README.md, especially the chapters "Requirements" for building and "Required manual preparation" for installation and running pyTango.)

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review
https://gitlab.com/tango-controls/pogo/-/merge_requests/126 
- Tango-Controls gitlab group members grooming proposal to be proposed to the steering committee

**Mateusz C. (S2Innovation)**: Follow up with S2Innovation on the LogViewer issues and merge requests.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)

**Thomas I. (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))

- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files. For now, keep CMake files in TangoSourceDistribution.  Later can consider moving to TangoDatabase repo.

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after Thomas J. will be back from India (December 2022) where he will gain some experience helping new colleagues getting their feet wet with Tango-Controls.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker Compose file with the same functionality. For future, SKAO need to decide which images will be the "official" ones.
- Comment https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15  
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  
- Arrange a brainstorming session on a new Tango Documentation Camp for week #42 (next week).


# Tango Kernel Follow-up Meeting - 2022/10/13

To be held on 2022/10/13 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-10-13-9x07?lang=en

# Agenda

1. Status of [Actions defined in the previous meetings](../2022-09-22/Minutes.md#summary-of-remaining-actions)
1. High priority issues
   1. Fix the tango debian package to avoid it being removed:
   
   From https://tracker.debian.org/pkg/tango:

   > Version 9.3.4+dfsg1-1 of tango is marked for autoremoval from testing on Mon 17 Oct 2022. 
   > It is affected by #1020056. You should try to prevent the removal by fixing these RC bugs.
1. Issues requiring discussion
1. AOB

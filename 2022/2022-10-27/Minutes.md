# Tango Kernel Follow-up Meeting
Held on 2022/10/27 at 15:00 CEST on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-10-27-9x9k?lang=en

**Participants**: 
 - Anton Joubert (MAX IV)
 - Benjamin Bertrand (MAX IV)
 - Becky Auger-Williams (OSL)
 - Damien Lacoste (ESRF)
 - Kieran Mulholland (OSL)
 - Reynald Bourtembourg (ESRF)
 - Thomas Juerges (SKAO)

## Status of [Actions defined in the previous meetings](../2022-10-13/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Need new version of Pogo to go with cppTango 9.4.  Damien needs help reviewing: 
  https://gitlab.com/tango-controls/pogo/-/merge_requests  
  For python  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/130  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/109  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
  and java  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/129  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
If you want all these MRs merged to get them in the next Pogo version, please review them.
> Remove this topic. Please follow on Pogo repository directly. Help is needed and welcome.

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/README.txt  
Use merge request below for commenting: https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107  
> Waiting for review from Thomas B and Nicolas - this is for 9.3.4 release.  After that, work starts on 9.4.x release. MR: https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/122

**Reynald (ESRF) and Stephane (SOLEIL):** to follow up with Frederic Picca (Debian maintainer) as soon as possible.  
> Done. Freexian will take care of the issue with the Tango Debian packages
This is to do with cppzmq packaging changes.  They aware that the deadline for "testing" stream removal is 28th October 2022.  If it falls out of testing, it can be re-introduced.  Either way, set deadline for work as 10th November.

**Andy (ESRF)**: Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Anton (MaxIV)**:  try Thomas J's magic build scripts on M1. (https://gitlab.com/tjuerges/build_tango.git  Please have a quick look at the README.md, especially the chapters "Requirements" for building and "Required manual preparation" for installation and running pyTango.)  
> Anton tried this using Thomas J's https://gitlab.com/tjuerges/build_tango.  The dependencies installed via homebrew are all available as MacOS arm64 packages.
>- TangoTest failed to run with 
```
tango/installed/tango.9.4/bin/TangoTest test -ORBendPoint giop:tcp::0 -nodb -dlist sys/tg_test/18
TangoTest(20346,0x1024d4580) malloc: *** error for object 0x600001550000: pointer being freed was not allocated
TangoTest(20346,0x1024d4580) malloc: *** set a breakpoint in malloc_error_break to debug
Abort trap: 6
```
>Using TangoTest from the MacOS x86_64 Conda-forge package, unit tests could be run.
>- All units tests, except for test_events.py passed.
>- The test_events.py tests indicate a problem with 64-bit double floating point values.  32-bit are OK, and all the other data types (integer, string) work.  Read/write attribute with 64-bit doubles is OK.  It is just event callbacks with data via ZMQ that don't seem to work.  Cause unknown.

>Benjamin has tried to get omniorb compilation working on osx-arm64 for conda-forge.
Asked for help on omniorb forum 10 days ago. https://www.omniorb-support.com/pipermail/omniorb-list/2022-October/032228.html  
**Action Reynald**: to find out how we can get support from the author, and help Benjamin get in contact. Done just after the meeting (e-mail sent to Duncan with Benjamin in CC).

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review
https://gitlab.com/tango-controls/pogo/-/merge_requests/126 
- Tango-Controls gitlab group members grooming proposal to be proposed to the steering committee

**Mateusz C. (S2Innovation)**: Follow up with S2Innovation on the LogViewer issues and merge requests.
There is a merge request open:
https://gitlab.com/tango-controls/LogViewer/-/merge_requests/3  
> **Action - Becky (OSL)**: review https://gitlab.com/tango-controls/LogViewer/-/merge_requests/3

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)

**Thomas I. (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))

- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files. For now, keep CMake files in TangoSourceDistribution.  Later can consider moving to TangoDatabase repo.

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after Thomas J. will be back from India (December 2022) where he will gain some experience helping new colleagues getting their feet wet with Tango-Controls.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker Compose file with the same functionality. For future, SKAO need to decide which images will be the "official" ones.
- Comment https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15  
> Done
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  
- Arrange a brainstorming session on a new Tango Documentation Camp for week #42 (next week).
> Framapoll sent out, but no responses.
Action Thomas J: Arrange a brainstorming session on a new Tango Documentation Camp. Will reopen/advertise the Framapoll for another 2 weeks.

## High priority issues


## Issues requiring discussion

Updated Windows packages for cppTango 9.4.0.  After fixes to the packaging.  https://gitlab.com/tango-controls/cppTango/-/packages/9644535 
**Action - Reynald**: Follow up Windows Packages for cppTango 9.4.0 with Thomas B.

## AOB

Conda-forge now has hdbpp-cm, hdbpp-es, libhdbpp-timescale, jive, and pogo!  Next: Astor.

TangoTest and DatabaseDS - should these be compiled with both 9.3.x and 9.4.x on conda-forge?  Yes.  Something to think about when new packages released.

Next Tango special-interest group meeting (3rd):  maybe talking about updating the IDL.  What happens if we do that?  What changes should we make?

2nd Tango SIG Meeting about Archiving and HDB++ is organized by Astron and will take place on Nov 15-17 in Dwingeloo (Netherlands). More information and registration on https://indico.astron.nl/event/306/

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 10th November 2022 at 15:00 CET.  
The next PyTango Teleconf meeting will take place on Thursday 3rd November 2022 at 15:00 CET.

## Summary of remaining actions

**All institutes**:
- 2nd Tango SIG Meeting about Archiving and HDB++ is organized by Astron and will take place on Nov 15-17 in Dwingeloo (Netherlands). More information and registration on https://indico.astron.nl/event/306/
- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/README.txt  
Use merge request below for commenting: https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107  

**Andy (ESRF)**: Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Anton (MaxIV)**:  follow up on Thomas J's magic build scripts on M1. 

**Becky (OSL)**: Review https://gitlab.com/tango-controls/LogViewer/-/merge_requests/3

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review
https://gitlab.com/tango-controls/pogo/-/merge_requests/126 
- Tango-Controls gitlab group members grooming proposal to be proposed to the steering committee

**Mateusz C. (S2Innovation)**: Follow up with S2Innovation on the LogViewer issues and merge requests.

**Reynald (ESRF)**: Follow up Windows Packages for cppTango 9.4.0 with Thomas B.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)

**Thomas I. (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))

- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files. For now, keep CMake files in TangoSourceDistribution.  Later can consider moving to TangoDatabase repo.

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after Thomas J. will be back from India (December 2022) where he will gain some experience helping new colleagues getting their feet wet with Tango-Controls.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker Compose file with the same functionality. For future, SKAO need to decide which images will be the "official" ones.
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  
- Arrange a brainstorming session on a new Tango Documentation Camp 

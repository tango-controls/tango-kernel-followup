# Tango Kernel Follow-up Meeting - 2022/11/10

To be held on 2022/11/10 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-11-10-9xif?lang=en

# Agenda

1. Status of [Actions defined in the previous meetings](../2022-10-27/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. AOB

# Tango Kernel Follow-up Meeting
Held on 2022/11/10 at 15:00 CET on Zoom.
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-11-10-9xif?lang=en

**Participants**:
 - Anton Joubert (MAX IV)
 - Benjamin Bertrand (MAX IV)
 - Becky Auger-Williams (OSL)
 - Damien Lacoste (ESRF)
 - Kieran Mulholland (OSL)
 - Thomas Juerges (SKAO)
 - Thomas Braun (byte physics e.K.)
 - Andy Gotz (ESRF)
 - Jan David Mol (Astron)
 - Yury Matveev (DESY)
 - Thomas Ives (OSL)
 - Lukasz Zyntniak (S2Innovation)

## Status of [Actions defined in the previous meetings](../2022-10-27/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- 2nd Tango SIG Meeting about Archiving and HDB++ is organized by Astron and will take place on Nov 15-17 in Dwingeloo (Netherlands). More information and registration on https://indico.astron.nl/event/306/

> Basically done as the meeting is next week.

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549

> We merged the installer for 9.3.4. It is now available at https://gitlab.com/tango-controls/TangoSourceDistribution/-/releases/9.3.4.
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/README.txt  .

Use merge request below for commenting: https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107
Test Beta version of Windows distribution.
Action:  Thomas B to post another update about this in Slack and info email list.

> Done as well.

**Andy (ESRF)**: Ask those that submitted feedback about their Tango installations for permission to publish some information.

No progress.

**Anton (MaxIV)**:  follow up on Thomas J's magic build scripts on M1.
>  Anton: Tried out. Events for DevDouble type did not work using Homebrew
>  packages. In the meanwhile Benjamin has made pyTango built for Conda and the
>  events work there.  No plan to debug the homebrew packages.
> https://gitlab.com/tango-controls/meeting-minutes/pytango/-/blob/main/2022/2022-11-03/minutes.md#arm64-compilation-on-macos

**Becky (OSL)**: Review https://gitlab.com/tango-controls/LogViewer/-/merge_requests/3
> Done. MR has been merged.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review
https://gitlab.com/tango-controls/pogo/-/merge_requests/126

> Not present.

- Tango-Controls gitlab group members grooming proposal to be proposed to the steering committee
> Done

**Lukasz Zyntniak (S2Innovation)**: Follow up with S2Innovation on the LogViewer issues and merge requests.

> Done.

**Reynald (ESRF)**: Follow up Windows Packages for cppTango 9.4.0 with Thomas B.
> Thomas B. has created a new cppTango release named 9.4.0.windows0 and has
> uploaded the windows appveyor artifacts on gitlab. See
> https://gitlab.com/tango-controls/cppTango/-/releases/9.4.0.windows0

> Done. New release for Windows-only is ready: "9.4.0 Windows only", see
> https://gitlab.com/tango-controls/cppTango/-/releases/9.4.0.windows0

> Another new release of cppTango is imminent because Benjamin found another
> bug in Windows linking (https://gitlab.com/tango-controls/cppTango/-/issues/1011).

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
> Not done yet.

**Thomas I. (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))

> Merged for 9.3.4. Not yet done for 9.3-backports and main.

- Some issues to work on:
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

> Done.

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files. For now, keep CMake files in TangoSourceDistribution.  Later can consider moving to TangoDatabase repo.

> To be done for the cmake files created for the main branch as the files for the 9.3.4 version are not suitable to be used generally

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after Thomas J. will be back from India (December 2022) where he will gain some experience helping new colleagues getting their feet wet with Tango-Controls.
> Thomas: Will be back from India on 2022-12-10.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
> Thomas: No action
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker
  Compose file with the same functionality. For future, SKAO need to decide
  which images will be the "official" ones.
> Thomas: Created issue https://gitlab.com/tango-controls/docker/tango-cs/-/issues/16 where reactions
> from community on e-mail below will be collected.
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.
> Thomas: E-mail proposal to be sent to tango-info mailing list:

```
    Dear list members,

Perhaps you are already aware that the Square Kilometre Array Observatory
(SKAO) is using Tango Controls (TC) as the foundation of its software. SKAO is
running everything in Kubernetes and thus creates Docker images for almost
every part of TC. The SKAO images can be found at
https://artefact.skao.int/#browse/browse:docker-all:v2. Look there for the
images named 'ska-tango-*'. The images are meant to be used by the community.

At the Tango Controls kernel meeting it was agreed that in the future SKAO's
images will become the official TC images and TC will slowly phase out its own
Docker images at https://gitlab.com/tango-controls/docker. Among the images to
be retired is also the Tango CS Docker image
(https://gitlab.com/tango-controls/docker/tango-cs). This image contains a lot
of TC software which makes it very time consuming to maintain due to the amount
of integrated TC software packages. To lower the maintenance burden, we plan to
compose individual Docker images into what is now the big Tango CS image.

I am writing to you today to enquire how much the Tango CS image is used by the
community and especially what parts of it you or your facility is using. This
will help the kernel team to gauge what aspects of the CS image are important
and if we were to retire it, what additional images should complement the ones
that SKAO already provides. Please add a comment to this issue on TC's
Docker/CS repo: https://gitlab.com/tango-controls/docker/tango-cs/-/issues/16

https://hub.docker.com/r/tangocs/tango-cs will also be retired/removed.

Thanks in advance for taking the time and adding your comment.

Kind regards,
Thomas
```

- Arrange a brainstorming session on a new Tango Documentation Camp
> Tried that, nobody replied to the Framapoll. Will do again during meeting next week. Will revive the Framapoll. Link follows later.

### Freexian
Overloaded at the moment, but have been asked to do the Tango Debian packages.  They will get to it.
pytango should also be taken over by freexian.
Action: Andy to follow up with them.

### Website
Olga has made a new one, but it needs review.  Who can help Andy review? Hosted by webu.
Contact Andy if you are interested.

Task for everyone:

- Review new website (secret link ask Thomas Braun or Andy Goetz). Please
  comment on https://gitlab.com/tango-controls/TangoTickets/-/issues/76.
  One post per person only. Thanks.

## High priority issues

- https://gitlab.com/tango-controls/JTango/-/issues/131 : Changes in recent
  JTango versions (9.7.0) impacting clients using atk (at least).  Clients are
  no longer notified (An exception was thrown before, now it's only a warning
  log on the **server** logs) during the event subscription when a remote
  attribute is not correctly configured to send events (polling not started,
  events not pushed by server, event thresholds not configured). As a
  consequence, the clients think they can communicate via events (no error
  reported) but they don't receive events (except the synchronous read
  attribute call occurring during the event subscription phase).  In the
  previous JTango version, the clients were receiving an exception in this use
  case and would then decide to poll the attributes directly from the client
  side to ensure the attributes values are refreshed.  Merge Request
  https://gitlab.com/tango-controls/JTango/-/merge_requests/164 should be
  reverted.

> Will be tackled on monday at the JTango meeting by Lukasz.

Nicolas (ESRF):
- JTango: Events are not properly reported, investigate issue and create ticket

- https://gitlab.com/tango-controls/cppTango/-/issues/1013 - Pushing change event from Windows device compiled with cppTango v9.4.0 crashes

Assigned to Thomas Ives

- https://gitlab.com/tango-controls/cppTango/-/issues/1012 - Tango::Util::init is slow in AppVeyor environment

Assigned to Thomas Juerges

## Issues requiring discussion

Thomas (SKAO):
- Feature request "New attribute event ALARM_EVENT"
    Please see https://gitlab.com/tango-controls/TangoTickets/-/issues/65 for rationale, proposal and impacted TC packages.

@Everyone: Read, understand, comment

- Kill https://hub.docker.com/u/tangocs or add readme and then kill the images. All present people agreed.

## AOB
2nd Tango SIG Meeting about Archiving and HDB++ is organized by Astron and will take place on Nov 15-17 in Dwingeloo (Netherlands). More information and registration on https://indico.astron.nl/event/306/

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 24th November 2022 at 15:00 CET.
The next PyTango Teleconf meeting will take place on Thursday 17th November 2022 at 15:00 CET.

No cppTango meeting next week at the 17th Nov 2022.

## Summary of remaining actions

**All institutes**:
- Review new website (secret link ;); ask Thomas Braun or Andy Goetz on slack
  to get it). Please comment on https://gitlab.com/tango-controls/TangoTickets/-/issues/76.
  *One* post per person only.
- Feature request for "New attribute event ALARM_EVENT". please see
  https://gitlab.com/tango-controls/TangoTickets/-/issues/65 for rationale,
  proposal and impacted TC packages.
  Action: Read, understand, comment

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for permission to publish some information.
- Debian packaging for tango/pytango; Freexian will do the work. Waiting for offer.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

**Reynald (ESRF)**:
- Nothing

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

**Thomas I. (OSL)**:
- Make MRs for getting cmake enhancements for windows into cpp servers
- Get installer into main/9.3-backports branch of TSD
- Pushing change event from Windows device compiled with cppTango v9.4.0
  crashes: [cppTango#1013](https://gitlabcom/tango-controls/cppTango/-/issues/1013)

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker
  Compose file with the same functionality. For future, SKAO need to decide
  which images will be the "official" ones. See also https://gitlab.com/tango-controls/docker/tango-cs/-/issues/16.
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.
- Arrange a brainstorming session on a new Tango Documentation Camp. Revive Framapoll.
- Kill https://hub.docker.com/u/tangocs or, add readme and then kill the images
- Tango::Util::init is slow in AppVeyor environment: [cppTango#1012](https://gitlab.com/tango-controls/cppTango/-/issues/1012)

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 24th November 2022 at 15:00 CET.
The next PyTango Teleconf meeting will take place on Thursday 17th November 2022 at 15:00 CET.
No cppTango meeting next week at the 17th November 2022.

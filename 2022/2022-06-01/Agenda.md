# Tango Kernel Follow-up Meeting - 2022/06/01

To be held on 2022/06/01 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-06-01-9uiw?lang=en

# Agenda

1. Next Tango Collaboration meeting  
2. RFC Workshop  
3. Tarenta Meeting  
4. Supported java versions in Maven Central
5. Status of [Actions defined in the previous meetings](../2022-05-12/Minutes.md#summary-of-remaining-actions)  
6. High priority issues  
7. Issues requiring discussion  
8. AOB  
    8.1 HDB++ Teleconf Meeting  
    8.2 Next Kernel Teleconf Meeting

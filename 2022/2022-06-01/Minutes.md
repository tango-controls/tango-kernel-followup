# Tango Kernel Follow-up Meeting
Held on 2022/06/01 at 15:00 CEST on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-06-01-9uiw?lang=en

**Participants**:
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics e. K.)
- Thomas Juerges (SKAO)
- Lorenzo Pivetta (Elettra)
- Sergi Rubio (ALBA)
- Becky Williams (OSL)
- Jack Yates (OSL)
- Aya Yoshimura (OSL)
- Nicolas Leclercq (ESRF)
 
## Next Tango Collaboration meeting

To be held on 29th and 30th of June 2022.  

The meeting is hosted in hybrid format by MAX IV Laboratory, Lund Sweden.

REGISTRATION AND CALL FOR ABSTRACT IS NOW OPEN! Don't wait too long, places are limited.
https://indico.tango-controls.org/event/51/ 

## RFC Workshop
Will be held in Castle residence in Korzkiew (https://www.booking.com/hotel/pl/zamekkorzkiewprzybyslawice.pl.html) from 21st to 23rd June 2022.  
Accommodation is paid by the Tango collaboration.  
Please register here: https://indico.tango-controls.org/event/53/registrations/40/

## Taranta meeting
The first Taranta (previously named webjive) face to face meeting will take place on 3rd and 4th of June 2022 in Lecce (Italy).  
Taranta is a software for creating web-user interfaces for Tango.  

More details here: https://www.tango-controls.org/community/events/1st-face-face-taranta-meeting-lecce-italy-3-4-june-2022

Details and registration on https://indico.tango-controls.org/event/52/

## Supported java versions in Maven Central

A teleconf meeting took place on Tuesday 31st May 2022 to talk about the work currently being done by S2Innovation on the automatic deployment of the Tango java software releases to Maven Central.  
ESRF Java developers proposed to upload on Maven Central at least one release of each Tango java tool/framework, compatible with java 1.8 and then to upload versions compatible with java >= 17 on Maven Central at some point.  
Tango Source Distribution 9.4 will provide jar files compatible with java >= 17.  
Jar files compatible with Java 1.8 will still be available on the Gitlab repositories of each java tool/library.

## Status of [Actions defined in the previous meetings](../2022-05-12/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
> Thomas B: WIP
- Review/merge https://gitlab.com/tango-controls/tango-doc/-/merge_requests/383
>Done

**Lorenzo, Thomas J and Thomas B (and Andy?)**:
- Make a proposal to streamline Gitlab access - this to be sent to steering committee  
Action - Lorenzo: Propose meeting date and time mid June

**Andy (ESRF)**:
- Provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354)
  into TangoDatabase repository
> First draft version almost done. This version includes exe files built from newer versions (starter,accesscontrol, database and tangotest) than the codes included in the source distribution 9.3.4.  
Haven't managed to test access control.  This can be tested by the community when the beta version of the Tango Windows installer will be available (Monday 6th June 2022?).  
Some files (start-notifd.bat, RestServer.jar...) will not be included in the installer.  
Create different installer for each MSVC version. 

**Benjamin (MaxIV)**:
- Create a bot user and give that user access to the java projects (or find an alternative solution).  
> Benjamin didn't create a bot user for java projects.  
He defined a project access token (https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) which is much easier to maintain.  
It's per project and can easily be revoked/changed.  
**@ALL**: Please comment.

**Damien (ESRF)**:
- Try to run cppTango test suite on Windows
> WIP: Open MR. Compiles, nobody has tried to run it yet.

**Reynald (ESRF)**:
- Create an issue or MR in tango-doc to improve documentation on whether the DevEnum in pipes are really not supported.
> Done: https://gitlab.com/tango-controls/tango-doc/-/issues/382

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
> In Progress.
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
> Not yet

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
> No action yet
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
> No action yet
- pytango: Revive pybind11 port? https://gitlab.com/tango-controls/pytango/-/merge_requests/322  
Let's see what is required to finish the job.  
**Action - Thomas J**: Talk to Geoffrey Mant for a summary where he left it and what might be the dark and untidy corners. After that talk to Anton and/or Yuri (MAX IV), cc Lorenzo, Nicolas, Andy

## High priority issues

## Issues requiring discussion

## AOB

### HDB++ Teleconf Meeting
The next HDB++ Teleconf Meeting will take place on Wednesday 8th June at 10:00 CEST.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 16th June 2022 at 15:00 CEST.

## Summary of remaining actions


**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
- Benjamin didn't create a bot user for java projects.  
He defined a project access token (https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) which is much easier to maintain.  
It's per project and can easily be revoked/changed.  
Please give feedback to Benjamin about this proposal.

**Lorenzo**: Organize a teleconf meeting around mid-June to discuss the proposal to streamline Gitlab access.  

**Lorenzo, Thomas J and Thomas B (and Andy?)**:
- Make a proposal to streamline Gitlab access - this to be sent to steering committee  

**Andy (ESRF)**:
- Provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354) into TangoDatabase repository
- Provide a Beta version of the Windows distribution when available

**Damien (ESRF)**:
- Try to run cppTango test suite on Windows

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.  
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- pytango: Revive pybind11 port? https://gitlab.com/tango-controls/pytango/-/merge_requests/322  
Let's see what is required to finish the job.  
Talk to Geoffrey Mant for a summary where he left it and what might be the dark and untidy corners. After that talk to Anton and/or Yuri (MAX IV), cc Lorenzo, Nicolas, Andy

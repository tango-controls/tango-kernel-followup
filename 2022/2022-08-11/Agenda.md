# Tango Kernel Follow-up Meeting - 2022/08/11

To be held on 2022/08/11 at 15:00 CEST on Zoom.
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-08-11-9vu9?lang=en

# Agenda

1. Status of [Actions defined in the previous meetings](../2022-07-21/Minutes.md#summary-of-remaining-actions)
1. Java version for next Tango Source Distribution release (https://gitlab.com/tango-controls/TangoSourceDistribution/-/issues/103)
1. High priority issues
1. Issues requiring discussion
1. AOB

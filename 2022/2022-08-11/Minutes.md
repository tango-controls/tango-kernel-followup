# Tango Kernel Follow-up Meeting
Held on 2022/08/11 at 15:00 CEST on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-08-11-9vu9?lang=en

**Participants**:
 - Aya Yoshimura (OSL)
 - Anton Joubert (MAX IV)
 - Becky Auger-Williams (OSL)
 - Benjamin Bertrand (MAX IV)
 - Damien Lacoste (ESRF)
 - Jack Yates (OSL)
 - Lorenzo Pivetta (Elettra)
 - Lukasz Zytniak (S2Innovation)
 - Reynald Bourtembourg (ESRF)
 - Thomas Braun (byte physics)
 - Thomas Juerges (SKAO)
 - Yury Matveev (DESY)
 
## Status of [Actions defined in the previous meetings](../2022-08-11/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz

- Need new version of Pogo to go with cppTango 9.4.  Damien needs help reviewing: 
  https://gitlab.com/tango-controls/pogo/-/merge_requests  
  For python  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/130  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/109  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
  and java  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/129  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106

- Test the Beta version of the Windows distribution. See link in
  https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_974367265)

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
> MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107) created.

- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354) into TangoDatabase repository
- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase, TangoAccessControl and starter repositories, update CMake files so that they can be built on Windows without special Windows CMake files.
 >   Created MR in Starter https://gitlab.com/tango-controls/starter/-/merge_requests/14

  3.Need to update README.md that will be included in the installer (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/5)
  I had to create a new repository. Please see the README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/tango-9.3.4-beta_win64/README.txt
  **New Action - All**: Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/tango-9.3.4-beta_win64/README.txt

  > Add 'Limitation Section' to README.txt file and mention that 'Access Control' hasn't been tested.
  
-> Done, added a line " The Access Control hasn't been tested" in README.txt.

  > After updating the README.txt file, will add link to the latest README.txt and the installer to Slack kernel channel and also Tango Forum.
-> Done

**Lorenzo (Elettra)**:
- Ping Claudio for pogo !126 review
>  Lorenzo awaiting feedback from Claudio.  Damien to continue in the meantime.

**Reynald (ESRF)**:
- Assign Merge Requests open for the Maven Migration for LogViewer. https://gitlab.com/groups/tango-controls/-/merge_requests?scope=all&state=opened&search=Migration+to+Maven  
> LogViewer MR assigned to Krzysztof Klimczyk and merged

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

> No progress.

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after I am back from India (September/October) where I will gain some experience helping new colleagues getting their feet wet with TC.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
>  No update.
- pytango: Revive pybind11 port? https://gitlab.com/tango-controls/pytango/-/merge_requests/322
>   SKAO/MCCS team can allocate some of Geoff's time to this.
- Try and find an equivalent of tango-cs in the SKAO images.
>  No update.  See https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15 - Thomas J and Benjamin to comment.  
**New action - Thomas J and Benjamin**: Comment https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15  
- Invite to final meeting to discuss remaining point regarding "Make a proposal to streamline Gitlab access"
  - Aim for 2nd half of August.
- Nicolas and Andy will be visiting SKAO head office - tentatively 13, 14 September.  One of the things will be a demo of JupyTango, and then more general discussion about the future of Tango.  Others are welcome - please ping Thomas J. Tentative agenda: https://confluence.skatelescope.org/x/kq0_Cw  
**New Action - All**: Ping Thomas J. if you're interested in attending (in remote or not) the meeting organized at SKAO on "JupyTango as an engineering toolkit, Tango Controls future development & admin". Here is the [agenda](https://confluence.skatelescope.org/x/kq0_Cw).

## Java version for next Tango Source Distribution release (TangoSourceDistribution#103)
ESRF wants to move past Java 8.  Current LTS version is 17.  Can we use that?  Ubuntu 20 has OpenJDK 11 by default but openjdk-17-jdk package is available (https://packages.ubuntu.com/focal/openjdk-17-jdk).  Java jars can be compiled to work with older versions, if we like.  
Does source need to change?  Probably.  E.g., some CORBA components have been removed from "standard" distribution, so need to get them from a different location.  
Oracle and OpenJDK don't ship the same components.

**Suggestion**: artifacts uploaded to maven central using Java 8.  Once that is done, then we can work on artifacts that are compiled with Java 17.   
**Decision**:  move to Java 17 after all existing tools available with Java 8.  For CI, we will target OpenJDK, but don't prevent working with any other distribution.

## TangoSourceDistribution
Individual project maintainers must get their new versions into the TangoSourceDistribution repo, if they want it updated.  This reduces the load on the source distribution maintainer.  
**Action - Thomas B**: to make an issue and README update with details about how the process is expected to work.

## High priority issues

Nothing

## Issues requiring discussion

Nothing

## AOB

### PyTango

Some problems identified in PyTango 9.3.4 in some use cases. The new version of Sardana (3.3.3) fixes the issues related to these specific problems and should be working fine with PyTango 9.3.4.  
PyTango 9.3.5 will be released soon.  
For 9.4 tests need TangoTest compiled with cppTango 9.4.  No conda package, but maybe we can use artifact from last TangoTest pipeline run.  https://gitlab.com/tango-controls/TangoTest/-/pipelines.  

### A regular PyTango meeting?
Thomas J suggested this at the Tango community meeting.  Something like the cppTango meeting.  Weekly, or every two weeks?  Assign milestones, look at open merge request, priorities, etc.
More can be discussed at Thomas J's meeting at SKAO in September.  
**Suggestion**:  every 2 weeks, same time as kernel meetings, but on the alternate weeks (1st and 3rd Thursday).  Starting 1 September at 15:00 CEST (or CET in Winter).  This allows people to attend cppTango meetings and PyTango meetings.
Yury will arrange Zoom room, and publish on Slack.
Thomas J will start with ticket grooming.

**Action - Yury**: Arrange Zoom room for PyTango regular meetings


### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 25th August 2022 at 15:00 CEST.

## Summary of remaining actions

**All institutes**:
- Ping Thomas J. if you're interested in attending (in remote or not) the meeting organized at SKAO on "JupyTango as an engineering toolkit, Tango Controls future development & admin". Here is the [agenda](https://confluence.skatelescope.org/x/kq0_Cw)  
- Need new version of Pogo to go with cppTango 9.4.  Damien needs help reviewing: 
  https://gitlab.com/tango-controls/pogo/-/merge_requests  
  For python  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/130  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/109  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
  and java  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/129  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/tango-9.3.4-beta_win64/README.txt

**Thomas J. (SKAO) and Benjamin (MaxIV)**: 
- Comment https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15  

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))

- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354) into TangoDatabase repository
- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- make an issue and README update with details about how the Tango Source Distribution update process is expected to work.

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after I am back from India (September/October) where I will gain some experience helping new colleagues getting their feet wet with TC.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images.
- Invite to final meeting to discuss remaining point regarding "Make a proposal to streamline Gitlab access"
  - Aim for 2nd half of August.

**Yury (DESY)**: Arrange Zoom room for PyTango regular meetings


# Tango Kernel Follow-up Meeting - 2022/11/24

To be held on 2022/11/24 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-11-24-9xsa?lang=en

# Agenda

1. Steering Committee News
1. Gitlab.com CI/CD Minutes Quota Warning
1. Status of [Actions defined in the previous meetings](../2022-11-10/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. AOB

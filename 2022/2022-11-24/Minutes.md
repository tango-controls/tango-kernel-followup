# Tango Kernel Follow-up Meeting
Held on 2022/11/24 at 15:00 CET on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-11-24-9xsa?lang=en

**Participants**:
 - Anton Joubert (MAX IV)
 - Benjamin Bertrand (MAX IV)
 - Becky Auger-Williams (OSL)
 - Damien Lacoste (ESRF)
 - Kieran Mulholland (OSL)
 - Thomas Juerges (SKAO)
 - Thomas Braun (byte physics e.K.)
 - Yury Matveev (DESY)
 - Thomas Ives (OSL)
 - Reynald Bourtembourg (ESRF)
 - Lorenzo Pivetta (Elettra)
 - Gwenaëlle Abeillé (SOLEIL)
 - Stéphane Poirier (SOLEIL)

## Steering Committee News

Feedback on Gitlab permissions.  Overall, well received and acceptable.  
Two points:  
- Maintainer role isn't that clear
- The hierarchy of permissions for subgroups.
Marco Bartolini says SKA have a policy that may be of interest.  

**Action - Lorenzo, Thomas J**: to take this forward.
Link to the proposal: https://gitlab.com/tango-controls/TangoTickets/-/issues/63
 
Budget spending looks to be on schedule.  
Candidate for 2023 Tango Controls Coordinator under consideration. The idea is to rotate this role across all institutes.

## Gitlab.com CI/CD Minutes Quota Warning

We reached 70% of our allocated minutes this week.  Today at 82%.   https://gitlab.com/groups/tango-controls/-/usage_quotas#pipelines-quota-tab.  We have 50k minutes per month.

Some options to improve things:  buy more minutes, improve pipeline to abort earlier, use some self-hosted runners (PyTango is already using some runners from MAX IV).   
Thomas J will put together a proposal for SKAO management to review - can they provide some funding or runners?  
SOLEIL use runners hosted outside of their network.

**Actions**:  
    **Thomas J**: write a proposal to solve CI/CD Minutes quota issue and post in Slack for all to review.  
    **Reynald**: send screen shots of the runner history to Thomas J for proposal (Done just after the meeting)

From https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#cost-factor  
> Cost factor  
The cost factors for jobs running on shared runners on GitLab.com are:  
  1 for internal and private projects.  
  0.5 for public projects in the GitLab for Open Source program.  
  0.008 for public forks of public projects in the GitLab for Open Source program. For every 125 minutes of job execution time, you use 1 CI/CD minute.  
  1 for other public projects, after October 1, 2022 (previously 0.04). For every 1 minute of job execution time, you use 1 CI/CD minute.

Can Andy check if tango-controls is considered part of the "Open Source" program.  
Cost factor seems to be 1 for us, but we expect 0.5.

**Action - Reynald**: ask Andy to find out if tango-controls Gitlab organization is considered part of the "Open Source" program.  

Currently cppTango CI is run on each (bunch of) commit(s) pushed to branches used in MRs targeting main or 9.3-backports branches. CI is run as well when an MR is merged or when a ta is created.


**Action - All**:  ask if your institute can host some Gitlab runners.

**Action - Thomas J**:  post the Raspberry compilation time for PyTango.  Could we use some of them as aarch64 runners?
> Done after the meeting. Here are the results:  
Timing of compilations on a Raspberry 4B (8GB RAM, 1TB NVME attached via USB3.1 Gen 2)  
tango-idl     real    0m2.667s  
cppTango      real    24m48.672s  
pyTango       real    36m54.618s  
TangoTest     real    1m6.711s  
TangoDatabase real    1m29.038s  


## Status of [Actions defined in the previous meetings](../2022-11-10/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Review new website (secret link ;); ask Thomas Braun or Andy Goetz on slack
  to get it). Please comment on https://gitlab.com/tango-controls/TangoTickets/-/issues/76.
  *One* post per person only.
> There was some feedback.  Thanks.
   
- Feature request for "New attribute event ALARM_EVENT". please see
  https://gitlab.com/tango-controls/TangoTickets/-/issues/65 for rationale,
  proposal and impacted TC packages.
  Action: Read, understand, comment
 > Still open.

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for permission to publish some information.
- Debian packaging for tango/pytango; Freexian will do the work. Waiting for offer.  
> Frederic Picca recommended we ask Freexian to take care of both cppTango and PyTango.  
In future we may want to add HDB++ packages.  
Note:  conda-forge already has most of these. See https://gitlab.com/tango-controls/hdbpp/hdbpp-tickets/-/issues/1

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
> No feedback.

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
> Still trying to reproduce problem systematically, so that is can be reported.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)
> No progress.

**Thomas I. (OSL)**:
- Make MRs for getting cmake enhancements for windows into cpp servers
> MR in TangoDatabase open - it will be the template for others.
- Get installer into main/9.3-backports branch of TSD
> Waiting on other MR.
- Pushing change event from Windows device compiled with cppTango v9.4.0
  crashes: [cppTango#1013](https://gitlabcom/tango-controls/cppTango/-/issues/1013)
> Fixed!  9.4.0.windows.1 fixes it.  
Omniorb 4.2.1 too old.  
Need at least 4.2.2.  
The Windows packages now include omniorb 4.2.5.

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
> No progress
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker
  Compose file with the same functionality. For future, SKAO need to decide
  which images will be the "official" ones. See also https://gitlab.com/tango-controls/docker/tango-cs/-/issues/16.
> No progress
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.
> No progress
- Arrange a brainstorming session on a new Tango Documentation Camp. Revive Framapoll.
> No progress
- Kill https://hub.docker.com/u/tangocs or, add readme and then kill the images
> No progress
- Tango::Util::init is slow in AppVeyor environment: [cppTango#1012](https://gitlab.com/tango-controls/cppTango/-/issues/1012)
> No progress

## Issues requiring discussion

### PyTango

PyTango 9.4.0rc2 release - should be out today.  Pipeline with all the binary wheels and tests is taking about 4h, and requires some retries!  The slow builds and tests are running on gitlab runners hosted at MAX IV.  aarch64 is really slow.
We will now have binary wheels for Linux and Windows on PyPI, thanks to Mateusz Celary.  We already have Linux, Windows and MacOS on conda-forge, thanks to Benjamin Bertrand.
9.4.0 still planned for end November. but would like some downstream projects to test release candidates first!  There was some failures in Sardana CI : https://gitlab.com/sardana-org/sardana/-/issues/1810
SKAO:  plan to test.  
ESRF:  BLISS would be good to try.
If there are major issues with downstream projects, then release will be delayed.

Empty spectrum/image types:  cppTango team will discuss the related changes.

### cppTango 9.4.0

Testing at Elettra on their development environment:  
    - which Pogo version is required?  Use latest 9.8.x  
    - recipe explaining how to migrate devices written for 9.3.x to 9.4.0.
    
Pogo can regenerate, but device server code might also need to fix some includes and using namespace std.  
See this note for part of the recipe:  https://gitlab.com/tango-controls/cppTango/-/blob/main/RELEASE_NOTES.md#940-changes-which-might-have-an-impact-on-users  
Pogo can be installed from conda-forge: https://github.com/conda-forge/pogo-feedstock

### Astor

Astor fat jar: https://gitlab.com/tango-controls/Astor/-/issues/24  
**Action - Reynald**: Ping Astor maintainer about https://gitlab.com/tango-controls/Astor/-/issues/24 

## AOB
Proposal TangoDB refactoring
https://gitlab.com/tango-controls/TangoTickets/-/issues/69
https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing  
**Action - all kernel members**: Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment

### CMake training
cppTango team interested in training from KitWare (makers of cmake).  Is anyone else interested? 

https://www.kitware.com/courses/cmake-training/

> **Action - Thomas B**: Make a poll to gauge interest in CMake Training by Kitware - ask if time of course from https://www.kitware.com/courses/cmake-training/ is OK, or if "custom" time required.  
> **Action - Thomas B**: Ask Kitware about custom CMake training

### Next Special Interest Group meeting
It should be something kernel related.  Any ideas?  Tango IDL update?  Demo some existing tools ?  
Maybe add new SIG Slack channel for this info?
> **Action - Sergi**:  confirm if ALBA can host the next Special Interest Group meeting.
    
### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 8th December 2022 at 15:00 CET.  
The next cppTango Teleconf meeting will take place on Thursday 1st December 2022 at 14:00 CET.  
The next PyTango Teleconf meeting will take place on Thursday 1st December 2022 at 15:00 CET.  

## Summary of remaining actions

**All institutes**:
- Ask if your institute can host some Gitlab runners.
- Review new website (secret link ;); ask Thomas Braun or Andy Goetz on slack
  to get it). Please comment on https://gitlab.com/tango-controls/TangoTickets/-/issues/76.
  *One* post per person only.
   
- Feature request for "New attribute event ALARM_EVENT". please see
  https://gitlab.com/tango-controls/TangoTickets/-/issues/65 for rationale,
  proposal and impacted TC packages.
  Action: Read, understand, comment

**All kernel members**: Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment

**Lorenzo, Thomas J**: to take the proposal on Gitlab permissions forward.  Get info about SKA current policy.
Link to the proposal: https://gitlab.com/tango-controls/TangoTickets/-/issues/63

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for permission to publish some information.
- Debian packaging for tango/pytango; Freexian will do the work. Waiting for offer.  

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

**Reynald (ESRF)**:
- Ask Andy to find out if tango-controls Gitlab organization is considered part of the "Open Source" program.  
- Ping Astor maintainer about https://gitlab.com/tango-controls/Astor/-/issues/24 

**Sergi (ALBA)**:
- Confirm if ALBA can host the next Special Interest Group meeting.

**Thomas B. (byte physics)**:
- Make a poll to gauge interest in CMake Training by Kitware - ask if time of course from https://www.kitware.com/courses/cmake-training/ is OK, or if "custom" time required.
- Ask Kitware about custom CMake training
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

**Thomas I. (OSL)**:
- Work on MRs for getting cmake enhancements for windows into cpp servers
- Get installer into main/9.3-backports branch of TSD

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker
  Compose file with the same functionality. For future, SKAO need to decide
  which images will be the "official" ones. See also https://gitlab.com/tango-controls/docker/tango-cs/-/issues/16.
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.
- Arrange a brainstorming session on a new Tango Documentation Camp. Revive Framapoll.
- Kill https://hub.docker.com/u/tangocs or, add readme and then kill the images
- Tango::Util::init is slow in AppVeyor environment: [cppTango#1012](https://gitlab.com/tango-controls/cppTango/-/issues/1012)
- write a proposal to solve CI/CD Minutes quota issue and post in Slack for all to review.
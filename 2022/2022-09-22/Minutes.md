# Tango Kernel Follow-up Meeting
Held on 2022/09/22 at 15:00 CEST on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-09-22-9wm7?lang=en

**Participants**: 
 - Anton Joubert (MAX IV)
 - Aya Yoshimura (OSL)
 - Benjamin Bertrand (MAX IV)
 - Lorenzo Pivetta (Elettra)
 - Reynald Bourtembourg (ESRF)
 - Sergi Rubio (ALBA)
 - Thomas Braun (byte physics)
 - Thomas Juerges (SKAO)
 - Yury Matveev (DESY)
 - Damien Lacoste (ESRF)
 - Lin Zhu (MAX IV)

 
## Tango-Controls group members grooming - vote (https://gitlab.com/tango-controls/TangoTickets/-/issues/63)
https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846 : Accepted.  No one in the meeting opposed the proposal.  It goes to Steering Committee next.

## Meeting at SKAO debrief
Positive meeting.  

Jupyter and JupyTango demos were good.  SKAO already trying to use JupyTango.. Already on the next morning after the demos one engineer showed a notebook where he had the temperature of a simulated maser plotted.

SKAO are looking to help more with PyTango - resources (people) working on the project is lower than they would like.

Good retrospective on Tango Controls.  Via Miro board.   4 quadrants:  good things to keep, limitations we want to remove, future risks ahead, features we want to add.  This needs a follow up meeting.   One idea was to start Tango V10 proof of concept in Python, driven by SKAO.  Next Program Increment (3 months away) will include this as feature.
Link Miro board: https://miro.com/app/board/uXjVPZO1xSo=/?share_link_id=106387926817

We should meet like this again in 3 months.  Next meeting will focus on HDB++ and archiving. Hosted by Astron in Dwingeloo, Netherlands.  Date hasn't been decided, likely end of November 2022.
> **Action - Thomas J**: to remind Jan David Mol to send out a poll for a date.  
> Done.

If anyone else has ideas for future meetings like this, post it in the #kernel Slack channel.

## Status of [Actions defined in the previous meetings](../2022-09-08/Minutes.md#summary-of-remaining-actions)

**All institutes**:

- Read Tango-Controls gitlab group members grooming proposal (https://gitlab.com/tango-controls/TangoTickets/-/issues/63) before the next kernel meeting - we will vote on it at the next kernel meeting.  Then it goes to the Steering Committee for approval/disapproval.
> Done.  
> **New Action Lorenzo**: Tango-Controls gitlab group members grooming proposal to be proposed to the steering committee.

- Ping Thomas J. if you're interested in attending (in remote or not) the meeting organized at SKAO on "JupyTango as an engineering toolkit, Tango Controls future development & admin". Here is the [agenda](https://confluence.skatelescope.org/x/kq0_Cw)  
> Done.

- Need new version of Pogo to go with cppTango 9.4.  Damien needs help reviewing: 
  https://gitlab.com/tango-controls/pogo/-/merge_requests  
  For python  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/130  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/109  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
  and java  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/129  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106
 TangoSourceDistribution rc release will include new Pogo (excluding above MRs).    

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/README.txt  
Use merge request below for commenting: https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107
> No progress.

**Andy (ESRF)**: Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Anton (Max IV)**: Have a look at https://www.tango-controls.org/community/forum/c/development/python/unable-to-load-c-class-with-multiclass-python-hl/?page=1#post-4997
> Done. 

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))

- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files. For now, keep CMake files in TangoSourceDistribution.  Later can consider moving to TangoDatabase repo.
> No progress.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review
https://gitlab.com/tango-controls/pogo/-/merge_requests/126 
> No feedback.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
> Done: See https://gitlab.com/tango-controls/cppTango/-/issues/994

    It can be done without changing IDL, but does affect all cppTango, PyTango and JTango users.  So if we do it, would that be still be considered V9 compatible?  Probably not...

- Create an issue on TangoTickets/TangoIDL to try to run the omniORB tests
> Done: See https://gitlab.com/tango-controls/TangoTickets/-/issues/73

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after I am back from India (September/October) where I will gain some experience helping new colleagues getting their feet wet with TC.
> Date of trip to India is still unclear.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker Compose file with the same functionality. For future, SKAO need to decide which images will be the "official" ones.
- Comment https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15  

> Image usage:  
    Who is using tango-cs?  JTango is using it for CI.  
    Which SKAO images are you using.  
**New Action: Thomas J** - send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  
SKAO will stop updating Alpine-based images, and they will change from Debian base image to Ubuntu base images.

## High priority issues

Nothing reported.

## Issues requiring discussion

No issue to discuss during the Tango Kernel Meeting.

## AOB

### cppTango
cppTango 9.4.0rc and Tango Source Distribution rc releases every friday (when there is something new) in September.  Please test, if you can. 

### PyTango
9.3.6 release is coming soon.  This to fix issues reported by ESRF's BLISS project.  These problems have been fixed.  Still trying to get AppVeyor built Windows binary wheels updated to cppTango 9.3.5.

### Java applications
Most artefact publishing moved to Maven Central, but two MRs open on Astor and Jive.  What is the status?  And how can they depend on each other?
> **Action - Reynald:** Ping the maintainers of Astor and Jive to review the MR to move to Maven Central.

Once all on Maven Central, the TangoSourceDistribution needs to be updated to get the binaries from there.

Packages pushed to Maven Central so far are still using Java 8.  After all are there, then start using Java 17.  Pogo has already moved to Java 11.  We will only upload an old version of Pogo (Java 8), if someone requests it.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place **in 3 weeks** on Thursday 13th October 2022 at 15:00 CEST.  
The next PyTango Teleconf meeting will take place on Thursday 6th October 2022 at 15:00 CEST.

## Summary of remaining actions

**All institutes**:
- Need new version of Pogo to go with cppTango 9.4.  Damien needs help reviewing: 
  https://gitlab.com/tango-controls/pogo/-/merge_requests  
  For python  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/130  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/109  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106  
  and java  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/129  
    https://gitlab.com/tango-controls/pogo/-/merge_requests/106
 TangoSourceDistribution rc release will include new Pogo (excluding above MRs).    

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_1047813549
- Please give feedback on the new Tango Windows Installer README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/README.txt  
Use merge request below for commenting: https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107  
- Test cppTango and Tango Source Distribution release candidates versions

**Andy (ESRF)**: Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution (MR [TangoSourceDistribution!107](https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107))

- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files. For now, keep CMake files in TangoSourceDistribution.  Later can consider moving to TangoDatabase repo.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo !126 review
https://gitlab.com/tango-controls/pogo/-/merge_requests/126 
- Tango-Controls gitlab group members grooming proposal to be proposed to the steering committee

**Reynald (ESRF)**: Ping the maintainers of Astor and Jive to review the MR to move to Maven Central.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after Thomas J. will be back from India where he will gain some experience helping new colleagues getting their feet wet with Tango-Controls.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker Compose file with the same functionality. For future, SKAO need to decide which images will be the "official" ones.
- Comment https://gitlab.com/tango-controls/docker/tango-cs/-/issues/15  
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  

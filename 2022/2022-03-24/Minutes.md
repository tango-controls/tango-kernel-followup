Tango Kernel Follow-up Meeting
Held on 2022/03/24 at 15:00 CET on Zoom.

Participants:
- Benjamin Bertrand (Max IV)
- Reynald Bourtembourg (ESRF)
- Jack Yates (OSL)
- Becky Williams (OSL)
- Aya Yoshimura (OSL)
- Jairo Moldes (ALBA)
- Anton Joubert (MAX IV)
- Lorenzo Pivetta (Elettra)
- Damien Lacoste (ESRF)
- Michal Liszcz (S2Innovation)
- Grzegorz Kowalski
- Nicolas Leclercq (ESRF)
- Sergi Rubio (ALBA)

## Status of [Actions defined in the previous meetings](../2022-02-24/Minutes.md#summary-of-remaining-actions)

**All institutes**:

- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
> No update

- Test cppTango 9.3.5-rc1 and report back before Thursday 17th March 2022 - 13:00 CET, please.
> No big issues reported.  
> cppTango 9.3.5 has been released and a new Tango Source Distribution release is available too.   
> ESRF installed 9.3.5-rc1 (same as 9.3.5) on accelerator control system on 15th March (shutdown period).
> The accelerator is restarted today so critical problems if any will be spotted in the coming days.  
> ALBA got it running in Debian 9, but may have own packaging issue in Debian 10.  
> Elettra: installed on Ubuntu 18.04 LTS, but no exhaustive testing.  
> MAX IV still trying Conda packaging under Windows.  
> cpptango 9.3.5 and tango-test 3.4 available for both Linux and Windows on conda-forge  

For TangoDatabase, would be nice to review https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/29 to enable easier Windows build

- Answer to doodle/framadate: https://framadate.org/xrZqIHSrjR1vNPRj to organize a meeting to discuss alarms
> Meeting on ALARM was held on Monday 21st March.

- Close all tickets and pull requests on archive github repos, see https://gitlab.com/tango-controls/TangoTickets/-/issues/62
> Still 16 repos to do.   Allow Thomas B to do this (make Owner on Github organisation).

- Give feedback about removal of TANGO_LONG32 and TANGO_LONG64 definitions: See https://gitlab.com/tango-controls/cppTango/-/merge_requests/792#note_868699440
> No complaints, so it has been merged, and will be in 9.4
- Comment [cppTango#897](https://gitlab.com/tango-controls/cppTango/-/issues/897) about more frequent cppTango releases.

**Anton, Lorenzo, Thomas J and Thomas B**:

- Make a proposal to streamline access - this to be sent to steering committee:
  Ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/63
  >> No update.  Low priority.

**Andy (ESRF)**:

- Update the description of issue cppTango#822 (CORBA transient timeout message).

- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu. Delegate task to one of the new companies.
  Proposal that Observatory Sciences try on Ubuntu and then document the installation on Ubuntu (TBD)
  > Jack Y (OSL) tested install on Ubuntu: https://www.tango-controls.org/community/forum/c/general/installation/installing-tango-934-on-ubuntu-lts-2004/
  > Testing on Ubuntu by Jack Y (OSL).  Still ongoing.  Issue with a library?

- Ask  Alessio / Emmanuel to get access for at least one kernel member to https://launchpad.net/~tango-controls/+archive/ubuntu/core
> Andy reported (via Slack) that he had been given access by Emmanuel.  If anyone else wants access, then ask Andy.

**Anton (Max IV)**:

- Make a poll about required Python versions and architectures for PyTango binary wheels.
> No update
- In PyTango, can we disable Tango state switching automatically to ALARM when state is ON and an attribute is in ALARM? Investigate and report to Thomas J.
> No update

**Damien (ESRF)**:
- Prepare a new Pogo release
> No update

**Elettra**:
- Address review comments on TangoDatabase!28
> Elettra have addressed comments.

**Piotr (S2Innovation)**:
- Arrange a meeting to discuss alarms.
> Meeting was held.  Thomas J (SKAO) will update issue 559 (https://gitlab.com/tango-controls/cppTango/-/issues/559) and 560 (https://gitlab.com/tango-controls/cppTango/-/issues/560) in cppTango with the outcome.  
> Also TangoTickets #65 (https://gitlab.com/tango-controls/TangoTickets/-/issues/65) has been created.

**Reynald (ESRF)**:
- Ping Jean-Luc Pons to review and merge tango-controls/device-servers/DeviceClasses/communication/Socket!1 
> Ping done. Waiting for Jean-Luc to review and merge
- Create an issue or MR in tango-doc to improve documentation on whether the DevEnum in pipes are really not supported.

**Sergi (ALBA)**
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
> Migrating PANIC and SimulatorDS today.

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
> Not attending.

**Thomas J. (SKAO)**:
- Decide if joining the ADASS conference will be done now that it's virtual.
  > Another option is SPIE? in July 2022, in Montreal, Canada.
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
> Not attending.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
> Aya Y (OSL) looking at .yml file steps to check, install, cmake, build.  
> "Chocolatey" package manager.   
> Busy creating steps to execute on shared Gitlab Windows runner.   
> Creating executables for Starter, TangoDatabaseDS, and tango_admin.

## High priority issues

## Issues requiring discussion

## AOB

Migration of DS from svn to gitlab.  
Jairo (ALBA) asks who is responsible for further migration?  
S2Innovation have already moved the items requested.   
Lukasz will provide info to Jairo about how to do this.

Migration Java projects to Maven?  
S2Innovation is working on this, with Gwen (Soleil).  
Lukasz to inform Benjamin when next meeting is happening.

https://gitlab.com/tango-controls/cppTango/-/merge_requests/925
> Needs further discussion in a different forum.  Possibly one of the regular cppTango meetings.
> The next cppTango meeting will be held on Thursday 31st March 2022 at 14:00 CEST.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday **7th** April 2022 at 15:00 **CEST**.

## Summary of remaining actions

**All institutes**:

- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
- Test cppTango 9.3.5 and report encountered issues
- For TangoDatabase, would be nice to review https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/29 to enable easier Windows build
- Comment [cppTango#897](https://gitlab.com/tango-controls/cppTango/-/issues/897) about more frequent cppTango releases.
- Andy reported (via Slack) that he had been given access to Ubuntu launchpad by Emmanuel Taurel. If anyone else wants access, then ask Andy.

**Anton, Lorenzo, Thomas J and Thomas B**:

- Make a proposal to streamline access - this to be sent to steering committee:
  Ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/63

**Andy (ESRF)**:

- Update the description of issue cppTango#822 (CORBA transient timeout message).

**Anton (Max IV)**:

- Make a poll about required Python versions and architectures for PyTango binary wheels.
- In PyTango, can we disable Tango state switching automatically to ALARM when state is ON and an attribute is in ALARM? Investigate and report to Thomas J.

**Damien (ESRF)**:
- Prepare a new Pogo release

**Reynald (ESRF)**:
- Ping Jean-Luc Pons to review and merge tango-controls/device-servers/DeviceClasses/communication/Socket!1
- Create an issue or MR in tango-doc to improve documentation on whether the DevEnum in pipes are really not supported.

**Sergi (ALBA)**
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- Close all tickets and pull requests on archive github repos, see https://gitlab.com/tango-controls/TangoTickets/-/issues/62

**Thomas J. (SKAO)**:
- Decide if joining the ADASS conference will be done now that it's virtual. 
- Another option is SPIE? in July 2022, in Montreal, Canada.
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution

**Jack Y. (OSL)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.

# Tango Kernel Follow-up Meeting - 2022/04/28

To be held on 2022/04/28 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-20220428-9tw7?lang=en

# Agenda

1. Next Tango Collaboration meeting  
    1.1 Week 26 (June 27) or Week 29 (July 18)  
    1.2 One day? Two days? Noon-to-noon?  
    1.3 No parallel sessions  
    1.4 Meeting fee
2. RFC Workshop  
3. Status of [Actions defined in the previous meetings](../2022-04-07/Minutes.md#summary-of-remaining-actions)  
4. High priority issues  
5. Issues requiring discussion
6. AOB  
    6.1 Taurus Webinar  
    6.2 HDB++ Teleconf Meeting  
    6.3 Next Kernel Teleconf Meeting

# Tango Kernel Follow-up Meeting  
Held on 2022/04/28 at 15:00 CEST on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-20220428-9tw7?lang=en

**Participants**:
- Benjamin Bertrand (Max IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics e. K.)
- Piotr Goryl (S2Innovation)
- Vincent Hardion (MAX IV)
- Anton Joubert (MAX IV)
- Thomas Juerges (SKAO)
- Nicolas Leclercq (ESRF)
- Yury Matveev (DESY)
- Lorenzo Pivetta (Elettra)
- Sergi Rubio (ALBA)
- Becky Williams (OSL)
- Aya Yoshimura (OSL)

## Next Tango Collaboration meeting
###  Week 26 (June 27) or Week 29 (July 18)?
Vincent will cross-check the availability for the venue for week 26.  
We should consider the possibility to combine RFC and Tango Collaboration Meeting

**Action**: Vincent to discuss with Piotr and Lorenzo
### One day? Two days? Noon-to-noon?

Noon to noon seems to be the preference.

### No parallel sessions

### Meeting fee
To be decided with the organizers

## RFC Workshop
Dates should be officially decided at the next Kernel Meeting at the latest.    
Vincent, Piotr and Lorenzo will talk about the RFC workshop during their discussions for the Tango Collaboration Meeting organization.

## Debian 9 support in cppTango main branch

t-b: Debian 9 LTS will be end of life on 30th June 2022. Could we remove support for it in cppTango main branch?  
cppTango to remove Debian 9 support in May 1 2023.

## PyTango dropping support for older Python versions

From PyTango 9.4.0, we will drop support for Python 2.7 and Python 3.5.

### cppTango code owners

With Michal leaving, we need another volunteer.   Thomas Juerges has volunteered, and will be added.  
Nicolas Leclercq requests to be removed, as he does not have time.  
**Action**:  Thomas B will make the merge request to do this (Done). 

## Status of [Actions defined in the previous meetings](../2022-04-07/Minutes.md#summary-of-remaining-actions)

**All institutes**:

- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
- PyTango on Windows (t-b): https://gitlab.com/tango-controls/cppTango/-/issues/912

**Lorenzo, Thomas J and Thomas B (and Andy?)**:

- Make a proposal to streamline access - this to be sent to steering committee:
> A meeting between Lorenzo, Thomas J and Thomas B is "planned".

**Andy (ESRF)**: 
- Provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.

**Anton (Max IV)**:

- Make a poll about required Python versions and architectures for PyTango binary wheels.
> No update.

https://gitlab.com/tango-controls/cppTango/-/blob/9d0acf65b8ee0456b71d5c6562db2333b64b4026/cppapi/server/device.cpp

**Lorenzo (ELETTRA)**:
- See with Graziano whether Graziano could become a Code Owner of TangoDatabase repository.
> Graziano wants to. Thanks Graziano!  Graziano will create a MR to update the code owners file.

**Reynald (ESRF)**:
- Create an issue or MR in tango-doc to improve documentation on whether the DevEnum in pipes are really not supported.
> No update

**Sergi (ALBA)**
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
> Done

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26
- Review https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/29
CI has been improved by Thomas B.  Will help with detecting issues between cppTango and TangoDatabase.
Now require C++11 and cmake 3.7.
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- cppTango as DLL on Windows, see https://gitlab.com/tango-controls/pytango/-/issues/440
- Review Damien's proposal on Pogo: pogo!126

**Thomas J. (SKAO)**:
- Send a proposal to organize a Tango workshop during SPIE, in July 2022, in Montreal, Canada.

> Tried but was too late. Now we try to pull some strings. Perhaps we still get it in.

- Organize a Tango Workshop at the next ADASS conference.

> Proposal sent. Was told by POC member that we are a bit late but the POC will discuss our proposal.

- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).

> Not yet.

- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904

> Not yet.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
>  Windows bat files for TangoDatabase added to issue (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354). 
  (Thanks Reynald!) The build CI script will download these files from the issue for now. 
  Need to figure out with Thomas Braun what is the best way to add these bat files into TangoDatabase repository.
  The files might need to be modified.  
  Not sure about Sleep.exe. Will investigate whether it is required.  
  Will be working on Apply.exe and dbconfig.exe next.

**Jack Y. (OSL)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
> No update (on holiday)

## High priority issues

- https://gitlab.com/tango-controls/cppTango/-/issues/912
- https://gitlab.com/tango-controls/cppTango/-/issues/915

## Issues requiring discussion
Would it make sense (save us developer time) to outsource building of Conda packages?

    Needs a bit more discussion.

## AOB

### cpptango and pytango on MacOS

Thomas J has got cppTango and PyTango build working on MacOS!   At least on Intel.  Haven't tried Apple silicon (M1) yet.  https://gitlab.com/tango-controls/pytango/-/merge_requests/447. (There are some workarounds)
Also a test osx-64 Conda package from Benjamin. For those who want to try: "conda create -y -c beenje -n tango tango-test pytango" (conda-forge shall be set as default channel).

    Tickets:

    https://gitlab.com/tango-controls/cppTango/-/issues/806

    https://gitlab.com/tango-controls/pytango/-/merge_requests/447


    Build scripts macOS (surprisingly also work on Gentoo Linux AARCH64): https://gitlab.com/tjuerges/build_tango.git

    Relevant branches:

    cppTango: main-macOS_build at https://gitlab.com/tjuerges/cppTango.git

    pytango: develop-update_for_cppTango_main_branch_and_macOS at https://gitlab.com/tjuerges/pytango.git

    Note: Tests do not work yet. Device Servers, Devices, Events, etc. appear to work though.

Old discussions:
MacOSX and cppTango: https://www.tango-controls.org/community/forum/post/4317/  
Tango on macOS: https://www.tango-controls.org/community/forum/c/general/installation/recipe-to-install-tango-9-from-source-on-macos-x-high-sierra/?page=3

### Taurus Webinar

Carlos Pascual is leaving Alba, the Tango-Controls and Taurus community.   
He kindly accepted to prepare and present a Tango Kernel Webinar dedicated to Taurus to share his precious knowledge on Taurus.  
This webinar took place on Wednesday 27th April 2022.
Videos are being uploaded on [tango-controls youtube channel](https://www.youtube.com/channel/UCezS9cMkektZNItYnPOAQvg).

**Action - Reynald**: Create an issue on Taurus repository to provide the links to the videos so these links could be added in the Taurus wiki.

### HDB++ Teleconf Meeting
The next HDB++ Teleconf Meeting will take place on 18th May at 10:00 CEST.

### Free tier of GitLab SaaS
gitlab.com will have a limit of 5 users per namespace beginning June 22, 2022: https://about.gitlab.com/blog/2022/03/24/efficient-free-tier/

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 12th May 2022 at 15:00 CEST.

## Summary of remaining actions

**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz

**Lorenzo, Thomas J and Thomas B (and Andy?)**:
- Make a proposal to streamline access - this to be sent to steering committee

**Vincent, Piotr and Lorenzo**:
- Discuss the organization of the next Tango-Controls collaboration Meeting and Tango RFC workshop

**Andy (ESRF)**:
- Provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.

**Anton (Max IV)**:
- Make a poll about required Python versions and architectures for PyTango binary wheels.

**Lorenzo (ELETTRA)**:
- Ask Graziano to create a MR in TangoDatabase to update the Code Owners files

**Reynald (ESRF)**:
- Create an issue or MR in tango-doc to improve documentation on whether the DevEnum in pipes are really not supported.
- Create an issue on Taurus repository to provide the links to the videos so these links could be added in the Taurus wiki.

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26
- Review https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/29
  CI has been improved by Thomas B.  Will help with detecting issues between cppTango and TangoDatabase.
  Now require C++11 and cmake 3.7.
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- cppTango as DLL on Windows, see https://gitlab.com/tango-controls/pytango/-/issues/440
- Review Damien's proposal on Pogo: pogo!126

**Thomas J. (SKAO)**:
- Send a proposal to organize a Tango workshop during SPIE, in July 2022, in Montreal, Canada.
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354) 
into TangoDatabase repository

**Jack Y. (OSL)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.

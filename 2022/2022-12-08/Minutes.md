# Tango Kernel Follow-up Meeting
Held on 2022/12/08 at 15:00 CET on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-12-08-9y1k?lang=en

**Participants**:
 - Benjamin Bertrand (MAX IV)
 - Becky Auger-Williams (OSL)
 - Damien Lacoste (ESRF)
 - Kieran Mulholland (OSL)
 - Ulrik Pedersen (OSL)
 - Thomas Juerges (SKAO)
 - Yury Matveev (DESY)
 - Reynald Bourtembourg (ESRF)
 - Stéphane Poirier (SOLEIL)
 - Lukasz Zytniak (S2Innovation)

## Status of [Actions defined in the previous meetings](../2022-11-24/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ask if your institute can host some Gitlab runners.
Action all: Please follow up with your facility until next kernel meeting.

- Review new website (secret link ;); ask Thomas Braun or Andy Goetz on slack
  to get it). Please comment on https://gitlab.com/tango-controls/TangoTickets/-/issues/76.
  *One* post per person only.
Still open
Action all: Please have a look!

- Feature request for "New attribute event ALARM_EVENT". please see
  https://gitlab.com/tango-controls/TangoTickets/-/issues/65 for rationale,
  proposal and impacted TC packages.
  Action: Read, understand, comment
  Action Thomas J.: Review until 2022-12-13.

**All kernel members**: Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment

**Lorenzo, Thomas J**: to take the proposal on Gitlab permissions forward.  Get info about SKA current policy.
Link to the proposal: https://gitlab.com/tango-controls/TangoTickets/-/issues/63
Action Thomas: Ask SKAO's Team System about policies until 2022-12-13.

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for permission to publish some information.
- Debian packaging for tango/pytango; Freexian will do the work. Waiting for offer.  

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

**Reynald (ESRF)**:
- Ask Andy to find out if tango-controls Gitlab organization is considered part of the "Open Source" program.  
- Ping Astor maintainer about https://gitlab.com/tango-controls/Astor/-/issues/24 
> Done - fat jar created for astor. Package submitted to conda-forge under the name "tango-astor": https://github.com/conda-forge/staged-recipes/pull/21397 (astor name is already taken by a python package). 

**Sergi (ALBA)**:
- Confirm if ALBA can host the next Special Interest Group meeting.

**Thomas B. (byte physics)**:
- Make a poll to gauge interest in CMake Training by Kitware - ask if time of course from https://www.kitware.com/courses/cmake-training/ is OK, or if "custom" time required.
- Ask Kitware about custom CMake training
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

**Thomas I. (OSL)**:
- Work on MRs for getting cmake enhancements for windows into cpp servers
> Ongoing
- Get installer into main/9.3-backports branch of TSD

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Action Thomas (SKAO): As soon as SKAO has reviewed the feedback publish the link to the Miro board with the feedback. Give an overview how the workshop went and make material available.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
  Nothing new to report.
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker
  Compose file with the same functionality. For future, SKAO need to decide
  which images will be the "official" ones. See also https://gitlab.com/tango-controls/docker/tango-cs/-/issues/16.
Action Thomas: Investigate until 2022-12-13.
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.
Action: None for the moment. We will wait for the outcome of Ulrik's investigation.

- Arrange a brainstorming session on a new Tango Documentation Camp. Revive Framapoll.
The session will be held on 2023-01-11, 14.30h CET. Tango Kernel Meetings Zoom room (https://esrf.zoom.us/j/94017863362?pwd=QWlMVzZWQTJyb1dPMk1OWmw0Z05SZz09).

- Kill https://hub.docker.com/u/tangocs or, add readme and then kill the images
Action Ulrik: Find out how to retire Tango-Controls images on Docker Hub, ideally with a redirection to the SKAO images.

- Tango::Util::init is slow in AppVeyor environment: [cppTango#1012](https://gitlab.com/tango-controls/cppTango/-/issues/1012)
For the moment we park this issue until somebody has time to dig into this.

- write a proposal to solve CI/CD Minutes quota issue and post in Slack for all to review.
Thomas started the proposal. Not finished and no feedback yet. Link is on the kernel Slack channel.
Action all: Please help finishing the proposal and/or give feedback.

## Issues requiring discussion
Blocking issue for pytango 9.4.0 release: https://gitlab.com/tango-controls/cppTango/-/issues/1022  
We decided to not release pytango 9.4.0 before this is fixed. Release won't happen before January.

TangoBox is migrating from GNOME to Xfce: 

    https://gitlab.com/tango-controls/tangobox/-/issues/56

We decided to switch to Xfce.

## AOB

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 22nd December 2022 at 15:00 CET.  
The next cppTango Teleconf meeting will take place on Thursday 15th December 2022 at 14:00 CET.  
The next PyTango Teleconf meeting will take place on Thursday 15th December 2022 at 15:00 CET.  

## Summary of remaining actions

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with your facility until next kernel meeting.

- Review new website (secret link ;); ask Thomas Braun or Andy Goetz on slack
  to get it). Please comment on https://gitlab.com/tango-controls/TangoTickets/-/issues/76.
  *One* post per person only.

- Feature request for "New attribute event ALARM_EVENT". please see
  https://gitlab.com/tango-controls/TangoTickets/-/issues/65 for rationale,
  proposal and impacted TC packages.
  Action: Read, understand, comment

- Help finishing the proposal to solve CI/CD Minutes quota issue and/or give feedback. Link is on the kernel Slack channel.

**All kernel members**: Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment

**Lorenzo, Thomas J**: to take the proposal on Gitlab permissions forward.  Get info about SKA current policy.
Link to the proposal: https://gitlab.com/tango-controls/TangoTickets/-/issues/63

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

**Reynald (ESRF)**:
- Ask Andy to find out if tango-controls Gitlab organization is considered part of the "Open Source" program.  

**Sergi (ALBA)**:
- Confirm if ALBA can host the next Special Interest Group meeting.

**Thomas B. (byte physics)**:
- Make a poll to gauge interest in CMake Training by Kitware - ask if time of course from https://www.kitware.com/courses/cmake-training/ is OK, or if "custom" time required.
- Ask Kitware about custom CMake training
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

**Thomas I. (OSL)**:
- Work on MRs for getting cmake enhancements for windows into cpp servers
- Get installer into main/9.3-backports branch of TSD

**Thomas J. (SKAO)**:
- Review comments on Feature request for "New attribute event ALARM_EVENT" (https://gitlab.com/tango-controls/TangoTickets/-/issues/65) until 2022-12-13
- Ask SKAO's Team System about gitlab policies until 2022-12-13.
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.  
- As soon as SKAO has reviewed the feedback publish the link to the Miro board with the feedback. Give an overview how the workshop went and make material available.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker
  Compose file with the same functionality. For future, SKAO need to decide
  which images will be the "official" ones. See also https://gitlab.com/tango-controls/docker/tango-cs/-/issues/16.  
  => Investigate until 2022-12-13
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will wait for the outcome of Ulrik's investigation.
- Arrange a brainstorming session on a new Tango Documentation Camp. Revive Framapoll.
The session will be held on 2023-01-11, 14.30h CET. Tango Kernel Meetings Zoom room (https://esrf.zoom.us/j/94017863362?pwd=QWlMVzZWQTJyb1dPMk1OWmw0Z05SZz09).

**Ulrik (OSL)**:
- Find out how to retire Tango-Controls images on Docker Hub, ideally with a redirection to the SKAO images.

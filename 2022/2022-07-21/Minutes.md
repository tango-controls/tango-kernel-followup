# Tango Kernel Follow-up Meeting
Held on 2022/07/21 at 15:00 CEST on Zoom.
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-07-20-9v6x?lang=en

**Participants**:
 - Thomas Juerges (SKAO)
 - Thomas Braun (byte physics)
 - Jack Yates (OSL)
 - Benjamin Bertrand (MAX IV)
 - Aya Yoshimura (OSL)
 - Becky Auger-Williams (OSL)
 - Lukasz Zytniak (S2Innovation)
 - Grzegorz Kowalski (S2Innovation)

## Status of [Actions defined in the previous meetings](../2022-07-07/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
  **Action - Lorenzo**: Ping Claudio for pogo !126 review

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_974367265)
> Need someone to test TangoAccessControl

**Lorenzo, Thomas J and Thomas B (and Andy?)**:
- Make a proposal to streamline Gitlab access - this to be sent to steering committee
Meeting held on 2022-06-17. Agreed on a set of rules/guidelines.
Lorenzo, Thomas B, Reynald, Nicolas, Andy: Please review
Review done.
Thomas: Invite to final meeting to discuss remaining point.

**Andy (ESRF)**:
- Provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.
> here is the code Andy was referring to:
    https://github.com/ALBA-Synchrotron/pytango-db
- see this issue for the discussion: https://gitlab.com/tango-controls/pytango/-/issues/266

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
> MR(https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107) created.

- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354) into TangoDatabase repository
- Some issues to work on:
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase, TangoAccessControl and starter repositories, update CMake files so that they can be built on Windows without special Windows CMake files.
 >   Created MR in Starter https://gitlab.com/tango-controls/starter/-/merge_requests/14

  3.Need to update README.md that will be included in the installer (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/5)
  I had to create a new repository. Please see the README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/tango-9.3.4-beta_win64/README.txt

  > Add 'Limitation Section' to README.txt file and mention that 'Access Control' hasn't been tested.
  > After updating the README.txt file, will add link to the latest README.txt and the installer to Slack kernel channel and also Tango Forum.

  4.Remove the default 'before_script' from the windows yml file (https://gitlab.com/ayaosl/tango-source-distribution-win/-/issues/2)
  > Done

**Damien (ESRF)**:
- Try to run cppTango test suite on Windows
>  https://gitlab.com/tango-controls/cppTango/-/merge_requests/950

**Reynald (ESRF)**:
- Assign Merge Requests open for the Maven Migration in the different Java projects. (https://gitlab.com/groups/tango-controls/-/merge_requests?scope=all&state=opened&search=Migration+to+Maven)
> Done. Except for LogViewer which appears to be still an orphan project.
  Krzysztof Klimczyk (S2Innovation) volunteered (through Lukasz) for maintainer
  - Create a Doodle poll to ask what version is used/required by the different users.
> Done.   See https://framaforms.org/mysql-and-mariadb-versions-for-your-tango-database-1655483577
16  replies so far.
3 persons replied "No" to "Would it be ok for you if support for MySQL for the Tango Database DS is dropped in future versions (only MariaDB would be supported)?" but among the 3, 2 commented that they could envisage to migrate to MariaDB if there is a clear documented procedure/tools to ease the migration.
> Support for mysql will be dropped on the next release.
  A clear migration procedure has to be produced.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- MR review for Aya done.
-

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after I am back from India (September/October) where I will gain some experience helping new colleagues getting their feet wet with TC.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
Still to do.
- pytango: Revive pybind11 port? https://gitlab.com/tango-controls/pytango/-/merge_requests/322
No news since last meeting
- Try and find an equivalent of tango-cs in the SKAO images.
Not done yet.
- SKAO interested in Tango Controls Community meeting?
Yes, absolutely.
- SKAO likes JupyTango
Visit of Andy and Nicolas to SKAO possible?
Yes. Will be discussed once Andy and Niclas are back from holidays.
Will then also dicuss then the hosting of the Commubnity Meeting.

## High priority issues

Nothing

## Issues requiring discussion

Nothing

## AOB

Nothing

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on 2022-08-11.

## Summary of remaining actions

**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz

- Test the Beta version of the Windows distribution. See link in
  https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_974367265)

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
> MR(https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/107) created.

- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354) into TangoDatabase repository
- Some issues to work on:
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase, TangoAccessControl and starter repositories, update CMake files so that they can be built on Windows without special Windows CMake files.
 >   Created MR in Starter https://gitlab.com/tango-controls/starter/-/merge_requests/14

  3.Need to update README.md that will be included in the installer (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/5)
  I had to create a new repository. Please see the README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/tango-9.3.4-beta_win64/README.txt

  > Add 'Limitation Section' to README.txt file and mention that 'Access Control' hasn't been tested.
  > After updating the README.txt file, will add link to the latest README.txt and the installer to Slack kernel channel and also Tango Forum.

**Lorenzo (Elletra)**:
- Ping Claudio for pogo !126 review

**Reynald (ESRF)**:
- Assign Merge Requests open for the Maven Migration for LogViewer. https://gitlab.com/groups/tango-controls/-/merge_requests?scope=all&state=opened&search=Migration+to+Maven

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
Suspended until after I am back from India (September/October) where I will gain some experience helping new colleagues getting their feet wet with TC.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- pytango: Revive pybind11 port? https://gitlab.com/tango-controls/pytango/-/merge_requests/322
- Try and find an equivalent of tango-cs in the SKAO images.
- Invite to final meeting to discuss remaining point regrading "Make a proposal to streamline Gitlab access"

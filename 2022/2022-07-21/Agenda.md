# Tango Kernel Follow-up Meeting - 2022/07/21

To be held on 2022/07/21 at 15:00 CEST on Zoom.
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-07-20-9v6x?lang=en

# Agenda

1. Status of [Actions defined in the previous meetings](../2022-07-07/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. AOB

# Tango Kernel Follow-up Meeting - 2022/06/16

To be held on 2022/06/16 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-06-16-9usz?lang=en

# Agenda

1. TangoDatabase: Minimum support MySQL/MariaDB versions
1. Tango docker images
1. Next Tango Collaboration meeting  
1. RFC Workshop  
1. Tarenta Meeting  
1. Status of [Actions defined in the previous meetings](../2022-06-01/Minutes.md#summary-of-remaining-actions)  
1. High priority issues  
1. Issues requiring discussion  
1. AOB  
    * Next Kernel Teleconf Meeting

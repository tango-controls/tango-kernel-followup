# Tango Kernel Follow-up Meeting
Held on 2022/06/16 at 15:00 CEST on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-06-16-9usz?lang=en

**Participants**:
- Reynald Bourtembourg (ESRF)
- Thomas Juerges (SKAO)
- Sergi Rubio (ALBA)
- Becky Williams (OSL)
- Jack Yates (OSL)
- Aya Yoshimura (OSL)
- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Johan Venter (SARAO)
 
## TangoDatabase: Minimum support MySQL/MariaDB versions
- ESRF: MariaDB 10.3
- MAX IV: MariaDB 5.5 (moving to 10.3 with Rocky Linux 8)
- ALBA: MariaDB 10.1, migrating to 10.3
- SKAO: prefer MariaDB, not MySQL.  Typically use the "recommended" version.   Currently, using mariadb:10 Docker images registry.gitlab.com/tango-controls/docker/mysql are based on mysql:5 and mysql:8

**Action - Reynald**: Create a Doodle poll to ask what version is used/required by the different users.  

## Tango docker images

SKAO have created many images, build and update frequently.  Always using latest available Tango releases.  Build on Debian and Alpine.  Alpine Linux gets security updates sooner.  
All built images will be kept forever.  

SKAO would like to be the "official" source of Tango docker images.

https://artefact.skao.int/#browse/search/docker
https://gitlab.com/ska-telescope/ska-tango-images

What is the equivalent of the tango-cs image?  It includes TangoDS, TangoTest, Starter, supervisord, etc.
https://gitlab.com/tango-controls/docker/tango-cs

https://gitlab.com/ska-telescope/ska-tango-images/-/tree/master/images/ska-tango-images-tango-libtango seems close to this

**Action - Anton**: to review https://gitlab.com/tango-controls/docker/tango-cs/-/merge_requests/10
Will try to merge it to have something up-to-date for SKAO to check  
**Action - Thomas (SKAO)**: Try and find an equivalent of tango-cs in the SKAO images.

## Next Tango Collaboration meeting

To be held on 29th and 30th of June 2022.  

The meeting is hosted in hybrid format by MAX IV Laboratory, Lund Sweden.

REGISTRATION AND CALL FOR ABSTRACT IS NOW OPEN! Don't wait too long, places are limited.
https://indico.tango-controls.org/event/51/ 

## RFC Workshop
Will be held in Castle residence in Korzkiew (https://www.booking.com/hotel/pl/zamekkorzkiewprzybyslawice.pl.html) from 21st to 23rd June 2022.  
Accommodation is paid by the Tango collaboration.  
Please register here: https://indico.tango-controls.org/event/53/registrations/40/

Zoom links will be advertised on the Slack tango-rfc channel:  https://tango-controls.slack.com/archives/CKHSF815W

## Status of [Actions defined in the previous meetings](../2022-06-01/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
- Benjamin didn't create a bot user for java projects.  
He defined a project access token (https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) which is much easier to maintain.  
It's per project and can easily be revoked/changed.  
Please give feedback to Benjamin about this proposal.
> No feedback yet.  
8 open merge requests - who is looking at this?
https://gitlab.com/groups/tango-controls/-/merge_requests?scope=all&state=opened&search=Migration+to+Maven  
**Action - Reynald**: assign * open Merge Requests open for the Maven Migration in the different Java projects.

**Lorenzo**:Organize a teleconf meeting around mid-June to discuss the proposal to streamline Gitlab access.
Done. Telecon on 2022-06-17, 15.00 - 16.00 CEST

**Lorenzo, Thomas J and Thomas B (and Andy?)**:
- Make a proposal to streamline Gitlab access - this to be sent to steering committee  

**Andy (ESRF)**:
- Provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354) into TangoDatabase repository
- Provide a Beta version of the Windows distribution when available 
> Done: See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_974367265)

>Some issues to work on
1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)
2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase, accesscontrol and starter repositories, update CMake files so that they can be built on Windows without special Windows CMake files.

3.Need to update README.md that will be included in the installer (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/5)
4.Remove the default 'before_script' from the windows yml file (https://gitlab.com/ayaosl/tango-source-distribution-win/-/issues/2)

**Damien (ESRF)**:
- Try to run cppTango test suite on Windows

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
>Nothing done.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
>Nothing done.
- pytango: Revive pybind11 port? https://gitlab.com/tango-controls/pytango/-/merge_requests/322  
Let's see what is required to finish the job.  
Talk to Geoffrey Mant for a summary where he left it and what might be the dark and untidy corners. After that talk to Anton and/or Yuri (MAX IV), cc Lorenzo, Nicolas, Andy  
>Talked to geoffrey Mant. he thinks that it is definitively worth pursuing the pybind11 path. He said that he will try to assess how much work a rebase onto 9.3.x would take if his time allows. Timeboxed until next Thursday morning.  
We would target PyTango 9.4.x.  pybind11 started on 9.3.x, so expect some conflicts, and missing features.  
Why considering pybind11 again?  Can it make it easier for PyTango to keep up with cppTango releases?  Still missing features in PyTango because of this.  
Another benefit might be easier CI for extensions.  See this Python talk:  https://www.youtube.com/watch?v=gROGDQakzas

>Sergi: Idea:  dynamic attributes in python HL!

## High priority issues

## Issues requiring discussion

### PyTango 9.3.4 release
Uploaded to PyPI.  Conda packages will be available in a few hours.  
Note: Windows binary wheels (non-Conda) included cppTango 9.3.4, not cppTango 9.3.5.  
This is the last 9.3.x release planned for PyTango.  
Next target is 9.4.0.

## AOB
Johan: Rust binding. Is there any plan to update it in the near future?  
Not planned at the moment and future of it is unclear. We will ask at the community meeting again.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 7th July 2022 at 15:00 CEST.

## Summary of remaining actions

**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_974367265)
 
**Lorenzo, Thomas J and Thomas B (and Andy?)**:
- Make a proposal to streamline Gitlab access - this to be sent to steering committee  

**Andy (ESRF)**:
- Provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.

**Anton (MaxIV)**: Review https://gitlab.com/tango-controls/docker/tango-cs/-/merge_requests/10  
Will try to merge it to have something up-to-date for SKAO to check

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354) into TangoDatabase repository
- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase, TangoAccessControl and starter repositories, update CMake files so that they can be built on Windows without special Windows CMake files.

  3.Need to update README.md that will be included in the installer (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/5)  
  4.Remove the default 'before_script' from the windows yml file (https://gitlab.com/ayaosl/tango-source-distribution-win/-/issues/2)

**Damien (ESRF)**:
- Try to run cppTango test suite on Windows

**Reynald (ESRF)**: 
- Assign Merge Requests open for the Maven Migration in the different Java projects. (https://gitlab.com/groups/tango-controls/-/merge_requests?scope=all&state=opened&search=Migration+to+Maven)
- Create a Doodle poll to ask what version is used/required by the different users.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- pytango: Revive pybind11 port? https://gitlab.com/tango-controls/pytango/-/merge_requests/322  
Let's see what is required to finish the job.  
Talk to Geoffrey Mant to assess how much work a rebase onto 9.3.x would take if his time allows.  
- Try and find an equivalent of tango-cs in the SKAO images.

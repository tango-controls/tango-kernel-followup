# Tango Kernel Follow-up Meeting
Held on 2022/12/22 at 15:00 CET on Zoom.
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-12-22-9y29?lang=en

**Participants**:
- Lorenzo Pivetta (Elettra)
 - Damien Lacoste (ESRF)
 - Andy Gotz (ESRF)
 - Lukasz Zytniak (S2Innovation)
 - Thomas Braun (Byte Physics)
 - Sergi Rubio (ALBA)
 - Becky Auger-Williams (OSL)
 - Nicolas Leclercq (ESRF)

## Status of [Actions defined in the previous meetings](../2022-12-08/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with your facility until next kernel meeting.

-> In progress.

- Review new website (secret link ;); ask Thomas Braun or Andy Goetz on slack
  to get it). Please comment on https://gitlab.com/tango-controls/TangoTickets/-/issues/76.
  *One* post per person only.
  
-> Andy reviewed the new website with Olga; no major problems discovered. 
-> Next step is to apply the changes to the live website. 
-> Andy will do this. Volunteers welcomed!

- Feature request for "New attribute event ALARM_EVENT". please see
  https://gitlab.com/tango-controls/TangoTickets/-/issues/65 for rationale,
  proposal and impacted TC packages.
  Action: Read, understand, comment

- Help finishing the proposal to solve CI/CD Minutes quota issue and/or give
  feedback. Link is on the kernel Slack channel.

-> Done.
-> We add additional gitlab runners instead of buying more minutes.

**All kernel members**: Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment

-> No update.

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for permission to publish some information.

-> This will be done at the same time the web site is updated.
-> Work in progress.

**Lorenzo, Thomas J**: to take the proposal on Gitlab permissions forward.  Get info about SKA current policy.
Link to the proposal: https://gitlab.com/tango-controls/TangoTickets/-/issues/63

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)

-> No update

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

-> Still open.

**Reynald (ESRF)**:
- Ask Andy to find out if tango-controls Gitlab organization is considered part of the "Open Source" program.  

-> Done. We currently have the SaaS Ultimate licence for EDUcation. Andy has
-> requested to move to the OSS program which is more interesting in terms of
-> CI/CD minutes quota.

**Sergi (ALBA)**:
- Confirm if ALBA can host the next Special Interest Group meeting.

-> Yes it can!

-> SIG meeting about Taurus in March 2023 @ ESRF
-> And yet another one in April 2023 about CI/CD @ ALBA

**Thomas B. (byte physics)**:
- Make a poll to gauge interest in CMake Training by Kitware - ask if time of
  course from https://www.kitware.com/courses/cmake-training/ is OK, or if
  "custom" time required.
- Ask Kitware about custom CMake training
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

-> No progress.

**Thomas I. (OSL)**:
- Work on MRs for getting cmake enhancements for windows into cpp servers
- Get installer into main/9.3-backports branch of TSD

-> No update.

**Thomas J. (SKAO)**:
- Review comments on Feature request for "New attribute event ALARM_EVENT"
  (https://gitlab.com/tango-controls/TangoTickets/-/issues/65) until 2022-12-13
- Ask SKAO's Team System about gitlab policies until 2022-12-13.
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.  
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker
  Compose file with the same functionality. For future, SKAO need to decide
  which images will be the "official" ones. See also https://gitlab.com/tango-controls/docker/tango-cs/-/issues/16.  
  => Investigate until 2022-12-13
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

-> No update.

**Ulrik (OSL)**:
- Find out how to retire Tango-Controls images on Docker Hub, ideally with a redirection to the SKAO images.

-> No update.

## Issues requiring discussion

## AOB

- User request (Lorenzo) that TangoSourceDistribution from the 9.3-backports
  branch should compile with plain C++98. Currently we only ensure that for
  cppTango but not for the DS'es aka build kernel tools (starter) with old compilers

-> Action TB: Look into how to get starter built with C++98. Lorenzo and Nicolas need that.

-> Proposal: cppTango and starter should work with plain C++98 for all 9.3.x
-> releases of the tango source distribution.

- any news on the TangoBox update?

-> S2 are working on this, there were some issues with Python 3 but these are on
-> the way to being fixed. The plan is to finish the update by the end of 2022.
-> They will inform the community as soon as it is ready to test.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 12th January 2022 at 15:00 CET.  
The next cppTango Teleconf meeting will take place on Thursday 5th January 2022 at 14:00 CET.  
The next PyTango Teleconf meeting will take place on Thursday 5th January 2022 at 15:00 CET.  

## Summary of remaining actions

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with your facility until next kernel meeting.

- Feature request for "New attribute event ALARM_EVENT". please see
  https://gitlab.com/tango-controls/TangoTickets/-/issues/65 for rationale,
  proposal and impacted TC packages.
  Action: Read, understand, comment

**All kernel members**: Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Lorenzo, Thomas J**: to take the proposal on Gitlab permissions forward.  Get info about SKA current policy.
Link to the proposal: https://gitlab.com/tango-controls/TangoTickets/-/issues/63

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

**Reynald (ESRF)**:

NONE

**Sergi (ALBA)**:

NONE

**Thomas B. (byte physics)**:
- Make a poll to gauge interest in CMake Training by Kitware - ask if time of
  course from https://www.kitware.com/courses/cmake-training/ is OK, or if
  "custom" time required.
- Ask Kitware about custom CMake training
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)
- Look into how to get starter built with C++98.
- Document: "cppTango and starter should work with plain C++98 for all 9.3.x
   releases of the tango source distribution."

**Thomas I. (OSL)**:
- Work on MRs for getting cmake enhancements for windows into cpp servers
- Get installer into main/9.3-backports branch of TSD

**Thomas J. (SKAO)**:
- Review comments on Feature request for "New attribute event ALARM_EVENT"
  (https://gitlab.com/tango-controls/TangoTickets/-/issues/65) until 2022-12-13
- Ask SKAO's Team System about gitlab policies until 2022-12-13.
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.  
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker
  Compose file with the same functionality. For future, SKAO need to decide
  which images will be the "official" ones. See also https://gitlab.com/tango-controls/docker/tango-cs/-/issues/16.  
  => Investigate until 2022-12-13
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

**Ulrik (OSL)**:
- Find out how to retire Tango-Controls images on Docker Hub, ideally with a redirection to the SKAO images.

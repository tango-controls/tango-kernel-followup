# Tango Kernel Follow-up Meeting - 2022/12/22

To be held on 2022/12/22 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-12-22-9y29?lang=en

# Agenda

1. Status of [Actions defined in the previous meetings](../2022-12-08/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. AOB
  - User request (Lorenzo) that TangoSourceDistribution from the 9.3-backports branch should compile with plain C++98. Currently we only ensure that for cppTango but not for the DS'es.

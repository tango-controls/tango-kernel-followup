# Tango Kernel Follow-up Meeting - 2022/07/07

To be held on 2022/07/07 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-07-07-9v6x?lang=en

# Agenda

1. Tango Community Meeting Debriefing
1. Definition of LTS versions (duration of the support, what kind of support, starting date, ...)
1. Status of [Actions defined in the previous meetings](../2022-06-16/Minutes.md#summary-of-remaining-actions)  
1. High priority issues  
1. Issues requiring discussion  
1. AOB  
    * Next Kernel Teleconf Meeting

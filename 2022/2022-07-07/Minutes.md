# Tango Kernel Follow-up Meeting
Held on 2022/07/07 at 15:00 CEST on Zoom.  
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2022-07-07-9v6x?lang=en

**Participants**:
- Andy Gotz (ESRF)
- Aya Yoshimura (OSL)
- Becky Williams (OSL)
- Benjamin Bertrand (MAX IV)
- Damien Lacoste (ESRF)
- Geoff Mant (STFC UKRI)
- Grzegorz Kowalski (S2Innovation)
- Lorenzo Pivetta (Elettra)
- Lukasz Zytniak (S2Innovation)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics e. K.)
- Thomas Juerges (SKAO)

## Tango Community Meeting Debriefing
Idea for the next Tango Controls Community Meeting June 2023: Could SKAO be host?  
**Action - Thomas J**:
    Take this to Marco B. and report back at the next kernel meeting.

**Action - Andy**: Contact Duncan and ask him how we could test omniORB.

## Definition of LTS versions

A recurrent question asked during the Tango Community Meetings is about the definition and duration of the Tango LTS versions.
Let's define it together and document it.
> At ICALEPCS 2017 it was foreseen to start LTS support for a certain major release when the next one is released. The duration of LTS support will be 5 years. Tango 9.4 release is date Oct 2022, thus 9.3 will get critical bugfix support until Oct 2027.  
The criticality of the bugs will be assessed by the Tango Kernel Developers, even if a patch is provided.

**Action - Reynald**: Create a MR in tango-doc to document LTS support

## Status of [Actions defined in the previous meetings](../2022-06-16/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz  
  **Action - Lorenzo**: Ping Claudio for pogo !126 review

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_974367265)  
> Need someone to test TangoAccessControl. If it cannot be tested in the next weeks, add a note in the beta release release notes saying that the Tango Access Control needs test contributions.  
**Action - Thomas Braun**: Review the merge requests proposed by Aya  
**Action - Thomas Braun**: Add CI for Starter and TangoAccessControl repositories

**Lorenzo, Thomas J and Thomas B (and Andy?)**:
- Make a proposal to streamline Gitlab access - this to be sent to steering committee  
Meeting held on 2022-06-17. Agreed on a set of rules/guidelines.
Thomas: Added the summary to https://gitlab.com/tango-controls/TangoTickets/-/issues/63
Lorenzo, Thomas B, Reynald, Nicolas, Andy: Please review

**Andy (ESRF)**:
- Provide link to SQLite-based implementation of DatabaseDS.  Used in Bliss project at ESRF.
> Here is the code Andy was referring to: 
    https://github.com/ALBA-Synchrotron/pytango-db
- see this issue for the discussion: https://gitlab.com/tango-controls/pytango/-/issues/266

**Anton (MaxIV)**: Review https://gitlab.com/tango-controls/docker/tango-cs/-/merge_requests/10  
Will try to merge it to have something up-to-date for SKAO to check  
> Reviewed and Merged

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354) into TangoDatabase repository
- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Will create MR in TangoDatabase, TangoAccessControl and starter repositories, update CMake files so that they can be built on Windows without special Windows CMake files.
 >   Created MR in Starter https://gitlab.com/tango-controls/starter/-/merge_requests/14

  3.Need to update README.md that will be included in the installer (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/5)  
  Aya had to create a new repository. Please see the README.txt file https://gitlab.com/ayaosl/tango-source-distribution-win/-/blob/WindowsInstaller9.3.4-beta/windows/tango-9.3.4-beta_win64/README.txt
  
  4.Remove the default 'before_script' from the windows yml file (https://gitlab.com/ayaosl/tango-source-distribution-win/-/issues/2)
  > Done

**Damien (ESRF)**:
- Try to run cppTango test suite on Windows
> WIP, See https://gitlab.com/tango-controls/cppTango/-/merge_requests/950

**Reynald (ESRF)**: 
- Assign Merge Requests open for the Maven Migration in the different Java projects. (https://gitlab.com/groups/tango-controls/-/merge_requests?scope=all&state=opened&search=Migration+to+Maven)  
> Done. Except for LogViewer which appears to be still an orphan project.
  Krzysztof Klimczyk (S2Innovation) volunteered (through Lukasz) for maintainer
  - Create a Doodle poll to ask what version is used/required by the different users.
> Done.   See https://framaforms.org/mysql-and-mariadb-versions-for-your-tango-database-1655483577  
16  replies so far.  
3 persons replied "No" to "Would it be ok for you if support for MySQL for the Tango Database DS is dropped in future versions (only MariaDB would be supported)?" but among the 3, 2 commented that they could envisage to migrate to MariaDB if there is a clear documented procedure/tools to ease the migration.  
> **Support for mysql will be dropped on the next release.
  A clear migration procedure has to be produced.**

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.
> Thomas J. talked to Jan David. He made a good point that online tutorial is not as efficient as face to face. Let's suspend this for the time being until Thomas J. is back from India (September/October) where he will gain some experience helping new colleagues getting their feet wet with TC.
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
> Still to do.
- pytango: Revive pybind11 port? https://gitlab.com/tango-controls/pytango/-/merge_requests/322  
Let's see what is required to finish the job.  
Talk to Geoffrey Mant to assess how much work a rebase onto 9.3.x would take if his time allows.  
> Talked to Geoff and he is today here to provide an overview.  
Anton (not at this kernel meeting) is not positive about the idea, but also not completely against it.  We need to have more evidence of the benefit of pybind11 over boost before continuing with this major effort.  
Geoff said he will have a more detailed look in 2 weeks for the rebase of the pybind11 branch on 9.3 branch.
He said rebasing on 9.3 should be easier than integrating pybind11 changes into 9.4 directly.

- Try and find an equivalent of tango-cs in the SKAO images.
> Not done yet.

## High priority issues

## Issues requiring discussion
pytango issue https://gitlab.com/tango-controls/pytango/-/issues/451 will require a new release (9.3.5) in the coming weeks.

## AOB

Nicolas L. would like to have some PyTango device servers development guidelines. All the Tango specific decorators should be clearly documented.

**Action - Nicolas L. (ESRF)**: Create an issue in PyTango to request the writing of some PyTango device servers development guidelines and a clear documentation of all the Tango specific decorators.

### Tango packages on Gentoo and ArchLinux

Damien said he created some Tango packages for Gentoo and archLinux. 
He would like to create repositories for the Gentoo packages on tango-controls Gitlab organization.  
Since there is already a subgroup for repositories used to build RPM packages, it was suggested to create a new dedicated subgroup for the repositories used to build the Gentoo packages.  
For archLinux, he will use the archLinux infrastructure.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on 21st July 2022 at 15:00 CEST. Thomas Braun will host it. The zoom link will be the usual one.

## Summary of remaining actions

**All institutes**:
- Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/-/merge_requests/126
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz  

- Test the Beta version of the Windows distribution. See link in https://gitlab.com/tango-controls/TangoTickets/-/issues/61#note_974367265)  

**Lorenzo, Thomas J and Thomas B (and Andy?)**:
- Make a proposal to streamline Gitlab access - this to be sent to steering committee  
Meeting held on 2022-06-17. Agreed on a set of rules/guidelines.
Thomas: Added the summary to https://gitlab.com/tango-controls/TangoTickets/-/issues/63
Lorenzo, Thomas B, Reynald, Nicolas, Andy: Please review

**Andy (ESRF)**:
Contact Duncan and ask him how we could test omniORB.

**Aya (OSL)**:
- Work on [TangoTickets#61](https://gitlab.com/tango-controls/TangoTickets/-/issues/61).: Automate the Packaging of the Windows Distribution
- Work with Thomas Braun to integrate the database schema creation bat files (https://gitlab.com/tango-controls/TangoDatabase/-/issues/38#note_920044354) into TangoDatabase repository
- Some issues to work on:  
  1.CPP server application versions (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/1)  
  2.CPP server applications Windows CMake files are not in the main branch. (https://gitlab.com/ayaosl/TangoSourceDistribution/-/issues/7)

    Create MR in TangoDatabase and TangoAccessControl repositories, update CMake files so that they can be built on Windows without special Windows CMake files.

**Damien (ESRF)**:
- Try to run cppTango test suite on Windows (See https://gitlab.com/tango-controls/cppTango/-/merge_requests/950)

**Geoff (STFC UKRI)**: pytango: Revive pybind11 port? Have a more detailed look at the rebase of the pybind11 branch into 9.3 branch.

**Lorenzo (ELETTRA)**: Ping Claudio for [pogo !126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126) review.

**Nicolas L. (ESRF)**: Create an issue in PyTango to request the writing of some PyTango device servers development guidelines and a clear documentation of all the Tango specific decorators.

**Reynald (ESRF)**: 
- Create a MR in tango-doc to document LTS support

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26) (missing default value for "date" field of "property_device_hist" table)
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- Review the merge requests proposed by Aya
- Add CI for Starter and TangoAccessControl repositories

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of conferences. Then decide on the format, the date, etc.   (Action suspended until Thomas J. is back from India (September/October)
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images.  
- Talk to Marco B. to see whether SKAO could organize the next Tango Community Meeting

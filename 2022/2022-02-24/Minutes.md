Tango Kernel Follow-up Meeting
Held on 2022/02/24 at 15:00 CET on Zoom.

Participants:
    - Andy (ESRF)
    - Thomas Juerges (SKAO)
    - Benjamin Bertrand (Max IV)
    - Olga Merkulova (IK)
    - Piotr Goryl (S2Innovation)
    - Jack Yates (OSL)
    - Becky Williams (OSL)
    - Aya Yoshimura (OSL)
    - Philip Taylor (OSL)
    - Thomas Braun (byte physics)
    - Gwenaëlle Abeillé (Synchrotron SOLEIL)
    - Sergi Rubio (ALBA)

## Status of [Actions defined in the previous meetings](../2021-02/10/Minutes.md#summary-of-remaining-actions)

All institutes:

    if you're willing to help in any way for the Tango workshop at the next ADASS conference which will take place in Victoria, Canada from 11-15 September 2022, please contact Thomas Juerges (SKAO)

Sad news from ADASS: It will not be help in-person bu online only.
Date changed, too: Now 2022-10-31 - 2022-11-04
Thomas (SKAO): Look at this year's SPIE in Montreal, Quebec, in June. This could be also a good venue for a Tango Controls tutorial.

    Review tango-controls/device-servers/DeviceClasses/communication/Socket!1 and shout if you don't want this proposed change.

Not yet.

    Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz

Not yet.

    Test cppTango 9.3.5-rc1 and report back, please

Issue reports starts to trickle in. We now also have a TangoSourceDistribution version, see https://gitlab.com/tango-controls/TangoSourceDistribution/-/releases/9.3.5-rc2.

Anton, Lorenzo, Thomas J and Thomas B:

    Make a proposal to streamline access - this to be sent to steering committee.

Not yet.

Andy (ESRF):

    Update the description of issue cppTango#822 (CORBA transient timeout message).

Bigger issue than thought. Still in progress.

    Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu. Delegate task to one of the new company.

Proposal that Observatory Sciences try on Ubuntu and then document the installation on Ubuntu (TBD)

    Freexian: What about having a tango-controls ppa (personal package archive)?

There is a tango-controls on launchpad: https://launchpad.net/~tango-controls/+archive/ubuntu/core

    Need to ask  Alessio / Emmanuel to get access for at least one kernel member (Andy?)

Anton (Max IV):

    Make a poll about required Python versions and architectures for PyTango binary wheels

    In PyTango, can we disable Tango state switching automatically to ALARM when state is ON and an attribute is in ALARM? Investigate and report to Thomas J.

Was not present.

Benjamin (Max IV):

    Ask Henrik to update the MR cpptango!870 with status, and plans/ideas to take it forward.

Done

Damien (ESRF):

    Prepare a new Pogo release

Not yet.

Elettra:

    Address review comments on TangoDatabase!28

Not yet.

Gwen (SOLEIL):

    Follow up TangoTickets#57 (bintray -> maven central) and communicate with Andy. Faranguiss (ESRF) can review the work done.

Andy sent an email to s2 and gave them the green light to start

    Ask Soleil developers to review tango-controls/device-servers/DeviceClasses/communication/Socket!1

Jean-Luc Pons (ESRF) is code owner so he should merge

Łukasz (S2Innovation):

    Provide quotation to take Java packaging forward (TangoTickets#57)

Done

    Create a doodle/framadate to arrange a meeting to discuss alarms.

Will be done by Piotr.

Reynald (ESRF):

    Clarify whether the DevEnum in pipes are really not supported. Verify that
    this limitation is well documented (It seems, according to the user who
    discussed with Andy) and ask the user to create an issue to ask for the support
    of  DevEnum in pipes if it's a real limitation. RFC does not list DevEnum as
    supported for pipes. See
    https://tango-controls.readthedocs.io/projects/rfc/en/latest/9/DataTypes.html?#pipe

    Is it really not supported?

Not present.

Sergi (ALBA):

    Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).

Not yet.

Thomas B. (byte physics):

    Look at TangoDatabase#26

Not yet.

    2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

Not yet.

    Archive https://github.com/tango-controls/cppTango-docs

Done

Thomas J. (SKAO):

    Decide if joining the ADASS conference will be done now that it's virtual.

    Organize a Tango Workshop at the next ADASS conference.

    Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).

    Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 10th March 2022 at 15:00 CET.

## Summary of remaining actions

All institutes:

    Review Damien's proposal on Pogo: pogo!126, https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz

    Test cppTango 9.3.5-rc1 and report back, please

Anton, Lorenzo, Thomas J and Thomas B:

    Make a proposal to streamline access - this to be sent to steering committee.

Andy (ESRF):

    Update the description of issue cppTango#822 (CORBA transient timeout message).

    Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu. Delegate task to one of the new company.
    Proposal that Observatory Sciences try on Ubuntu and then document the installation on Ubuntu (TBD)

    Ask  Alessio / Emmanuel to get access for at least one kernel member to https://launchpad.net/~tango-controls/+archive/ubuntu/core

Anton (Max IV):

    Make a poll about required Python versions and architectures for PyTango binary wheels

    In PyTango, can we disable Tango state switching automatically to ALARM when state is ON and an attribute is in ALARM? Investigate and report to Thomas J.

Damien (ESRF):

    Prepare a new Pogo release

Elettra:

    Address review comments on TangoDatabase!28

Jean-Luc Pons (ESRF):

    Review and merge tango-controls/device-servers/DeviceClasses/communication/Socket!1

Piotr (S2Innovation):

    Create a doodle/framadate to arrange a meeting to discuss alarms.

Reynald (ESRF):

    Clarify whether the DevEnum in pipes are really not supported. Verify that
    this limitation is well documented (It seems, according to the user who
    discussed with Andy) and ask the user to create an issue to ask for the support
    of  DevEnum in pipes if it's a real limitation. RFC does not list DevEnum as
    supported for pipes. See
    https://tango-controls.readthedocs.io/projects/rfc/en/latest/9/DataTypes.html?#pipe

Sergi (ALBA):

    Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).

Thomas B. (byte physics):

    Look at TangoDatabase#26

    2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango

    Close all tickets and pull requests on archive github repos, see https://gitlab.com/tango-controls/TangoTickets/-/issues/62

Thomas J. (SKAO):

    Decice if joing the ADASS conference will be done now that it's virtual.

    Organize a Tango Workshop at the next ADASS conference.

    Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).

    Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904

Aya (OSL):

    Work on TangoTickets#61.

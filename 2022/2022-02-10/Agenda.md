# Tango Kernel Follow-up Meeting - 2022/02/10

To be held on 2022/02/10 at 15:00 CET on Zoom.

# Agenda

1. RFCs. Please reply to Framadate poll: https://framadate.org/TuWhlhrGJ8NFGcBW 
2. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2022/2022-01-27/Minutes.md#summary-of-remaining-actions)
3. Storing secrets in CI/CD (See comments in https://gitlab.com/tango-controls/TangoTickets/-/issues/57)
4. High priority issues
5. Issues requiring discussion
6. AOB

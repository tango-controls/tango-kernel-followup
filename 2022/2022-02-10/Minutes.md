# Tango Kernel Follow-up Meeting

Held on 2022/02/10 at 15:00 CET on Zoom.

**Participants**:

- Gwenaëlle Abeillé (SOLEIL)
- Benjamin Bertrand (MAX IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics)
- Vincent Hardion (MAX IV)
- Anton Joubert (MAX IV)
- Thomas Juerges (SKAO)
- Igor Khokhriakov (IK)
- Damien Lacoste (ESRF)
- Katleho Madisa (SARAO)
- Yury Matveev (DESY)
- Olga Merkulova (IK)
- Jairo Moldes (ALBA)
- Lorenzo Pivetta (ELETTRA)
- Sergi Rubio (ALBA)
- Łukasz Żytniak (S2Innovation)

## RFC Workshop Organization

Zoom workshop was held on 3rd Feb 2022.  About 11 participants, including new authors.  Thanks!

Questions about differences between Java and cpp implementations came up.

Feedback:  it was nice to work in groups.  At least 2.

Next workshop (in person, in Krakow, and remote):  https://framadate.org/TuWhlhrGJ8NFGcBW  
Travel is still not certain for some institutes, so we will have to wait a little longer before deciding on a date.


## Status of [Actions defined in the previous meetings](../2021-01/27/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- if you're willing to help in any way for the Tango workshop at the next [ADASS conference](https://www.adass.org) which
  will take place in Victoria, Canada from 11-15 September 2022, please contact Thomas Juerges (SKAO) before end of January 2022.  
  Link to the proposal: https://docs.google.com/document/d/1RYrj4tzjF3ZjYpuLZpWDQhMhFHYcaIl6rDdpN63Y1nM/edit?usp=sharing

  > No-one is opposed.  The proposal is good.  Thanks for Thomas Juerges!  Vote of confidence from Kernel developers.

  > Thomas J will take this proposal to the organisers and report back at the next kernel meeting..

- Ask java developers to update their projects and remove any reference to bintray.

  > There is a ticket: https://gitlab.com/tango-controls/TangoTickets/-/issues/57

  > Question - where to store secrets like tokens needed for publishing?

  - **ACTION - Thomas J:** ask SKAO how they are storing/handling secrets that are needed for CI/CD

  The SKAO developer documentation explains how secrets are handled: https://developer.skao.int/en/latest/tools/containers/orchestration-guidelines.html#secrets

  > If storing in gitlab, need to limit this access.  Owners and maintainers can see the secrets (see https://docs.gitlab.com/ee/user/permissions.html#project-features-permissions).  Do we change most owners/maintainers to developers, and then limit it per project.

  >  Suggestion:  require 2FA to access the secrets.

  > Need at least 2, maybe 3 owners.

  - **ACTION**:  Make a proposal to streamline access - this to be sent to executive committee.  Anton, with help from Lorenzo, Thomas J and Thomas B.

  - **ACTION**:  Łukasz to provide quotation to take Java packaging forward.  Gwen will follow up and communicate with Andy.

- Review https://gitlab.com/tango-controls/device-servers/DeviceClasses/communication/Socket/-/merge_requests/1 and shout if you don't want this proposed change.

  > Elettra are not using it

  > SOLEIL are.

  - **ACTION**: Gwen to check with developers at SOLEIL.

- Answer to the RFC framadate poll before Monday 31st January: https://framadate.org/4AZ1zUzjO5IdLcQI
- Upvote/comment on the launchpad issue [https://bugs.launchpad.net/ubuntu/+source/tango/+bug/1932290], which may prompt the Ubuntu maintainers to do the work.
  > Thomas B has talked to Freexian.  OmniORB needs to be updated before cppTango (https://bugs.launchpad.net/ubuntu/+source/tango/+bug/1932290) can.  
  > Thomas B made a Launchpad issue (https://bugs.launchpad.net/ubuntu/+source/omniorb-dfsg/+bug/1959511), which is already in progress.  Python 3 migration is holding things up.

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Freexian: What about having a tango-controls ppa (personal package archive)?

**Benjamin (Max IV)**:
- Ask Henrik to update the MR [cpptango!870](https://gitlab.com/tango-controls/cppTango/-/merge_requests/870) with status, and plans/ideas to take it forward.

**Damien (ESRF)**:
- Prepare a new Pogo release
- Improve https://gitlab.com/tango-controls/pogo/-/merge_requests/126. 
> Done - Waiting for your review  
> New version available for testing:
  https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz

**Elettra**:
- Address review comments on [TangoDatabase!28](https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/28)
  > Alessio (Elettra) still needs to look at it.

**Reynald (ESRF)**:
- Clarify whether the DevEnum in pipes are really not supported. Verify that this limitation is well documented
  (It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of  DevEnum in pipes if it's a real limitation.
  RFC does not list DevEnum as supported for pipes. See https://tango-controls.readthedocs.io/projects/rfc/en/latest/9/DataTypes.html?#pipe  
  Is it really not supported?
- Clarify with Andy how to request officially work from Krystian Kedron on the Maven migration. 
  > Done
- Grant Thomas B. owner status in https://github.com/tango-controls/cppTango-docs for the migration
  > Admin role given on Friday 11th February

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
  > No news

**Thomas B. (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26
  > Not yet
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
  > Not yet
- Archive https://github.com/tango-controls/cppTango-docs

**Thomas J. (SKAO)**:
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).
  > Not yet.

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124
  > It has been merged:  https://gitlab.com/tango-controls/rfc/-/merge_requests/124

## High Priority Issues

No new High Priority issue mentioned during the meeting.

## Issues requiring discussion

PyTango - omniorb-libs conda for build doesn't include .h files.  Should it?

PyTango binary wheels.  What platforms are required/nice to have?  Python versions, and architectures.

    No feedback.  Make a poll.


Can Tango be updated to support alarms quality for binary attributes?  E.g., set state to alarm if value is True.  Maybe we can use "min alarm" for the threshold.  Such a change could affect tools - need to check.

    https://gitlab.com/tango-controls/TangoTickets/-/issues/60 

Should we do it for DevEnum as well?  For enums, the range is a set, rather than a numeric.  So are booleans more like enums (sets) or more like integers (ranges?).


## AOB

- cppTango 9.3.5-rc1 is out!  Test it, and report back, please.   https://gitlab.com/tango-controls/cppTango/-/tags/9.3.5-rc1

- Learning from others: Archiving of Tango Controls attributes, how is it done elsewhere?

  Web page with recordings: https://confluence.skatelescope.org/x/mY8NCg

  -> Demo on Friday, 2022-02-11 13.00 - 14.30 UTC by Lorenzo. Zoom link: https://skatelescope.zoom.us/j/2423523291?pwd=Z1RmRTNOUlBidlVvYWhINTNpQnBqUT09

  -> Demo on Tuesday, 2022-02-15, 13.00 - 14.30 UTC by Sergi. Zoom link: https://skatelescope.zoom.us/j/99820008214?pwd=cFZjSFVybG1iZGZ3ckpyQ1V3UXdHdz09

  - Ideas for future show cases:  alarms, logging.


- Olga M is working on the Tango website.  Regular meetings with Andy.  Many things to change, including github -> gitlab.  We can mention things here:   https://gitlab.com/tango-controls/TangoTickets/-/issues/59

- Thomas J:  can we have an FAQ somewhere?  Getting answers from documentation, forums, can be time consuming.

> **Action**:  Thomas J can start tango-controls faq on Gitlab.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904


- Thomas J:  Device goes to alarm state when current state is ON and at least one attribute goes into alarm range (or is having ALARM quality factor).
- Is there a way to disable that at the device level?

  - Yes, if you override dev_state, and don't call DeviceImpl::dev_state

  - Can we do it in PyTango devices?  Maybe... **ACTION**: Anton to look, and report back to Thomas J.

- Can we have a way to subscribe to new types of events:  warning_event, alarm_event?  Probably sent on entry/exit to the subscribers.

  **ACTION**:  Łukasz will create a doodle/framadate to arrange a meeting to discuss alarms.


### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 24th February 2022 at 15:00 CET.
(Hosted by Thomas Braun)

## Summary of remaining actions

**All institutes**:
- if you're willing to help in any way for the Tango workshop at the next [ADASS conference](https://www.adass.org) which
  will take place in Victoria, Canada from 11-15 September 2022, please contact Thomas Juerges (SKAO)
- Review https://gitlab.com/tango-controls/device-servers/DeviceClasses/communication/Socket/-/merge_requests/1 and shout if you don't want this proposed change.
- Review Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126, 
https://gitlab.com/tango-controls/pogo/uploads/95841c532ef55c87245d07138707aa63/TangoTest.tar.gz
- Test [cppTango 9.3.5-rc1](https://gitlab.com/tango-controls/cppTango/-/tags/9.3.5-rc1) and report back, please

**Anton, Lorenzo, Thomas J and Thomas B**:
- Make a proposal to streamline access - this to be sent to steering committee.

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Freexian: What about having a tango-controls ppa (personal package archive)?

**Anton (Max IV)**:
- Make a poll about required Python versions and architectures for PyTango binary wheels
- In PyTango, can we disable Tango state switching automatically to ALARM when state is ON and an attribute is in ALARM?
Investigate and report to Thomas J.

**Benjamin (Max IV)**:
- Ask Henrik to update the MR [cpptango!870](https://gitlab.com/tango-controls/cppTango/-/merge_requests/870) with status, and plans/ideas to take it forward.

**Damien (ESRF)**:
- Prepare a new Pogo release

**Elettra**:
- Address review comments on [TangoDatabase!28](https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/28)

**Gwen (SOLEIL)**:
- Follow up https://gitlab.com/tango-controls/TangoTickets/-/issues/57 and communicate with Andy. 
Faranguiss (ESRF) can review the work done.
- Ask Soleil developers to review https://gitlab.com/tango-controls/device-servers/DeviceClasses/communication/Socket/-/merge_requests/1

**Łukasz (S2Innovation)**:
- Provide quotation to take Java packaging forward (https://gitlab.com/tango-controls/TangoTickets/-/issues/57)
- Create a doodle/framadate to arrange a meeting to discuss alarms.

**Reynald (ESRF)**:
- Clarify whether the DevEnum in pipes are really not supported. Verify that this limitation is well documented
  (It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of  DevEnum in pipes if it's a real limitation.
  RFC does not list DevEnum as supported for pipes. See https://tango-controls.readthedocs.io/projects/rfc/en/latest/9/DataTypes.html?#pipe  
  Is it really not supported?

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).

**Thomas B. (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26
- 2D arrays in commands: Prepare an estimate of the man hours to implement this in cppTango
- Archive https://github.com/tango-controls/cppTango-docs

**Thomas J. (SKAO)**:
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...).
- Start tango-controls faq on gitlab.com/tango-controls.  See also the website:  https://www.tango-controls.org/about-us/#faq_9904




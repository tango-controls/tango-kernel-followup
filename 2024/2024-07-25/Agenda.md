# Tango Kernel Follow-up Meeting - 2024/07/25

To be held on 2024/07/25 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

# Agenda

## cppTango and pyTango 10.0.0 release candidates
## Status of [Actions defined in the previous meetings](../2024-07-11/Minutes.md#summary-of-remaining-actions)
## High priority issues
## Issues requiring discussion
## AOB

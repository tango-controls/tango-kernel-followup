# Tango Kernel Follow-up Meeting
Held on 2024/07/25 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Michal Gandor (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics e.K.)
- Thomas Ives (OSL)
- Damien Lacoste (ESRF)
- Yury Matveev (DESY)
- Sergi Rubio (ALBA)

## cppTango and pyTango 10.0.0 release candidates

cppTango 10.0.0-rc4, pytango 10.0.0-rc1 and Tango Source Distribution 10.0.0-rc2 have been released.

conda packages are available for pytango and cpptango.

You need to pass both pytango and cpptango rc channels to install pytango 10.0.0rc1:  
`conda create -n pytango -c conda-forge/label/pytango_rc -c conda-forge/label/cpptango_rc pytango=10.0.0rc1`

PyTango rc2 coming soon.  It will be required to test cppTango rc4.

Want to include https://gitlab.com/tango-controls/cppTango/-/merge_requests/1306, instead of waiting for 1 year.

Action - All:
- [ ] Please test cppTango 10.0.0-rc4, pytango 10.0.0-rc1 and Tango Source Distribution rc2

## Status of [Actions defined in the previous meetings](../2024-05-27/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.
- [ ] Please test cppTango 10.0.0-rc3, pytango 10.0.0-rc1 and Tango Source Distribution rc2
  Tested so far:
  - ESRF: running cppTango 10.0.0-rc3 on a simulator. Not intensively used yet, not using telemetry (but compiled support in), plan to install rc4 asap.
    An issue was found with the Blackbox => https://gitlab.com/tango-controls/Astor/-/issues/28
  -
**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024
  => To be improved. Add a tickbox in the registration form "Needs funding help from Tango Community"
  => https://indico.tango-controls.org/e/tango-doc-workshop-2024

  no progress

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance (sybscribe_event_asynch) as draft.  (need to update proposal to match latest cppTango)

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

no progress

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
  See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread_ts=1716472950.766739&cid=CQV9F2BTJ
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9 : issue deploying java software on Maven Central.  
Who created MAVEN_CENTRAL_USERNAME and MAVEN_CENTRAL_PASSWORD used in gitlab-ci-templates settings.xml?  
It seems like it does no longer work. We might need to create a new token to deploy on oss.sonatype.org.  
There might be some issues with oss.sonatype.org server itself because they announced recently some changes in their OSSRH authentication backend.

## High priority issues

TangoDatabase, problem using 5.23.  And 5.22, which started using InnoDB.  
https://gitlab.com/tango-controls/TangoDatabase/-/issues/76.  This is lower priority than cppTango and TangoSourceDistribution for v10.0.0.

Also, 5.23 pipeline failed to create tarball with source and submodules.  https://gitlab.com/tango-controls/TangoDatabase/-/jobs/7128885115 .  Couldn't make a conda package.  TB re-triggered job, and now archive is available.

## AOB

MAX IV found PyTango 9.5.0 caused rapid memory leak pushing events.  
There are two bugs here, one in PyTango (since v9.4.2) and one in cppTango (for longer, but only for writeable, scalar attributes if not subscribers).  
These have either been fixed in 10.0.0, or have pending MRs for fixes.

### Documentation workshop

https://indico.tango-controls.org/e/tango-doc-workshop-2024

Registrations will be open as soon as we get more information about the payment details and when the location will be fully booked.    
Date is now 28/10 to 1/11.  
General preference from this Kernel Follow-up Meeting participants for Escandille (Autrans) location over ZeCamp (Correncon).

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 8th August 2024 at 15:00 CEST.

## Summary of remaining actions

**All**:
- [ ] Please test cppTango 10.0.0-rc4, pytango 10.0.0-rc1 and Tango Source Distribution rc2
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.

- **Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024
  => To be improved. Add a tickbox in the registration form "Needs funding help from Tango Community"
  => https://indico.tango-controls.org/e/tango-doc-workshop-2024

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance (sybscribe_event_asynch) as draft.  (need to update proposal to match latest cppTango)
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
  See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread_ts=1716472950.766739&cid=CQV9F2BTJ
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner
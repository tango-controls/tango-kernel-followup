# Tango Kernel Follow-up Meeting
Held on 2024/09/26 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Johan Forsberg (MAX IV)
- Michal Gandor (S2Innovation)
- Thomas Ives (OSL & SKAO)
- Sergi Rubio (ALBA)
- Thomas Braun (byte physics e.K.)
- Becky Auger-Williams (OSL)

## cppTango and pyTango 10.0.0 release candidates

cppTango rc6 is available  
pytango rc4 has been tested.    
Tango Source Distribution rc6 is available.  

cppTango 10.0.0 has been tagged today!  Watching pipeline...  Will announce in the next day or two.
PyTango will follow shortly after - there haven't been any issues.

## Status of [Actions defined in the previous meetings](../2024-09-12/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
  e-mail has been sent by T-J, tango-cs dockerhub organization can be killed. Don't remove name, if someone else could use it in the future.
  All images should be deleted and a disclaimer should be available to notify the potential users
>> Some images are still pulled. Some images seem to be still used in some projects? See https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs 
- [X] Check whether it could be possible to pay the Tango Documentation Workshop fees with Credit Card?
> Payment with CB will be possible

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance (subscribe_event_asynch) as draft.  (need to update proposal to match latest cppTango)
  A new issue will be created by ALBA to propose new options during the event subscription
  It is being discussed in Taurus forum, since affects this project.  Sergio may update an old ticket, or make a new ticket.  Considering specifying asynchronous read/no read/sync read.
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.
  Alba is considering subcontracting this task

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

no progress...

**Thomas J. (SKAO)**:
- [ ] Update https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html and replace the URLs referring to artefact.skao.int/.../ with harbor.skao.int/.../.
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner
  The RPI5 stopped working reliably with NFS mounted volumes. T-J has replaced it and changed the NFS mounts. Now it works again. Will get in touch with Benjamin again to get help to add it to our runners.

**Thomas Ives (SKAO & OSL)**
- [ ] Update the TC doc about SKAO being now the maintainer of the images and that the Docker Hub presence has been/will be retired.
  Documentation should be updated to update the location of the images    
  https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html should be updated to replace artefact.skao.int with harbor.skao.int

**Vincent (MAX IV) & Yury (DESY)**
- [ ] Steering committee: Ask Igor to retire the TC presence on Docker Hub. Don't remove name, if someone else could use it in the future.
- [ ] Create an Epic and create 2 issues inside the epic. https://gitlab.com/groups/tango-controls/-/epics/6

**Michal (S2Innovation)**:
- [ ] https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9 : issue deploying java software on Maven Central.  Follow up with Patrick Madela.


## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

## High priority issues

## AOB

###  pytango-databaseds.

ALBA did some work in early Tango 9, but project is "dead".  Johan include this work, and has now improved it a lot, and it is working much better.  This is using the sqlite backend.  Useful on a dev system, without any mysql or mariadb.  The fixes will be included in PyTango 10.0.0.  (already in release candidate 4).

Should we backport fixes to PyTango 9.x?  Not critical.

Testing?  Only a reference design, C++ version.  Johan is working on a better test suite.  Ideally, want to run it against both C++ and Python versions.
This test suite could be well placed in the cross platfrom / cross version integration tests:  https://gitlab.com/tango-controls/TangoTickets/-/issues/108.

If we want to publish it as a separate PyPI package, https://pypi.org/project/pytango-db/ was used before, but need to contact Tiago Coutinho since he is the only maintainer on PyPI.

**Action:  Sergio** contact Tiago, and try to get him in touch with Johan.
https://github.com/ALBA-Synchrotron/pytango-db

### Tango-doc

Tango docs repository MR merged, but readthedocs not updated.  Last update was 8 months ago.
https://tango-controls.readthedocs.io/en/latest/
```
Web hook from gitlab repo may have an issue: 
    Hook executed successfully but returned HTTP 400 {"detail":"This webhook doesn't have a secret configured.\nFor security reasons, webhooks without a secret are no longer permitted.\nFor more information, read our blog post: https://blog.readthedocs.com/security-update-on-incoming-webhooks/."}
```
**Action: Michal Gandor** to have a look at this issue.

Also not being published successfully on https://tango-controls.gitlab.io/tango-doc/

**Action:  Benjamin** disable this publishing, as we don't want two locations.  However, do want a way to view new doc when merge requests are open (see artefacts).

### Documentation workshop

https://indico.tango-controls.org/e/tango-doc-workshop-2024

Registrations are open. Payment details will come soon. Date is from 28/10 to 1/11.
Payment will be possible in about two or three weeks after the NoBugs conference.  
Payment will be possible with credit card.

A Tango Documentation Workshop Preparation Meeting will take place on Wednesday 16th October at 14:00 CEST.  
Zoom link available here: https://indico.tango-controls.org/event/316/

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 10th October 2024 at 15:00 CEST.

## Summary of remaining actions

**All**:
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
  e-mail has been sent by T-J, tango-cs dockerhub organization can be killed. Don't remove name, if someone else could use it in the future.
  All images should be deleted and a disclaimer should be available to notify the potential users
>> Some images are still pulled. Some images seem to be still used in some projects? See https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs 

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance (subscribe_event_asynch) as draft.  (need to update proposal to match latest cppTango)
  A new issue will be created by ALBA to propose new options during the event subscription
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.
  Alba is considering subcontracting this task
- [ ] Contact Tiago Coutinho to try to get a new maintainer (Johan Forsberg) added to https://pypi.org/project/pytango-db/

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

**Thomas J. (SKAO)**:
- [ ] Update https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html and replace the URLs referring to artefact.skao.int/.../ with harbor.skao.int/.../.
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner
  The RPI5 stopped working reliably with NFS mounted volumes. T-J has replaced it and changed the NFS mounts. Now it works again. Will get in touch with Benjamin again to get help to add it to our runners.

**Thomas Ives (SKAO & OSL)**
- [ ] Update the TC doc about SKAO being now the maintainer of the images and that the Docker Hub presence has been/will be retired.
  Documentation should be updated to update the location of the images    
  https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html should be updated to replace artefact.skao.int with harbor.skao.int

**Vincent (MAX IV) & Yury (DESY)**
- [ ] Steering committee: Ask Igor to retire the TC presence on Docker Hub. Don't remove name, if someone else could use it in the future.
- [ ] Create an Epic and create 2 issues inside the epic. https://gitlab.com/groups/tango-controls/-/epics/6

**Michal (S2Innovation)**
- [ ] https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9 : issue deploying java software on Maven Central.  Follow up with Patrick Madela.
- [ ] Investigate read https://tango-controls.readthedocs.io not being updated due to webhook configuration issue.

**Benjamin (MAX IV)**
- [ ] Disable publishing of https://gitlab.com/tango-controls/tango-doc to https://tango-controls.gitlab.io/tango-doc/.  Only want readthedocs.

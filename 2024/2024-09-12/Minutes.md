# Tango Kernel Follow-up Meeting
Held on 2024/09/12 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Michal Gandor (S2Innovation)
- Thomas Juerges (SKAO)
- Thomas Ives (OSL & SKAO)
- Sergi Rubio (ALBA)
- Vincent Hardion (MAX IV)
- Thomas Braun (byte physics e.K.)
- Reynald Bourtembourg (ESRF)

## cppTango and pyTango 10.0.0 release candidates

cppTango rc5 is available and rc6 will be released after 2 MRs merged.  
pytango rc3 has been tested. SARDANA, Bliss, Lofar and SKAO tests passed.  
Tango Source Distribution rc5 is available since last week.  
Lofar saw a crash, but it is known: https://gitlab.com/tango-controls/cppTango/-/issues/1202  
cppTango release is now planned for Friday 20th September.

## Status of [Actions defined in the previous meetings](../2024-08-08/Minutes.md#summary-of-remaining-actions)

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9 : issue deploying java software on Maven Central.  
Who created MAVEN_CENTRAL_USERNAME and MAVEN_CENTRAL_PASSWORD used in gitlab-ci-templates settings.xml?  
It seems like it does no longer work. We might need to create a new token to deploy on oss.sonatype.org.  
Michal got in touch with Reynald. He suspects that the issue lied in the Gitlab variables.
Action: Michal will follow up with Patrick Madela.
Followed up with Gwen. Outcome: Patrick has to make the modifications.
=> Action - Micħal: Will follow up with Patrick.

## High priority issues

**All**:
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.  
> Action - Vincent: create an Epic and create 2 issues inside the epic. https://gitlab.com/groups/tango-controls/-/epics/6

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.
>  No news

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.
>  No news

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.
>  No news

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))
>  No news

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
> e-mail has been sent by T-J, tango-cs dockerhub organization can be killed.  
> Documentation should be updated to update the location of the images  
> https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html should be updated to replace artefact.skao.int with harbor.skao.int  
> All images should be deleted and a disclaimer should be available to notify the potential users

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance (subscribe_event_asynch) as draft.  (need to update proposal to match latest cppTango)
> A new issue will be created by ALBA to propose new options during the event subscription
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.
> Alba is considering subcontracting this task

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

> no update

**Thomas J. (SKAO)**:
- [ ] Update https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html and replace the URLs referring to artefact.skao.int/.../ with harbor.skao.int/.../.
- [x] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
>  See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread_ts=1716472950.766739&cid=CQV9F2BTJ
>  Steering committee: Email is OK, SKAO commits to providing Tango images and to maintain them
>  Email sent to the info mailing list on 2024-08-24
- [x] TJ send the email
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner
  The RPI5 stopped working reliably with NFS mounted volumes. I have replaced it and changed the NFS mounts. Now it works again. Will get in touch with Benjamin again to get help to add it to our runners.

**Thomas Ives (SKAO & OSL)**
- [ ] Update the TC doc about SKAO being now the maintainer of the images and that the Docker Hub presence has been/will be retired.
>  No news

**Vincent (MAX IV) & Yury (DESY)**
- [ ] Steering committee: Ask Igor to retire the TC presence on Docker Hub.
> No news, waiting for Yury to talk to Igor.. Reynald will try and retire it if he has maintenance permissions.  
> Don't remove name, if someone else could use it in the future.

**Michal (S2Innovation)**:
- [ ] https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9 : issue deploying java software on Maven Central.  Follow up with Patrick Madela.

## AOB
We have more reports coming in about the 90 days history limitation on Slack. When will Mattermost replace Slack?  
There will be a meeting with Webu to get a quote for hosting Mattermost on our servers.   
Desy also mentioned that they are using it already and could create a node for us... A problem might be a later migration to our own servers.
Discourse forum software: Steering committee will ask Webu for a quote.

### Documentation workshop

https://indico.tango-controls.org/e/tango-doc-workshop-2024

Registrations are open. Payment details will come soon. Date is from 28/10 to 1/11.
Payment will be possible in about two or three weeks after the NoBugs conference.  
Payment possible with credit card/Paypal? (Bank transfers from outside Europe are not that easy.)  Reynald will check.

**Action - Reynald**: Check whether it could be possible to pay the Tango Documentation Workshop fees with Credit Card?

A Tango Documentation Workshop Preparation Meeting will take place on Wednesday 18th September at 14:00 CEST.    
Please have a look at #temp-sig-documentation-workshop messages to get the Zoom link to this meeting.

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 26th September 2024 at 15:00 CEST.

## Summary of remaining actions

**All**:
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
  e-mail has been sent by T-J, tango-cs dockerhub organization can be killed. Don't remove name, if someone else could use it in the future.
  All images should be deleted and a disclaimer should be available to notify the potential users
- [ ] Check whether it could be possible to pay the Tango Documentation Workshop fees with Credit Card?

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance (subscribe_event_asynch) as draft.  (need to update proposal to match latest cppTango)
A new issue will be created by ALBA to propose new options during the event subscription
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.
Alba is considering subcontracting this task

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

**Thomas J. (SKAO)**:
- [ ] Update https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html and replace the URLs referring to artefact.skao.int/.../ with harbor.skao.int/.../.
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner
  The RPI5 stopped working reliably with NFS mounted volumes. T-J has replaced it and changed the NFS mounts. Now it works again. Will get in touch with Benjamin again to get help to add it to our runners.

**Thomas Ives (SKAO & OSL)**
- [ ] Update the TC doc about SKAO being now the maintainer of the images and that the Docker Hub presence has been/will be retired.
  Documentation should be updated to update the location of the images    
  https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html should be updated to replace artefact.skao.int with harbor.skao.int


**Vincent (MAX IV) & Yury (DESY)**
- [ ] Steering committee: Ask Igor to retire the TC presence on Docker Hub. Don't remove name, if someone else could use it in the future.
- [ ] Create an Epic and create 2 issues inside the epic. https://gitlab.com/groups/tango-controls/-/epics/6

**Michal (S2Innovation)**:
- [ ] https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9 : issue deploying java software on Maven Central.  Follow up with Patrick Madela.

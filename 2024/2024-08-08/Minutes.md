# Tango Kernel Follow-up Meeting
Held on 2024/08/08 at 15:00 CEST on Zoom.
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en
*Meeting closed 15.22h CEST.*

**Participants**:
~~- Anton Joubert (MAX IV)~~
- Benjamin Bertrand (MAX IV)
- Michal Gandor (S2Innovation)
~~- Reynald Bourtembourg (ESRF)~~
~~- Thomas Braun (byte physics e.K.)~~
- Thomas Ives (SKAO \& OSL)
- Thomas Juerges (SKAO)
~~- Damien Lacoste (ESRF)~~
- Yury Mateev (DESY)
~~- Sergi Rubio (ALBA)~~
- Vincent Hardion (MAX IV)

## cppTango and pyTango 10.0.0 release candidates

Update on PyTango rc2? coming soon.

- cppTango 10.0.0-rc4 tested at SKAO: no issues found
- PyTango 10.0.0-rc2 based on cppTango 10.0.0-rc4 released 2024/07/26 (tested on Sardana, BLISS and Lofar 2.0)
- Anton found an issues related to the telemetry code: https://gitlab.com/tango-controls/cppTango/-/issues/1312
TB has an MR open for it.

- Update on: Want to include https://gitlab.com/tango-controls/cppTango/-/merge\_requests/1306 , instead of waiting for 1 year.
No progress

Action - All:
- [ ] Please test cppTango 10.0.0-rc4, pytango 10.0.0-rc1 and Tango Source Distribution rc2
Nobody has tested TSD RC2 yet.

## Status of [Actions defined in the previous meetings](../2024-07-25/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108 .  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.
- [ ] Please test cppTango 10.0.0-rc4, pytango 10.0.0-rc2 and Tango Source Distribution rc4
  Tested so far:
  - ESRF: running cppTango 10.0.0-rc3 on a simulator. Not intensively used yet, not using telemetry (but compiled support in), plan to install rc4 asap.
    An issue was found with the Blackbox => https://gitlab.com/tango-controls/Astor/-/issues/28
    Reynald started conversation with Jean-Luc.

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024
  => To be improved. Add a tickbox in the registration form "Needs funding help from Tango Community"
  => https://indico.tango-controls.org/e/tango-doc-workshop-2024
 Apologies, Reynald is on leave

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance (subscribe\_event\_asynch) as draft.  (need to update proposal to match latest cppTango)
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property\_device\_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note\_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.
Apologies, TB is travelling

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
  See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread\_ts=1716472950.766739\&cid=CQV9F2BTJ
  Steering committee: Email is OK, SKAO commits to providing Tango images and to maintain them
- [ ] TJ send the email
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner
  Received info from TB. Runner is installed but need to configure it properly before it can be added.

**Thomas Ives (SKAO \& OSL)**
- [ ] Update the TC doc about SKAO being now the maintainer of the images and that the Docker Hub presence has been/will be retired.

**Vincent (MAX IV) \& Yury (DESY)**
- [ ] Steering committee: Ask Igor to retire the TC presence on Docker Hub.

** Michal Gandor (S2Innovation)**
- [ ] Follow up Maven Central issue with Benjamin

** Benjamin Bertrand (MAX IV)**
- [ ] Follow up Maven Central issue with Michal

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened\&label\_name%5B%5D=Discussion%20needed

[https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9](https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9) : issue deplying java software on Maven Central.
Who created MAVEN\_CENTRAL\_USERNAME and MAVEN\_CENTRAL\_PASSWORD used in gitlab-ci-templates settings.xml?
  It seems like it does not longer work. We might need to create a new token to deploy on oss.sonatype.org.
  Michal got in touch with Reynald. Michal suspects that the issue lies in the Gitlab variables. Michal and Benjamin will follow up. Actions see above.

## High priority issues

## AOB

### Documentation workshop

https://indico.tango-controls.org/e/tango-doc-workshop-2024

Registrations will be open as soon as we get more information about the payment details and the location is fully booked.  Date is now 28/10 to 1/11.
- [] Reynald: Please update the date on Indico.

### Tango 10 SIG meeting

Thursday 2024-08-15, 15.00h CEST
- [] Vincent: Please send an the invitation.

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/
The next Tango Kernel Follow-up Meeting will take place on Thursday 22nd August 2024 at 15:00 CEST.

## Summary of remaining actions

# Tango Kernel Follow-up Meeting
Held on 2024/11/14 at 15:00 CET on Zoom.
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Benjamin Bertrand (MAX IV)
- Vincent Hardion (MAX IV)
- Anton Joubert (MAX IV)
- Thomas Ives (OSL & SKAO)
- Sergi Rubio (ALBA)
- Thomas Braun (byte physics e.K.)
- Becky Auger-Williams (OSL)
- Reynald Bourtembourg (ESRF)
- Thomas Juerges (SKAO)

## Status of [Actions defined in the previous meetings](../2024-10-10/Minutes.md#summary-of-remaining-actions)

Andy (ESRF):
 - [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango
       controls) Andy will check with our Tango-Controls web host if they can provide what we need, how files
       can be uploaded, etc.

-> Not present.

Damien (ESRF):
 - [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
     recommended, Windows not supported, or whatever the case.

-> Not present.

Gwenaëlle (SOLEIL):
 - [ ] Try to reproduce the observed event issues
 - [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
        version, and you can no longer configure a default log level from code. It is a bad practice to put a
        logging conf file because it risks overloading that of the project which has another one.

-> Not present.

Lorenzo (Elettra):
 - [ ] Gitlab Runners: Follow up with Elettra IT Team.
 - [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

-> Not present.

Reynald (ESRF):
 - [ ] JTango: Events are not properly reported, investigate issue and create ticket Action to be closed.  Nothing to fix.
 - [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
 - [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
        Steering committee has approved the process but there has to be an overlap period where images are available
        both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
        organization can be killed. Don't remove name, if someone else could use it in the future. All images should
        be deleted and a disclaimer should be available to notify the potential users
        Some images are still pulled. Some images seem to be still used in some projects? See
        https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
        https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
        gitlab-ci-templates!3 (comment 942846713)

-> Not present.

Sergio (ALBA):

 - [ ] Work on cppTango#814, dynamic device attributes. Alba is considering subcontracting this task
 - [ ] Will revise fandango section in new tango-doc

-> No update

Thomas B. (byte physics):

 - [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)

    -> https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/95 is ready to test

 - [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
        "Organization" and make it public. We will start removing members without organization in 8 weeks.
 - [ ] TangoTickets#103 (comment 1723745893)

 - [ ] We noticed that there is a problem having two different omniORB versions installed on the same
     system. TB will create a bug report for Duncan.

Thomas J. (SKAO):
 - [ ] TJ to create an issue, an MR and update the doc again with correct addresses for docker images
 - [ ] TJ to make a list with all the java tools that would need to have a release page/or be built by the Tango Source Distribution.

-> No progress

Vincent (MAX IV) & Yury (DESY)

- [ ] set up a v10 sync and telemetry RFC meeting

 -> Not done yet.

Discussion needed issues

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

High priority issues

AOB

Documentation workshop
Now have a weekly session to work on this.   13:00-15:00 CET, Thursdays.

Fandango documentation: https://gitlab.com/tango-controls/tango-doc/-/issues/414
Move most of this to Fandango's own documentation.  Move Fandango docs to readthedocs, and then make it a subproject of the tango-docs project.

Software with unclear maintenance status (https://gitlab.com/tango-controls/tango-doc/-/issues/415):

Question to the Executive Committee?
* Tango documentation: Ecosystem software, should we ask every institute to update the new doc to link their software
* Status of Mattermost

PyTango logo
Anton wants a new one.  Should there be some process, or can Anton just change it?
Could involve the community, but project maintainers can decide.  Each project can do their own thing.
Vincent protested.
We acknowledged his protest and moved on.

PyTango DB
pytango-db latest version (0.3.0) is on PyPI: https://pypi.org/project/pytango-db/
and conda-forge
Sergi gave Johan and Benjamin access to the PyPI project. It is ready for a test ride.

Tests are run against the real Databaseds and PyDatabaseds: https://gitlab.com/tango-controls/incubator/pytango-db/-/blob/main/tests/specs/db_device_commands.json?ref_type=heads

Slack/MatterMost migration
Process is waiting for Andy to take it forward.

Next Kernel Follow-up Meeting
https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 28th November 2024 at 15:00 CET..

## Summary of remaining actions

Andy (ESRF):
 - [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango
       controls) Andy will check with our Tango-Controls web host if they can provide what we need, how files
       can be uploaded, etc.
 - [ ] Move from slack to mattermost (feel free to delegate)

Damien (ESRF):
 - [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
     recommended, Windows not supported, or whatever the case.

Gwenaëlle (SOLEIL):
 - [ ] Try to reproduce the observed event issues
 - [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
        version, and you can no longer configure a default log level from code. It is a bad practice to put a
        logging conf file because it risks overloading that of the project which has another one.

Lorenzo (Elettra):
 - [ ] Gitlab Runners: Follow up with Elettra IT Team.
 - [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

Reynald (ESRF):
 - [ ] JTango: Events are not properly reported, investigate issue and create ticket Action to be closed.  Nothing to fix.
 - [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
 - [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
        Steering committee has approved the process but there has to be an overlap period where images are available
        both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
        organization can be killed. Don't remove name, if someone else could use it in the future. All images should
        be deleted and a disclaimer should be available to notify the potential users
        Some images are still pulled. Some images seem to be still used in some projects? See
        https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
        https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
        gitlab-ci-templates!3 (comment 942846713)

Sergio (ALBA):
 - [ ] Work on cppTango#814, dynamic device attributes. Alba is considering subcontracting this task
 - [ ] Will revise fandango section in new tango-doc

Thomas B. (byte physics):
 - [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
        "Organization" and make it public. We will start removing members without organization in 8 weeks.
 - [ ] TangoTickets#103 (comment 1723745893)

 - [ ] We noticed that there is a problem having two different omniORB versions installed on the same
     system. TB will create a bug report for Duncan.

Thomas J. (SKAO):
 - [ ] TJ to create an issue, an MR and update the doc again with correct addresses for docker images
 - [ ] TJ to make a list with all the java tools that would need to have a release page/or be built by the Tango Source Distribution.

Vincent (MAX IV) & Yury (DESY):
 - [ ] set up a v10 sync and telemetry RFC meeting
 - [ ] Replies from SC for the questions from 11/14/2024:

  > Question to the Executive Committee?
  > * Tango documentation: Ecosystem software, should we ask every institute to update the new doc to link their software
  > * Status of Mattermost

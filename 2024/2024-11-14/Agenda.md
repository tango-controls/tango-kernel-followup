# Tango Kernel Follow-up Meeting

Held on 2024/11/14 at 15:00 CET on Zoom.

Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
<!-- - Benjamin Bertrand (MAX IV) -->
<!-- - Vincent Hardion (MAX IV) -->
<!-- - Thomas Braun (byte physics e.K.) -->
<!-- - Becky Auger-Williams (OSL) -->
<!-- - Reynald Bourtembourg (ESRF) -->
<!-- - Thomas Juerges (SKAO) -->
<!-- - Zbigniew Reszlea (ALBA) -->

## Status of [Actions defined in the previous meetings](../2024-10-24/Minutes.md#summary-of-remaining-actions)

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)
      Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
      recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the
      logging configuration API in its latest version, and you can no longer configure a default log level from
      code. It is a bad practice to put a logging conf file because it risks overloading that of the project which
      has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
  Action to be closed.  Nothing to fix.
- [ ] Follow up on ESRF Gitlab Windows runner
  Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available
  both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
  organization can be killed. Don't remove name, if someone else could use it in the future.
  All images should be deleted and a disclaimer should be available to notify the potential users
>> Some images are still pulled. Some images seem to be still used in some projects? See https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs
E.g., https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55
Check https://gitlab.com/tango-controls/gitlab-ci-templates/-/merge_requests/3#note_942846713

**Sergio (ALBA)**:
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.
  Alba is considering subcontracting this task

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
      "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system.
      TB will create a bug report for Duncan.

**Thomas J. (SKAO)**:
- [ ] TJ to create an issue, an MR and update the doc again with correct addresses for docker images
- [ ] TJ to make a list with all the tools that would need to have a release page/or be built by the Tango Source Distribution.

**Vincent (MAX IV) & Yury (DESY)**
- [ ] set up a v10 sync and telemetry RFC meeting

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

## High priority issues

## AOB

### pytango-db project

### Documentation workshop

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 14th November 2024 at 15:00 **CET**.

## Summary of remaining actions

# Tango Kernel Follow-up Meeting - 2024/10/10

To be held on 2024/10/10 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

# Agenda

## cppTango/pyTango/Tango Source Distribution 10.0.0 releases
## Status of [Actions defined in the previous meetings](../2024-09-26/Minutes.md#summary-of-remaining-actions)
## High priority issues
## Issues requiring discussion
## AOB
### cppTango 9.3.7-rc1
### Documentation workshop

# Tango Kernel Follow-up Meeting
Held on 2024/10/10 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Vincent Hardion (MAX IV)
- Michal Gandor (S2Innovation)
- Thomas Ives (OSL & SKAO)
- Sergi Rubio (ALBA)
- Thomas Braun (byte physics e.K.)
- Becky Auger-Williams (OSL)
- Reynald Bourtembourg (ESRF)
- Thomas Juerges (SKAO)
- Yury Matveev (DESY)

## cppTango/pyTango/Tango Source Distribution 10.0.0 releases

cppTango 10.0.0 has been released on 26th September 2024.  
Tango Source Distribution 10.0.0 has been released as well on 26th September 2024.
pytango v10.0.0 has been released on 1st October 2024.

Many thanks to all the contributors.

Vincent will set up a v10 sync and telemetry RFC meeting on Thu 24 October, 14:00 CEST.

## Status of [Actions defined in the previous meetings](../2024-09-12/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
  Action to be closed.  Nothing to fix.
- [ ] Follow up on ESRF Gitlab Windows runner
  Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
  e-mail has been sent by T-J, tango-cs dockerhub organization can be killed. Don't remove name, if someone else could use it in the future.
  All images should be deleted and a disclaimer should be available to notify the potential users
>> Some images are still pulled. Some images seem to be still used in some projects? See https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs
E.g., https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55
Check https://gitlab.com/tango-controls/gitlab-ci-templates/-/merge_requests/3#note_942846713

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance (subscribe_event_asynch) as draft.  (need to update proposal to match latest cppTango)
  A new issue will be created by ALBA to propose new options during the event subscription
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.
  Alba is considering subcontracting this task
- [x] Contact Tiago Coutinho to try to get a new maintainer (Johan Forsberg) added to https://pypi.org/project/pytango-db/.  Sergi has contacted him.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

No progress.

**Thomas J. (SKAO)**:
- [X] Update https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html and replace the URLs referring to artefact.skao.int/.../ with harbor.skao.int/.../. MR (https://gitlab.com/tango-controls/tango-doc/-/merge_requests/424) has been merged.:
  TI: I don't think we should have done this
- [ ] Check with SKAO's System team what the URL of the images should now really be. Three months ago it was harbor, now TI has been told it is artefact again.
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner
  The RPI5 stopped working reliably with NFS mounted volumes. T-J has replaced it and changed the NFS mounts. Now it works again. Will get in touch with Benjamin again to get help to add it to our runners.
  TJ has been on leave. This will be picked up next week.

**Thomas Ives (SKAO & OSL)**
- [X] Update the TC doc about SKAO being now the maintainer of the images and that the Docker Hub presence has been/will be retired.
  Documentation should be updated to update the location of the images    
  https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html should be updated to replace artefact.skao.int with harbor.skao.int


**Vincent (MAX IV) & Yury (DESY)**
- [x] Steering committee: Ask Igor to retire the TC presence on Docker Hub. Don't remove the name or the presence, so that nobody else will be able to use it in the future.
>> Reynald has access, so he will do this (see earlier action).
- [X] Create an Epic and create 2 issues inside the epic. https://gitlab.com/groups/tango-controls/-/epics/6

**Michal (S2Innovation)**
- [ ] https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9 : issue deploying java software on Maven Central.  Follow up with Patrick Madela.
- [X] Investigate read https://tango-controls.readthedocs.io not being updated due to webhook configuration issue.

**Benjamin (MAX IV)**
- [X] Disable publishing of https://gitlab.com/tango-controls/tango-doc to https://tango-controls.gitlab.io/tango-doc/.  Only want readthedocs.
  https://gitlab.com/tango-controls/tango-doc/-/merge_requests/425

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

## High priority issues

## AOB

### PyTango

PyTango now has the option of manually publishing dev packages to conda, on the `tango-controls` channel:  https://gitlab.com/tango-controls/conda/pytango-feedstock/
We did this for a recent fix with Group destructor deadlock:  https://gitlab.com/tango-controls/pytango/-/merge_requests/762

pybind11:  Yury working on it.  Thomas Braun may also help, if he gets an offer from DESY to work on it.

###  cppTango 9.3.7-rc1

cppTango 9.3.7-rc1 Release Candidate has been released on 4th October 2024.  
If you are able, please could you try the release candidate out and let us know
if you run into any issues by replying to the following comment: https://gitlab.com/tango-controls/cppTango/-/issues/1250#note_2143428519

cppTango 9.3.7 should not be used with omniORB 4.3.x.  It will compile, but won't work.  Use 4.2.x.  Benjamin to enforce this on conda-forge.

### Documentation workshop

All on-site participants should now register on https://indico.esrf.fr/e/tango-doc-workshop-2024
and pay on this ESRF Indico event page.

A Tango Documentation Workshop Preparation Meeting will take place on Wednesday 16th October at 14:00 CEST.  
Zoom link available here: https://indico.tango-controls.org/event/316/

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 24th October 2024 at 15:00 CEST.

## Summary of remaining actions

**All**:
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Benjamin (MaxIV)**:
- [ ] Benjamin to enforce omniORB4.2 dependency for cppTango 9.3.7 on conda-forge

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
  Action to be closed.  Nothing to fix.
- [ ] Follow up on ESRF Gitlab Windows runner
  Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
  e-mail has been sent by T-J, tango-cs dockerhub organization can be killed. Don't remove name, if someone else could use it in the future.
  All images should be deleted and a disclaimer should be available to notify the potential users
>> Some images are still pulled. Some images seem to be still used in some projects? See https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs
E.g., https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55
Check https://gitlab.com/tango-controls/gitlab-ci-templates/-/merge_requests/3#note_942846713

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance (subscribe_event_asynch) as draft.  (need to update proposal to match latest cppTango)
  A new issue will be created by ALBA to propose new options during the event subscription
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.
  Alba is considering subcontracting this task

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

No progress.

**Thomas J. (SKAO)**:
- [ ] Check with SKAO's System team what the URL of the images should now really be. Three months ago it was harbor, now TI has been told it is artefact again.
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner
  The RPI5 stopped working reliably with NFS mounted volumes. T-J has replaced it and changed the NFS mounts. Now it works again. Will get in touch with Benjamin again to get help to add it to our runners.
  TJ has been on leave. This will be picked up next week.

**Vincent (MAX IV) & Yury (DESY)**
- [ ] set up a v10 sync and telemetry RFC meeting

**Michal (S2Innovation)**
- [ ] https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9 : issue deploying java software on Maven Central.  Follow up with Patrick Madela.


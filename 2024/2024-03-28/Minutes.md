# Tango Kernel Follow-up Meeting
Held on 2024/03/28 at 15:00 CET on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (Max IV)
- Michał Gandor (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics e.K.)
- Dmitry Egorov (DESY/ almost MAX IV)
- Damien Lacoste (ESRF)
- Emil Rosendahl (MaxIV / EISCAT)

## Status of [Actions defined in the previous meetings](../2024-03-14/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- [ ] Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846).
- [ ] Follow-up licensing issues

**Anton (MaxIV)**:
- [ ] Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - [ ] Step 1:  create proposal on TangoTickets. ==> Anton
  - [ ] Step 2:  get someone to start building it - Thomas Ives is keen.

**Benjamin (MaxIV)**:
- [X] Contact Patrick Madela for the update of the GPG Passphrase and keyfile to something more secure.

> Done. the keyfile stays the same, just the password was updated.


**Gwenaëlle (SOLEIL)**:
- [X] Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- [ ] Try to reproduce the observed event issues
- [X] Make the indico page for the Tango Community Meeting public, so people can start registration

> Done, there was some minor hiccups, but it’s fixed. Go register!


**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32

> No progress

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] Test DESY Gitlab runners and check with Yury that all required software is installed.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

> no progress

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- [ ] Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846), TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. 
TJ will send out a reminder to the owners to nominate their maintainers and to begin with the house cleaning.
- [X] Create a new poll for the Tango Documentation Workshop location

    https://framadate.org/knHZgpstsa74WwHe

    It will be in Grenoble or around.

- [ ] Add first FAQs
> No progress

**Vincent (MaxIV)**:
- [X] Make the indico page fot the Tango Community Meeting public, so people can start registration

**Yury (DESY)**:
- [ ] Gitlab Runners: Follow up with TB (try DESY runner with cppTango)

## High priority issues

## Issues requiring discussion
With the latest jive there is a bunch of debug messages coming from jacorb.
It’s an issue with JTango and its dependence to jacorb.
https://gitlab.com/tango-controls/JTango/-/issues/138

## Meetings in 2024

### Tango and Cyber Security

https://indico.tango-controls.org/event/237/  

Minutes available here: https://gitlab.com/tango-controls/rfc/-/issues/163

### Tango Community Meeting

Registrations are open:  
https://indico.tango-controls.org/event/261/

### Tango Documentation Workshop

[Framadate Poll](https://framadate.org/knHZgpstsa74WwHe) result: the next Tango Documentation Workshop will take place in France, near Grenoble from 14th to 18th October 2024.

## AOB
- TANGO Controls server-side role based access control (RBAC) repository https://git.jinr.ru/tango-auth. Developed by Evgeny Gorbachev (JINR). (Should he present it on Tango Community Meeting?)  
VM with RBAC installed (Debian 11, Tango 9.3.6) https://disk.jinr.ru/index.php/s/qzG93oLTxwoXMzb

## Summary of remaining actions
**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- [ ] Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846).
- [ ] Follow-up licensing issues

**Anton (MaxIV)**:
- [ ] Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - [ ] Step 1:  create proposal on TangoTickets. ==> Anton
  - [ ] Step 2:  get someone to start building it - Thomas Ives is keen.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] Test DESY Gitlab runners and check with Yury that all required software is installed.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- [ ] Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846), TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
  TJ will send out a reminder to the owners to nominate their maintainers and to begin with the house cleaning.
- [ ] Add first FAQs

**Yury (DESY)**:
- [ ] Gitlab Runners: Follow up with TB (try DESY runner with cppTango)

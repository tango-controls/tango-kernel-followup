# Tango Kernel Follow-up Meeting - 2024/03/28

To be held on 2024/03/28 at 15:00 CET on Zoom.
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

# Agenda

## Status of [Actions defined in the previous meetings](../2024-03-14/Minutes.md#summary-of-remaining-actions)
## High priority issues
## Issues requiring discussion
## Meetings in 2024
### Tango and Cyber Security
### Tango Community Meeting
### Tango Documentation Camp
## AOB

# Tango Kernel Follow-up Meeting
Held on 2024/08/22 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Michal Gandor (S2Innovation)
- Thomas Juerges (SKAO)
- Yury Matveev (DESY)
- Sergi Rubio (ALBA)
- Vincent Hardion (MAX IV)

## cppTango and pyTango 10.0.0 release candidates

Expect cppTango rc5 next Monday, and PyTango rc3 by end of the week.

## Status of [Actions defined in the previous meetings](../2024-08-08/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ.   Remove this for future meetings.
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
- [x] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024
Registrations  are now open on https://indico.tango-controls.org/e/tango-doc-workshop-2024

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance (subscribe_event_asynch) as draft.  (need to update proposal to match latest cppTango)
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
  See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread_ts=1716472950.766739&cid=CQV9F2BTJ
Steering committee: Email is OK, SKAO commits to providing Tango images and to maintain them
- [ ] TJ send the email
Still to do

- [ ] Provide a RaspberryPi 5 for a pytango-builder runner
Received info from TB. Runner is installed but need to configure it properly before it can be added.
Benjamin can help.

**Thomas Ives (SKAO & OSL)**
- [ ] Update the TC doc about SKAO being now the maintainer of the images and that the Docker Hub presence has been/will be retired.
No update, TI is on leave

**Vincent (MAX IV) & Yury (DESY)**
- [ ] Steering committee: Ask Igor to retire the TC presence on Docker Hub.
In progress.  Yury will speak to Igor.

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9 : issue deplying java software on Maven Central.  
Who created MAVEN_CENTRAL_USERNAME and MAVEN_CENTRAL_PASSWORD used in gitlab-ci-templates settings.xml?  
It seems like it does not longer work. We might need to create a new token to deploy on oss.sonatype.org. 
Michal got in touch with Reynald. He suspects that the issue lied in the Gitlab variables.
Action: Michal will follow up with Patrick Madela.

## High priority issues


## AOB

Question:  alarm handlers.  PyAlarm alarm handlers and arrays.  Is a rule like attribute > 0.5, applied to all elements or just one?
Answer:  PyAlarm allows both options, "any", or "all".  Sergi will provide syntax to TJ.

### Documentation workshop

https://indico.tango-controls.org/e/tango-doc-workshop-2024

Registrations are now open. Payment details will come soon. Date is now 28/10 to 1/11.

### Tango 10 SIG meeting

Are rc's available in conda-forge?  Yes, normally within a few days of the release.

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 12th September 2024 at 15:00 CEST.

PyTango meeting will move from 5 Sept to 29 August.

## Summary of remaining actions

**All**:
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance (subscribe_event_asynch) as draft.  (need to update proposal to match latest cppTango)
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
  See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread_ts=1716472950.766739&cid=CQV9F2BTJ
Steering committee: Email is OK, SKAO commits to providing Tango images and to maintain them
- [ ] TJ send the email
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner

**Thomas Ives (SKAO & OSL)**
- [ ] Update the TC doc about SKAO being now the maintainer of the images and that the Docker Hub presence has been/will be retired.

**Vincent (MAX IV) & Yury (DESY)**
- [ ] Steering committee: Ask Igor to retire the TC presence on Docker Hub.

**Michal (S2Innovation)**:
- [ ] https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9 : issue deploying java software on Maven Central.  Follow up with Patrick Madela.

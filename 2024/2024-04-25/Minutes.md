# Tango Kernel Follow-up Meeting
Held on 2024/04/25 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (Max IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics e.K.)
- Damien Lacoste (ESRF)
- Vincent Hardion (MAX IV)
- Sergi Rubio (ALBA)
- Michal Gandor (S2Innovation)
- Thomas Juerges (SKAO)
- Andy Gotz (ESRF)

## Status of [Actions defined in the previous meetings](../2024-04-11/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108
> This will be discussed at the next Tango Executive Committee Meeting

**Andy (ESRF)**:
- [ ] Follow-up licensing issues
  We can close this action for now.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138
  BB: Created https://gitlab.com/tango-controls/TangoSourceDistribution/-/issues/135 that is linked and should be investigated before next release

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32.  Reynald has asked Nicolas Tappret to work on this.
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [x] Test DESY Gitlab runners and check with Yury that all required software is installed.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

> No progress, Yury did all the work.

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
> Not done
- [x] Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846), TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
  TJ will ping the maintainers for a final time and close the issue.
> Done but not closed yet.
- TJ will send out a reminder to the owners to nominate their maintainers and to begin with the house cleaning.
> Done
- [x] Add first FAQs
>  Added first FAQ about TANGO_HOST in the language independent section. Please have a look.

**Yury (DESY)**:
- [x] Gitlab Runners: Follow up with TB (try DESY runner with cppTango)

>  Done. cppTango now uses DESY Windows runner.


## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

## High priority issues
Already talked about it: SKAO has moved the OCI images to a new host and platform (harbor).  
Everybody who is currently pulling images from an older *.skatelescope,org or skaobservatory.org domain, needs to update. Will include this in the email to the mailing list.

## Meetings in 2024

### Tango Community Meeting
Registrations are open:  
https://indico.tango-controls.org/event/261/

There will be a meeting next week to arrange the program.  
**Action on Vincent**: publish link to the meeting to discuss/organize the program.

There will be a Taurus workshop and a Sardana workshop.

The deadline for submitting abstracts is 20th May.

### Tango Documentation Workshop

The next Tango Documentation Workshop will take place in France, near Grenoble from 14th to 18th October 2024.  
So 3 full days of documenting!

**Action - Executive Committee**: Commit to a budget for the documentation workshop

Idea: add a column in the registration: "I need financial support"

## AOB

Do we want HDB++ in TSD with timescale backend?!  
=> No for the moment but we want to add CI in HDB++  
=> Recommendations how to install and on what platforms (Damien's recommendations are the gold standard until we/he suggest/s a better one)  
=> No Windows support. - Do not ask for it.  

**Actions**:
**TB**: Issue on HDB++ what is needed to be done.
**Damien**: Recommendation for a platform (own words, not too much detail)

Space for TangoBox (Michal)

**Action - Andy**: Find a location were to host the TangoBox files. See with Webu

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 16h May 2024.  
The PyTango Meeting from 16th May is canceled.

## Summary of remaining actions

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108

**Executive Committee**: 
- [ ] Commit to a budget for the documentation workshop

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu

**Damien (ESRF)**: 
- HDB++: Give Recommendations for a platform (own words, not too much detail)

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32.  Reynald has asked Nicolas Tappret to work on this.
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] Create an issue on HDB++ what is needed to be done to ease the installation of the HDB++ SW.

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- [ ] Close TangoTickets#63


# Tango Kernel Follow-up Meeting
Held on 2024/12/12 at 15:00 CET on Zoom.
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Benjamin Bertrand (MAX IV)
- Vincent Hardion (MAX IV)
- Becky Auger-Williams (OSL & SKAO)
- Sergi Rubio (ALBA)
- Reynald Bourtembourg (ESRF)

## Status of [Actions defined in the previous meetings](../2024-11-28/Minutes.md#summary-of-remaining-actions)

Andy (ESRF):
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango
  controls) Andy will check with our Tango-Controls web host if they can provide what we need, how files
  can be uploaded, etc.
- [ ] Move from slack to mattermost (feel free to delegate)
- [X] Reynald and Andy will do the Gitlab Ultimate License renewal and check whether only 1 person can do it or not.
> Only 1 person can do it, but it is possible to change the person who must do it.  
> The renewal is now done. We have the Ultimate SaaS license and are part of Gitlab for Open Source program
> (No longer the edu license).

Damien (ESRF):
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
  recommended, Windows not supported, or whatever the case.

Gwenaëlle (SOLEIL):
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
  version, and you can no longer configure a default log level from code. It is a bad practice to put a
  logging conf file because it risks overloading that of the project which has another one.

Lorenzo (Elettra):
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

Reynald (ESRF):
- [ ] Ask Webu for a quote about SSO using Gitlab.com for Mattermost.
> Gitlab SSO seems to be a feature available only in Mattermost Enterprise and Professional plans (See https://docs.mattermost.com/onboard/sso-gitlab.html).   
> Professional plan costs 10$ / month/ user.  We currently have 275 users on tango-controls slack.
- [ ] JTango: Events are not properly reported, investigate issue and create ticket Action to be closed.  Nothing to fix.
- [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available
  both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
  organization can be killed. Don't remove name, if someone else could use it in the future. All images should
  be deleted and a disclaimer should be available to notify the potential users
  Some images are still pulled. Some images seem to be still used in some projects? See
  https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
  https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
  gitlab-ci-templates!3 (comment 942846713)

Sergio (ALBA):
- [ ] Work on cppTango#814, dynamic device attributes (will be done later).
- [ ] Will revise fandango section in new tango-doc

Thomas B. (byte physics):
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
  "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] TangoTickets#103 (comment 1723745893)
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same
  system. TB will create a bug report for Duncan.

Thomas J. (SKAO):
- [ ] TJ to make a list with all the java tools that would need to have a release page/or be built by the Tango Source Distribution.

Vincent (MAX IV) & Yury (DESY):
- [ ] set up a v10 sync and telemetry RFC meeting
> RFC for telemetry meeting done last week
Vincent created issue https://gitlab.com/tango-controls/rfc/-/issues/170
Vincent will create a MR and everybody is welcome to contribute/comment.
- [ ] Replies from SC for the questions from 11/14/2024:

> Questions to the Executive Committee?
> * Tango documentation: Ecosystem software, should we ask every institute to update the new doc to link their software. If nobody or no institute steps up as a maintainer of a specific project, can we consider the project as Unmaintained and do we remove its documentation?

The idea would be to create a page in the doc listing all the projects and their corresponding maintainers.
If there is no maintainer, the project is considered as unmaintained.
We should ping the community at least once a year to update this page.
New action to Vincent: Create a MR in tango-doc (https://tango-controls.readthedocs.io/en/doc-restructure-template/Reference/ecosystem.html) and send the link to this MR to the kernel mailing list asking maintainers to update it.

> * Status of Mattermost


## Discussion needed issues

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

### RFC
https://gitlab.com/tango-controls/rfc/-/issues/168
It  looks like we should fix the RFC.
*New Action - All*: Comments are welcome on this issue. This issue should be solved at the next kernel meeting.

https://gitlab.com/tango-controls/TangoDatabase/-/issues/28 and https://gitlab.com/tango-controls/TangoDatabase/-/issues/7

## High priority issues

## AOB

Season's Greetings!

### Next Kernel Follow-up Meeting
https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 9th January 2025 at 15:00 CET.

## Summary of remaining actions

All:
- Comments are welcome on issue https://gitlab.com/tango-controls/rfc/-/issues/168  
  (Should the device name specification require domain/family/member to start with ALPHA?).  
  This issue should be solved at the next kernel meeting.
- Contribute to the MR about Telemetry RFC

Andy (ESRF):
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango
  controls) Andy will check with our Tango-Controls web host if they can provide what we need, how files
  can be uploaded, etc.
- [ ] Move from slack to mattermost (feel free to delegate)

Damien (ESRF):
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
  recommended, Windows not supported, or whatever the case.

Gwenaëlle (SOLEIL):
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
  version, and you can no longer configure a default log level from code. It is a bad practice to put a
  logging conf file because it risks overloading that of the project which has another one.

Lorenzo (Elettra):
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

Reynald (ESRF):
- [ ] Ask Webu for a quote about SSO using Gitlab.com for Mattermost.
- [ ] JTango: Events are not properly reported, investigate issue and create ticket Action to be closed.  Nothing to fix.
- [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available
  both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
  organization can be killed. Don't remove name, if someone else could use it in the future. All images should
  be deleted and a disclaimer should be available to notify the potential users
  Some images are still pulled. Some images seem to be still used in some projects? See
  https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
  https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
  gitlab-ci-templates!3 (comment 942846713)

Sergio (ALBA):
- [ ] Work on cppTango#814, dynamic device attributes (will be done later).
- [ ] Will revise fandango section in new tango-doc

Thomas B. (byte physics):
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
  "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] TangoTickets#103 (comment 1723745893)
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same
  system. TB will create a bug report for Duncan.

Thomas J. (SKAO):
- [ ] TJ to make a list with all the java tools that would need to have a release page/or be built by the Tango Source Distribution.

Vincent (MAX IV) & Yury (DESY):
- [ ] set up a v10 sync and telemetry RFC meeting
- [ ] create MR for telemetry RFC
- [ ] Create a MR in tango-doc ( to modify https://tango-controls.readthedocs.io/en/doc-restructure-template/Reference/ecosystem.html) and send the link to this MR to the kernel mailing list asking maintainers to update it.
- [ ] Replies from SC for the questions from 11/14/2024:

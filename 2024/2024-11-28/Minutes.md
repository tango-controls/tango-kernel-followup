# Tango Kernel Follow-up Meeting
Held on 2024/11/28 at 15:00 CET on Zoom.
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Benjamin Bertrand (MAX IV)
- Vincent Hardion (MAX IV)
- Thomas Ives (OSL & SKAO)
- Sergi Rubio (ALBA)
- Thomas Braun (byte physics e.K.)
- Reynald Bourtembourg (ESRF)
- Lorenzo Pivetta (Elettra)
- Lukasz Zytniak (S2Innovation)

## Status of [Actions defined in the previous meetings](../2024-11-14/Minutes.md#summary-of-remaining-actions)

Andy (ESRF):
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango
  controls) Andy will check with our Tango-Controls web host if they can provide what we need, how files
  can be uploaded, etc.
- [ ] Move from slack to mattermost (feel free to delegate)

Majority of votes for the Webu hosting option compared to HZDR hosting option

- [ ] New Action on Reynald. Ask Webu for a quote about SSO using Gitlab.com for Mattermost.


Damien (ESRF):
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
  recommended, Windows not supported, or whatever the case.

Gwenaëlle (SOLEIL):
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
  version, and you can no longer configure a default log level from code. It is a bad practice to put a
  logging conf file because it risks overloading that of the project which has another one.

Lorenzo (Elettra):
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))
  -> No update

Reynald (ESRF):
- [ ] JTango: Events are not properly reported, investigate issue and create ticket Action to be closed.  Nothing to fix.
- [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available
  both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
  organization can be killed. Don't remove name, if someone else could use it in the future. All images should
  be deleted and a disclaimer should be available to notify the potential users
  Some images are still pulled. Some images seem to be still used in some projects? See
  https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
  https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
  gitlab-ci-templates!3 (comment 942846713)

Sergio (ALBA):
- [ ] Work on cppTango#814, dynamic device attributes (will be done later).
> Alba is no longer considering subcontracting this task but will subcontract for the event subscription enhancement
- [ ] Will revise fandango section in new tango-doc

Thomas B. (byte physics):
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
  "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] TangoTickets#103 (comment 1723745893)
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same
  system. TB will create a bug report for Duncan.

-> No udpate

Thomas J. (SKAO):
- [X] TJ to create an issue, an MR and update the doc again with correct addresses for docker images
- [ ] TJ to make a list with all the java tools that would need to have a release page/or be built by the Tango Source Distribution.

-> Not present

Vincent (MAX IV) & Yury (DESY):
- [ ] set up a v10 sync and telemetry RFC meeting
- [ ] Replies from SC for the questions from 11/14/2024:

> Question to the Executive Committee?
> * Tango documentation: Ecosystem software, should we ask every institute to update the new doc to link their software. If nobody or no institute steps up as a maintainer of a specific project, can we consider the project as Unmaintained and do we remove its documentation?

The idea would be to create a page in the doc listing all the projects and their corresponding maintainers.
If there is no maintainer, the project is considered as unmaintained.
We should ping the community at least once a year to update this page.

> * Status of Mattermost

-> Already discussed

## Discussion needed issues

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

v10 feedback? Some bug have been reported but they were probably present before.

Are Tango v10 Debian packages at the same level as the conda packages?
Debian packages and conda packages are based on v-10.0.0 but the conda packages are updated independently from the Tango Source Distribution releases.

## High priority issues

## AOB

### Gitlab License

GitLab license: Your Ultimate subscription for tango-controls will expire on 2024-12-11. If you do not renew by 2024-12-25, you can't use merge approvals, epics, security risk mitigation, or any other paid features.

Reynald and Andy will do the renewal and check whether only 1 person can do it or not.

### Vincent leaving Max IV

Vincent Hardion, our current Tango Community Coordinator, is leaving Max IV mid-January.    
Yury will be the new Tango Community Coordinator starting from January on.  
Benjamin Bertrand will be new Max IV representative in the Tango Executive Committee.

### Next Kernel Follow-up Meeting
https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 12th December 2024 at 15:00 CET.

## Summary of remaining actions

Andy (ESRF):
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango
  controls) Andy will check with our Tango-Controls web host if they can provide what we need, how files
  can be uploaded, etc.
- [ ] Move from slack to mattermost (feel free to delegate)
- [X] Reynald and Andy will do the Gitlab Ultimate License renewal and check whether only 1 person can do it or not.
> Only 1 person can do it, but it is possible to change the person who must do it.  
> The renewal is now done. We have the Ultimate SaaS license and are part of Gitlab for Open Source program
> (No longer the edu license).

Damien (ESRF):
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64)
  recommended, Windows not supported, or whatever the case.

Gwenaëlle (SOLEIL):
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: JTango#138 : Logback removed the logging configuration API in its latest
  version, and you can no longer configure a default log level from code. It is a bad practice to put a
  logging conf file because it risks overloading that of the project which has another one.

Lorenzo (Elettra):
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

Reynald (ESRF):
- [ ] Ask Webu for a quote about SSO using Gitlab.com for Mattermost.
- [ ] JTango: Events are not properly reported, investigate issue and create ticket Action to be closed.  Nothing to fix.
- [ ] Follow up on ESRF Gitlab Windows runner Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available
  both on docker and on SKAO before deleting them on dockerhub e-mail has been sent by T-J, tango-cs dockerhub
  organization can be killed. Don't remove name, if someone else could use it in the future. All images should
  be deleted and a disclaimer should be available to notify the potential users
  Some images are still pulled. Some images seem to be still used in some projects? See
  https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs E.g.,
  https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55 Check
  gitlab-ci-templates!3 (comment 942846713)

Sergio (ALBA):
- [ ] Work on cppTango#814, dynamic device attributes (will be done later).
- [ ] Will revise fandango section in new tango-doc

Thomas B. (byte physics):
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under
  "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] TangoTickets#103 (comment 1723745893)
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same
  system. TB will create a bug report for Duncan.

Thomas J. (SKAO):
- [ ] TJ to make a list with all the java tools that would need to have a release page/or be built by the Tango Source Distribution.

Vincent (MAX IV) & Yury (DESY):
- [ ] set up a v10 sync and telemetry RFC meeting
- [ ] Replies from SC for the questions from 11/14/2024:

> Questions to the Executive Committee?
> * Tango documentation: Ecosystem software, should we ask every institute to update the new doc to link their software. If nobody or no institute steps up as a maintainer of a specific project, can we consider the project as Unmaintained and do we remove its documentation?

The idea would be to create a page in the doc listing all the projects and their corresponding maintainers.
If there is no maintainer, the project is considered as unmaintained.
We should ping the community at least once a year to update this page.

> * Status of Mattermost




# Tango Kernel Follow-up Meeting
Held on 2024/02/22 at 15:00 CET on Zoom.
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Andy Götz (ESRF)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (Max IV)
- Damien Lacoste (ESRF)
- Lukasz Zytniak (S2Innovation)
- Lorenzo Pivetta (Elettra)
- Sergio Rubio (ALBA)
- Thomas Braun (byte physics e. K.)
- Thomas Ives (OSL)
- Vincent Hardion (MAX IV)
- Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../2024-02-08/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- [ ] Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846).

- [ ] Follow-up licensing issues

**Anton (MaxIV)**:
- [ ] Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).
  The goal is to find incompatibilities.
  - [ ] Step 1:  create proposal on TangoTickets. ==> Anton
  - [ ] Step 2:  get someone to start building it - Thomas Ives is keen.

**Benjamin (MaxIV)**:
- [ ] Contact Patrick Madela for the update of the GPG Passphrase and keyfile to something more secure. (WIP: Done in three weeks)

**Gwenaëlle (SOLEIL)**:
- [ ] Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- [ ] Try to reproduce the observed event issues

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team. Is waiting for ICT.

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] Create Conda Package for UniversalTest Tango Device Server and make it available on conda-forge
- [ ] Look into https://gitlab.com/tango-controls/starter/-/issues/32

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] Test DESY Gitlab runners and check with Yury that all required software is installed.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- [ ] Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
- [ ] Create a new poll for the Tango Documentation Workshop location as soon as Astron replies.
- [ ] Add first FAQs

**Vincent (MaxIV)**:
- [ ] Check on the automation part to update the device classes catalogue

**Yury (DESY)**:
- [ ] Gitlab Runners: Follow up with TB (try DESY runner with cppTango)

## High priority issues

## Issues requiring discussion

https://gitlab.com/tango-controls/starter/-/issues/32

-> Action on Reynald

alarm event implementation discussion
https://gitlab.com/tango-controls/cppTango/-/issues/1030

Poll for finding date to chat: https://tango-controls.slack.com/archives/CKHEEPM4P/p1708096588587839

### Meetings in 2024

## Tango and CyberSecurity

-> Dedicated planning meeting on monday 26.02. 16:00 CET

## Tango Documentation CampTango

## Community meeting

## Summary of remaining actions

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- [ ] Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846).

- [ ] Follow-up licensing issues

**Anton (MaxIV)**:
- [ ] Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).
  The goal is to find incompatibilities.
  - [ ] Step 1:  create proposal on TangoTickets. ==> Anton
  - [ ] Step 2:  get someone to start building it - Thomas Ives is keen.

**Benjamin (MaxIV)**:
- [ ] Contact Patrick Madela for the update of the GPG Passphrase and keyfile to something more secure.

**Gwenaëlle (SOLEIL)**:
- [ ] Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- [ ] Try to reproduce the observed event issues
- [x] Set up Indico website for the next Tango Community Meeting with Vincent

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team. Waiting for ICT.

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] Create Conda Package for UniversalTest Tango Device Server and make it available on conda-forge
- [ ] Look into starter: https://gitlab.com/tango-controls/starter/-/issues/32

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault
      in PyTango, but can push from read_attribute_hardware.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in
      their profile under "Organization" and make it public. We will start removing
      members without organization in 8 weeks.
- [ ] Test DESY Gitlab runners and check with Yury that all required software is installed.
- [ ] Look into encoding in tango-controls: https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- [ ] Implement gitlab user grooming proposal TangoTickets#63 (comment
      1110688846) and comment in the thread that you finished it. From the input
      that is now available in the issue, TJ compiles the list of owners, adds it
      to the issue and assigns it to Reynald to make those on the list owners.
      After that TJ will tag each owner to apply maintainer role to the people they proposed.
- [ ] Create a new poll for the Tango Documentation Workshop location as soon as Astron replies.
- [ ] Add first FAQs

**Vincent (MaxIV)**:
- [ ] Check on the automation part to update the device classes catalogue

**Yury (DESY)**:
- [ ] Gitlab Runners: Follow up with TB (try DESY runner with cppTango)

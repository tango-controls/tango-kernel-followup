# Tango Kernel Follow-up Meeting
Held on 2024/10/24 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Benjamin Bertrand (MAX IV)
- Vincent Hardion (MAX IV)
- Thomas Braun (byte physics e.K.)
- Becky Auger-Williams (OSL)
- Reynald Bourtembourg (ESRF)
- Thomas Juerges (SKAO)
- Zbigniew Reszlea (ALBA)

## Status of [Actions defined in the previous meetings](../2024-10-10/Minutes.md#summary-of-remaining-actions)

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Benjamin (MaxIV)**:
- [X] Benjamin to enforce omniORB4.2 dependency for cppTango 9.3.7 on conda-forge
  https://github.com/conda-forge/cpptango-feedstock/pull/46/files

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
  Action to be closed.  Nothing to fix.
- [ ] Follow up on ESRF Gitlab Windows runner
  Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
  e-mail has been sent by T-J, tango-cs dockerhub organization can be killed. Don't remove name, if someone else could use it in the future.
  All images should be deleted and a disclaimer should be available to notify the potential users
>> Some images are still pulled. Some images seem to be still used in some projects? See https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs
E.g., https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55
Check https://gitlab.com/tango-controls/gitlab-ci-templates/-/merge_requests/3#note_942846713

> No progress

**Sergio (ALBA)**:
- [X] push test code fixing event subscription performance (subscribe_event_asynch) as draft.  (need to update proposal to match latest cppTango)
  A new issue will be created by ALBA to propose new options during the event subscription - issue created https://gitlab.com/tango-controls/cppTango/-/issues/1344
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.
  Alba is considering subcontracting this task

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

> Nothing done :(

**Thomas J. (SKAO)**:
- [ X Check with SKAO's System team what the URL of the images should now really be. Three months ago it was harbor, now TI has been told it is artefact again.
>  Done: It is artefact.skao.int/
- [X] Provide a RaspberryPi 5 for a pytango-builder runner
  The RPI5 stopped working reliably with NFS mounted volumes. T-J has replaced it and changed the NFS mounts. Now it works again. Will get in touch with Benjamin again to get help to add it to our runners.
>  Done: Benjamin did the magic and the RPi5 is now available.
  Labels: linux, docker, aarch64
- [ ] TJ to create an issue, an MR and update the doc again with correct addresses for images

**Vincent (MAX IV) & Yury (DESY)**
- [ ] set up a v10 sync and telemetry RFC meeting

**Michal (S2Innovation)**
- [X] https://gitlab.com/tango-controls/gitlab-ci-templates/-/issues/9 : issue deploying java software on Maven Central.  Follow up with Patrick Madela.
>  Fixed by Patrick (he updated maven central user and password). Tested on jtango and other projects.

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

https://gitlab.com/tango-controls/cppTango/-/issues/1344
We all agreed that this feature is interesting, and we should not change the default value which is the synchronous read.

## High priority issues

## AOB
Currently one has to find the jar files for jTango-related tools because some/all projects do not make releases on Gitlab, or have not done releases in the past six months or the release build failed. 
It would be good if the jTango tools could be fixed to have releases on their relevant Gitlab project pages.
- [ ] TJ to make a list with all the tools that would need to have a release page/or be built by the Tango Source Distribution.

### pytango-db project

Sergio is now owner of pytango-db project on pypi.
He asked for Johan Forsberg username to add him to the project. Action Benjamin.

### Documentation workshop

- Is there organized travel from Grenoble Central to Autrans?
  Arrivals at Grenoble:
- TB 14:40h
- TJ 15:10h
- VH 16:40h

- Vincent & TJ will take the bus at 16:58h to Autrans.
  - If yes:
    - At what time?
    - Where shall people meet?

- Benjamin and Anton will have a car and will come at the ESRF for lunchtime.

### RFC for Telemetry meeting

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 14th November 2024 at 15:00 **CET**.

## Summary of remaining actions

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
  Action to be closed.  Nothing to fix.
- [ ] Follow up on ESRF Gitlab Windows runner
  Someone else at ESRF is working on this.
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
  e-mail has been sent by T-J, tango-cs dockerhub organization can be killed. Don't remove name, if someone else could use it in the future.
  All images should be deleted and a disclaimer should be available to notify the potential users
>> Some images are still pulled. Some images seem to be still used in some projects? See https://gitlab.com/search?group_id=360928&scope=blobs&search=tangocs
E.g., https://gitlab.com/tango-controls/gitlab-ci-templates/-/blob/main/Java.gitlab-ci.yml#L55
Check https://gitlab.com/tango-controls/gitlab-ci-templates/-/merge_requests/3#note_942846713

**Sergio (ALBA)**:
- [ ] Work on https://gitlab.com/tango-controls/cppTango/-/issues/814, dynamic device attributes.
  Alba is considering subcontracting this task

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

**Thomas J. (SKAO)**:
- [ ] TJ to create an issue, an MR and update the doc again with correct addresses for docker images
- [ ] TJ to make a list with all the tools that would need to have a release page/or be built by the Tango Source Distribution.

**Vincent (MAX IV) & Yury (DESY)**
- [ ] set up a v10 sync and telemetry RFC meeting

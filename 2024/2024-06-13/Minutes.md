# Tango Kernel Follow-up Meeting
Held on 2024/06/13 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Andy Götz (ESRF)
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (Max IV)
- Emil Rosendahl (EISKAT)
- Lorenzo Pivetta (Elettra)
- Michal Gandor (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics e.K.)
- Thomas Ives (OSL)
- Thomas Juerges (SKAO)
- Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../2024-05-16/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.
  1 place to start latest v10 with latest v9 cppTango.  Can we run existing test suites with old and new devices.
  Or, basic server in cppTango, JTango and PyTango.  With client in each language.

Possible variables for a test matrix:

- kernel implementation:  cppTango, PyTango, JTango.
- device server version:  8.x, 9.x, 10.x
- client version:  8.x, 9.x, 10.x
- OS: Linux, Windows, macOS (just from conda-forge?)

We need to pick small subset of this to get started.  We can expand in the future.


**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)
  No progress. Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.
  No progress, absent

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))
  No progress

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32.  Reynald has asked Nicolas Tappret to work on this.  (Nicolas has been looking at a different Starter issue).
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024
  => https://indico.tango-controls.org/event/289/ To be improved. Add a tickbox in the registration form "Needs funding help from Tango Community"

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft.  (need to update proposal to match latest cppTango)
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Can push from read attribute methods in 9.5.0, but in 9.5.1 it deadlocks. Fix for regression in 9.5.1: pytango MR opened: https://gitlab.com/tango-controls/pytango/-/merge_requests/702.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] Create an issue on HDB++ what is needed to be done to ease the installation of the HDB++ SW.

No progress

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
  No progress, waiting for feedback from the executive. See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread_ts=1716472950.766739&cid=CQV9F2BTJ

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

## High priority issues

Device-level dynamic attributes in cppTango. https://gitlab.com/tango-controls/cppTango/-/issues/814
JTango "sort-of" has this, limitation if attributes with same name have different data types.   JTango is working on a fix for that.
- Details can be asked to Stephane Poirier (Soleil). It's implemented in Yat4Tango through the DynamicAttributeManager class. Available here : https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/yat4tango
  An issue is under testing and will be committed soon.

## AOB

Documentation workshop
- How many seats will be available for the Tango Doc workshop?  Currently, considering 10.
- Registration open?  Not yet, but Indico page added (see earlier in notes).
- First come, first serve or will the people from the doc workshop at last year's community meeting have priority? The Indico page will show a box where registrants can express that they need financial support from the Tango Controls community (executive board) for board and lodging. General travel funding is not available but the executive committee reserves the right to support members of the community when they cannot finance their travel but the executive deems them as valueable contributors to the Tango Controls doc workshop.
- Would it be useful if members from our Indian teams came? Not at this time because our framework users, i.e. developers that use Tango Controls to develop Tango devices, lack the in-depth knowledge that the kernel developers have and it is the opinion of the kernel team that at the workshop that in-depth knowledge is needed to contribute and write the new or modify the old doc.

PyTango v10.0.0rc1 still working on this
- Telemetry MR has been reviewed, busy addressing issues.
- Packaging with all the new dependencies due to OpenTelemetry has been a lot of work!
- For binary wheels, can't include telemetry in Windows.  Some cppTango and PyTango issues to fix first:  https://gitlab.com/tango-controls/pytango/-/merge_requests/712#note_1947219680
- Can anyone provide a server with aarch64 (ARM) CPU for a pytango-builder runner?  Current build will take over 24 h in an emulated environment.
  - TJ has a RaspberryPi 5 we can try.
- event tests are quite unreliable - this has long been an issue, but we've increased the number of tests, so seeing more failures.  
SKAO also experiencing missed events in some CI tests.  
cppTango occasionally has some event-related failures in CI.

DESY is hosting SRI conference, can Yury present Tango Collaboration status?  Yes.

Tango IDL:  MR fixing pom file for Java, but no API change.  https://gitlab.com/tango-controls/tango-idl/-/merge_requests/23
TJ will review the MR until Monday.

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 27th June 2024 at 15:00 CEST.

## Summary of remaining actions

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.
  No progress, absent

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32.  Reynald has asked Nicolas Tappret to work on this.  (Nicolas has been looking at a different Starter issue).
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024
  => https://indico.tango-controls.org/event/289/ To be improved. Add a tickbox in the registration form "Needs funding help from Tango Community"

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft.  (need to update proposal to match latest cppTango)
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Can push from read attribute methods in 9.5.0, but in 9.5.1 it deadlocks. Fix for regression in 9.5.1: pytango MR opened: https://gitlab.com/tango-controls/pytango/-/merge_requests/702.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] Create an issue on HDB++ what is needed to be done to ease the installation of the HDB++ SW.

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
  See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread_ts=1716472950.766739&cid=CQV9F2BTJ
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner



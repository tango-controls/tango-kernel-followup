# Tango Kernel Follow-up Meeting - 2024/06/13

To be held on 2024/06/13 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

# Agenda

## Status of [Actions defined in the previous meetings](../2024-05-16/Minutes.md#summary-of-remaining-actions)
## High priority issues
## Issues requiring discussion
## AOB

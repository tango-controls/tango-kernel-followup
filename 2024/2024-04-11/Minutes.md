# Tango Kernel Follow-up Meeting
Held on 2024/04/11 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (Max IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics e.K.)
- Damien Lacoste (ESRF)
- Anton Joubert (MAX IV)
- Thomas Juerges (SKAO)
- Thomas Ives (OSL)
- Vincent Hardion (MAX IV)

## Status of [Actions defined in the previous meetings](../2024-03-28/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- [X] Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846).
- [ ] Follow-up licensing issues

**Anton (MaxIV)**:
- [X] Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - [X] Step 1:  create proposal on TangoTickets. ==> Anton
  - [ ] Step 2:  get someone to start building it - Thomas Ives is keen.
- Anton has no time for this.  Does anyone want to take it over?  TB to create a ticket.

-> Done: https://gitlab.com/tango-controls/TangoTickets/-/issues/108

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32.  Reynald has asked Nicolas Tappret to work on this.

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] Test DESY Gitlab runners and check with Yury that all required software is installed.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

> No progress.

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
>  Will be done by 2024-04-18
- [ ] Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846), TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
  TJ will ping the maintainers for a final time and close the issue.
  TJ will send out a reminder to the owners to nominate their maintainers and to begin with the house cleaning.
>  1st ping was sent on 2024-01-25
- [ ] Add first FAQs
> Will be done until 2024-04-18

**Yury (DESY)**:
- [ ] Gitlab Runners: Follow up with TB (try DESY runner with cppTango)

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

Gitlab Windows runners: changing server config in May.  Some repos need updating:  https://gitlab.com/tango-controls/TangoTickets/-/issues/107

PyTango, does it need methods like read_attributes_5, write_read_attributes_5, which involves ClntIdent?  
No - this should be handled by cppTango.  
PyTango users call read_attribute, and then cppTango uses CORBA interceptor
PyTango may expose client connection info - that could be useful.

## Steering committee report back:
- move from Slack?  Plan: to move to MatterMost for messages and Discourse for forum.
- ELI Beamlines is no longer part of the Tango Collaboration.
- Very few hours left on the S2Innovation contract

## V10 co-ordination meeting report back:
- DevInfo_6, ALARM_EVENT and Open Telemetry are in good shape and will be part of the upcoming cppTango 10.0.0 release at the community meeting.

- There is a chat about the options, how to switch it on /off. The discussion needs to come to a conclusion before we can move on and release cppTango 10.0.0!  Discussion on-going about telemetry configuration on `#temp-tango10-observability-service` slack channel. 

- JTango, not before September 2024.

Any cybersecurity features for V10.0?  
If there are API changes, could we include them in 10.0?  
E.g., for EventSubscriptionChange command.  
Probably not - too early to tell.  
Won't be ready for May 2024.  
Maybe V11, for first new security features.


## High priority issues

None reported.

## Meetings in 2024

### Tango Community Meeting

Registrations are open:  
https://indico.tango-controls.org/event/261/

Please volunteer to help arrange the program.  Chat to your local steering committee representative.

### Tango Documentation Workshop

The next Tango Documentation Workshop will take place in France, near Grenoble from 14th to 18th October 2024.  So 3 full days of documenting!  
Reynald to create indico entry.  Becky has done a lot of preparatory work.  
Spreadsheet with detailed review of current documentation (Read only version - see Slack channel with link to editable version): https://docs.google.com/spreadsheets/d/1wVyqsYN5lxTqpscEKL_9_3mxrNHZGDp1nk7iFmuy1P0/edit?usp=sharing

Yury: updated PyTango part to latest

## AOB

Pogo updates...  XML identifier changing, so some minor compatibility issue.  
Compilation issue on conda-forge.  Damien to chat with Benjamin.

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 25th April 2024.

## Summary of remaining actions

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108

**Andy (ESRF)**:
- [ ] Follow-up licensing issues

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32.  Reynald has asked Nicolas Tappret to work on this.

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] Test DESY Gitlab runners and check with Yury that all required software is installed.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- [ ] Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846), TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
  TJ will ping the maintainers for a final time and close the issue.
  TJ will send out a reminder to the owners to nominate their maintainers and to begin with the house cleaning.
- [ ] Add first FAQs

**Yury (DESY)**:
- [ ] Gitlab Runners: Follow up with TB (try DESY runner with cppTango)


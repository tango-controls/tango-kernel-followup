# Tango Kernel Follow-up Meeting
Held on 2024/06/27 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Andy Götz (ESRF)
- Damien Lacoste (ESRF)
- Michal Gandor (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Sergio Rubio (ALBA)
- Thomas Braun (byte physics e.K.)
- Yury Matveev (DESY)

## cppTango and pyTango 10.0.0 release candidates

cppTango 10.0.0-rc2, pytango 10.0.0-rc1 and Tango Source Distribution rc2 have been released.

-> https://gitlab.com/tango-controls/TangoSourceDistribution/-/releases/10.0.0-rc2

conda packages are available for pytango and cpptango.

You need to pass both pytango and cpptango rc channels to install pytango 10.0.0rc1:  
`conda create -n pytango -c conda-forge/label/pytango_rc -c conda-forge/label/cpptango_rc pytango=10.0.0rc1`

Astron ran already their pipeline tests successfully with pytango-rc1.

Action - All:
- [ ] Please test cppTango 10.0.0-rc2, pytango 10.0.0-rc1 and Tango Source Distribution rc2

## Status of [Actions defined in the previous meetings](../2024-05-16/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.
  No progress

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32.  Reynald has asked Nicolas Tappret to work on this.  Nicolas has prepared https://gitlab.com/tango-controls/starter/-/merge_requests/42 which needs to be reviewed.
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024
  => To be improved. Add a tickbox in the registration form "Needs funding help from Tango Community"
  https://indico.tango-controls.org/e/tango-doc-workshop-2024

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft.  (need to update proposal to match latest cppTango)
- [x] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Can push from read attribute methods in 9.5.0, but in 9.5.1 it deadlocks. Fix for regression in 9.5.1: pytango MR opened: https://gitlab.com/tango-controls/pytango/-/merge_requests/702.
  Sergio will create an issue on PyTango.
  There is already an issue for that. It is fixed in 10.0.0 and will be fixed in pytango 9.5.2
  This action can be removed.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [X] Create an issue on HDB++ what is needed to be done to ease the installation of the HDB++ SW.
  ->  https://gitlab.com/tango-controls/hdbpp/hdbpp-tickets/-/issues/4

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
  See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread_ts=1716472950.766739&cid=CQV9F2BTJ
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

## High priority issues

Sergio reminds that the [Support for device level dynamic attributes](https://gitlab.com/tango-controls/cppTango/-/issues/814) is a High Priority issue for ALBA.  
The cppTango team is now focusing on 10.0.0 but will tackle this problem after 10.0.0 release if we have enough resources.

## AOB

### Documentation workshop

https://indico.tango-controls.org/e/tango-doc-workshop-2024

Registrations will be open as soon as we get more information about the payment details.

### Debian packages

Freexian can work again on the Debian packages. t-b is following-up.

We will ask for backports for version 10.0.0.

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 11th July 2024 at 15:00 CEST.

## Summary of remaining actions

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.
- [ ] Please test cppTango 10.0.0-rc2, pytango 10.0.0-rc1 and Tango Source Distribution rc2

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32.  Nicolas Tappret has prepared https://gitlab.com/tango-controls/starter/-/merge_requests/42 which needs to be reviewed.
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024
  => To be improved. Add a tickbox in the registration form "Needs funding help from Tango Community"
  => https://indico.tango-controls.org/e/tango-doc-workshop-2024

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft.  (need to update proposal to match latest cppTango)

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
  See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread_ts=1716472950.766739&cid=CQV9F2BTJ
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner

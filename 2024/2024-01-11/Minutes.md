# Tango Kernel Follow-up Meeting
Held on 2024/01/11 at 15:00 CET on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:

- Andy Götz (ESRF)
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Damien Lacoste (ESRF)
- Grzegorz Kowalski (S2Innovation)
- Guillaume Communie (ESRF)
- Lorenzo Pivetta (Elettra)
- Lukasz Zytniak (S2Innovation)
- Nicolas Gonzalez (Las Campanas Observatory, Chile)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergio Rubio (ALBA)
- Thomas Braun (byte physics e. K.)
- Thomas Juerges (SKAO)
- Vincent Hardion( MAX IV)
- Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../../2023/2023-12-21/Minutes.md#summary-of-remaining-actions)

**All**:
- Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
    - Step 1:  create proposal on TangoTickets. ==> Anton
    - Step 2:  get someone to start building it - Thomas Ives is keen.
  > No update

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.
- Co-ordinate hysteresis (deadband) change infos in TangoTickets
- bring BSD-3 license discussion to SC, Jan 2024. Maybe better to adopt LGPL for TangoTickets as well.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
> The following issue was created: https://gitlab.com/tango-controls/JTango/-/issues/140  
> Investigations still in progress on potential other issues
- Follow up on ESRF Gitlab Windows runner (Will work on it in January 2024)
> Waiting for feedback from ESRF IT team
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- Move UniversalTest Tango Device Server from ESRF Gitlab to gitlab.com/tango-controls/device-servers
- Invite Guillaume and Damien to show/share their work on the command line tool
> Done

**Sergio (ALBA)**:
- push test code fixing event subscription performance issues as draft

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.

No progress.

- Ask Freexian to Backport omniORB 4.3 package to Bookworm (current stable) so that cppTango can be compiled on the latest stable release.

Done. Now available at https://packages.debian.org/bookworm-backports/libomniorb4-3.

New action: https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

**Thomas J. (SKAO)**:
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
  Then the maintainers can apply developer role to the remaining people.

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB (try DESY runner with cppTango)

## Status of development of MJ (Modern Jive) command line tool

Damien and Guillaume present their command line tool `mj` (a.k.a Modern Jive), currently in development.

read/write attributes, subscribe, list servers, list devices, device info, properties.  Can even pipe image data to gnuplot.

Based on cppTango.  Working on a better cppTango client API which might be useful upstream in the future.
Runtime dependencies: cppTango (libtango, and its dependencies).

https://gitlab.esrf.fr/accelerators/tango/mj

## High priority issues

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?label_name%5B%5D=Discussion%20needed

### Alarm events
Grzegorz Kowalski will implement the new alarm events and alarm deadband/hysteresis in C++.
Lorenzo will work on the specifications for the deadband feature.
Nicolas Leclercq will take care of the coordination to support these features in the different languages and tools.

## SIG Meetings in 2024

### Tango and CyberSecurity

This SIG Meeting will take place at Max IV from 19th to 21st March 2024.

### Tango Documentation Camp

Action - TJ: Send a poll to define the best dates. (Week 38, W40 and W42).

### Other SIG Meetings proposals

## AOB

### Gitlab.com licensing

Open source program status renewal last year needed OSI-compliant open source licenses in all repos on tango-controls.  This was done at the end of last year.  
Two remaining repos (https://gitlab.com/bourtemb/Eurotherm2408, waiting for Johannes Blume from DESY and https://gitlab.com/bourtemb/repository (gentoo), waiting for Damien).

DESY have more code they'd like to share.  Do we want 100s of repos in the tango-controls namespace?  We don't have a clear guideline about what goes in the tango-controls namespace.  Something the steering committee can discuss.  In the sourceforge days we had tango-cs and tango-ds.

Maybe tango-controls:  includes device server that are distributed with source distribution (TangoTest, DatabaseDS, Starter).  Other device servers can go somewhere else.

#### Device server catalogue

Vincent will check on the automation part.

There is some outdated info, e.g., pointing to svn repos.  Should we hide unreachable devices?

### More permissive licensing in future?

Should we consider this for Tango 10 and future?  Steering committee needs to think about this - it is planned for the next committee meeting.  Could be a major effort if we need to get approval from every single previous contributor.


TB's 2 cents:
- Every developer who is working on his employer's behalf is easy as the employer has to agree only.
- But every developer who has contributed in here free time needs to agree herself. If you don't reach the person you have to remove her code and reimplement it using clean-room development.

libzmq did a relicense effort, this was a major effort which took around 6 years, see https://github.com/zeromq/libzmq/issues/2376.

### SVG Synoptic Editor - publicly available
SVGEditor enhances the capabilities of the popular open-source project SVGEdit by integrating the Tango interface via TangoGQL:
https://gitlab.com/s2innovation-partners/svg-synoptic-editor/frontend-s2i-sse

### Gitlab.com partner program

Gitlab.com is offering that Tango Controls joins their partner program.  
One useful advantage is we need to reapply only once every 36 months.  
There are other benefits and requirements listed in this pdf: https://cloud.esrf.fr/s/kYofiofEby6X8kF

### Tango community meeting
Do we already have a date and place for the tango meeting this year?

Location: Soleil (Paris, France)
Date: around end of May

### Next Meetings
https://indico.tango-controls.org/category/6/
- The next Tango Kernel follow-up meeting will take place on Thursday 25th January 2024 at 15:00 CET.
- The next cppTango follow-up meeting will take place on Thursday 18th January 2024 at 16:00 CET.
- The next PyTango follow-up meeting will take place on Thursday 18th January 2024 at 15:00 CET.

## Summary of remaining actions

**All**:
- Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - Step 1:  create proposal on TangoTickets. ==> Anton
  - Step 2:  get someone to start building it - Thomas Ives is keen.

**Damien (ESRF)**:
- Add a license to https://gitlab.com/bourtemb/repository (Gentoo repo)

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.
- Co-ordinate hysteresis (deadband) change infos in TangoTickets
- bring BSD-3 license discussion to SC, Jan 2024. Maybe better to adopt LGPL for TangoTickets as well.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Follow up on ESRF Gitlab Windows runner (Will work on it in January 2024)
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- Move UniversalTest Tango Device Server from ESRF Gitlab to gitlab.com/tango-controls/device-servers

**Sergio (ALBA)**:
- push test code fixing event subscription performance issues as draft

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.
- https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

**Thomas J. (SKAO)**:
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
  Then the maintainers can apply developer role to the remaining people.
- Send a poll to define the best dates for the Tango Documentation Workshop (Week 38, W40 and W42?).

**Vincent (MaxIV)**:
- Check on the automation part to update the device classes catalogue

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB (try DESY runner with cppTango)
- Ask Johannes Blume to add an OSI Compliant license to https://gitlab.com/bourtemb/Eurotherm2408 

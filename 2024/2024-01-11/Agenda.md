# Tango Kernel Follow-up Meeting - 2024/01/11

To be held on 2024/01/11 at 15:00 CET on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

# Agenda

## Status of [Actions defined in the previous meetings](../../2023/2023-12-21/Minutes.md#summary-of-remaining-action)
## Status of development of MJ (Modern Jive) command line tool
## High priority issues
## Issues requiring discussion
## AOB
### Gitlab.com partner program
### Next Meetings
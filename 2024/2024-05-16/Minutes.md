# Tango Kernel Follow-up Meeting
Held on 2024/05/16 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (Max IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics e.K.)
- Vincent Hardion (MAX IV)
- Sergi Rubio (ALBA)
- Thomas Juerges (SKAO)
- Anton Joubert (MAX IV)
- Thomas Ives (OSL)
- Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../2024-04-25/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108

**Executive Committee**:
- [x] Commit to a budget for the documentation workshop
> Done during the last EC Meeting

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu

**Damien (ESRF)**:
- HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

> Not anymore in the V10 roadmap, to reconsider for the next v11


**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32.  Reynald has asked Nicolas Tappret to work on this.  (Nicolas has been looking at a different Starter issue).
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft.  (need to update proposal to match latest cppTango)
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
>  Can push from read attribute methods in 9.5.0, but in 9.5.1 it deadlocks. Fix for regression in 9.5.1: pytango MR opened: https://gitlab.com/tango-controls/pytango/-/merge_requests/702.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] Create an issue on HDB++ what is needed to be done to ease the installation of the HDB++ SW.

> No update

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- [x] Close TangoTickets#63

Is Tango REST server in source distribution still in used?  Should SKAO still build images for it?
- DESY still use this.
  Keep it in source distribution.  And in SKAO images - Thomas Ives will look after Dockerfile for SKAO.

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

## High priority issues

### PyTango CI

PyTango CI broken with latest cppTango - client access works, but starting a device server fails.    https://gitlab.com/tango-controls/pytango/-/jobs/6869038688
```
       util.server_init()
       RuntimeError: unidentifiable C++ exception
```
Could be related to FileDb changes.  
Benjamin has tried reverting this, and that seems to fix the problem.

### Telemetry
Seems like context isn't passed from client to device across process boundaries anymore, so spans related to same call (e.g., read_attribute) don't share the same parent.  Anton thinks it was working back in December 2023, but hasn't tested with multiple processes since then (just a single process with MultiDeviceTestContext).
This is critical to distributed tracing working, so needs to be fixed before release.

Will be fixed as part of https://gitlab.com/tango-controls/cppTango/-/issues/1264

## Meetings in 2024

### Tango Community Meeting
https://indico.tango-controls.org/event/261/

There will be a Taurus workshop and a Sardana workshop.

The deadline for submitting abstracts is 20th May.

Roadmap - we need a presentation about this.
Suggest a poll/reminder to all at the meeting that we want to drop Notifd (could mark deprecated in v10, since ZMQ events already available - steering committee needs be involved in decision), and that pipes will get deprecated once DevDict is available (e.g., v11), and then removed a version later (e.g., v12).

AJ and TJ will do V10/IDL6 presentation + demo (Tuesday).

### Tango Documentation Workshop

The next Tango Documentation Workshop will take place in France, near Grenoble from 14th to 18th October 2024.

## AOB

POGO:  can we remove PyTango low-level option, or make it harder to access?  Make the high-level option the obvious choice.  
Yury to ping Damien on https://gitlab.com/tango-controls/pogo/-/issues/167.

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 13th June 2024 at 15:00 CEST.

## Summary of remaining actions

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32.  Reynald has asked Nicolas Tappret to work on this.  (Nicolas has been looking at a different Starter issue).
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft.  (need to update proposal to match latest cppTango)
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Can push from read attribute methods in 9.5.0, but in 9.5.1 it deadlocks. Fix for regression in 9.5.1: pytango MR opened: https://gitlab.com/tango-controls/pytango/-/merge_requests/702.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] Create an issue on HDB++ what is needed to be done to ease the installation of the HDB++ SW.

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.




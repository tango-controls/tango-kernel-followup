# Tango Kernel Follow-up Meeting
Held on 2024/02/08 at 15:00 CET on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (Max IV)
- Lukasz Zytniak (S2Innovation)
- Lorenzo Pivetta (Elettra)
- Reynald Bourtembourg (ESRF)
- Sergio Rubio (ALBA)
- Thomas Braun (byte physics e. K.)
- Thomas Juerges (SKAO)
- Thomas Ives (OSL)
- Vincent Hardion (MAX IV)
- Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../2024-01-25/Minutes.md#summary-of-remaining-actions)

**All**:
- Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
> Action: TJ will add first FAQs until kernel meeting 2024-03-14. Before he cannot do it because of a trip abroad & leave.

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)
- Follow-up licensing issues

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - Step 1:  create proposal on TangoTickets. ==> Anton
  - Step 2:  get someone to start building it - Thomas Ives is keen.
> No update

**Damien (ESRF)**:
- Add a license to https://gitlab.com/bourtemb/repository (Gentoo repo)
> Done. LGPL License added. The repo is back on gitlab.com/tango-controls

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
> Action: Benjamin to contact Patrick.
- Try to reproduce the observed event issues
- Set up Indico website for the next Tango Community Meeting with Vincent
> Started last week.  Date is visible.  Continue next week.

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.
> Feasible, but need to follow up with IT team.
- Co-ordinate hysteresis (deadband) change infos in TangoTickets
> Ticket has been updated with details:  https://gitlab.com/tango-controls/cppTango/-/issues/560#note_1737528060.  Expect S2Innovation to work on it.
- bring BSD-3 license discussion to SC, Jan 2024. Maybe better to adopt LGPL for TangoTickets as well.
> Complicated, and seems very difficult to change.  Andy to follow up.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
> No news on the ticket.
Experienced some refresh issues when displaying some of our Java widgets on recent OSes. Update of JRE to >=17 solved the issue.
- Follow up on ESRF Gitlab Windows runner
> Will use computer hosted outside ESRF network.
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
>  Not done. TJ is delaying this. :-/  
> Action: TJ really get this done until kernel meeting 2024-03-14
- Move UniversalTest Tango Device Server from ESRF Gitlab to gitlab.com/tango-controls/device-servers
> Done. Device server available on https://gitlab.com/tango-controls/device-servers/DeviceClasses/simulation/UniversalTest.  Will add to conda-forge as well.
- Ping the Java experts for https://gitlab.com/tango-controls/TangoSourceDistribution/-/issues/132
> Done

**Sergio (ALBA)**:
- push test code fixing event subscription performance issues as draft
> can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.  Sergio will create an issue on PyTango.  Better to push events from somewhere else, e.g., a command handler.   Note current PyTango MR may cause a dead-lock or undefined behaviour if pushing from read attribute due to additional lock:  https://gitlab.com/tango-controls/pytango/-/merge_requests/664

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.
- https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

> no progress

- [x] Ask Santiago for a quote for adding opentelemetry-cpp to debian unstable, also with backport to bookworm

**Thomas J. (SKAO)**:
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
> Action: TJ really get this done until kernel meeting 2024-03-14
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
- Notify ASTRON about them being volunteered to host the Tango Documentation workshop.
  Done but 0 reply.
> Action: TJ Poke Jan David again.  
Done. Jan David just said that he still needs to chase that. He will get back to TJ asap.
- Create a new poll for the Tango Documentation Workshop location.
  Not done because blocked by ASTRON not replying.
> Action: TJ Setup poll as soon as ASTRON has replied.

**Vincent (MaxIV)**:
- Check on the automation part to update the device classes catalogue
> No news
- Set up Indico website for the next Tango Community Meeting with Gwen
> [WIP] Tentative date 28-30 May 12:00 to 12:00 with possibility of satellite 28th from 9:00 to 12:00.  
> Last year we had workshops on morning of 3rd day, after 2 full days.

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB (try DESY runner with cppTango)
  Yuri checked, pipelines unfortunately failing. Investigating.
- Ask Johannes Blume to add an OSI Compliant license to https://gitlab.com/bourtemb/Eurotherm2408
> It is outdated version, DESY uses another, from desy.gitlab.com. This one has to be deleted (Done on 9th February).

## High priority issues

V10 release plan after meeting on 7 Feb 2024.  What is included, what not.  Aim for release candidates in April, and final release in May, before community meeting.
At yesterday's coordination the group looked at the "readiness" of the deadband/hysteresis feature and reassessed it as a "potentially rather have it in 11" (semantic versioning feature. 
Unfortunately it has now become clear that Lorenzo was not present because he was not invited. :-( 
The main issue is that all languages need the new feature. 
This in turn means that there is a lot of work to be done plus the work that needs to be done in the tooling.

Should we do semantic versioning, https://semver.org, do we do it?  Does IDL change mean major version bump?  
Historically it has, but cppTango major version has been bumped more often than IDL.  
At last community meeting did we say would do semver?  
Slides don't indicate that, https://indico.tango-controls.org/event/57/contributions/815/.

## Issues requiring discussion
https://gitlab.com/tango-controls/TangoTickets/-/issues/?label_name%5B%5D=Discussion%20needed

## Meetings in 2024

### Tango and CyberSecurity

This SIG Meeting will take place at Max IV from 19th to 20th March 2024.  
https://indico.tango-controls.org/event/237/  
**Action - TJ**: contact people from South Africa for doing a presentation on security and OPC/UA

### Tango Documentation Camp
- The Tango Documentation Camp will take place at the following dates: 2024-10-14 (Monday) - 2024-10-18 (Friday)  
  Location? TBD, poll soon (after ASTRON's reply. See above.) online
  Becky is far ahead of everybody. Already reviewing the existing docs, taking notes and about to feed the gathered information in Corne's spreadsheet.

### Tango community meeting
Location: Soleil (Paris, France)  
Tentative date 28-30 May 12:00 to 12:00 with possibility of satellite 28th from 9:00 to 12:00.

## AOB

### Next Meetings
https://indico.tango-controls.org/category/6/
- The next Tango Kernel follow-up meeting will take place on Thursday 22nd February 2024 at 15:00 CET.
- The next cppTango follow-up meeting will take place on Thursday 15th February 2024 at 16:00 CET.
- The next PyTango follow-up meeting will take place on Thursday 15th February 2024 at 15:00 CET.

## Summary of remaining actions

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- [ ] Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)
- [ ] Follow-up licensing issues

**Anton (MaxIV)**:
- [ ] Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - [ ] Step 1:  create proposal on TangoTickets. ==> Anton
  - [ ] Step 2:  get someone to start building it - Thomas Ives is keen.

**Benjamin (MaxIV)**:
- [ ] Contact Patrick Madela for the update of the GPG Passphrase and keyfile to something more secure.

**Gwenaëlle (SOLEIL)**:
- [ ] Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- [ ] Try to reproduce the observed event issues
- [ ] Set up Indico website for the next Tango Community Meeting with Vincent

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets 

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] Create Conda Package for UniversalTest Tango Device Server and make it available on conda-forge

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Sergio will create an issue on PyTango.   

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] Test DESY Gitlab runners and check with Yury that all required software is installed.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- [ ] Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
- [ ] Create a new poll for the Tango Documentation Workshop location as soon as Astron replies.
- [ ] Add first FAQs

**Vincent (MaxIV)**:
- [ ] Check on the automation part to update the device classes catalogue
- [ ] Set up Indico website for the next Tango Community Meeting with Gwen

**Yury (DESY)**:
- [ ] Gitlab Runners: Follow up with TB (try DESY runner with cppTango)

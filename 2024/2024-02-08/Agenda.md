# Tango Kernel Follow-up Meeting - 2024/02/08

To be held on 2024/02/08 at 15:00 CET on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

# Agenda

## Status of [Actions defined in the previous meetings](../2024-01-25/Minutes.md#summary-of-remaining-actions)
## High priority issues
## Issues requiring discussion
## Meetings in 2024
### Tango and CyberSecurity
### Tango Documentation Camp
### Tango community meeting
## AOB

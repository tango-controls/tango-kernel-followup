# Tango Kernel Follow-up Meeting
Held on 2024/01/25 at 15:00 CET on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Andy Götz (ESRF)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (Max IV)
- Damien Lacoste (ESRF)
- Grzegorz Kowalski (S2Innovation)
- Lukasz Zytniak (S2Innovation)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergio Rubio (ALBA)
- Thomas Braun (byte physics e. K.)
- Thomas Juerges (SKAO)
- Vincent Hardion (MAX IV)

## Status of [Actions defined in the previous meetings](../2024-01-11/Minutes.md#summary-of-remaining-actions)

**All**:
- Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
>  No additional comments were received.

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - Step 1:  create proposal on TangoTickets. ==> Anton
  - Step 2:  get someone to start building it - Thomas Ives is keen.

**Damien (ESRF)**:
- Add a license to https://gitlab.com/bourtemb/repository (Gentoo repo)

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.
- Co-ordinate hysteresis (deadband) change infos in TangoTickets
> I've updated the ticket an provided the definition of **deadband** and a draft specification for the implementation; see https://gitlab.com/tango-controls/cppTango/-/issues/560
- bring BSD-3 license discussion to SC, Jan 2024. Maybe better to adopt LGPL for TangoTickets as well.
> discussed; besides the actual license adopted, the main question if whether the Collaboration is empowered to change the license or not; one option is to adopt somethin similar to https://www.apache.org/licenses/contributor-agreements.html; specific analysis is required (initially SKAO support), possibly legal advice is required as well
Andy will follow up this task.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Follow up on ESRF Gitlab Windows runner (Will work on it in January 2024)
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- Move UniversalTest Tango Device Server from ESRF Gitlab to gitlab.com/tango-controls/device-servers
> UniversalTest is in the process of being modified like TangoTest repository to use Tango CMake submodule and add CI jobs

**Sergio (ALBA)**:
- push test code fixing event subscription performance issues as draft
- Who is replacing Guifre as the owner of the Tango Controls project on Gitlab?
>  Sergi confirmed that Zibi is the owner.

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.
- https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

> No update

**Thomas J. (SKAO)**:
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
>  TJ trying to find the email proposal that he sent to the kernel channel for review quite a while ago. Asked Reynald if he would be able to find it in the backups.
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
>  List has been compiled. ALBA's new owner is still missing. TJ sent Sergi a message in the ticket. Asked Reynald to check that nominated owners are assigned the owner role at project level and have 2FA enabled.  
  SR: I wrote to Zibi (ALBA's new owner) and we will review together the users and permissions

Then the maintainers can apply developer role to the remaining people.
>  Maintainers can already begin with this as soon as they have the maintainer role. Tagged the already known owners in a message where TJ ask them to do begin with this.

- Send a poll to define the best dates for the Tango Documentation Workshop (Week 38, W40 and W42?).

>  Done. https://framadate.org/fLH1YdynOha5ydlb The poll has now closed. 10 users voted. Clear majority of votes (9 plus 1  "if needed") is for 2024-10-14 (Monday) - 2024-10-18 (Friday).


**Vincent (MaxIV)**:
- Check on the automation part to update the device classes catalogue

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB (try DESY runner with cppTango)
- Ask Johannes Blume to add an OSI Compliant license to https://gitlab.com/bourtemb/Eurotherm2408

## Steering Committee Report

Separate Gitlab.com project/organization/workspace for the Tango device servers?
Action on SC.

SC agreed to become gitlab.com partner.

Next SC meeting will be on 25th March.


## High priority issues

## Issues requiring discussion
https://gitlab.com/tango-controls/TangoTickets/-/issues/?label_name%5B%5D=Discussion%20needed

- Remove java "-mx128m" option for java applications: https://gitlab.com/tango-controls/TangoSourceDistribution/-/issues/132

**Action - Reynald**: Ping the java experts

- Discuss the location of the Tango Controls documentation workshop 2024-10-14 (Monday) - 2024-10-18 (Friday).  
  Proposals: ESRF (Grenoble, FR), ASTRON (Dwingeloo, NL), S2Innovations (Krakow, PL)
- **New action on TJ**: Notify ASTRON about them being volunteered to host the workshop. And create a new poll.
- Shall TJ proceed and begin with adding items to the FAQ?  
  Yes, put the first items on the Wiki and we will take it from there.

- Any progress on the subscription limit in pytango/cpptango?  
  To be investigated
## Meetings in 2024

### Tango and CyberSecurity

This SIG Meeting will take place at Max IV from 19th to 20th March 2024.  
Call for contribution for the agenda.  
A zoom meeting is scheduled on Monday 29th January at 16:00 CET to review/improve the agenda.  
Thomas Braun proposed to do a presentation about ZMQ.  
News: Duncan Grisby (main maintainer of OmniORB) will be present.

### Tango Documentation Camp
- New: Date decided: 2024-10-14 (Monday) - 2024-10-18 (Friday)  
  Location TBD => **Action on TJ to send a poll**

### Other SIG Meetings proposals

### Tango community meeting
Location: Soleil (Paris, France)
Date: Week#22 (Last week of May). Exact days in that week still TBD.
**Action on Vincent and Gwen to set up the indico page.**

## AOB
- omniORB issue on macOS: https://www.omniorb-support.com/pipermail/omniorb-list/2024-January/032280.html (no reply from Duncan).
Benjamin will send an e-mail specifying explicitly that he's member of the Tango-Controls Community.   
- Please update the calendars on Indico.

## OpenTelemetry

RFC will be written. Looking for reviewers. Vincent volunteers. Others? (Anton, ...?)

Nicolas announces that Tango 10 will be released before the Tango Community Meeting in May.

Wed 7th February 10am CET: Tango 10 Coordination Meeting to coordinate activities for the different languages.

### Next Meetings
https://indico.tango-controls.org/category/6/
- The next Tango Kernel follow-up meeting will take place on Thursday 8th February 2024 at 15:00 CET.
- The next cppTango follow-up meeting will take place on Thursday 1st February 2024 at 16:00 CET.
- The next PyTango follow-up meeting will take place on Thursday 1st February 2024 at 15:00 CET.

## Summary of remaining actions

**All**:
- Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)
- Follow-up licensing issues

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - Step 1:  create proposal on TangoTickets. ==> Anton
  - Step 2:  get someone to start building it - Thomas Ives is keen.

**Damien (ESRF)**:
- Add a license to https://gitlab.com/bourtemb/repository (Gentoo repo)

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues
- Set up Indico website for the next Tango Community Meeting

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.
- Co-ordinate hysteresis (deadband) change infos in TangoTickets
- bring BSD-3 license discussion to SC, Jan 2024. Maybe better to adopt LGPL for TangoTickets as well.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Follow up on ESRF Gitlab Windows runner (Will work on it in January 2024)
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- Move UniversalTest Tango Device Server from ESRF Gitlab to gitlab.com/tango-controls/device-servers
- Ping the Java experts for https://gitlab.com/tango-controls/TangoSourceDistribution/-/issues/132

**Sergio (ALBA)**:
- push test code fixing event subscription performance issues as draft

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.
- https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

**Thomas J. (SKAO)**:
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
- Notify ASTRON about them being volunteered to host the Tango Documentation workshop. 
- Create a new poll for the Tango Documentation Workshop location.

**Vincent (MaxIV)**:
- Check on the automation part to update the device classes catalogue
- Set up Indico website for the next Tango Community Meeting

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB (try DESY runner with cppTango)
- Ask Johannes Blume to add an OSI Compliant license to https://gitlab.com/bourtemb/Eurotherm2408

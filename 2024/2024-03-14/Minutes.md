# Tango Kernel Follow-up Meeting
Held on 2024/03/14 at 15:00 CET on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Adriaan De Beer (SARAO)
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (Max IV)
- Michał Gandor (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Sergio Rubio (ALBA)
- Thomas Juerges (SKAO)
- Thomas Ives (OSL)
- Vincent Hardion (MAX IV)
- Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../2024-02-22/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- [ ] Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846).
- [ ] Follow-up licensing issues
> No update

**Anton (MaxIV)**:
- [ ] Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - [ ] Step 1:  create proposal on TangoTickets. ==> Anton
  - [ ] Step 2:  get someone to start building it - Thomas Ives is keen.
> No update

**Benjamin (MaxIV)**:
- [ ] Contact Patrick Madela for the update of the GPG Passphrase and keyfile to something more secure.
>  Have followed up again.

**Gwenaëlle (SOLEIL)**:
- [ ] Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- [ ] Try to reproduce the observed event issues

> Not present

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

> Not present

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [X] Create Conda Package for UniversalTest Tango Device Server and make it available on conda-forge
  > Done --> https://anaconda.org/conda-forge/universaltest
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] Test DESY Gitlab runners and check with Yury that all required software is installed.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

> Not present

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- [ ] Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
- [ ] Create a new poll for the Tango Documentation Workshop location as soon as Astron replies.
- [ ] Add first FAQs
  No progress.
  TJ will send out a reminder to thew owners to nominate their maintainers and to begin with the house cleaning.

**Vincent (MaxIV)**:
- [ ] Check on the automation part to update the device classes catalogue
>  We decided to remove this action.

**Yury (DESY)**:
- [ ] Gitlab Runners: Follow up with TB (try DESY runner with cppTango)
>  Seems to be using the version of cppTango installed for PyTango tests instead of new cppTango version from source.

## Tango v10 development coordination

Nicolas Leclercq can't follow up on the co-ordination, so need someone new:  Vincent will take over the co-ordination.  
Will try to manage the project via Gitlab issues.  Group the related issues.  
Looking at either milestones or epic at the tango-controls group level.  
“temp-sig-idlv6-workshop” slack channel was renamed “v10-coordination”.

Device Info:  RFC and cppTango in progress.  https://gitlab.com/tango-controls/rfc/-/merge_requests/171

## High priority issues

## Issues requiring discussion

## Meetings in 2024

### Tango and Cyber Security

https://indico.tango-controls.org/event/237/  
There will be a remote option.

### Tango Documentation Camp
TJ to create framapoll for location.  Won't be at ASTRON.  Host will be either: s2Innovation or ESRF.

### Tango Community meeting

https://indico.tango-controls.org/event/261/
Adriaan De Beer (AdB) pitched his talk about SKAO's integration efforts for the SKA Mid telescope. 
Will get follow-up information from TJ regarding needs, also got advised to make contact on Slack.

Need to make the indico page public, so people can start registration.
> Action on Vincent (to follow up with Gwen).

## AOB

Vincent: Planning to add a project into incubation group with new tangogql implementation.

cppTango merge to main builds conda dev package failed due a permission issue.  Pipeline "triggerer" has to have correct role/permission.  
Note conda pipeline runs in a different subgroup.  Benjamin to check (Done just after the meeting).

## Summary of remaining actions

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- [ ] Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846).
- [ ] Follow-up licensing issues

**Anton (MaxIV)**:
- [ ] Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - [ ] Step 1:  create proposal on TangoTickets. ==> Anton
  - [ ] Step 2:  get someone to start building it - Thomas Ives is keen.

**Benjamin (MaxIV)**:
- [ ] Contact Patrick Madela for the update of the GPG Passphrase and keyfile to something more secure.

**Gwenaëlle (SOLEIL)**:
- [ ] Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- [ ] Try to reproduce the observed event issues
- [ ] Make the indico page fot the Tango Community Meeting public, so people can start registration

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- [ ] https://gitlab.com/tango-controls/starter/-/issues/32

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft
- [ ] can event be pushed from read attribute method in cppTango?  Get segfault in PyTango, but can push from read_attribute_hardware.
  Sergio will create an issue on PyTango.

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] Test DESY Gitlab runners and check with Yury that all required software is installed.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- [ ] Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
- [ ] Create a new poll for the Tango Documentation Workshop location
- [ ] Add first FAQs
  No progress.
  TJ will send out a reminder to thew owners to nominate their maintainers and to begin with the house cleaning.

**Vincent (MaxIV)**:
- [ ] Make the indico page fot the Tango Community Meeting public, so people can start registration

**Yury (DESY)**:
- [ ] Gitlab Runners: Follow up with TB (try DESY runner with cppTango)


# Tango Kernel Follow-up Meeting
Held on 2024/07/11 at 15:00 CEST on Zoom.  
Framapad: https://semestriel.framapad.org/p/tango-kernel-meeting-a5fl?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Michal Gandor (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics e.K.)
- Thomas Ives (OSL)
- Thomas Juerges (SKAO)

## cppTango and pyTango 10.0.0 release candidates

cppTango 10.0.0-rc3, pytango 10.0.0-rc1 and Tango Source Distribution 10.0.0-rc2 have been released.

conda packages are available for pytango and cpptango.

You need to pass both pytango and cpptango rc channels to install pytango 10.0.0rc1:  
`conda create -n pytango -c conda-forge/label/pytango_rc -c conda-forge/label/cpptango_rc pytango=10.0.0rc1`

Astron ran already their pipeline tests successfully with pytango 10.0.0-rc1.  
BLISS tests suite passed with pytango 10.0.0-rc1  
MAX-IV did some telemetry tests with pytango 10.0.0-rc1 using grafana tempo (for traces) and grafana loki (for logs).  For logs to loki over HTTPS, we needed opentelemetry-cpp to be upgraded from 1.15.0 to 1.16.0.  PyTango will bundle 1.16.0.  Suggest cppTango Windows packages also uses 1.16.0.

TB: Update cppTango opentelemetry-cpp package version to 1.16, require 1.16 if it requires source code changes in cppTango

Next week PyTango 10.0.0-rc2. Still without OpenTelemetry in Windows,  but work is progressing.

Action - All:
- [ ] Please test cppTango 10.0.0-rc2, pytango 10.0.0-rc1 and Tango Source Distribution rc2

## Status of [Actions defined in the previous meetings](../2024-05-27/Minutes.md#summary-of-remaining-actions)

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
  TJ will add items next week.
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.
- [ ] Please test cppTango 10.0.0-rc2, pytango 10.0.0-rc1 and Tango Source Distribution rc2
  Tested so far:
  - ESRF: running cppTango 10.0.0-rc2 on a simulator for a week. Not intensively used yet, not using telemetry (but compiled support in), plan to install rc3 next week.
  - MAX IV: PyTango 10.0.0-rc1 not intensively used yet but found a minor issue. Fixed.
  - SKAO: Our tests pass, new features have not been tested yet. Source distribution will be tested soon.
  - ASTRON ran their unit tests and did not see any problems.

We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
- [X] https://gitlab.com/tango-controls/starter/-/issues/32.  Nicolas Tappret fixed this issue. Bug fix available in starter 8.4 which will be in the next Tango Source Distribution Release.
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024
  => To be improved. Add a tickbox in the registration form "Needs funding help from Tango Community"
  => https://indico.tango-controls.org/e/tango-doc-workshop-2024

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft.  (need to update proposal to match latest cppTango)

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893

no progress

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
  See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread_ts=1716472950.766739&cid=CQV9F2BTJ
  Steering committee has not signed off yet on the text of the email.
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner
  Forgot about this. Will do later to day. Need to sync with TB.

## Issues requiring discussion

https://gitlab.com/tango-controls/TangoTickets/-/issues/?state=opened&label_name%5B%5D=Discussion%20needed

## High priority issues

## AOB

### Documentation workshop

https://indico.tango-controls.org/e/tango-doc-workshop-2024

Registrations will be open as soon as we get more information about the payment details.
There is a bus from Grenoble, but before 18:00.  Taxi would be very expensive.  May be able to help each other with own cars.

### Next Kernel Follow-up Meeting

https://indico.tango-controls.org/category/6/

The next Tango Kernel Follow-up Meeting will take place on Thursday 25th July 2024 at 15:00 CEST.

## Summary of remaining actions

**All**:
- [ ] Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- [ ] Next step for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango)? See https://gitlab.com/tango-controls/TangoTickets/-/issues/108.  Need to write down the goals of this system first (what are the key results?).  Iterate, and then see what the scope is.
- [ ] Please test cppTango 10.0.0-rc3, pytango 10.0.0-rc1 and Tango Source Distribution rc2

**Andy (ESRF)**:
- [ ] Find a location were to host the TangoBox files. See with Webu (web hosting company for tango controls)  
  Andy will check with our Tango-Controls web host if they can provide what we need, how files can be uploaded, etc.

**Damien (ESRF)**:
- [ ] HDB++: Give Recommendations for a platform (own words, not too much detail).  E.g., Linux (amd64) recommended, Windows not supported, or whatever the case.

**Gwenaëlle (SOLEIL)**:
- [ ] Try to reproduce the observed event issues
- [ ] Jtango debug log issue: https://gitlab.com/tango-controls/JTango/-/issues/138 : Logback removed the logging configuration API in its latest version, and you can no longer configure a default log level from code. It is a bad practice to put a logging conf file because it risks overloading that of the project which has another one.

**Lorenzo (Elettra)**:
- [ ] Gitlab Runners: Follow up with Elettra IT Team.
- [ ] Co-ordinate hysteresis (deadband) change infos in TangoTickets (shifted to next tango release (IDL v7))

**Reynald (ESRF)**:
- [ ] JTango: Events are not properly reported, investigate issue and create ticket
- [ ] Follow up on ESRF Gitlab Windows runner
- [ ] Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
  Steering committee has approved the process but there has to be an overlap period where images are available both on docker and on SKAO before deleting them on dockerhub
- [ ] Create Indico Entry for the Tango Documentation Workshop Meeting in October 2024
  => To be improved. Add a tickbox in the registration form "Needs funding help from Tango Community"
  => https://indico.tango-controls.org/e/tango-doc-workshop-2024

**Sergio (ALBA)**:
- [ ] push test code fixing event subscription performance issues as draft.  (need to update proposal to match latest cppTango)

**Thomas B. (byte physics)**:
- [ ] Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- [ ] Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- [ ] https://gitlab.com/tango-controls/TangoTickets/-/issues/103#note_1723745893
- [ ] We noticed that there is a problem having two different omniORB versions installed on the same system. TB will create a bug report for Duncan.

**Thomas J. (SKAO)**:
- [ ] Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
  See Slack message https://tango-controls.slack.com/archives/CQV9F2BTJ/p1716472994682979?thread_ts=1716472950.766739&cid=CQV9F2BTJ
- [ ] Provide a RaspberryPi 5 for a pytango-builder runner

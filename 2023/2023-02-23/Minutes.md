# Tango Kernel Follow-up Meeting
Held on 2023/02/23 at 15:00 CET on Zoom.

**Participants**:
- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Lorenzo Pivetta (ELETTRA)
- Lukasz Zytniak (S2Innovations)
- Sergi Rubio (ALBA)
- Thomas Braun (byte physics)
- Thomas Ives (OSL)
- Ulrik Pedersen (OSL)
- Yury Matveev (DESY)
- Jean-Luc Pons (ESRF)
- Nicolas Leclercq (ESRF)
- Gwenaëlle Abeillé (SOLEIL)
- Stéphane Poirier (SOLEIL)
- Kieran Mulholland (OSL)
- Becky Auger-Williams (OSL)

## Summary of remaining actions
All institutes:

    Ask if your institute can host some Gitlab runners. Please follow up with
    your facility until next kernel meeting. Please add comments on
    TangoTickets #77

> Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners
>
> We ran out of minutes this month!
> MAX IV have added more runners.  PyTango ran OK.  Busy testing with cppTango.
> Tags can defined via gitlab.  Keeping tag simple.
>
> ALBA have some runners coming.  First add them to test runner project.  You can
> make a new branch for testing your runners.  Only make available on other
> projects one you know testing was successful.
>
> Individual repos will need to change tag config in gitlab CI YAML file.
>
> cppTango tests are using the most, by far.  Can it be optimised?  E.g., why are docker images rebuilt every time?
>
> [ ] TB: Check if we can optimzie the CI runner minutes usage by not building the docker images every time

Done, see https://gitlab.com/tango-controls/cppTango/-/issues/1069.

> [ ] TB: See if we can abort the pipeline if a new change is pushed

Done, see https://gitlab.com/tango-controls/cppTango/-/issues/1070

> [ ] TB: Create graph to avoid testing exotic setups when basic tests fail

Done, see https://gitlab.com/tango-controls/cppTango/-/issues/1071

All kernel members:

    Read Tango DB Refactoring issue, https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/65,  and proposal and comment

> no update.

Andy (ESRF):

    Ask those that submitted feedback about their Tango installations for permission to publish some information.

> not present.

    Lorenzo, Thomas J: to take the proposal on Gitlab permissions forward, write
    down the details of the proposal with concrete numbers, and then communicate
    with Steering Committee. Link to the proposal in TangoTickets 63

> no progess.

Lorenzo (Elettra):

    Provide feedback from Claudio for pogo pogo!126

> no progess.

    Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM.

> merge request is being prepared.

Nicolas (ESRF):

    JTango: Events are not properly reported, investigate issue and create ticket

> no progress.

Reynald (ESRF):

    Update the Serial Tango class to be compatible with cppTango 9.4 so we
    could run the BLISS tests with latest pyTango release candidates.

> Anton said this is done.  PyTango release candidate had a failure.  A fix was added before 9.4.0, and that test worked.

Thomas B. (byte physics):

    Make a poll to gauge interest in CMake Training by Kitware - ask if time of
    course from https://www.kitware.com/courses/cmake-training/ is OK, or if
    "custom" time required.

Done, see https://tango-controls.slack.com/archives/CFH1RETU2/p1677239004669459

    Ask Kitware about custom CMake training

> Standard course is 2 days, EUR 5k.  For up to 15 people.  Cost is fixed, so
> more people is cheaper per person.  Kernel developers get priority, if
> oversubscribed.  They suggested a consulting session as well, at EUR 2.5k for
> 1 day.
>
> - [x] Nicolas will find out if Tango Collaboration will pay.
> - [x] Thomas B will send out a poll for interest.

    Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)

> no progress.

Thomas I. (OSL):

    Work on MRs for getting cmake enhancements for windows into cpp servers.

> Done.

    Get installer into main/9.3-backports branch of TSD

> Works already, needs some more love for better code.

Thomas J. (SKAO):

    Gather ideas on slack about this new idea: Tutorial independent of
    conferences. Then decide on the format, the date, etc. Thomas J presented a
    hands-on course in India.  Lots of code, few slides. Seems like a good
    model for future workshops. Johan V volunteered Thomas J for a Tango
    workshop at ICALEPCS 2023.  Thomas J is keen.

    As soon as SKAO has reviewed the feedback publish the link to the Miro
    board with the feedback. Give an overview how the workshop went and make
    material available.

    attendees liked the open nature of the workshop - they could ask about
    areas of interest (e.g., green modes, debugging, working in containers,
    ...) and the workshop would focus on those topics.

    Start tango-controls faq on gitlab.com/tango-controls. See also the
    website: https://www.tango-controls.org/about-us/#faq_9904

    Send email to tango mailing list and Slack asking who is using tango-cs and
    which SKAO images are used.  This action is suspended for the moment. We
    will wait for the outcome of Ulrik's investigation.


> Not present.

Ulrik (OSL):

    retire Tango-Controls images on Docker Hub, but will need some help.
    Discussion on ticket TangoTickets#71 Plan: - prepare docker compose file
    that references correct images. - update documentation with the "right"
    method:
    https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html?highlight=docker

    - delete all old images

> Have a docker-compose file that runs up DB, databaseds and rest-server all from
> SKAO Nexus repository. Working on verifying that this is indeed working. Then
> will update the tango docs with this docker-compose file and add a little
> instruction on how to test/use this.

> Issue: SKA docker images are still on 9.3 and we should not update the docs
> with this version.  Wait for new SKA images with 9.4 before finalising this.

> SKA System Team can slow down the process of testing cppTango and PyTango
> release candidates. We should be able to rebuild their images and update for
> testing RCs without waiting for them. This may help catch issues earlier and
> overall speed up test and release process.

## High priority issues

Nothing

## Issues requiring discussion

### OmniORB compilation on Conda

macOS cross-compilation - message send to omniORB mailing list, but waiting for response: https://www.omniorb-support.com/pipermail/omniorb-list/2023-February/032250.html
https://gitlab.com/tango-controls/cppTango/-/issues/1066 - we are using custom cmake for Windows build on conda-forge.

### PyTango

PyTango 9.4.0 wasn't so good :-(  At least a couple of regressions seen by SKA
and Astron.  One is fixed, working on the second.  9.4.1 with reduced scope?
Aim to be more compatible with 9.3.x.

## Update GPG_PASSPHRASE (gpg key used for maven central)

Private key file and passphrase in CI needs to be updated

- [ ] Gwenaëlle to update. Speak to Benjamin if helped needed changing top level gitlab CI settings.

## AOB

### Panic Python 3

Sergi has released Panic and Fandango with Python 3 support on PyPI.org!

### Logging improvement to cppTango

Nicolas has a huge MR open. https://gitlab.com/tango-controls/cppTango/-/merge_requests/1051
Feedback is important!  You can try the 9.5. preview build.

## Summary of remaining actions

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with
  your facility until next kernel meeting. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)

Test project to test the runners:
https://gitlab.com/tango-controls/test-institute-runners

**All kernel members**:
- Read Tango DB Refactoring
  [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and
  [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing)
  and comment

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure

**Lorenzo, Thomas J**:
- To take the proposal on Gitlab permissions forward,
  write down the details of the proposal with concrete numbers, and then
  communicate with Steering Committee.
  Link to the proposal in [TangoTickets 63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63)

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
- Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM.

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

<!-- **Reynald (ESRF)**: -->

**Thomas B. (byte physics)**:
- Get number of participants from https://dud-poll.inf.tu-dresden.de/anonymous/cmake-training/
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

**Thomas I. (OSL)**:
- Get installer into main/9.3-backports branch of TSD

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
  Thomas J presented a hands-on course in India.  Lots of code, few slides.
  Seems like a good model for future workshops.
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
   - attendees liked the open nature of the workshop - they could ask about
     areas of interest (e.g., green modes, debugging, working in containers,
     ...) and the workshop would focus on those topics.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

**Ulrik (OSL)**:
- retire Tango-Controls images on Docker Hub, but will need some help.
  Discussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71
  Plan:
      - prepare docker compose file that references correct images.
      - update documentation with the "right" method:
        https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html?highlight=docker
     - delete all old images

# Tango Kernel Follow-up Meeting - 2023/23/09

To be held on 2023/23/09 at 15:00 CET on Zoom.
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-02-23-9z7m?lang=en

# Agenda

1. Status of [Actions defined in the previous meetings](../2023-02-09/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. AOB

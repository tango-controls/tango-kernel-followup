# Tango Kernel Follow-up Meeting
Held on 2023/05/25 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-05-25-a15n?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (MAX IV)
- Damien Lacoste (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Thomas Ives (OSL)
- Thomas Braun (byte physics e.K.)
- Lorenzo Pivetta (Elettra)
- Ulrik Pedersen (OSL)
- Thomas Juerges (SKAO)

## Status of [Actions defined in the previous meetings](../2023-04-13/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)  
Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners  
We need a list of people to approach, if a runner is broken.  
Important to keep runners updated.  Can we automate this?

Soleil will try to add some Windows runners.
DESY worker offline for 3 weeks?
Elettra:  Lorenzo to follow up with their IT team

- Identify missing responsible persons (owner) from other institutes for gitlab user grooming
  ELI Beamlines: ???
  INAF: ??? (Thomas J will contact Matteo Canzari)
  MAX IV:  Benjamin has done cleanup for MAX IV, and is only owner at tango-controls organisation level.
  ESRF:  in progress
  ALBA:  still have many owners at group level.  Sergi will discuss internally and decide ALBA owner.
  
  Additional seats (we are using 85 of 84!)?  Action:  Reynald to ask Andy to ask gitlab if we can have some more.

- Please test the latest rc version of cppTango 9.4.2-rc1
ALBA have done some tests.  Timeout issue with Tango 7 devices and lots of subscriptions, but issue was already there is 9.3.5.
Elettra:  some versions of ZMQ are buggy (found with cppTango 9.3.6).  
**Action - Lorenzo:** to confirm versions, and make an issue on cppTango. Buggy 0MQ releases are 4.3.3 and 4.3.4.
Done => See https://gitlab.com/tango-controls/cppTango/-/issues/1126

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango).
  The goal is to find incompatibilities.
  Step 1:  create proposal on TangoTickets. ==> Anton
  Step 2:  get someone to start building it - Thomas Ives is keen.
No progress.

**Damien (ESRF)**:
- To create an issue on TangoDatabase repo that shows the benefit of creating
  some indexes
  Damien will do some more testing at ESRF.
  But Graziano's approach is doing the same and more, so should be preferred:
      https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/71

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure
  Benjamin send a request via Slack to Patrick Madela, but no reply yet.
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Johan Venter (SARAO)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Sergi (ALBA)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Becky (OSL)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

      Done


**Lorenzo (Elettra)**:
- Provide feedback from Claudio or someone else for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)

    no progress

- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

      done


**Action - Lukasz (S2Innovation)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
  
  Done according to issue.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
   No progress.
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
  In progress.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)
  
  no progress

**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
  No progress.
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.
    No progress.
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
    No progress.

**Benjamin (MaxIV)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
  In progress - cleaning done at the group level
  More to do in web subgroup and asking users to enable 2FA

**Yury (DESY)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

## High priority issues


## Issues requiring discussion

### Clarify status of support for SUN C++ compilers
Some of our C++ projects (cppTango, TangoDatabase, tango_admin) have #ifdef code checking for __SUNPRO_CC doing workarounds for very ancient looking issues. According to 1 there exists newer Sun compiler versions supporting C++14 (which is required for cppTango >= 9.4.0).
Is there someone actively using SUN C++ compilers? If no, can we remove these workarounds?

No known users - OK to remove.

https://gitlab.com/tango-controls/cppTango/-/merge_requests/1105
https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/77
https://gitlab.com/tango-controls/tango_admin/-/merge_requests/14

### tango_admin maintainer
I opened an MR but there appears to be no maintainer who could sign off. TB explained that it is a community effort which would be perfectly fine. Then I wonder how to deal with MRs?
https://gitlab.com/tango-controls/tango_admin

Option 1:  if no maintainer, the get review from maintainer of tango-controls project that is closely related (e.g., cppTango for C++ projects).
Option 2: assign a dedicated maintainer

Option 2 selected.  Thomas J and Thomas B will be codeowners on tango_admin.

-> https://gitlab.com/tango-controls/tango_admin/-/merge_requests/13

(Damien offered to be maintainer from May 2024).

## AOB

### Tango Collaboration Meeting

Hosted by SKAO near Manchester, UK from Tuesday 27 to Thursday 29 June 2023.  
Hybrid.  
Registration and more details on https://indico.tango-controls.org/event/57/

### IDLv6 Meeting Outcome

Framapad: https://mensuel.framapad.org/p/tango-idl6-sig-meeting-may-2023-agenda-a0dp?lang=en

Indico Page: https://indico.tango-controls.org/event/54/

Headline features for v10:  tracing + alarm warning/hysteresis settings + version info + alarm events
cppTango 10.0.0 - October 2023

### Shared calendar

Can we use https://indico.tango-controls.org/ for all Tango meetings?  
That will make it easier for the community (and new people) to see what is happening and join in.  
Yes.  We should make a section for "regular meetings", or similar.
**Action - Reynald:** to look into it.

### ICALEPCS

Workshop:  discussions in Slack.  Sergi interested in demoing command line tools.
Paper:  can start on this later.

### Write the docs workshop
The workshop will be after(!) the community meeting.
We will have a "planning meeting" on the last day of the community meeting. We will flesh out the agenda and prepare everything as much as possible so that we will be ready to go when we are at the workshop.

### Anton's demo
OpenTelemetry:  https://opentelemetry.io/docs/what-is-opentelemetry/
Demo from:  https://opentelemetry.io/docs/demo/. Mostly Jaeger UI, showing spans and traces timeline.

### CMake training
Poll has lots of replies.
**Action - Reynald**: check constraints from kitware.  Update poll, or choose date.

### Next Teleconf Meetings
- The next Tango Kernel Teleconf meeting will take place on Thursday 8th June 2023 at 15:00 CEST.
- The next cppTango Teleconf meeting will take place on Thursday 1st June 2023 at 14:00 CEST.
- The next PyTango Teleconf meeting will take place on Thursday 1st June 2023 at 15:00 CEST.

## Summary of remaining actions

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)  
Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners  
We need a list of people to approach, if a runner is broken.  
Important to keep runners updated.  Can we automate this?

Soleil will try to add some Windows runners.
DESY worker offline for 3 weeks?
Elettra:  Lorenzo to follow up with their IT team

- Identify missing responsible persons (owner) from other institutes for gitlab user grooming
  ELI Beamlines: ???
  INAF: ??? (Thomas J will contact Matteo Canzari)
  MAX IV:  Benjamin has done cleanup for MAX IV, and is only owner at tango-controls organisation level.
  ESRF:  in progress
  ALBA:  still have many owners at group level.  Sergi will discuss internally and decide ALBA owner.
  
- Please test the latest rc version of cppTango 9.4.2-rc1

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango).
  The goal is to find incompatibilities.
  Step 1:  create proposal on TangoTickets. ==> Anton
  Step 2:  get someone to start building it - Thomas Ives is keen.

**Damien (ESRF)**:
- To create an issue on TangoDatabase repo that shows the benefit of creating
  some indexes
  Damien will do some more testing at ESRF.
  But Graziano's approach is doing the same and more, so should be preferred:
      https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/71

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure
  Benjamin send a request via Slack to Patrick Madela, but no reply yet.
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Johan Venter (SARAO)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Sergi (ALBA)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio or someone else for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)


**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
- to ask Andy to ask gitlab if we can have some more seats.
- CMake Training: to check constraints from kitware.  Update poll, or choose date.
- See if we can create a section for "regular meetings" or similar in https://indico.tango-controls.org

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)


**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
  No progress.
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.
    No progress.
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
    No progress.

**Benjamin (MaxIV)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
  In progress - cleaning done at the group level
  More to do in web subgroup and asking users to enable 2FA

**Yury (DESY)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.


# Tango Kernel Follow-up Meeting - 2023/05/25

To be held on 2023/05/25 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-05-25-a15n?lang=en

# Agenda

1. Status of [Actions defined in the previous meetings](../2023-04-13/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. AOB
    1. Tango Collaboration Meeting
    1. IDLv6 Meeting Outcome
    1. ICALEPCS
    1. Write the docs workshop
    1. Clarify status of support for SUN C++ compilers
       Some of our C++ projects (cppTango, TangoDatabase, tango_admin) have #ifdef code checking for __SUNPRO_CC doing workarounds for very ancient looking issues.
       According to [1] there exists newer Sun compiler versions supporting 
       C++14 (which is required for cppTango >= 9.4.0).

       Is there someone actively using SUN C++ compilers? If no, can we remove these workarounds?

       [1]: https://www.boost.org/doc/libs/1_82_0/boost/config/compiler/sunpro_cc.hpp
       
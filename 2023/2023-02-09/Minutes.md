# Tango Kernel Follow-up Meeting
Held on 2023/02/09 at 15:00 CET on Zoom.
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-02-09-9z7m?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Damien Lacoste (ESRF)
- Grzegorz Kowalski (S2Innovations)
- Jean-Luc Pons (ESRF)
- Lorenzo Pivetta (ELETTRA)
- Lukasz Zytniak (S2Innovations)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Thomas Braun (byte physics)
- Thomas Ives (OSL)
- Ulrik Pedersen (OSL)
- Vincent Hardion (MAX IV)
- Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../../2023/2023-01-26/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with your facility until next kernel meeting. Please add comments on [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)

> Meeting held last Friday.  Some new runners will be added for testing in the repo below.  Tags discussed:  docker, dind (docker-in-docker), linux, Windows, <institute name>.  ALBA have some servers set up now, that they can start testing.

Test project to test the runners:
https://gitlab.com/tango-controls/test-institute-runners

**All kernel members**:
- Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment

> Jean-Luc has tested some work by Damien (https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/65 on storage ring simulator.  Seems OK so far, but didn't test every single command yet.


<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Lorenzo, Thomas J**: to take the proposal on Gitlab permissions forward,
  write down the details of the proposal with concrete numbers, and then
  communicate with Steering Committee.
  Link to the proposal in [TangoTickets 63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63)

> No news - haven't been able to meet yet.  Lorenzo still taking this forward.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
> No news.
- Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM.
> This is running in production at Elettra.  More testing before opening merge request.

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
> Major issue for ESRF - GUI not showing correct information.  Need help from Java core developers.  Seems to be related to Java server, not cppTango servers.  Haven't tested Java server with cppTango client.

**Reynald (ESRF)**:
- Update the Serial Tango class to be compatible with cppTango 9.4 so we could
  run the BLISS tests with latest pyTango release candidates.
> Work in progress.  Have source code, but need conda packaging.  Aiming for tomorrow.

**Sergi (ALBA)**:
-  Sergi to arrange a meeting to talk about how to create runners.
> Done.

**Thomas B. (byte physics)**:
- Make a poll to gauge interest in CMake Training by Kitware - ask if time of
  course from https://www.kitware.com/courses/cmake-training/ is OK, or if
  "custom" time required.
- Ask Kitware about custom CMake training
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)
  
  > No update
  
- Look into how to get starter built with C++98.

> Done.  Added a 9.3-backports branch with this.  It will be included in next 9.3.x source distribution.

**Thomas I. (OSL)**:
- Work on MRs for getting cmake enhancements for windows into cpp servers.
- Get installer into main/9.3-backports branch of TSD

> TangoDatabase and TangoTest MRs are merged.  Working on other device servers (Starter, TangoAccessControl), hoping to have this done by the end of the week.  Planning to start on the Windows installer for TSD/main branch next week.

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
  Thomas J presented a hands-on course in India.  Lots of code, few slides.
  Seems like a good model for future workshops.
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
   - attendees liked the open nature of the workshop - they could ask about
     areas of interest (e.g., green modes, debugging, working in containers,
     ...) and the workshop would focus on those topics.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

**Ugur (SKAO)**:
- Meet with Ulrik, Benjamin to test SKAO runners. Should be ready by Tuesday 31 Jan.
> Done.

**Ulrik (OSL)**:
- retire Tango-Controls images on Docker Hub, but will need some help.
  Discussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71
  Plan:
      - prepare docker compose file that references correct images.
      - update documentation with the "right" method:
        https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html?highlight=docker
     - delete all old images
> Working on it.

## High Priority Issues

- Regression: device server started with empty TCP host endpoint crashes in 9.4.1 https://gitlab.com/tango-controls/cppTango/-/issues/1055.  Discussed pros and cons - documentation mentions empty host sometimes, and DatabaseDS may be launched this way.  Meeting decided not to make another quick release to fix this.
  - Could trigger pytango periodically (nightly) to build with latest dev release to help prevent these kind of issues.

- PyTango 9.4.0rc3 release available.  Aim for 9.4.0 next week, 15 Feb 2023, unless feedback from rc3 testing indicates otherwise. 
  - For binary Linux wheels on PyPI we used omniorb 4.2.4 - it that good enough, or must we move to 4.2.5?  (Windows wheels, and Conda-forge are using 4.2.5).  
  - omniorb double on M1 issue - waiting for feedback from Duncan Grisby before applying any patch on conda-forge.  Compilation option to disable long double seems to work.

## Enhancement of the Logging Service and Related Tools Proposal

Presentation from Nicolas.  
Goal: to improve centralised logging.  
Additional logging level, TRACE.  
Splitting device and kernel (dserver) logs.  
Blackbox events get logged as well, and include some profiling info (timing).  
Breaks binary compatibility, and needs updates to some tools related to logs. 
Structured (JSON) logs would be possible.  
Nicolas created the following TangoTicket issue describing the enhancement of the Logging Service and Related Tools proposal: https://gitlab.com/tango-controls/TangoTickets/-/issues/78

## Next SIG Meetings

Taurus workshop @ ESRF 7-8 March.  Hybrid.  Users are also welcome - would be nice to get their perspective.

CI/CD @ ALBA after that, probably April.  More topics can be added, but can't be too wide - send ideas to Sergi.

Should we organize a SIG meeting on logging? Maybe in Italy?

## Issues Requiring Discussion

Jean-Luc mentioned https://gitlab.com/tango-controls/pytango/-/issues/512 as well as some work that would need to be done to improve the documentation of the methods to help modern IDEs to do some automatic completion. 
Jean-Luc will attend the next PyTango Meeting next week to discuss these points (which can also already be discussed on the Gitlab issue(s)).

## AOB

S2Innovation engineer has developed a LImA plugin for Pixy2 (inexpensive) camera is available on GitHub:  
- https://github.com/kryptoTomo/Lima-camera-pixy2

### Next Meetings

The next Tango Kernel Teleconf meeting will take place on Thursday 23rd February 2023 at 15:00 CET.  
The next cppTango Teleconf meeting will take place on Thursday 16th February 2023 at 14:00 CET.  
The next PyTango Teleconf meeting will take place on Thursday 16th February 2023 at 15:00 CET.

## Summary of remaining actions

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with your facility until next kernel meeting. Please add comments on [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)

Test project to test the runners:
https://gitlab.com/tango-controls/test-institute-runners

**All kernel members**:
- Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Lorenzo, Thomas J**: to take the proposal on Gitlab permissions forward,
  write down the details of the proposal with concrete numbers, and then
  communicate with Steering Committee.
  Link to the proposal in [TangoTickets 63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63)

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
- Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM.

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

**Reynald (ESRF)**:
- Update the Serial Tango class to be compatible with cppTango 9.4 so we could
  run the BLISS tests with latest pyTango release candidates.

**Thomas B. (byte physics)**:
- Make a poll to gauge interest in CMake Training by Kitware - ask if time of
  course from https://www.kitware.com/courses/cmake-training/ is OK, or if
  "custom" time required.
- Ask Kitware about custom CMake training
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)
  
**Thomas I. (OSL)**:
- Work on MRs for getting cmake enhancements for windows into cpp servers.
- Get installer into main/9.3-backports branch of TSD

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
  Thomas J presented a hands-on course in India.  Lots of code, few slides.
  Seems like a good model for future workshops.
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
   - attendees liked the open nature of the workshop - they could ask about
     areas of interest (e.g., green modes, debugging, working in containers,
     ...) and the workshop would focus on those topics.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

**Ulrik (OSL)**:
- retire Tango-Controls images on Docker Hub, but will need some help.
  Discussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71
  Plan:
      - prepare docker compose file that references correct images.
      - update documentation with the "right" method:
        https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html?highlight=docker
     - delete all old images

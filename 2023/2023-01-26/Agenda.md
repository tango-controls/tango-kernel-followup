# Tango Kernel Follow-up Meeting - 2023/01/26

To be held on 2023/01/26 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-01-26-9yxs?lang=en

# Agenda

1. Gitlab Runners
1. Status of [Actions defined in the previous meetings](../2023-01-12/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. Next SIG meetings
1. AOB

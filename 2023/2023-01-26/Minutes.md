# Tango Kernel Follow-up Meeting
Held on 2023/01/26 at 15:00 CET on Zoom.
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-01-26-9yxs?lang=en

**Participants**:
- Thomas Braun (byte physics)
- Sergi Rubio (ALBA)
- Becky Auger-Williams (OSL)
- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Yury Matveev (DESY)
- Thomas Ives (OSL)
- Kieran Mulholland (OSL)
- Ulrik Pedersen (OSL)
- Thomas Juerges (SKAO)
- Lukasz Zytniak (S2Innovations)
- Grzegorz Kowalski (S2Innovations)
- Vincent Hardion (MAX IV)
- Ugur Yilmaz (SKAO)
- Damien Lacoste (ESRF)
- Gwenaëlle Abeillé (SOLEIL)

## Status of [Actions defined in the previous meetings](../../2023/2023-01-12/Minutes.md#summary-of-remaining-actions)

 **All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with your facility until next kernel meeting. Please add comments on [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)

-> SKAO:  Ugur Yilmaz says SKAO can provide some, using spare resources.
-> Which kind of runners are needed?  Typically Docker via Kubernetes.
-> - cppTango:  using gitlab's runners, which are Docker.  Need Docker in Docker.  Later would like Windows runners too.
-> - PyTango: Docker runner would be fine for unit tests.  We do use a shell runner for building Docker images (currently hosted by MAX IV).  Interested in MacOS runners as well.
-> Need to decide on tags associated with the SKAO runners:  discuss on TangoTickets ticket 77.
-> Action:  Ugur to meet with Ulrik, Benjamin to test SKAO runners.  Should be ready by Tuesday 31 Jan.
-> Action:  Sergi to arrange a meeting to talk about how to create runners.

- Feature request for "New attribute event ALARM_EVENT". please see
  [TangoTickets 65](https://gitlab.com/tango-controls/TangoTickets/-/issues/65) for rationale,
  proposal and impacted TC packages.
  Action: Read, understand, comment
  Last chance for discussions is 13 January 2023.  After that date, Thomas J will create IDL and cppTango issues.
  Sergi suggests we should check if this additional event affects performance (once we get to implementation).

-> Done - see the new tickets.

**All kernel members**:
- Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment

**All kernel members and TangoBox users**:
- Please review the new TangoBox VM (Software versions used, first tests) before next kernel meeting:
    - New TangoBox VM: https://s2innovation.sharepoint.com/:u:/s/Developers/ERU58sTPhAdNpURoJMNMemYBJLwIc2ILBhfnQ6Yma1MaZw?e=dgp53L
    - Related Gitlab Issue : https://gitlab.com/tango-controls/tangobox/-/issues/56

-> No positive or negative feedback.  Thomas J said people are happy.
-> Done

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for permission to publish some information.

-> Not present.

**Lorenzo, Thomas J**: to take the proposal on Gitlab permissions forward, write down the details of the proposal with concrete numbers, and then communicate with Steering Committee.
Link to the proposal: https://gitlab.com/tango-controls/TangoTickets/-/issues/63

-> Partially not present.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
- Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM.

-> Not present.

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

-> Not present.

**Reynald (ESRF)**:
- Update the Serial Tango class to be compatible with cppTango 9.4 so we could run the BLISS tests with latest pyTango release candidates.
> Could not find time to do it and won't have time this week

-> Not present.

**Thomas B. (byte physics)**:
- Make a poll to gauge interest in CMake Training by Kitware - ask if time of
  course from https://www.kitware.com/courses/cmake-training/ is OK, or if
  "custom" time required.
- Ask Kitware about custom CMake training
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)
- Look into how to get starter built with C++98.
-> Thomas B has started working on the C++98 support in starter.

- Document: "cppTango and starter should work with plain C++98 for all 9.3.x
   releases of the tango source distribution."
-> Done, see https://gitlab.com/tango-controls/tango-doc/-/merge_requests/399.

**Thomas I. (OSL)**:
- Work on MRs for getting cmake enhancements for windows into cpp servers.
  (Currently waiting on review for TangoDatabase MR)
- Get installer into main/9.3-backports branch of TSD (currently waiting on above MRs to be merged)

 Still waiting on reviews.

**Thomas J. (SKAO)**:
- Review comments on Feature request for "New attribute event ALARM_EVENT"
  (https://gitlab.com/tango-controls/TangoTickets/-/issues/65) until 2022-12-13
-> Done
- Ask SKAO's Team System about gitlab policies until 2022-12-13.
One group (skadev), everyone guest in the top level group. developers -> skadev, then add skadev to projects with developer role. single subgroup to contain security issues. maintainers assigned per project.
-> Done
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
  Thomas J presented a hands-on course in India.  Lots of code, few slides.
  Seems like a good model for future workshops.
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
-> no update
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
   - attendees liked the open nature of the workshop - they could ask about
     areas of interest (e.g., green modes, debugging, working in containers,
     ...) and the workshop would focus on those topics.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
   - no update
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.
  - still waiting on Ulrik's part.

**Ulrik (OSL)**: retire Tango-Controls images on Docker Hub, but will need some help.
Dicussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71
  Plan:
      - prepare docker compose file that references correct images.
      - update documentation with the "right" method:  https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html?highlight=docker
     - delete all old images
-> working on this.

## High Priority Issues

 cppTango 9.4.1 release? Then PyTango 9.4.0rc3 + 9.4.0 (target for final release 15 Feb 2023)
 cppTango 9.4.1 release on Sunday, 2023-01-29.

 Ugur proposes automating testing of SKA images on PyTango tag.  Benjamin and Ugur can discuss.
 SKAO will move to the binary wheels instead of compiling all cppTango dependencies from scratch.

## Issues Requiring Discussion

## Next SIG Meetings

Taurus workshop @ ESRF 7-8 March.  Hybrid.  Users are also welcome - would be nice to get their perspective.

CI/CD @ ALBA after that, probably April.  More topics can be added, but can't be too wide - send ideas to Sergi.

## AOB

cpptango is now available for linux-ppc64le and linux-aarch64 on conda-forge
Would be nice to add tangotest and databaseds.
Plus UniversalTest & tango_admin (work on my Raspberry Pi 4B & Gentoo Linux, compiled with my build_tango scripts)

Panic GUI migration to Python 3:  TangoBox has an "unofficial" updated version that works on Python 3 + Qt5.  Sergi will test the proposed change.  https://gitlab.com/tango-controls/panic/-/merge_requests/44
If Sergi has any important fixes then create issue in TangoBox project.

### Next Kernel Teleconf Meeting

The next Tango Kernel Teleconf meeting will take place on Thursday 9th February 2023 at 15:00 CET.
The next cppTango Teleconf meeting will take place on Thursday 2nd February 2023 at 14:00 CET.
The next PyTango Teleconf meeting will take place on Thursday 2nd February 2023 at 15:00 CET.

## Summary of remaining actions

 **All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with your facility until next kernel meeting. Please add comments on [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)

**All kernel members**:
- Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Lorenzo, Thomas J**: to take the proposal on Gitlab permissions forward,
  write down the details of the proposal with concrete numbers, and then
  communicate with Steering Committee.
  Link to the proposal in [TangoTickets 63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63)

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
- Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM.

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

**Reynald (ESRF)**:
- Update the Serial Tango class to be compatible with cppTango 9.4 so we could
  run the BLISS tests with latest pyTango release candidates.

**Sergy (ALBA)**:
-  Sergi to arrange a meeting to talk about how to create runners.

**Thomas B. (byte physics)**:
- Make a poll to gauge interest in CMake Training by Kitware - ask if time of
  course from https://www.kitware.com/courses/cmake-training/ is OK, or if
  "custom" time required.
- Ask Kitware about custom CMake training
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)
- Look into how to get starter built with C++98.

**Thomas I. (OSL)**:
- Work on MRs for getting cmake enhancements for windows into cpp servers.
  (Currently waiting on review for TangoDatabase MR)
- Get installer into main/9.3-backports branch of TSD (currently waiting on
  above MRs to be merged)

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
  Thomas J presented a hands-on course in India.  Lots of code, few slides.
  Seems like a good model for future workshops.
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
   - attendees liked the open nature of the workshop - they could ask about
     areas of interest (e.g., green modes, debugging, working in containers,
     ...) and the workshop would focus on those topics.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

**Ugur (SKAO)**:
- Meet with Ulrik, Benjamin to test SKAO runners. Should be ready by Tuesday 31 Jan.

**Ulrik (OSL)**:
- retire Tango-Controls images on Docker Hub, but will need some help.
  Dicussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71
  Plan:
      - prepare docker compose file that references correct images.
      - update documentation with the "right" method:
        https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html?highlight=docker
     - delete all old images

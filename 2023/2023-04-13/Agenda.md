# Tango Kernel Follow-up Meeting - 2023/04/13

To be held on 2023/04/13 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-04-13-a0dk?lang=en

# Agenda

1. Status of [Actions defined in the previous meetings](../2023-03-23/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. AOB
    1. Next SIG meetings
    1. Tango Collaboration Meeting
    1. ICALEPCS

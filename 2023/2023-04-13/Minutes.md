# Tango Kernel Follow-up Meeting
Held on 2023/04/14 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-04-13-a0dk?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (MAX IV)
- Damien Lacoste (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Thomas Ives (OSL)
- Thomas Braun (byte physics e.K.)
- Lorenzo Pivetta (Elettra)
- Ulrik Pedersen (OSL)
- Thomas Juerges (SKAO)
- Łukasz Żytniak (S2Innovation)

## Status of [Actions defined in the previous meetings](../2023-03-23/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)  
Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners

> Reynald working on this for ESRF.  
We need a list of people to approach, if a runner is broken.  
Important to keep runners updated.  Can we automate this?

**Steering Committee members**:
- Read Tango DB Refactoring
  [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and
  [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing)
  , comment and decide how to work with this.  
  Questions for steering committee:  
    - Should it be done?  
    - Do they agree with the work packages?  
    - Should we continue with detailed estimates of the effort?  
  Intent is to improve the quality.  Maintainability, readability, testability, ...  
  Guesstimate is a few months of work (1 person?).  
- gitlab owners/maintainer/developers definitions.  
- Date/location for the next Tango community meeting.

> Feedback:  
    - TangoDB refactoring has a green light from Steering Committee for effort in proposal (2 person months).

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Anton (MaxIV)**:  
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.  
  Step 1:  create proposal on TangoTickets. ==> Anton  
  Step 2:  get someone to start building it - Thomas Ives is keen.
> Update: Anton has not done it yet.

**Damien (ESRF)**:
- to create an issue on TangoDatabase repo that shows the benefit of creating some indexes

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
- Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM 
> Done -> https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/71
- In Steering Committee meeting ask if they will cover the fee for the CMake Training
> Collaboration will cover the cost (up to 15 people).

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
> Not able to reproduce reliably.  Still working on it. 

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)
  
>  No progress.

- Add tags for TSD windows installer release of 9.4.1

> Done -> https://gitlab.com/tango-controls/TangoSourceDistribution/-/releases/9.4.1.windows1

**Thomas I. (OSL)**:
- Get installer into main/9.3-backports branch of TangoSourceDistribution
> Merged for main branch.  Need to tag the various device servers and then TSD itself (Thomas B) for 9.4.x.

> Done

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
  Thomas J presented a hands-on course in India.  Lots of code, few slides.
  Seems like a good model for future workshops.
    SKAO Tango Basics training 2023-05-02 - 2023-05-04: Same hand-on approach, few slides only., as much time as possible writing software
    
>  Done
    
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
There will be a Tango workshop at ICALEPCS (weekend before conference).
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
> Still open

- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.
>  Please see Ulrik's task below.. Shouldl be done as soon as the MR is merged?

**Ulrik (OSL)**:
- retire Tango-Controls images on Docker Hub, but will need some help.  
  Discussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71  
  Plan:  
      - prepare docker compose file that references correct images.  
      - update documentation with the "right" method:
        https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html  
      - delete all old images
      
> Update:   Ulrik drafted a MR with some updated docs and a new docker compose file that uses SKAO container images. https://gitlab.com/tango-controls/tango-doc/-/merge_requests/406

## High priority issues


## Issues requiring discussion

Sergi mentioned a couple of issues from Zibi.  
Post details here:  
    - Tango version reported by device servers is inconsistent
       https://www.tango-controls.org/community/forum/c/general/other/reading-tango-version-of-a-server/?page=1#post-5113  
    - Starter can fail to launch with strange command lines
       https://gitlab.com/tango-controls/starter/-/issues/20

## AOB
### Next SIG Meetings

At ALBA.  IDLv6 discussion.

Date is early May, 2 days - please choose here: https://framadate.org/zzqxwlDTJY2rZU9q

Tentative agenda: https://mensuel.framapad.org/p/tango-idl6-sig-meeting-may-2023-agenda-a0dp?lang=en

Indico Page (less-than-draft): https://indico.tango-controls.org/event/54/

### Tango Collaboration Meeting

Hosted by SKAO near Manchester, UK.
Proposed dates are Tuesday 27 to Thursday 29 June 2023, to be confirmed by Marco Bartolini.  
Hybrid.  
Will use tango-controls indico site.  
  **Action - Thomas J**: to confirm the dates of the Tango Collaboration Meeting with SKAO. Proposed dates are Tuesday 27 to Thursday 29 June 2023.

### Shared calendar

Can we use https://indico.tango-controls.org/ for all Tango meetings?  
That will make it easier for the community (and new people) to see what is happening and join in.  
Yes.  We should make a section for "regular meetings", or similar.

### omniorb compilation issue on windows (conda)

https://github.com/conda-forge/omniorb-feedstock/issues/37  
No reply from Duncan: https://www.omniorb-support.com/pipermail/omniorb-list/2023-March/032257.html

**Action - Reynald**: to escalate issue https://github.com/conda-forge/omniorb-feedstock/issues/37 with Duncan.
    
### ICALEPCS

The deadline for submitting abstract papers is 30th April.

Papers 

- Overview of kernel updates. (see previous papers)
- 9.4, move to more regular releases.
- Cross compilation issues on conda-forge?  Java things like Jive and Astor on conda-forge
- PyTango Python version policy
- Logging and traceability updates

Driver:  Thomas J (but he won't be doing all the work)

**Action - Thomas J**: to arrange a place to co-write the abstract for ICALEPCS 2023

- AI code generation of Tango devices. => Driver:  Andy G
- HDB++ status review / evolution?  E.g., version 1 to 2 changes.
  TimescaleDB done previously? - yes https://accelconf.web.cern.ch/icalepcs2019/doi/JACoW-ICALEPCS2019-WEPHA020.html.  Decimation probably not mentioned before.

    Driver:  Sergi? At least decide if there is enough to write about, otherwise include in kernel overview paper.  


Workshop:  
    - jupyTango  
    - Simple example of HDB++?  Normally in TangoBox.  
Johan V, on behalf of SARAO, will be setting up the workshop.  
Location, webpage, program (should be on tango-controls indico instance https://indico.tango-controls.org - speak to Andy), speakers.   
SARAO will do intro to Tango, first few hours. We need to define the rest of the programme and identify the speakers. Some suggestions are: how to setup / use HDB++ and extracting data in a jupytango notebook

### Write the docs workshop

Castle in Krakow, Poland or Alps near Grenoble, France.  
Need it to happen before the community meeting.  
**Action - Lukasz**: to send framadate poll to community for the Write the docs workshop.  
Slack channel for the documentation workshop:  https://tango-controls.slack.com/archives/C052WKJU61L

### Next Teleconf Meetings
- The next Tango Kernel Teleconf meeting will take place on Thursday 27th April 2023 at 15:00 CEST.
- The next cppTango Teleconf meeting will take place on Thursday 20th April 2023 at 14:00 CEST.
- The next PyTango Teleconf meeting will take place on Thursday 20th April 2023 at 15:00 CEST.

## Summary of remaining actions

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)  
Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners  
We need a list of people to approach, if a runner is broken.  
Important to keep runners updated.  Can we automate this?

**Steering Committee members**:
- gitlab owners/maintainer/developers definitions.  

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Anton (MaxIV)**:  
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.  
  Step 1:  create proposal on TangoTickets. ==> Anton  
  Step 2:  get someone to start building it - Thomas Ives is keen.

**Damien (ESRF)**:
- to create an issue on TangoDatabase repo that shows the benefit of creating some indexes

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure

**Lorenzo (Elettra)**:
- Provide feedback from Claudio or someone else for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)

**Action - Lukasz (S2Innovation)**: to send framadate poll to community for the Write the docs workshop.  


**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- to escalate issue https://github.com/conda-forge/omniorb-feedstock/issues/37 with Duncan.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.
- to arrange a place to co-write the abstract for ICALEPCS 2023
- to confirm the dates of the Tango Collaboration Meeting with SKAO. Proposed dates are Tuesday 27 to Thursday 29 June 2023.

**Ulrik (OSL)**:
- retire Tango-Controls images on Docker Hub, but will need some help.  
  Discussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71  
  Plan:  
      - prepare docker compose file that references correct images.  
      - update documentation with the "right" method:
        https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html  
      - delete all old images
      

# Tango Kernel Follow-up Meeting - 2023/11/09

To be held on 2023/11/09 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-11-09-a49l?lang=en
# Agenda

## Status of [Actions defined in the previous meetings](../2023-10-26/Minutes.md#summary-of-remaining-action)
## High priority issues
## Issues requiring discussion
## AOB

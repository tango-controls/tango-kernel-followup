# Tango Kernel Follow-up Meeting
Held on 2023/11/09 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-11-09-a49l?lang=en

**Participants**:

- Andy Götz (ESRF)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (MAX IV)
- Gwenaëlle Abeillé (Soleil)
- Lorenzo Pivetta (Elettra)
- Lukasz Zytniak (S2Innovation)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergio Rubio (ALBA)
- Thomas Braun  (byte physics e.K.)
- Vincent Hardion (MAX IV)
- Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../2023-10-26/Minutes.md#summary-of-remaining-actions)

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

> Still pending

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.

> No news

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  > Still pending
- Try to reproduce the observed event issues
  > At SOLEIL, the only issue reproduced is: event exchanges between different controls systems which rely on different DNS servers, and which all have the same DNS alias as TANGO_HOST environment variable.


**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.
> Still pending

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Follow up on ESRF Gitlab Windows runner
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
> Only 2 ESRF users are not yet using 2FA. They have been reminded today to switch to 2FA.
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).

**Sergio (ALBA)**:
- create a ticket in cppTango to add the possibility to change the starter levels easily
> Not yet done.

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.

> No progress (all).

**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904

- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.

- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.

> No news

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB
  > Some progress. The first run worked smoothly but the second failed. Current set up will have to be improved.

**Vincent Hardion (MaxIV)**:
- Organize TC Cybersecurity SIG Meeting in Feb 2024?!
  - [ ] Send poll
  - choose place

  >  two proposals: ESRF or MAX IV

- Follow up the cppTango C++17 moving proposal with the steering committee

> Some questions were asked and answered on https://gitlab.com/tango-controls/TangoTickets/-/issues/97  
> Vincent proposes to give a deadline (e.g. 1 week) to the Steering Committee to ask questions and report concerns on 
> https://gitlab.com/tango-controls/TangoTickets/-/issues/97

## High priority issues
- event subscription performance issues in pytango; Sergi to push current test code as draft

## Issues requiring discussion

## AOB
pytango 9.5.0rc2 has been released yesterday (8th November). Bliss tests passing on Linux: https://gitlab.esrf.fr/bliss/bliss/-/jobs/844985

### Next Meetings
https://indico.tango-controls.org/category/6/
- The next Tango Kernel follow-up meeting will take place on Thursday 23rd November 2023 at 15:00 CET.
- The next cppTango follow-up meeting will take place on Thursday 16th November 2023 at 16:00 CET.
- The next PyTango follow-up meeting will take place on Thursday 16th November 2023 at 15:00 CET.

## Summary of remaining actions

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.

**Becky (OSL)**:
- Ask Thomas Ives to enable 2FA on his gitlab.com account

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Follow up on ESRF Gitlab Windows runner
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).

**Sergio (ALBA)**:
- create a ticket in cppTango to add the possibility to change the starter levels easily
- push test code fixing event subscription performance issues as draft

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.

**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904

- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.

- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB

**Vincent Hardion (MaxIV)**:
- Organize TC Cybersecurity SIG Meeting in Feb 2024?!
  - [ ] Send poll
  - [ ] Choose place
- Follow up the cppTango C++17 moving proposal with the steering committee (https://gitlab.com/tango-controls/TangoTickets/-/issues/97)

# Tango Kernel Follow-up Meeting - 2023/09/14

To be held on 2023/09/14 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-09-14-a388?lang=en
# Agenda

## Status of [Actions defined in the previous meetings](../2023-08-24/Minutes.md#summary-of-remaining-actions)
## ICALEPCS
### Who will present THE TANGO COLLABORATION STATUS IN 2023?
### Paper and Presentation Status
### Tango Workshop Preparation Status
## High priority issues
## Issues requiring discussion
## AOB


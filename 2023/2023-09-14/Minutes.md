# Tango Kernel Follow-up Meeting
Held on 2023/09/14 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-09-14-a388?lang=en

**Participants**:

- Andy Goetz (ESRF)
- Thomas Braun  (byte physics e.K.) 
- Becky Auger-Williams (OSL)
- Damien Lacoste (ESRF)
- Reynald Bourtembourg (ESRF)
- Nicolas Leclercq (ESRF)
- Thomas Juerges (SKAO)
- Benjamin Bertrand (MAX IV)
- Ulrik Pedersen (OSL)
- Sergi Rubio Manrique (Alba)
- Lorenzo Pivetta (Elettra)


## Status of [Actions defined in the previous meetings](../2023-08-24/Minutes.md#summary-of-remaining-actions)

**Andy (ESRF)**: Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)  

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.  

**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues  

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.
  > No progress

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket
  > No progress

  - Ask about Windows gitlab runner to host/rent/buy  
  > ESRF can rent a Windows server (8 cores, ...) where a Windows Gitlab Runner will run.  
  
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  > Some progress (2FA enabled for most ESRF users). Waiting for some colleagues to come back from holidays.

  - Kill dockerhub organization (tango-cs?!) of tango-controls
  > Waiting for T-J to send the e-mail

**Thomas B. (byte physics)**:  
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)  
  
  - Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.

**Thomas J. (SKAO)**:  
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904  
> Not done

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
> Not done

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.
> Not done

  - Clean new paper for ICALEPCS from old text. Try to preserve the new content.

**Yury (DESY)**:
  - Gitlab Runners: Follow up with DESY IT Team.  

## ICALEPCS 2023
### Who will present THE TANGO COLLABORATION STATUS IN 2023?

Sergi and Thomas Juerges will co-present (it takes 2 to Tango).

**Action - Thomas J. (SKAO)**: Share link to the slides for the collaborative ICALEPCS presentation

### Paper and Presentation Status

**Action - Thomas J. (SKAO)**: Ask for an overleaf paid account and share the link to a new Overleaf project (Starting from the JACoW latest template).
https://www.overleaf.com/user/subscription/plans  
Free account: only one contributor  
Standard: 10 collaborators  
Professional: unlimited collaborators  
-> There was already a request and T-J has been added as a requester.  
SKAO and Overleaf have already met. Cannot share more details. SKAO will get a licence pack but T-J is doubtful that we will have if before ICALEPCS.

### Tango Workshop Preparation Status

https://indico.tango-controls.org/event/55/timetable/#20231007

Sergi will do the Synoptic presentation.

T-J will coordinate with Devon to see how to get Rapsberry PI, wifi, etc...  
T-J will flight to Cape Town one week before the workshop. This should give him some time to setup the workshop with Devon.

## High priority issues

## Issues requiring discussion

## AOB

Thomas Ives and Thomas Juerges created an event subscription compatibility matrix: https://docs.google.com/document/d/1_bMCQ0mHyo6nhKOBZVVXiGmM3sJIFNh85Vfpx7jXJzE/edit?usp=sharing  
Comments are welcome.

### cppTango

cppTango 9.5.0-rc1 is out. It is not binary compatible with cppTango 9.4. Please test it.  
**IMPORTANT**: cppTango meetings from 2023-09-21 on at 16:00 CEST (CET during European Winter)!

### Next Meetings
https://indico.tango-controls.org/category/6/
- The next Tango Kernel Teleconf meeting will take place on Thursday 28th September 2023 at 15:00 CEST.  
- The next cppTango Teleconf meeting will take place on Thursday 21st September 2023 at 16:00 CEST.  
- The next PyTango Teleconf meeting will take place on Thursday 21st September 2023 at 15:00 CEST.  
- The next HDB++ Meeting (ICALEPCS preparation) will take place on Tuesday 19th September 2023 at 14:00 CEST.

## Summary of remaining actions

**Andy (ESRF)**: Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)  

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.  

**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues  

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket

  - Follow up on ESRF Gitlab Windows runner
  
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  - Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).

**Thomas B. (byte physics)**:  
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)  
  
  - Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.

**Thomas J. (SKAO)**:  
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904  

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.

  - Clean new paper for ICALEPCS from old text. Try to preserve the new content.

  - Share link to the slides for the collaborative ICALEPCS presentation

  - Ask for an overleaf paid account and share the link to a new Overleaf project (Starting from the JACoW latest template).
 
**Yury (DESY)**:
  - Gitlab Runners: Follow up with DESY IT Team.  

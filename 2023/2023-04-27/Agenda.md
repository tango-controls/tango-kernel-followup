# Tango Kernel Follow-up Meeting - 2023/04/27

To be held on 2023/04/27 at 15:00 CEST on Zoom.
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-04-27-a0dk?lang=en

# Agenda

1. Status of [Actions defined in the previous meetings](../2023-04-13/Minutes.md#summary-of-remaining-actions)
2. High priority issues
3. Issues requiring discussion
4. AOB

# Tango Kernel Follow-up Meeting
Held on 2023/04/27 at 15:00 CEST on Zoom.
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-04-27-a0dk?lang=en

**Participants**:
- Becky Auger-Williams (OSL)
- Sergi Rubio (ALBA)
- Thomas Braun (byte physics e.K.)
- Lorenzo Pivetta (Elettra)
- Thomas Juerges (SKAO)
- Łukasz Żytniak (S2Innovation)
- Yury Matveev (DESY)
- Nicolas Leclercq (ESRF)
- Tomasz Madej (S2Innovations)
- Andy Gotz (ESRF)

## Summary of remaining actions

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)
Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners
We need a list of people to approach, if a runner is broken.
Important to keep runners updated.  Can we automate this?

TB: Add readme to https://gitlab.com/tango-controls/test-institute-runners on how to add new runners and use them
Done-> https://gitlab.com/tango-controls/test-institute-runners/-/merge_requests/4

- Please test the latest rc version of cppTango 9.4.2-rc1

**Steering Committee members**:
- gitlab owners/maintainer/developers definitions.

Add TangoTickets for each of the named persons

ESRF: Reynald
SKAO: Tj
Elettra: Lorenzo
Alba: Sergi
DESY: Yury
MaxIV: Vincent
Soleil: Gwenaelle
OSL: Becky
byte physics: Thomas
S2 Innovation: Lukasz
SARAO: Johan Venter
ELI Beamlines:
INAF:
Solaris:

where they document the implementation of the procedure  of the steering comitee from https://gitlab.com/tango-controls/TangoTickets/-/issues/63

Action on all institutes: Identify missing responsible person from other institutes for gitlab user grooming
<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

->  No news.

  Updated website will soon be online (green light was given to web hosting
  company today). This will be the place to publish the above information.

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango).
  The goal is to find incompatibilities.
  Step 1:  create proposal on TangoTickets. ==> Anton
  Step 2:  get someone to start building it - Thomas Ives is keen.

-> not present

**Damien (ESRF)**:
- to create an issue on TangoDatabase repo that shows the benefit of creating some indexes

-> not present

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure

-> not present

**Lorenzo (Elettra)**:
- Provide feedback from Claudio or someone else for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)

-> no feedback

**Action - Lukasz (S2Innovation)**: to send framadate poll to community for the Write the docs workshop.

-> Action: Adapt poll to mention date (week starting on the 5th of June), two *full* days, circulate again, deadline 5.5.2023, -> Will be hosted at Krakau in the castle (again)

-> Done.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- to escalate issue https://github.com/conda-forge/omniorb-feedstock/issues/37 with Duncan.

-> not present

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

-> no progress.

**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
->  no
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.
->  no
- to arrange a place to co-write the abstract for ICALEPCS 2023
-> done
- to confirm the dates of the Tango Collaboration Meeting with SKAO. Proposed dates are Tuesday 27 to Thursday 29 June 2023.

Dates are confirmed! Two and a half days!

**Ulrik (OSL)**:
- retire Tango-Controls images on Docker Hub, but will need some help.
  Discussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71
  Plan:
      - prepare docker compose file that references correct images.
      - update documentation with the "right" method:
        https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html
      - delete all old images

not present

AOB

IDLv6 SIG meeting is in two weeks! 11/12 may @ALBA

Create Agenda:
TJ: Alarm Event
Nicolas: Logging proposal
TJ: N-dim arrays
TJ: Multiple parameters in commands
TJ: Add a  high volume data streaming service embedded in the kernel
TJ: Deprecate pipes?
TB?: CI/CD and testing of the new release?
All (discussion): How to implement the new IDLV6 piecewise?
  - Perhaps a starting point for the discussion: Would something like a feature
    set that can be exchanged between client and server be helpful?
  - Expected outcome of the discussion:
      - Possibly an additional extension to the IDL file
      - Then have a procedure that allows to implement individual changes of the IDL.

The goal is to have a new IDL version and also estimate the effort to implement it.

Poll for cmake training: -> https://framadate.org/OqvMAIreGQ51n6m7

High priority issues

None

## Summary of remaining actions

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)
  Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners

- Identify missing responsible persons (owner) from other institutes for gitlab user grooming
  ELI Beamlines: ???
  INAF: ???
  Solaris: ???

- Please test the latest rc version of cppTango 9.4.2-rc1

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango).
  The goal is to find incompatibilities.
  Step 1:  create proposal on TangoTickets. ==> Anton
  Step 2:  get someone to start building it - Thomas Ives is keen.

**Damien (ESRF)**:
- To create an issue on TangoDatabase repo that shows the benefit of creating
  some indexes

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Johan Venter (SARAO)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Sergi (ALBA)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Becky (OSL)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio or someone else for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Action - Lukasz (S2Innovation)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Vincent (MaxIV)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Yury (DESY)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

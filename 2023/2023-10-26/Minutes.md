# Tango Kernel Follow-up Meeting
Held on 2023/10/26 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-10-26-a3zp?lang=en

**Participants**:

- Andy Götz (ESRF)
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Dmitry Egorov (CFEL, DESY)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergio Rubio (ALBA)
- Stéphane Poirier (Soleil)
- Thomas Braun  (byte physics e.K.)
- Thomas Juerges (SKAO)
- Vincent Hardion (MAX IV)
- Yury Matveev (DESY)

## ICALEPCS 2023 Debriefing

- Tango summary paper - still undergoing some minor edits (TJ handling this)
- Workshop: attendance not as high as we would have liked.  Not having desks made it difficult.
- Some interest about using Tango at new and existing facilities.
- Thanks to TJ and Sergio for doing the presentation!
- Long running commands from SKAO are interesting.  Might be nice to set the default timeout per command/attribute - then client knows how long to wait.  Downside of concurrent work in the server is that it is more effort for client to know what is happening.
- Taranta presentation was good.
- European Southern Observatory's Extremely Large Telescope interested in HDB++ - discussions with TJ, Maurizio Zambrano (SKAO) & Damien (joined later).
- Cyber security: big trend, and growing concern.  We have room to improve.  Could we add encryption with TLS (supported in omniORB and ZeroMQ).   Good candidate for a Special Interest Group meeting, possibly mid February 2024.
- Good opportunity to network with developers working on other systems, e.g., HDB++ group chatting with Epics archiving devs.
  The Tango Controls collaboration and especially how the kernel, cpp and pytango works attracted a number of questions and interest from other collaborations. Very few of the other open source control system frameworks have setup an equivalent system.  This is seen as one of the strengths of Tango Controls.

## Status of [Actions defined in the previous meetings](../2023-10-12/Minutes.md#summary-of-remaining-actions)

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)
>  Still to do. No-one from ELI at ICALEPCS.

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.
>  No update.

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues
  Stéphane to ping Gwenaëlle about these actions.

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Follow up on ESRF Gitlab Windows runner
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).
>  No update

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.

> No update

**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904
>  Not done

- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
>  Not done

- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.
>  Not done

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB

**Vincent Hardion (MaxIV)**:
- Organize TC Cybersecurity SIG Meeting in Feb 2024?!
  - [ ] Send poll
  - choose place

## High priority issues

## Issues requiring discussion

SKAO docker runners are offline until end of next week.  This is a problem for cppTango, which depends on a docker-in-docker.  Not urgent for the next week.  Ideally, we should try and remove this requirement from cppTango.  There is already a cppTango issue for this ->
https://gitlab.com/tango-controls/cppTango/-/issues/1120


States:
When reading state, all alarmed attributes get read.  If there are a lot of attributes, it can affect performance.  Is there a way to turn this off?  Device authors can override the dev_state method.  Can we have a more convenient way of doing it?  General agreement that there should be an easy way to disable this.
https://gitlab.com/tango-controls/TangoTickets/-/issues/92

Command-line tools to read and write attributes?
https://gitlab.com/tango-controls/tango_admin/-/issues/11
TJ will move the ticket to TangoTickets.  
Another idea is to change Starter level from the command line.  
Some functionality for this is missing in cppTango (need to read SQL table directly)

**Action - Sergio (ALBA)**: to create a ticket in cppTango to add the possibility to change the starter levels easily.

## AOB

PyTango 9.5.0rc1 should be out this week (if we can sort out build issues).

PyTango - need a C++ expert to help with some deprecation warnings like `auto_ptr<>`, and old-style C casting.  
Yury will make an issue - TB offered to help.

Proposal for moving cppTango to C++17: https://gitlab.com/tango-controls/TangoTickets/-/issues/97 
Currently at the steering committee for review/acceptance.  
Since Guifre Cuni is leaving ALBA, the new coordinator for Tango-Controls is now Vincent Hardion.  
Vincent will bring the question about C++17 to the Steering Committee.
- [ ] Action on Vincent to follow up the cppTango C++17 moving proposal with the steering committee

### Next Meetings
https://indico.tango-controls.org/category/6/
- The next Tango Kernel follow-up meeting will take place on Thursday 9th November 2023 at 15:00 **CET**.
- The next cppTango follow-up meeting will take place on Thursday 2nd November 2023 at 16:00 **CET**.
- The next PyTango follow-up meeting will take place on Thursday 2nd November 2023 at 15:00 **CET**.

## Summary of remaining actions

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues
  Stéphane to ping Gwenaëlle about these actions.

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Follow up on ESRF Gitlab Windows runner
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).

**Sergio (ALBA)**:
- create a ticket in cppTango to add the possibility to change the starter levels easily

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.

**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904

- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.

- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB

**Vincent Hardion (MaxIV)**:
- Organize TC Cybersecurity SIG Meeting in Feb 2024?!
  - [ ] Send poll
  - choose place
- Follow up the cppTango C++17 moving proposal with the steering committee

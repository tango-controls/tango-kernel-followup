# Tango Kernel Follow-up Meeting - 2023/10/26

To be held on 2023/10/26 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-10-26-a3zp?lang=en
# Agenda

## ICALEPCS 2023 Debriefing
## Status of [Actions defined in the previous meetings](../2023-10-12/Minutes.md#summary-of-remaining-actions)
## High priority issues
## Issues requiring discussion
## AOB

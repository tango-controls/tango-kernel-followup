# Tango Kernel Follow-up Meeting
Held on 2023/03/23 at 15:00 CET on Zoom.
Framapad: https://mensuel.framapad.org/p/tango-kernel-teleconf-2023-03-23-9zzo?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (MAX IV)
- Damien Lacoste (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Thomas Ives (OSL)
- Vincent Hardion (MAX IV)
- Thomas Braun (byte physics e.K.)
- Gwenaëlle Abeillé (SOLEIL)
- Lorenzo Pivetta (Elettra)
- Ulrik Pedersen (OSL)
- Thomas Juerges (SKAO)

## Status of [Actions defined in the previous meetings](../2023-03-09/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with
  your facility until next kernel meeting. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)
> SOLEIL and ESRF are investigating.  Elettra are talking with their ICT team as well about hosting runners.
cppTango CI has been modified to use our group runners,  but still some issues with dind & docker runners.
ALBA are exploring Windows runners.

> Test project to test the runners:
https://gitlab.com/tango-controls/test-institute-runners

**All kernel members**:
- Read Tango DB Refactoring
  [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and
  [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing)
  and comment
> The proposal needs to be submitted to the steering committee so they can decide how to work with this.  Questions for steering committee:  Should it be done, do they agree with the work packages:  should we continue with detailed estimates of the effort?  Intent is to improve the quality.  Maintainabilty, readability, testability, ...
Guesstimate is a few months of work (1 person?).

Other item for Steering Committee:  gitlab owners/maintainer/developers definitions.  And date/location for the next Tango community meeting.

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Anton (MaxIV)**:
- Make sure PyTango (builder) CI is using a tag for the Tango IDL
> Yes, 5.1.1.  Anton also got SKAO to update their relevant Docker image to use a tag as well (5.1.2).

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure
> Not done.

**Lorenzo, Thomas J**:
- To take the proposal on Gitlab permissions forward,
  write down the details of the proposal with concrete numbers, and then
  communicate with Steering Committee.
  Link to the proposal in [TangoTickets 63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63)
> Done, ready for the SC.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
> No update.
- Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM.
> Waiting for Elettra staff to create MR.

**Nicolas (ESRF)**:
- Create a merge request in tango-idl for first v6 changes
> Done: https://gitlab.com/tango-controls/tango-idl/-/merge_requests/15

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
> No update
- make cppTango CI use a tag for the tango IDL.  
>  **Action**:  
    - [x] Thomas B to make an issue on cppTango for this. Done after the meeting: see https://gitlab.com/tango-controls/cppTango/-/issues/1082 and https://gitlab.com/tango-controls/cppTango/-/issues/1083
- Ask Gwen to do the same to jTango.
> Already there.

**Sergi (ALBA)**:
- Finalise the theme for the SIG meeting which will take place in ALBA (CICD?, IDLv6?, Both?).
> Done. It will be an IDLv6 SIG Meeting

**Thomas B. (byte physics)**:
- Get number of participants from https://dud-poll.inf.tu-dresden.de/anonymous/cmake-training/

> -> Results: 9 interested people (7 on-site in france, 9 online)  
Course would be 2 days for up to 15 seats.  Online is preferred.  
Cost for online training is 5k€  
We don't take the additional cmake consultancy option.  
**Action**  
> - [ ] Lorenzo:  In Steering Committee meeting ask if they will cover the fee.

- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

- Add tags for TSD windows installer release of 9.4.1

**Thomas I. (OSL)**:
- Get installer into main/9.3-backports branch of TangoSourceDistribution
> Merged for main branch.  Need to tag the various device servers and then TSD itself (Thomas B) for 9.4.x.

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
  Thomas J presented a hands-on course in India.  Lots of code, few slides.
  Seems like a good model for future workshops.
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
There will be a Tango workshop at ICALEPCS (weekend before conference).
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
   - attendees liked the open nature of the workshop - they could ask about
     areas of interest (e.g., green modes, debugging, working in containers,
     ...) and the workshop would focus on those topics.
     Miro board with feedback: https://miro.com/app/board/uXjVP9_nOI0=/?share_link_id=194456711949
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

**Ulrik (OSL)**:
- retire Tango-Controls images on Docker Hub, but will need some help.  
  Discussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71  
  Plan:  
      - prepare docker compose file that references correct images.  
      - update documentation with the "right" method:
        https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html  
      - delete all old images
> Docs updated on a branch.  Was waiting on SKAO image for PyTango 9.4.1, but that was published today, so can continue.

## High priority issues

- cppTango empty hostname fix - needs to be tested: https://gitlab.com/tango-controls/cppTango/-/merge_requests/1065

## Issues requiring discussion

## AOB
- omniorb and compilation issue on Windows  
Could use dumpbin tool to see the differences in the libraries.  Thomas Ives can help.

- Damien added some indexes to Tango database at ESRF.  This improved performance when restarting whole control system.
**Action**:
    - [ ] Damien to make an issue on TangoDatabase repo that shows the benefit.

### Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango)  
The goal is to find incompatibilities.  
Step 1:  create proposal on TangoTickets.  
Step 2:  get someone to start building it - Thomas Ives is keen.
- [ ] Anton to create proposal, with some phases to the implementation.

### Next SIG Meetings

At ALBA.  Date is probably 2nd week of May.  IDLv6 discussion.

CICD workshop will be at a later date.  Show and tell.  Talk about what we can share.  SKAO HQ might be a location.  Probably after ICALEPCS.

Likely timeline:  IDLv6 Meeting, Documentation Workshop, Community Meeting, ICALEPCS, CICD workshop.

### Next Teleconf Meetings
- The next Tango Kernel Teleconf meeting will take place on Thursday 13th April 2023 at 15:00 CEST.
- The next cppTango Teleconf meeting will take place on Thursday 30th March 2023 at 14:00 CEST.
- The next PyTango Teleconf meeting will take place on Thursday 6th April 2023 at 15:00 CEST.

## Summary of remaining actions

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)  
Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners

**Steering Comittee members**:
- Read Tango DB Refactoring
  [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and
  [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing)
  , comment and decide how to work with this.  
  Questions for steering committee:  
    - Should it be done?  
    - Do they agree with the work packages?  
    - Should we continue with detailed estimates of the effort?  
  Intent is to improve the quality.  Maintainabilty, readability, testability, ...  
  Guesstimate is a few months of work (1 person?).  
- gitlab owners/maintainer/developers definitions.  
- Date/location for the next Tango community meeting.

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Anton (MaxIV)**:  
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.  
  Step 1:  create proposal on TangoTickets. ==> Anton  
  Step 2:  get someone to start building it - Thomas Ives is keen.

**Damien (ESRF)**:
- to create an issue on TangoDatabase repo that shows the benefit of creating some indexes

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
- Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM.
- In Steering Committee meeting ask if they will cover the fee for the CMake Training

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table
- Add tags for TSD windows installer release of 9.4.1

**Thomas I. (OSL)**:
- Get installer into main/9.3-backports branch of TangoSourceDistribution
> Merged for main branch.  Need to tag the various device servers and then TSD itself (Thomas B) for 9.4.x.

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
  Thomas J presented a hands-on course in India.  Lots of code, few slides.
  Seems like a good model for future workshops.
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
There will be a Tango workshop at ICALEPCS (weekend before conference).
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

**Ulrik (OSL)**:
- retire Tango-Controls images on Docker Hub, but will need some help.  
  Discussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71  
  Plan:  
      - prepare docker compose file that references correct images.  
      - update documentation with the "right" method:
        https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html  
      - delete all old images

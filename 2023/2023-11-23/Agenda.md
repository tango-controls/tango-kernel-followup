# Tango Kernel Follow-up Meeting - 2023/11/23

To be held on 2023/11/23 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-11-23-a4ix?lang=en
# Agenda

## Status of [Actions defined in the previous meetings](../2023-11-09/Minutes.md#summary-of-remaining-action)
## High priority issues
## Issues requiring discussion
## Pytango 9.5.0 release
## AOB

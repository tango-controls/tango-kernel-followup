# Tango Kernel Follow-up Meeting
Held on 2023/11/23 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-11-23-a4ix?lang=en

**Participants**:

- Andy Götz (ESRF)
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (MAX IV)
- Lukasz Zytniak (S2Innovation)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergio Rubio (ALBA)
- Thomas Braun  (byte physics e.K.)
- Thomas Juerges (SKAO)
- Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../2023-11-09/Minutes.md#summary-of-remaining-actions)

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)
> Andy in contact, but no resolution yet.

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.
>  No update.

**Becky (OSL)**:
- Ask Thomas Ives to enable 2FA on his gitlab.com account

> Done


**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues
> Not present.

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.
> Not present.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
> Some progress. Problem reproduced once and wireshark investigations ongoing.
JTango Client thinks no heartbeats are coming, but we can see heartbeats events on the wire.

- Follow up on ESRF Gitlab Windows runner
> No news
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
> No news
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).
> Waiting for T-J e-mail

**Sergio (ALBA)**:
- create a ticket in cppTango to add the possibility to change the starter levels easily
> Is this a cppTango Database class API change or DatabaseDS change?  Easier if we change it in DatabaseDS - add some new commands.  Already done - look at GetServerInfo and PutServerInfo?  
https://pytango.readthedocs.io/en/stable/database.html#tango.Database.get_server_info and https://pytango.readthedocs.io/en/stable/database.html#tango.Database.put_server_info
DONE: checked those commands and they work
- push test code fixing event subscription performance issues as draft
> still working on this.  Initial tests (subscribe in background) didn't improve performance.

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.
  Not present.

**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904
> Has been started - see message on Slack.  Need some feedback from kernel devs. Message: https://tango-controls.slack.com/archives/CQV9F2BTJ/p1699964344432269 FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
> low priority.
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.
> started on this today.

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB
> Windows runner is being used for PyTango.  Much faster, and saves us 700 minutes on shared runners per commit to main branch.  
> Shell runner, so maybe not a good idea to share with other projects - cleaning up environments isn't that easy. 
> cppTango would also like his own runner, since shared runners are slow to boot and run.  
> Could use DESY runner, but we need clearly define what is to be installed/removed per run.  Need cmake and Visual Studio 2022.
Action:  Yury and TB to discuss further and try this out.

**Vincent Hardion (MaxIV)**:
- Organize TC Cybersecurity SIG Meeting in Feb 2024?!
  - [ ] Send poll
  - [ ] Choose place
> proposal to do it at MAX IV.
- Follow up the cppTango C++17 moving proposal with the steering committee (https://gitlab.com/tango-controls/TangoTickets/-/issues/97)
> Consider it accepted.  Andy will mention it again at tomorrow's committee meeting.

## High priority issues

None.

## Issues requiring discussion

## Pytango 9.5.0 release

Done!  Next 10.0.0, which will include IDL v6.

## AOB

IDLv6 and cppTango v10 work?  IDLv6 device_6 has been defined and merged.  cppTango has MRs open related to telemetry.  
Discussion ongoing related to data structure for version info (key-value pairs).  
There is some risk that telemetry won't be available in other languages soon enough (Java and Python).
Re: prioritisation of what else should be done by (insert person here): We have several proposals besides the IDLv6 roadmap that "just" need to be implemented.
There are many discussion in the cppTango meetings, so join if you are interested.

GitLab repo archive with submodules: https://gitlab.com/tango-controls/gitlab-ci-templates/-/merge_requests/10

Is anyone using new Tango Virtual Box?  Yes - TJ wil discuss with S2Innovation.

omniORB 4.3 packaging on Debian?  cppTango 9.5 requires this.  Backport to Bookworm (current stable).  
Yes, we should make this available so that cppTango can be compiled on the latest stable release.  
**Action**: TB will ask Freexian to do this.

### Next Meetings
https://indico.tango-controls.org/category/6/
- The next Tango Kernel follow-up meeting will take place on Thursday 14th December 2023 at 15:00 CET.
- The next cppTango follow-up meeting will take place on Thursday 30th November 2023 at 16:00 CET.
- The next PyTango follow-up meeting will take place on Thursday 7 December 2023 at 15:00 CET.

## Summary of remaining actions

**All**: 
- Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Follow up on ESRF Gitlab Windows runner
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).

**Sergio (ALBA)**:
- push test code fixing event subscription performance issues as draft

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.
- Ask Freexian to Backport omniORB 4.3 package to Bookworm (current stable) so that cppTango can be compiled on the latest stable release.

**Thomas J. (SKAO)**:
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB (try DESY runner with cppTango)

**Vincent Hardion (MaxIV)**:
- Organize TC Cybersecurity SIG Meeting in Feb 2024?!
  - [ ] Send poll
  - [X] Choose place (=> MAX IV)
- Follow up the cppTango C++17 moving proposal with the steering committee (https://gitlab.com/tango-controls/TangoTickets/-/issues/97)

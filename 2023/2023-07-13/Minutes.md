# Tango Kernel Follow-up Meeting
Held on 2023/07/13 at 15:00 CEST on Zoom.
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-07-13-a227

**Participants**:

- Thomas Braun  (byte physics e.K.) 
- Andy Goetz (ESRF)
- Lukasz Zytniak (S2 Innovation)
- Sergi Rubio (Alba)
- Benjamin Bertrand (MaxIV)
- Thomas Ives (OSL)
- Yury Matveev (Desy)
- Graziano Scalamera (Elettra)
- Ulrik Pedersen (OSL)
- Becky Auger-Williams (OSL)
    
## Status of [Actions defined in the previous meetings](../2023-06-08/Minutes.md#summary-of-remaining-actions)

**All Institutes**
-Ask if your institute can host some Gitlab runners. Please add comments on TangoTickets #77
  Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners

  We need a list of people to approach, if a runner is broken.

  Important to keep runners updated.  Can we automate this?

Soleil will try to add some Windows runners.

Sergio will try get a windows runner in september

Yury asks IT for another server with windows and gitlab runner

-Identify missing responsible persons (owner) from other institutes for gitlab user grooming

  ELI Beamlines: ?

  INAF: ?

  SOLARIS:
    Ireneusz Zadworny (@izadworny)

pytango 9.4.2rc1 is released, uses cppTango 9.4.2. please test, release is planned in the next two weeks.

**Andy (ESRF)**:
  Contact ELI Beamlines for Gitlab User Grooming
    -> Done

**Anton (MaxIV)**:
  Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.

Not present

**Benjamin (MaxIV)**:
  Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  Done

**Gwenaëlle (SOLEIL)**:
  Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues

Not present

**Johan Venter (SARAO)**:
  Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

Not present

**Lorenzo (Elettra)**:
  Gitlab Runners: Follow up with Elettra IT Team.

Not present

**Lukasz (S2Innovation)**:
  Contact Solaris for Gitlab User Grooming

Done

**Reynald (ESRF)**:
  JTango: Events are not properly reported, investigate issue and create ticket

  Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  See if we can create a section for "regular meetings" or similar in https://indico.tango-controls.org Reynald will create new event categories for the different regular meetings.

  Kill dockerhub organization (tango-cs?!) of tango-controls once https://gitlab.com/tango-controls/tango-doc/-/merge_requests/406 is merged

Not present and no news.

**Sergi (ALBA)**:
  Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

Will tell Guifre to look into that

**Thomas B. (byte physics)**:
  Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)

  Task: Ask every tango-controls member to add their institute/comany in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.

  Task: Review https://gitlab.com/tango-controls/tango-doc/-/merge_requests/406

No progress, but will soon happen.

**Thomas J. (SKAO)**:
  Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904

  Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  This action is suspended for the moment. We will wait for the outcome of Ulrik's investigation.

  The outcome was that we will drop docker hub and instead use SKAO published images. Tango docs to be updated as per TangoTicket 71 - awaiting merge of Tango docs MR406.

  https://gitlab.com/tango-controls/TangoTickets/-/issues/71

  https://gitlab.com/tango-controls/tango-doc/-/merge_requests/406

  Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  Contact Matteo Canzari for the Gitlab User Grooming

Not present

**Yury (DESY)**:
  Try to put DESY Gitlab Runners back online.

Works again.

  Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it

Done

## High priority issues
Tango 7 and Tango9 compatibility, MR from Jairo
https://gitlab.com/tango-controls/cppTango/-/issues/1135

Review MR and get it merged.

## Issues requiring discussion
Andy wants to expose TangoGraphQL server to the internet, but needs a solid security in the tangodatabase for that. Currently this is not the case, see e.g. https://gitlab.com/tango-controls/TangoDatabase/-/issues/32

This needs to be fixed by September 2023.

## AOB
Proposal to deprecate TangoAccessControl

-> ESRF uses it according to Andy, not for security but for fat finger errors
   Use case:
     - Separate beamlines against each other
    Look into possible replacements.
Benjamin:
    - Have separate networks for each beamline
Sergi:
    - Uses gateways
Graziano:
    - Are looking into adopting TangoAccessControl?!

Maybe we need to rename the DS and repo and make it clearer what are the limitations?

## Summary of remaining actions

**All Institutes**
-Ask if your institute can host some Gitlab runners. Please add comments on TangoTickets #77
  Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners

  We need a list of people to approach, if a runner is broken.

  Important to keep runners updated.  Can we automate this?

-Identify missing responsible persons (owner) from other institutes for gitlab user grooming

  ELI Beamlines: ?

  INAF: ?

**Anton (MaxIV)**:
  Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.

**Gwenaëlle (SOLEIL)**:
  Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues

**Johan Venter (SARAO)**:
  Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

**Lorenzo (Elettra)**:
  Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
  JTango: Events are not properly reported, investigate issue and create ticket

  Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  See if we can create a section for "regular meetings" or similar in https://indico.tango-controls.org Reynald will create new event categories for the different regular meetings.

  Kill dockerhub organization (tango-cs?!) of tango-controls once https://gitlab.com/tango-controls/tango-doc/-/merge_requests/406 is merged

**Sergi (ALBA)**:
  Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

**Thomas B. (byte physics)**:
  Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)

  Task: Ask every tango-controls member to add their institute/comany in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.

  Task: Review https://gitlab.com/tango-controls/tango-doc/-/merge_requests/406

**Thomas J. (SKAO)**:
  Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904

  Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  This action is suspended for the moment. We will wait for the outcome of Ulrik's investigation.

  The outcome was that we will drop docker hub and instead use SKAO published images. Tango docs to be updated as per TangoTicket 71 - awaiting merge of Tango docs MR406.

  https://gitlab.com/tango-controls/TangoTickets/-/issues/71

  https://gitlab.com/tango-controls/tango-doc/-/merge_requests/406

  Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  Contact Matteo Canzari for the Gitlab User Grooming

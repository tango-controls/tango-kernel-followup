# Tango Kernel Follow-up Meeting - 2023/07/13

To be held on 2023/07/13 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-07-13-a227

# Agenda

1. Status of [Actions defined in the previous meetings](../2023-06-08/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. AOB

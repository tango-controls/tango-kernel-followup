# Tango Kernel Follow-up Meeting
Held on 2023/09/28 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-09-28-a3hn?lang=en

**Participants**:

- Andy Goetz (ESRF)
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (MAX IV)
- Lorenzo Pivetta (Elettra)
- Lukasz Zytniak (S2Innovation)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio Manrique (Alba)
- Thomas Braun  (byte physics e.K.) 
- Thomas Ives (OSL)
- Thomas Juerges (SKAO)
- Ulrik Pedersen (OSL)
- Yury Matveev (DESY)


## Status of [Actions defined in the previous meetings](../2023-09-14/Minutes.md#summary-of-remaining-actions)

**Andy (ESRF)**: Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)  
> Will talk directly to them during ICALEPCS

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.  
  > No news.

**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues  

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket
> A project dedicated to this issue is being created at the ESRF to follow up this issue.  
Nicolas is proposing to send a survey to the other institutes to see whether they are seeing similar problems or not.

  - Follow up on ESRF Gitlab Windows runner
> No news
  
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
> Still waiting for some colleagues to come back from holidays

  - Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).

**Thomas B. (byte physics)**:  
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)  

> No update
  
  - Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.

> No update

**Thomas J. (SKAO)**:  
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904  
> Not done. He will do it after ICALEPCS. Promised. He would need a higher Gitlab role, because he cannot modify anything on the gitlab.com/tango-controls.

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
> Not done. He will do it after ICALEPCS.

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.
> Not done. He will do it after ICALEPCS.

  - Clean new paper for ICALEPCS from old text. Try to preserve the new content.
> Done. New document was created.

  - Share link to the slides for the collaborative ICALEPCS presentation
> Done.  (Others in meeting don't know where this is). T-J will add it as bookmark in the ICALEPCS-2023 slack channel.

  - Ask for an overleaf paid account and share the link to a new Overleaf project (Starting from the JACoW latest template).
> It looks like there is no issue to collaborate without an enterprise account the way we are doing it now. T-J wrote that SKAO will have an enterprise account with all features and unlimited collaborators.

**Yury (DESY)**:
  - Gitlab Runners: Follow up with DESY IT Team.  
> Ready and running (Windows shell runner).  TB to discuss what software is installed with YM.


## ICALEPCS 2023

### Paper and Presentation Status

Paper:  4 pages so far.   Reynald will ping Gwen.  TangoBox:  Tomasz Noga will do an update (Lukasz will ask him).   
Final submission will be done by Thomas J.   
Deadline for contributions is **4th October**.  (Submission will be on 6th October).

Other papers:  HDB++.  Taranta.

### Tango Workshop Preparation Status
TJ has created a WIP page for Venue/HW/SW that we need. When he will be in Cape Town next week, he will try and take care of these things together with Devon Petrie. Linked at the top of the ICALEPCS2023 channel.  
Speakers/trainers:  
    - Please read Devon's email! it contains important information about what technical equipment is available etc..  
    - Please head over to the linked doc and add/edit/discuss what you need or want.

## High priority issues

cppTango and ZeroMQ "buggy releases".  Can anyone else reproduce it?   https://gitlab.com/tango-controls/cppTango/-/issues/1126#note_1578846063
> Lorenzo will ask Graziano to report on how to reproduce, and symptoms, and comment on issue.

Fix tango 7 to tango 9.4 events - https://gitlab.com/tango-controls/cppTango/-/merge_requests/1155.  Will be discussed further in next cppTango meeting.  

cppTango 9.5.0 release will be delayed 1 week, to allow for more testing of release candidate.

cppTango release cycle will be shifted forward, so that more people are available for testing.   
Proposed targets are 2 May and 2 November.   
This also gives PyTango developers some time before holiday periods start.

## Issues requiring discussion
 

## AOB

TangoStarter: anyone using the automatic restart on crash successfully?  ESRF are using 7.5 - device server needs to have been running for 2h, by default.  
Interesting conversion from minutes to seconds if not debug mode?  Might be a way to speed up testing.  
https://gitlab.com/tango-controls/starter/-/blob/Starter-7.7/Starter.cpp?ref_type=tags#L1544-1545

### Next Meetings
https://indico.tango-controls.org/category/6/
- The next Tango Kernel follow-up meeting will take place on Thursday 12th October 2023 at 15:00 CEST.  
- The next cppTango follow-up meeting will take place on Thursday 5th October 2023 at 16:00 CEST.  
- The next PyTango follow-up meeting will take place on Thursday 5th October 2023 at 15:00 CEST.  

## Summary of remaining actions

**All TANGO CONTROLS COLLABORATION STATUS IN 2023 ICALEPCS paper contributors**: Contribute to the paper (overleaf link as bookmark in icalepcs-2023 slack channel). Deadline for contributions is 4th October to give time for review. Submission will be on 6th October.

**Andy (ESRF)**: Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.  

**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues  

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket
  - Follow up on ESRF Gitlab Windows runner
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  - Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).

**Thomas B. (byte physics)**:  
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)  
  - Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
  - Test DESY Gitlab runners and check with Yury that all required software is installed.

**Thomas J. (SKAO)**:  
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904  
> Will be done after ICALEPCS.

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
> Will be done after ICALEPCS.

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.
> Will be done after ICALEPCS.

  - Share link to the slides for the collaborative ICALEPCS presentation. Add it as bookmark in the ICALEPCS-2023 slack channel.

**Yury (DESY)**:
  - Gitlab Runners: Follow up with TB

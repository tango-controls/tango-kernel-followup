# Tango Kernel Follow-up Meeting - 2023/09/28

To be held on 2023/09/28 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-09-28-a3hn?lang=en
# Agenda

## Status of [Actions defined in the previous meetings](../2023-09-14/Minutes.md#summary-of-remaining-actions)
## ICALEPCS
### Paper and Presentation Status
### Tango Workshop Preparation Status
## High priority issues
## Issues requiring discussion
## AOB


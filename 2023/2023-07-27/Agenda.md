# Tango Kernel Follow-up Meeting - 2023/07/27

To be held on 2023/07/27 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-07-27-a2bm?lang=en

# Agenda

## Status of [Actions defined in the previous meetings](../2023-07-13/Minutes.md#summary-of-remaining-actions)
## High priority issues
## Issues requiring discussion
## AOB

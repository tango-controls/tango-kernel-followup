# Tango Kernel Follow-up Meeting
Held on 2023/07/27 at 15:00 CEST on Zoom.
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-07-27-a2bm?lang=en

**Participants**:

- Thomas Braun  (byte physics e.K.) 
- Sergi Rubio (Alba)
- Benjamin Bertrand (MaxIV)
- Thomas Ives (OSL)
- Yury Matveev (Desy)
- Ulrik Pedersen (OSL)
- Becky Auger-Williams (OSL)
- Reynald Bourtembourg (ESRF)
- Thomas Juerges (SKAO)
- Vincent Hardion (MAX IV)
    
## Status of [Actions defined in the previous meetings](../2023-07-13/Minutes.md#summary-of-remaining-actions)

**All Institutes**

- Ask if your institute can host some Gitlab runners. Please add comments on TangoTickets #77  
  > Desy will setup soon some Windows Gitlab Runners for the Tango community when the sys admin are back from holidays.   

  Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners

  We need a list of people to approach, if a runner is broken.  
  > --> The list is available here https://gitlab.com/tango-controls/test-institute-runners#available-runners

  Important to keep runners updated.  Can we automate this?
  > Benjamin created [TangoTickets#91](https://gitlab.com/tango-controls/TangoTickets/-/issues/91) with the recipe used by MaxIV to automate this


- Identify missing responsible persons (owner) from other institutes for gitlab user grooming

  ELI Beamlines: Andy will contact them.

  INAF: Vincent will ask Matteo Canzari
  From INAF Just Matteo Canzari and Matteo Di Carlo have a gitlab account. Should they still define a owner and grooming the users? Answer => Yes. They should define an owner and do the grooming of the INAF users from time to time (New INAF users might come in the future), which should be quick if they are the only 2.

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.

**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues

**Johan Venter (SARAO)**:
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  - See if we can create a section for "regular meetings" or similar in https://indico.tango-controls.org Reynald will create new event categories for the different regular meetings.

  - Kill dockerhub organization (tango-cs?!) of tango-controls once https://gitlab.com/tango-controls/tango-doc/-/merge_requests/406 is merged. 
   > https://gitlab.com/tango-controls/tango-doc/-/merge_requests/406 is merged now and documentation updated but documentation is not deployed because of https://gitlab.com/tango-controls/tango-doc/-/issues/392 (t-b will try to fix the deployment).  
   Wait for TB to fix the publishing to RTD and TJ to send the e-mail and slack messages.

**Sergi (ALBA)**:
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  > Done on ALBA side, Guifre did it.

**Thomas B. (byte physics)**:
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)

  - Task: Ask every tango-controls member to add their institute/comany in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.

  - Task: Review https://gitlab.com/tango-controls/tango-doc/-/merge_requests/406 
  >  Done

**Thomas J. (SKAO)**:
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  This action is suspended for the moment. 
  > Wait for TB to fix the publishing to RTD.

  - The outcome was that we will drop docker hub and instead use SKAO published images. Tango docs to be updated as per TangoTicket 71 - was awaiting merge of Tango docs MR406.

  - https://gitlab.com/tango-controls/TangoTickets/-/issues/71 
  > Done

  - https://gitlab.com/tango-controls/tango-doc/-/merge_requests/406
  > Done

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  - Contact Matteo Canzari for the Gitlab User Grooming
> Done. Nothing further to report. But Vincent will talk to him tomorrow.


## High priority issues
Tango 7 and Tango 9 compatibility, MR from Jairo
https://gitlab.com/tango-controls/cppTango/-/issues/1135

Event subscription performances: Sergi will try to use the stateless option and also dig on how to use the existing cppTango Delayed Event subscription thread. Sergi would like to have an asynchronous subscription method.  
Another possibility would be to have ways to group subscriptions.  
There is a dedicated slack channel on this hot topic: #temp-event-subscription-performance

## Issues requiring discussion

## AOB

### PyTango 9.4.2
PyTango 9.4.2 is about to be released.

### SIG Meetings
An idea for another SIG Meeting is the Polling Algorithm.

### Icalepcs

Our Tango Control status update paper has been accepted as an oral contribution.  
Follow-up on this topic on #icalepcs-2023 slack channel. The overleaf link to the paper preparation is a bookmark of #icalepcs-2023 slack channel.

### Next Meetings
- The next Tango Kernel Teleconf meeting will take place on Thursday 10th August 2023 at 15:00 CEST.  
- The next cppTango Teleconf meeting will take place on Thursday 3rd August 2023 at 14:00 CEST.  
- The next PyTango Teleconf meeting will take place on Thursday 3rd August 2023 at 15:00 CEST.  

## Summary of remaining actions

**All Institutes**

- Ask if your institute can host some Gitlab runners. Please add comments on TangoTickets #77   

  Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners

  The list of people to approach, if a runner is broken is available here: https://gitlab.com/tango-controls/test-institute-runners#available-runners . Please keep it up to date.

  Important to keep runners updated.  See [TangoTickets#91](https://gitlab.com/tango-controls/TangoTickets/-/issues/91) for a recipe.

- Identify missing responsible persons (owner) from other institutes for gitlab user grooming

  ELI Beamlines: Andy will contact them.

  INAF: Vincent will ask Matteo Canzari

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.

**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues

**Johan Venter (SARAO)**:
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  - See if we can create a section for "regular meetings" or similar in https://indico.tango-controls.org Reynald will create new event categories for the different regular meetings.

  - Kill dockerhub organization (tango-cs?!) of tango-controls once https://gitlab.com/tango-controls/tango-doc/-/issues/392 is fixed. 

**Thomas B. (byte physics)**:  
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)

  - Task: Ask every tango-controls member to add their institute/comany in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.

  - Fix https://gitlab.com/tango-controls/tango-doc/-/issues/392

**Thomas J. (SKAO)**:  
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  This action is suspended for the moment, waiting for TB to fix the publishing to RTD.

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

**Yuri (DESY)**:
  - Gitlab Runners: Follow up with DESY IT Team.


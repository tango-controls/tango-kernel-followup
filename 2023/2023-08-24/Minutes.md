# Tango Kernel Follow-up Meeting
Held on 2023/08/24 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-08-24-a2u9?lang=en

**Participants**:

- Thomas Braun  (byte physics e.K.) 
- Becky Auger-Williams (OSL)
- Reynald Bourtembourg (ESRF)
- Thomas Juerges (SKAO)
- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Ulrik Pedersen (OSL)
- Sergi Rubio Manrique (Alba)
- Thomas Ives (OSL)
- Vincent Hardion (MAX IV)
- Lukasz Zytniak (S2Innovation)

## Status of [Actions defined in the previous meetings](../2023-08-10/Minutes.md#summary-of-remaining-actions)

**All Institutes**

- Ask if your institute can host some Gitlab runners. Please add comments on TangoTickets #77   

  Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners

  The list of people to approach, if a runner is broken is available here: https://gitlab.com/tango-controls/test-institute-runners#available-runners . Please keep it up to date.

  Important to keep runners updated.  See [TangoTickets#91](https://gitlab.com/tango-controls/TangoTickets/-/issues/91) for a recipe.
  
cppTango:  moved from AppVeyor to Gitlab shared Windows runners.  Allows parallel builds, but uses more of the 50k minutes we have per month.  Shouldn't be a lot.
PyTango:  is also moving in this direction (open MR).

This general issue about institute runners can be removed from the actions.

**Andy (ESRF)**: Contact ELI Beamlines about user grooming proposal TangoTickets#63 (comment 1110688846) 

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.  
>  Still no progress.

**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues  
> No update.

**Johan Venter (SARAO)**:
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
> TJ says this is done.

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket
  - Ask about Windows gitlab runner to host/rent/buy

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  - Kill dockerhub organization (tango-cs?!) of tango-controls
> Reynald has access.  Discussing Waltz and REST server images with Igor (who originally set up the repository).

**Thomas B. (byte physics)**:  
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)  

> No progress.
  
  - Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.

> No progress.

**Thomas J. (SKAO)**:  
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904
> No progress

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  
> TJ believes he can do this now?  Yes.

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.
> TJ Forgot to do this. Will do it until next meeting.

**Yury (DESY)**:
  - Gitlab Runners: Follow up with DESY IT Team.  
  
> Not present.

## High priority issues

## Issues requiring discussion

## AOB
cppTango decided that the release on 2023-10-02 will be 9.5.0. The release will be binary incompatible with 9.4.x. IDLv6 changes will be introduced in the 2024-04-02 10.0.0 release. This will allow cppTango more time to implement the features.

### Icalepcs

Follow-up on this topic on #icalepcs-2023 slack channel. The overleaf link to the paper preparation is a bookmark of #icalepcs-2023 slack channel.

Workshop is still being worked.  Speakers include TJ and Sergi.  Speakers need to sync with each other so that they don't overlap.  See tango controls indico page.

**Action - TJ**: Clean new paper from old text. Try to preserve the new content.

### Next Meetings
https://indico.tango-controls.org/category/6/
- The next Tango Kernel Teleconf meeting will take place *in 3 weeks* on Thursday 14th September 2023 at 15:00 CEST.  
- The next cppTango Teleconf meeting will take place on Thursday 31st August 2023 at 14:00 CEST.  
- The next PyTango Teleconf meeting will take place on Thursday 7th September 2023 at 15:00 CEST.  
- The next HDB++ Meeting to work on the ICALEPCS Poster and talk will take place on Thursday 7th September 2023 at 10:00 CEST.  

## Summary of remaining actions

**Andy (ESRF)**: Contact ELI Beamlines about user grooming proposal TangoTickets#63 (comment 1110688846) 

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.  

**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues  

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket
  - Ask about Windows gitlab runner to host/rent/buy

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  - Kill dockerhub organization (tango-cs?!) of tango-controls

**Thomas B. (byte physics)**:  
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)  
  
  - Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.

**Thomas J. (SKAO)**:  
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904  

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.

  - Clean new paper for ICALEPCS from old text. Try to preserve the new content.

**Yury (DESY)**:
  - Gitlab Runners: Follow up with DESY IT Team.  


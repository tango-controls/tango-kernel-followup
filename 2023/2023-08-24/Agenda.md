# Tango Kernel Follow-up Meeting - 2023/08/24

To be held on 2023/08/24 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-08-24-a2u9?lang=en
# Agenda

## Status of [Actions defined in the previous meetings](../2023-08-10/Minutes.md#summary-of-remaining-actions)
## High priority issues
## Issues requiring discussion
## AOB
### ICALEPCS

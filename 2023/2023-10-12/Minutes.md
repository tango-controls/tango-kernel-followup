# Tango Kernel Follow-up Meeting
Held on 2023/10/12 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/2023-10-12-tango-kernel-meeting-a3r0?lang=en

**Participants**:

- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun  (byte physics e.K.) 
- Thomas Ives (OSL)
- Yury Matveev (DESY)


## Status of [Actions defined in the previous meetings](../2023-09-14/Minutes.md#summary-of-remaining-actions)

**All TANGO CONTROLS COLLABORATION STATUS IN 2023 ICALEPCS paper contributors**: Contribute to the paper (overleaf link as bookmark in icalepcs-2023 slack channel). Deadline for contributions is 4th October to give time for review. Submission will be on 6th October.
> DONE!

**Andy (ESRF)**: Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.  
> Not done.

**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues  

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket
  - Follow up on ESRF Gitlab Windows runner
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  - Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).
  
>  No progress

**Thomas B. (byte physics)**:  
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)  
  - Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
  - Test DESY Gitlab runners and check with Yury that all required software is installed.
  
>  No progress

**Thomas J. (SKAO)**:  
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904  
> Will be done after ICALEPCS.

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
> Will be done after ICALEPCS.

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.
> Will be done after ICALEPCS.

  - Share link to the slides for the collaborative ICALEPCS presentation. Add it as bookmark in the ICALEPCS-2023 slack channel.
  
>  Done

**Yury (DESY)**:
  - Gitlab Runners: Follow up with TB
> No progress


## High priority issues


## Issues requiring discussion
 
 cppTango 9.5.0 released!  Thanks to all contributors, including TB who did the actual release.  And BB for conda forge updates.

PyTango has a few merge requests to finalise before first release candidate.  
Aim for first release candidate by end of next week.  

ZMQ bug? Has anyone else managed to reproduce?  No updates.  

PyTango has a segfault in server_init_hook.  https://gitlab.com/tango-controls/pytango/-/issues/551. However, when running through debugger we don't get the crash.  Suggestion is to configure Linux to create a core dump file.  cppTango even do this in CI.  

TangoTest - can we add an echo mode?  I.e., when you write to an attribute, can you read back the same value?  Currently, the automatic generation replaces the values.   Yes, we could add an attribute to toggle this, or maybe some commands.   Yury, will work on this.  Suggest he creates an issue first (have a look at mthreaded_impl, in the code)  

What happens next in cppTango?  Work towards v10.0.0 with IDLv6 changes for May 2024.

## AOB


### Next Meetings
https://indico.tango-controls.org/category/6/
- The next Tango Kernel follow-up meeting will take place on Thursday 26th October 2023 at 15:00 CEST.  
- The next cppTango follow-up meeting will take place on Thursday 19th October 2023 at 16:00 CEST.  
- The next PyTango follow-up meeting will take place on Thursday 19th October 2023 at 15:00 CEST.  

## Summary of remaining actions

**Andy (ESRF)**: Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.  

**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues  

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket
  - Follow up on ESRF Gitlab Windows runner
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
  - Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).

**Thomas B. (byte physics)**:  
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)  
  - Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
  - Test DESY Gitlab runners and check with Yury that all required software is installed.

**Thomas J. (SKAO)**:  
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904  

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.

**Yury (DESY)**:
  - Gitlab Runners: Follow up with TB

# Tango Kernel Follow-up Meeting - 2023/10/12

To be held on 2023/10/12 at 15:00 CEST on Zoom.  
Framapad: 
# Agenda

## Status of [Actions defined in the previous meetings](../2023-09-28/Minutes.md#summary-of-remaining-actions)
## High priority issues
## Issues requiring discussion
## AOB


# Tango Kernel Follow-up Meeting
Held on 2023/06/08 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-06-08

**Participants**:

- Andy Goetz (ESRF)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (MAX IV)
- Łukasz Żytniak (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Thomas Ives (OSL)
- Lorenzo Pivetta (Elettra)
- Vincent Hardion (MAX IV)
- Thomas Juerges (SKAO)
- Gwenaëlle Abeillé (SOLEIL)
- Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../2023-05-25/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)  
Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners  
We need a list of people to approach, if a runner is broken.  
Important to keep runners updated.  Can we automate this?

Soleil will try to add some Windows runners. 
Patrick Madela is in touch with Benjamin Bertrand.  
DESY worker offline for 1 month? Yuri will have a look  
Elettra:  Lorenzo to follow up with their IT team

- Identify missing responsible persons (owner) from other institutes for gitlab user grooming  
  ELI Beamlines: Andy will contact ELI Beamlines  
  INAF: ??? (Thomas J will contact Matteo Canzari)  
  ESRF:  in progress  
  ALBA:  still have many owners at group level.  Sergi will discuss internally and decide ALBA owner.  
  > Guifré will be the owner. Grooming in progress.  
  SOLARIS: Lukasz will contact them
  
- Please test the latest rc version of cppTango 9.4.2-rc2
> No issue when running PyTango tests. See https://gitlab.com/tango-controls/pytango/-/issues/530
ALBA: Problem subscribing to State events on pytango client side  
ESRF: difference in the jpeg quality factor values meaning discovered between cppTango 9.3 and cppTango 9.4 (using libjpeg-turbo).  
Comment from TJ: This is not a 9.4.2 issue but due to the fact that we switched to the JPEG-Turbo library in 9.4.

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.  
    > We can drop this action

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.  
  Step 1:  create proposal on TangoTickets. ==> Anton  
  Step 2:  get someone to start building it - Thomas Ives is keen.  

**Damien (ESRF)**:
- To create an issue on TangoDatabase repo that shows the benefit of creating
  some indexes  
  Damien will do some more testing at ESRF.  
  But Graziano's approach is doing the same and more, so should be preferred:
      https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/71

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure
  Benjamin sent a mail to Patrick Madela, who will do it.
>  in progress
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Johan Venter (SARAO)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
>  TJ will ping him.

**Sergi (ALBA)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio or someone else for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
> We decided to close this long standing action

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
> No progress. Soleil saw some issues as well related to events recently.
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
> In progress
- to ask Andy to ask gitlab if we can have some more seats.  
> action suspended until grooming is over to see whether we really need to ask for that.
- CMake Training: to check constraints from kitware.  Update poll, or choose date.
> Done: CMake Training will take place on 21st and 22nd June.
- See if we can create a section for "regular meetings" or similar in https://indico.tango-controls.org
> In progress. Reynald will create new event categories for the different regular meetings.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)
> TB looked into this (https://gitlab.com/tango-controls/TangoDatabase/-/issues/26#note_968364122):
    TL;DR: Mariadb and mysql treat the current SQL table definition differently. It works only with Mariadb.

**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
> No progress
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.
> Waiting for Ulrik
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
> No progress

**Benjamin (MaxIV)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
 >  In progress - cleaning done at the group level
  Done in web subgroup as well. Asked users to enable 2FA.

**Yury (DESY)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

## High priority issues


## Issues requiring discussion



## AOB

### New website

tango-controls.org website has been upgraded.  
We can now add categories, like Blogs for instance.  


### Tango Community Meeting

Hosted by SKAO near Manchester, UK from Tuesday 27 to Thursday 29 June 2023.  
Hybrid.  
Registration and more details on https://indico.tango-controls.org/event/57/

Several presenters for the same presentation is possible but in thic case only 1 document for the slides to make the transition between speakers efficient.  

The deadline for in person registrations is 20 June 2023. If you are joining remotely you can still register up to and including the first day.

### CMake training

CMake training will take place on 21st and 22nd June. 13 participants so far (the ones who replied to the Framadate Poll). 
Still 2 seats available for interested tango-controls contributors (preference will be given to the C++ developers).

### Tango workshop at ICALEPCS 2023

Basic training in the morning.
Quizz/competition during the afternoon.

Timetable is available here: https://indico.tango-controls.org/event/55/timetable/#20231007

### Next Teleconf Meetings
- The next Tango Kernel Teleconf meeting will take place on Thursday 22nd June 2023 at 15:00 CEST. There will be the CMake training at the same time but the Kernel Teleconf Meeting is maintained for the others.
- The next cppTango Teleconf meeting will take place on Thursday 15th June 2023 at 14:00 CEST.
- The next PyTango Teleconf meeting will take place on Thursday 15th June 2023 at 15:00 CEST.

## Summary of remaining actions

- Ask if your institute can host some Gitlab runners. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)  
Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners  
We need a list of people to approach, if a runner is broken.  
Important to keep runners updated.  Can we automate this?

Soleil will try to add some Windows runners. 

- Identify missing responsible persons (owner) from other institutes for gitlab user grooming  
  ELI Beamlines: ?  
  INAF: ?  
  SOLARIS: ?  
  
- Please test the latest rc version of cppTango 9.4.2-rc2

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Contact ELI Beamlines for Gitlab User Grooming

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango).
  The goal is to find incompatibilities.
  Step 1:  create proposal on TangoTickets. ==> Anton
  Step 2:  get someone to start building it - Thomas Ives is keen.

**Benjamin (MaxIV)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Damien (ESRF)**:
- To create an issue on TangoDatabase repo that shows the benefit of creating
  some indexes
  Damien will do some more testing at ESRF.
  But Graziano's approach is doing the same and more, so should be preferred:
      https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/71

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues

**Johan Venter (SARAO)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.

**Lukasz (S2Innovation)**:
- Contact Solaris for Gitlab User Grooming

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
- See if we can create a section for "regular meetings" or similar in https://indico.tango-controls.org
Reynald will create new event categories for the different regular meetings.

**Sergi (ALBA)**:
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.

**Thomas B. (byte physics)**:
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

**Thomas J. (SKAO)**:
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.
- Contact Matteo Canzari for the Gitlab User Grooming

**Yury (DESY)**:
- Try to put DESY Gitlab Runners back online.
- Implement gitlab user grooming proposal
  https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846
  and comment in the thread that you finished it.


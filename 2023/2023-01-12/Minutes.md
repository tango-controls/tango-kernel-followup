# Tango Kernel Follow-up Meeting
Held on 2023/01/12 at 15:00 CET on Zoom.
**Framapad**: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-01-23-9yow?lang=en

**Participants**:
- Lorenzo Pivetta (Elettra)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics)
- Sergi Rubio (ALBA)
- Becky Auger-Williams (OSL)
- Anton Joubert (MAX IV)
- Benjamin Bertrand (MAX IV)
- Yury Matveev (DESY)
- Thomas Ives (OSL)
- Kieran Mulholland (OSL)
- Ulrik Pedersen (OSL)
- Johan Venter (SARAO)
- Thomas Juerges (SKAO)
- Lukasz Zytniak (S2Innovations)
- Grzegorz Kowalski
- Jan David Mol (Astron)

## Status of [Actions defined in the previous meetings](../../2022/2022-12-22/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with your facility until next kernel meeting.
**Thomas B**: Add an issue to TangoTickets that will list the currently available runners.

Done: --> https://gitlab.com/tango-controls/TangoTickets/-/issues/77

> MAX IV has few already.
ESRF cannot host any internally for the moment, but could get some cloud hosted-server.
ALBA:  could probably host some, but haven't discussed officially
SKAO:  Thomas J has created a couple on old personal machines (Linux and MacOS)
Tags are useful when defining runners - what are their capabilities - then jobs can be assigned
Elettra:  haven't asked yet.
SARAO:  haven't asked yet, but unlikely.
DESY:  haven't asked yet.
Astron: waiting on feedback.

- Feature request for "New attribute event ALARM_EVENT". please see
  https://gitlab.com/tango-controls/TangoTickets/-/issues/65 for rationale,
  proposal and impacted TC packages.
  Action: Read, understand, comment
> Discussions ongoing.  Pushing alarm events manually is a good addition.  Last chance for discussions is 13 January 2023.  After that date, Thomas J will create IDL and cppTango issues.
Sergi suggests we should check if this additional event affects performance (once we get to implementation).

**All kernel members**: Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment
> Reynald says Damien has started experimenting with some refactoring.
Lorenzo says they have replaced the engine with InnoDB instead of MyISAM - this is much faster, as fewer tables are locked. Sergi adds that there is less risk of corrupted db files with InnoDB.
**Action - Lorenzo (Elettra)**: Create a ticket or merge request about this.

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Lorenzo, Thomas J**: to take the proposal on Gitlab permissions forward.  Get info about SKA current policy.
Link to the proposal: https://gitlab.com/tango-controls/TangoTickets/-/issues/63
> SKA policy is based on groups (groups of users, not groups of projects/repos).
We will keep our policy as-is for the first implementation.   One owner per institute, a few maintainers, and many developers.
**Action - Thomas J and Lorenzo**:  Next step: write down the details of the proposal on Gitlab permissions with concrete numbers, and then communicate with Steering Committee.

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
> No progress for the moment. Attempts to reproduce the bug but it is not easily reproducible so far.

**Thomas B. (byte physics)**:
- Make a poll to gauge interest in CMake Training by Kitware - ask if time of
  course from https://www.kitware.com/courses/cmake-training/ is OK, or if
  "custom" time required.
- Ask Kitware about custom CMake training
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)
- Look into how to get starter built with C++98.
- Document: "cppTango and starter should work with plain C++98 for all 9.3.x
   releases of the tango source distribution."

> No progress

**Thomas I. (OSL)**:
- Work on MRs for getting cmake enhancements for windows into cpp servers
> Waiting on review for TangoDatabase MR
- Get installer into main/9.3-backports branch of TSD
> Waiting on above MRs to be merged

**Thomas J. (SKAO)**:
- Review comments on Feature request for "New attribute event ALARM_EVENT"
  (https://gitlab.com/tango-controls/TangoTickets/-/issues/65) until 2022-12-13
- Ask SKAO's Team System about gitlab policies until 2022-12-13.
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
>  Thomas J presented a hands-on course in India.  Lots of code, few slides.  Seems like a good model for future workshops.
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
  Ideas for papers:  cppTango release cycle changes.  We should have an ICALECPS channel on Slack to further discuss these things.  "icalepcs-2023".
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Try and find an equivalent of tango-cs in the SKAO images. Provide a Docker
  Compose file with the same functionality. For future, SKAO need to decide
  which images will be the "official" ones. See also https://gitlab.com/tango-controls/docker/tango-cs/-/issues/16.
    => Investigate until 2022-12-13
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

**Ulrik (OSL)**:
- Find out how to retire Tango-Controls images on Docker Hub, ideally with a redirection to the SKAO images.
> Ulrik P looked into deprecating images on Docker hub - not really a thing.  Options:  upload a "broken" image that tells users where to go, what alternative docker images to use.  Or upload image with a warning.
  Breaking image is the only way to ensure people react.
  https://hub.docker.com/u/tangocs
  Can do it for images with "latest" tag, or for all tags.
  We do already have images on gitlab:  https://gitlab.com/tango-controls/docker/tango-cs, so could point people there.
  Plan:
      - prepare docker compose file that references correct images.
      - update documentation with the "right" method:  https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html?highlight=docker
      - delete all old images
  What happens with rest and waltz images?
 SKAO have https://gitlab.com/ska-telescope/ska-tango-images/-/tree/master/images/ska-tango-images-tango-rest
**Action - Ulrik**: to work on this, but will need some help.  Create a ticket on https://gitlab.com/tango-controls/TangoTickets/

## High Priority Issues

## Issues Requiring Discussion

Is there a target date for cppTango 9.4.1 release?  (For usage by PyTango with its 9.4.0)

New due date for cppTango 9.4.1, 27th January 2023, see also https://gitlab.com/tango-controls/cppTango/-/milestones/8#tab-issues

**Action - Reynald (ESRF)**: Update the Serial Tango class to be compatible with cppTango 9.4 so we could run the BLISS tests with latest pyTango release candidates.

### New TangoBox details:
Test version: https://gitlab.com/tango-controls/tangobox/-/issues/56
New TangoBox VM: https://s2innovation.sharepoint.com/:u:/s/Developers/ERU58sTPhAdNpURoJMNMemYBJLwIc2ILBhfnQ6Yma1MaZw?e=dgp53L
**Action - all kernel members and TangoBox users**: Please review the new TangoBox VM (Software versions used, first tests) before next kernel meeting.

## Next SIG Meetings

Taurus workshop @ ESRF 7-8 March.  Hybrid.  Users are also welcome - would be nice to get their perspective.

CI/CD @ ALBA after that, probably April.

## AOB

Astor is now on conda-forge: https://github.com/conda-forge/tango-astor-feedstock
**WARNING!** The package is named "tango-astor". If you "conda install astor" you will get a Python package to manipulate AST: https://github.com/conda-forge/astor-feedstock

### Write the docs camp

Brainstorming was yesterday.  5 days = 1 travel + 3*24h writing! + 1 travel.

You can add ideas here:  https://docs.google.com/document/d/16IaiLlxrEFjvBfxBRi__3nO6wtqqJ1ehgw9YoI7y-YI/edit

Probably in Northern hemisphere Summer.  Want to avoid remote attendees.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 26th January 2023 at 15:00 CET.
The next cppTango Teleconf meeting will take place on Thursday 19th January 2023 at 14:00 CET.
The next PyTango Teleconf meeting will take place on Thursday 19th January 2023 at 15:00 CET.
The next HDB++ Teleconf meeting will take place on Thursday 19th January 2023 at 10:00 CET.

## Summary of remaining actions

 **All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with your facility until next kernel meeting. Please add comments on https://gitlab.com/tango-controls/TangoTickets/-/issues/77

- Feature request for "New attribute event ALARM_EVENT". please see  https://gitlab.com/tango-controls/TangoTickets/-/issues/65 for rationale, proposal and impacted TC packages.
  Action: Read, understand, comment
  Last chance for discussions is 13 January 2023.  After that date, Thomas J will create IDL and cppTango issues.
  Sergi suggests we should check if this additional event affects performance (once we get to implementation).

**All kernel members**:
- Read Tango DB Refactoring [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing) and comment

**All kernel members and TangoBox users**:
- Please review the new TangoBox VM (Software versions used, first tests) before next kernel meeting:
    - New TangoBox VM: https://s2innovation.sharepoint.com/:u:/s/Developers/ERU58sTPhAdNpURoJMNMemYBJLwIc2ILBhfnQ6Yma1MaZw?e=dgp53L
    - Related Gitlab Issue : https://gitlab.com/tango-controls/tangobox/-/issues/56

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for permission to publish some information.

**Lorenzo, Thomas J**: to take the proposal on Gitlab permissions forward, write down the details of the proposal with concrete numbers, and then communicate with Steering Committee.
Link to the proposal: https://gitlab.com/tango-controls/TangoTickets/-/issues/63

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
- Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM.

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

**Reynald (ESRF)**:
- Update the Serial Tango class to be compatible with cppTango 9.4 so we could run the BLISS tests with latest pyTango release candidates.

**Thomas B. (byte physics)**:
- Make a poll to gauge interest in CMake Training by Kitware - ask if time of
  course from https://www.kitware.com/courses/cmake-training/ is OK, or if
  "custom" time required.
- Ask Kitware about custom CMake training
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)
- Look into how to get starter built with C++98.
- Document: "cppTango and starter should work with plain C++98 for all 9.3.x
   releases of the tango source distribution."

**Thomas I. (OSL)**:
- Work on MRs for getting cmake enhancements for windows into cpp servers. (Currently waiting on review for TangoDatabase MR)
- Get installer into main/9.3-backports branch of TSD (currently waiting on above MRs to be merged)

**Thomas J. (SKAO)**:
- Review comments on Feature request for "New attribute event ALARM_EVENT"
  (https://gitlab.com/tango-controls/TangoTickets/-/issues/65) until 2022-12-13
- Ask SKAO's Team System about gitlab policies until 2022-12-13.
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
  Thomas J presented a hands-on course in India.  Lots of code, few slides.  Seems like a good model for future workshops.
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

**Ulrik (OSL)**: retire Tango-Controls images on Docker Hub, but will need some help.
Dicussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71
  Plan:
      - prepare docker compose file that references correct images.
      - update documentation with the "right" method:  https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html?highlight=docker
      - delete all old images

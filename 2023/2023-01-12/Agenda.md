# Tango Kernel Follow-up Meeting - 2023/01/12

To be held on 2023/01/12 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-01-23-9yow?lang=en

# Agenda

1. Status of [Actions defined in the previous meetings](../../2022/2022-12-22/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. Next SIG meetings
1. AOB

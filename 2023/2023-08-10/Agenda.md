# Tango Kernel Follow-up Meeting - 2023/08/10

To be held on 2023/08/10 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-08-10-a2kv?lang=en

# Agenda

## Status of [Actions defined in the previous meetings](../2023-07-27/Minutes.md#summary-of-remaining-actions)
## High priority issues
## Issues requiring discussion
## AOB
### ICALEPCS

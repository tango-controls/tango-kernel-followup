# Tango Kernel Follow-up Meeting
Held on 2023/08/10 at 15:00 CEST on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-08-10-a2kv?lang=en

**Participants**:

- Thomas Braun  (byte physics e.K.) 
- Yury Matveev (Desy)
- Becky Auger-Williams (OSL)
- Reynald Bourtembourg (ESRF)
- Thomas Juerges (SKAO)
- Vincent Hardion (MAX IV)
- Anton Joubert (MAX IV)
- Lukasz Zytniak (S2Innovation)
    
## Status of [Actions defined in the previous meetings](../2023-07-27/Minutes.md#summary-of-remaining-actions)
**All Institutes**

- Ask if your institute can host some Gitlab runners. Please add comments on TangoTickets #77   

  Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners

  The list of people to approach, if a runner is broken is available here: https://gitlab.com/tango-controls/test-institute-runners#available-runners . Please keep it up to date.

  Important to keep runners updated.  See [TangoTickets#91](https://gitlab.com/tango-controls/TangoTickets/-/issues/91) for a recipe.

- Identify missing responsible persons (owner) from other institutes for gitlab user grooming

**Andy (ESRF)**

Contact ELI Beamlines about user grooming proposal TangoTickets#63 (comment 1110688846) 

  INAF: Vincent will ask Matteo Canzari
  - Vincent has discussed, but they haven't chosen a single owner. 
 > DONE

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.
> No news


**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues
> No news

**Johan Venter (SARAO)**:
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
> No news

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.
> No news

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket

> No news

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  - See if we can create a section for "regular meetings" or similar in https://indico.tango-controls.org Reynald will create new event categories for the different regular meetings.

> Done.  You can connect your calendar to one or all categories (category 0 for all categories).  Try it.  Ask Reynald if you need permission to manage your meetings.

  - Kill dockerhub organization (tango-cs?!) of tango-controls once https://gitlab.com/tango-controls/tango-doc/-/issues/392 is fixed. 

**Thomas B. (byte physics)**:  
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
  
>  No update

  - Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.

>  No update


  - Fix https://gitlab.com/tango-controls/tango-doc/-/issues/392

> Done

**Thomas J. (SKAO)**:  
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904

> Still not done. :-/

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  This action is suspended for the moment, waiting for TB to fix the publishing to RTD.

> TB is now done and TJ can dig up the email and send it to the mailing list.

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

> From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.

**Yury (DESY)**:
  - Gitlab Runners: Follow up with DESY IT Team.
>  DESY IT struggles with problems, waiting

## High priority issues

## Issues requiring discussion

What version of libzmq should PyTango be packaging in the binary wheels?  cppTango has black listed 4.3.3 and 4.3.4.  Windows build for cppTango and PyTango has 4.0.5, but that's just because we haven't tried updating.  PyTango could use 4.3.2.

DEV_INT DevInt removal:  https://gitlab.com/tango-controls/pytango/-/issues/544.  PyTango can drop this for 10.0 release.  Option 2, in the linked issue.

Decoupling of device state from alarm quality factor value:  https://gitlab.com/tango-controls/TangoTickets/-/issues/92
Discussed.  Could be useful.  Current behaviour is a little strange.  Interested parties to comment on the ticket.

## AOB
What to do with the Write the Doc Camp? 
At previous Community meeting, we had some homework to review existing docs by end of August.  Discuss status in September.  The Doc Camp is tentatively scheduled for early 2024.

What about an incubator projects?
> thoughts on an tango-incubator gitlab group/project/... to collect all the crazy prototype around tango which should not be advertise as production ready as they change some concept of Tango.
Proposal:  add a group called "incubator" to existing tango-controls organisation on gitlab.com.  Access permissions can be set on project level.  

**Action - Reynald**: to create group, give Vincent admin permission on group.  
> Done just after the meeting

cppTango V10 and observability - ready in October?  Is RFC update done?  When will IDLv6 finalised? How will distributed trace context be passed around, modified, used, etc.

### Icalepcs

Follow-up on this topic on #icalepcs-2023 slack channel. The overleaf link to the paper preparation is a bookmark of #icalepcs-2023 slack channel.

### Next Meetings
- The next Tango Kernel Teleconf meeting will take place on Thursday 24th August 2023 at 15:00 CEST.  
- The next cppTango Teleconf meeting will take place on Thursday 17th August 2023 at 14:00 CEST.  
- The next PyTango Teleconf meeting will take place on Thursday 17th August 2023 at 15:00 CEST.  

## Summary of remaining actions

**All Institutes**

- Ask if your institute can host some Gitlab runners. Please add comments on TangoTickets #77   

  Test project to test the runners: https://gitlab.com/tango-controls/test-institute-runners

  The list of people to approach, if a runner is broken is available here: https://gitlab.com/tango-controls/test-institute-runners#available-runners . Please keep it up to date.

  Important to keep runners updated.  See [TangoTickets#91](https://gitlab.com/tango-controls/TangoTickets/-/issues/91) for a recipe.

- Identify missing responsible persons (owner) from other institutes for gitlab user grooming

**Andy (ESRF)**: Contact ELI Beamlines about user grooming proposal TangoTickets#63 (comment 1110688846) 

**Anton (MaxIV)**:
  - Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.

**Gwenaëlle (SOLEIL)**:
  - Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
  Try to reproduce the observed event issues

**Johan Venter (SARAO)**:
  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

**Lorenzo (Elettra)**:
  - Gitlab Runners: Follow up with Elettra IT Team.

**Reynald (ESRF)**:
  - JTango: Events are not properly reported, investigate issue and create ticket

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.

  - Kill dockerhub organization (tango-cs?!) of tango-controls

**Thomas B. (byte physics)**:  
  - Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)  
  - Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.

**Thomas J. (SKAO)**:  
  - Start tango-controls faq on gitlab.com/tango-controls. See also the website: https://www.tango-controls.org/about-us/#faq_9904

  - Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.  This action is suspended for the moment, waiting for TB to fix the publishing to RTD.

  - Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.

**Yury (DESY)**:
  - Gitlab Runners: Follow up with DESY IT Team.  


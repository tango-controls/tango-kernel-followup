# Tango Kernel Follow-up Meeting - 2023/12/21

To be held on 2023/12/21 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-12-21-a4si?lang=en

# Agenda

## Gitlab.com Ultimate SaaS Plan Renewal (https://gitlab.com/tango-controls/TangoTickets/-/issues/102)
## Status of [Actions defined in the previous meetings](../2023-12-07/Minutes.md#summary-of-remaining-action)
## High priority issues
## Issues requiring discussion
## Lua Binding
## AOB

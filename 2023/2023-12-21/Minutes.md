# Tango Kernel Follow-up Meeting
Held on 2023/12/21 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-12-21-a4si?lang=en

**Participants**:

- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (MAX IV)
- Grzegorz Kowalski (S2Innovation)
- Lorenzo Pivetta (Elettra)
- Lukasz Zytniak (S2Innovation)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergio Rubio (ALBA)
- Thomas Juerges (SKAO)
- Yury Matveev (DESY)

## Gitlab.com Ultimate SaaS Plan Renewal (https://gitlab.com/tango-controls/TangoTickets/-/issues/102)

What license to use for TangoTickets?  CC.0 isn't OSI compliant, but we don't have code.  Maybe BSD-3-Clause or Apache?  
Proposal:  
BSD-3-Clause for TangoTickets and repos with minutes from meetings.  
This will need a copyright holder.  
What text to use for that?  "Tango Controls Collaboration"?  
**Action: Lorenzo**: - bring BSD-3 license discussion to SC, Jan 2024. Maybe better to adopt LGPL for TangoTickets as well.

tango-controls.org LICENSE section:
" Tango Controls system libraries are made available under LGPL licence. This means that Tango Controls can be used and modified without constraints on the derived system built on Tango Controls."
"The graphical tools and some of device servers are made available under GPL licence. This means modifications to them need to be shared with the community."

- Fix licenses, where it is obvious what to use.
- For device servers without an obvious license choice, move away from gitlab.com/tango-controls until they can be updated.  E.g., to ESRF instance.
- Use LGPL for TangoTickets, minutes, docs, benchmarks.  This follows the spirit tango-controls.org LICENSE section, and is the least restrictive.
- Reynald to ask Andy to resubmit renewal request to Gitlab once all is fixed today.

## Status of [Actions defined in the previous meetings](../2023-12-07/Minutes.md#summary-of-remaining-actions)

**All**:
- Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
  - Are the gitlab wikis searchable?  That allows us to keep separate layout, but also allows people looking for answers that aren't sure exactly where to start.
- Before 15th December: Reply to poll for TC Cybersecurity SIG Meeting in Feb 2024 at MaxIV: https://framadate.org/ouAhigpYdR28ivP8
> The poll for the next Tango Cybersecurity SIG meeting has unanimously elected the 19th to 21st March.
An indico page will be published soon for the registration.  Organisers still need to decide if this is noon-to-noon, or 3 full days.

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - Step 1:  create proposal on TangoTickets. ==> Anton
  - Step 2:  get someone to start building it - Thomas Ives is keen.
> No update

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.
> Progressing, but not available yet.
- Co-ordinate hysteresis change infos in TangoTickets
> Two at Elettra are working on this.  Lorenzo will provide some updates via Slack/kernel mailing list.  Contact him if you have suggestions or want to discuss.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Follow up on ESRF Gitlab Windows runner (Will work on it in January 2024)
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- Move UniversalTest Tango Device Server from ESRF Gitlab to gitlab.com/tango-controls/device-servers
- Invite Guillaume and Damien to show/share their work on the command line tool
> They will present their work in 2024

**Sergio (ALBA)**:
- push test code fixing event subscription performance issues as draft
> No performance improvement, but did find that 9.5.0 is 10 - 15% slower than 9.3.  Still need to make a report with details.  9.5.0 seems to bypass read_attribute cache by default, so subscription is slower.

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.
- Ask Freexian to Backport omniORB 4.3 package to Bookworm (current stable) so that cppTango can be compiled on the latest stable release.

**Thomas J. (SKAO)**:
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
  Then the maintainers can apply developer role to the remaining people.

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB (try DESY runner with cppTango)

## High priority issues

## Issues requiring discussion

## Lua Binding

A new Lua binding (using cppTango) is being developed by Grzegorz: https://git.daneos.com/Public/lutango/src/branch/develop
The PyTango high-level API was used as inspiration.

## AOB

Can we use the same framapad link each time?  Makes it easier to find.  Yes, we can!

Tango documentation workshop.  Definitely need to do it in first half of 2024.  The earlier, the better.
Becky is busy reading through the docs, and will look at the spreadsheet where we can comment on each section of the docs, and also review the "Grand Unified Theory of Documentation".  Links in Slack channel `temp-sig-documentation-workshop`

### Next Meetings
https://indico.tango-controls.org/category/6/
Do we keep the same schedule for the Kernel Follow-up Meetings in 2024 (2nd and 4th Thursdays of each month)?
- The next Tango Kernel follow-up meeting will take place on Thursday 11th January 2024 at 15:00 CET.
- The next cppTango follow-up meeting will take place on Thursday 4th January 2024 at 16:00 CET.
- The next PyTango follow-up meeting will take place on Thursday 18th January 2024 at 15:00 CET.

## Summary of remaining actions
**All**:
- Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
  The goal is to find incompatibilities.
  - Step 1:  create proposal on TangoTickets. ==> Anton
  - Step 2:  get someone to start building it - Thomas Ives is keen.

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.
- Co-ordinate hysteresis change infos in TangoTickets
- bring BSD-3 license discussion to SC, Jan 2024. Maybe better to adopt LGPL for TangoTickets as well.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Follow up on ESRF Gitlab Windows runner (Will work on it in January 2024)
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- Move UniversalTest Tango Device Server from ESRF Gitlab to gitlab.com/tango-controls/device-servers
- Invite Guillaume and Damien to show/share their work on the command line tool

**Sergio (ALBA)**:
- push test code fixing event subscription performance issues as draft

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.
- Ask Freexian to Backport omniORB 4.3 package to Bookworm (current stable) so that cppTango can be compiled on the latest stable release.

**Thomas J. (SKAO)**:
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed.
  Then the maintainers can apply developer role to the remaining people.

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB (try DESY runner with cppTango)

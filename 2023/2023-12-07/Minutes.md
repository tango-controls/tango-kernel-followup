# Tango Kernel Follow-up Meeting
Held on 2023/12/07 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-12-07-a4s9?lang=en

**Participants**:

- Andy Goetz (ESRF)
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (MAX IV)
- Graziano Scalamera (Elettra)
- Lorenzo Pivetta (Elettra)
- Lukasz Zytniak (S2Innovation)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Thomas Braun  (byte physics e.K.)
- Thomas Ives (OSL)
- Thomas Juerges (SKAO)
- Ulrik Pedersen (OSL)
- Vincent Hardion (MAX IV)
- Yury Matveev (DESY)

## Status of [Actions defined in the previous meetings](../2023-11-23/Minutes.md#summary-of-remaining-actions)

**All**:
- Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)
> Birgit answered last week, but still no action about the missing payment, not the ticket.

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango - cross-platform, cross-system (JTango/cppTango/PyTango). The goal is to find incompatibilities. Step 1:  create proposal on TangoTickets. ==> Anton Step 2:  get someone to start building it - Thomas Ives is keen.
> No update

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.
> Feasible, according to IT group.  It is progressing.  More info next time.

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
> Issue created today: related to renaming server instance https://gitlab.com/tango-controls/JTango/-/issues/140
- Follow up on ESRF Gitlab Windows runner
> No progress.  Will look into it in January.
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.
> 2FA in use.  Still have 2 owners - agreed that this OK, but only one will be point of contact.
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail).

**Sergio (ALBA)**:
- push test code fixing event subscription performance issues as draft

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.
- Ask Freexian to Backport omniORB 4.3 package to Bookworm (current stable) so that cppTango can be compiled on the latest stable release.
> In progress.  PyTango will need to be updated to 9.5.0, to support Python 3.12.  Which means cppTango needs to be updated to 9.5.0 as well.  This is OK.

**Thomas J. (SKAO)**:
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. Then the maintainers can apply developer role to the remaining people.
> TJ still needs owner list to be finalised.

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB (try DESY runner with cppTango)
> Didn't try it with cppTango yet, but is working for PyTango.

**Vincent Hardion (MaxIV)**:
- Organize TC Cybersecurity SIG Meeting in Feb 2024?!
  - [x] Send poll (https://framadate.org/ouAhigpYdR28ivP8). (respond before 15 Dec)
  - [X] Choose place (=> MAX IV)
- Follow up the cppTango C++17 moving proposal with the steering committee (https://gitlab.com/tango-controls/TangoTickets/-/issues/97)
> Accepted.

## High priority issues

## IPV6 Support
There was an old Debian report PyTango tests do not succeed on an IPv6 only host.  
Proposal:  document that cppTango doesn't support this.  
What until there is a user reports this is required, before doing anything in cppTango.  
It will probably be quite a large effort.  
omniORB and ZeroMQ claim to support it.

-> debian devel discussion started by TB: https://lists.debian.org/debian-devel/2023/11/msg00247.html

## Issues requiring discussion
Crash on read attribute combined with pushing events (SerialModel.NO_SYNC): https://gitlab.com/tango-controls/cppTango/-/issues/1202.  cppTango does have a way for the user to use their own mutexes to protect an attribute value buffer.  Maybe PyTango could use this feature:  `set_user_attr_mutex`. https://tango-controls.github.io/cppTango-docs/classTango_1_1Attribute.html#a938e182ff0b0c1664b30b713f3d11d3f

Co-ordinating OpenTelemetry implementations
- need to think about how this will be done in different languages
- cppTango with OpenTelemetry demo with NL and AJ yesterday.  Trying it and looking at implementation for PyTango.
- NL will meet with Gwenaëlle in January 2024.
- Performance impact is unknown.  Github keeping about 3% of traces.  We need an implementation, and significant scale to test the impact.
- We need to provide a solution to end users.  Documentation with best practices.
- Still need to think about events.  Will we add the trace context into the event payload?

## AOB
New attribute alarm event (https://gitlab.com/tango-controls/cppTango/-/issues/1030) and support for attribute alarm hysteresis
- who is driving this work?  SKAO and Elettra are interested in using it.  TJ to provide feedback in the next meeting.
- cppTango, JTango and PyTango.
- who is taking care of the changes to tools?  Jive is the main one.  Taurus.  Database schema.
- Dependencies:
  - IDL
    - database schema
    - cppTango
      - PyTango
        - Taurus
      - DatabaseDS (also needs database schema)
    - JTango
      - Jive

Lorenzo will co-ordinate hysteresis change.  Info in TangoTickets.

Tickets with "Discussion needed" label:
- missing data types:  https://gitlab.com/tango-controls/TangoTickets/-/issues/101
  - low priority, but have a chance to change things with new IDL.  However, improving the symmetry might break some behaviour (e.g., if DevCharArray uses DevUChar now, but we change it to DevChar).  Big impact on generic tools.  Not for Tango v10.   Think about it again in mid 2024.
- command line tools to read/write attribute:  https://gitlab.com/tango-controls/TangoTickets/-/issues/99
  - simple, depends on cppTango.
  - NL mentions someone at ESRF working on a similar tool. 
  It would be interesting to share the existing code and maybe make a demo of what is already developed/available.

UniversalTest device repo - can it be moved from ESRF gitlab to gitlab.com/tango-controls? Yes.  RB will do it.

### Next Meetings
https://indico.tango-controls.org/category/6/
- The next Tango Kernel follow-up meeting will take place on Thursday 21st December 2023 at 15:00 CET.
- The next cppTango follow-up meeting will take place on Thursday 14th December 2023 at 16:00 CET.
- The next PyTango follow-up meeting will take place on Thursday 14th December 2023 at 15:00 CET.

## Summary of remaining actions

**All**:
- Give feedback and improve the FAQ: https://gitlab.com/groups/tango-controls/-/wikis/The-Tango-Controls-FAQ
- Before 15th December: Reply to poll for TC Cybersecurity SIG Meeting in Feb 2024 at MaxIV: https://framadate.org/ouAhigpYdR28ivP8

**Andy (ESRF)**:
- Contact ELI Beamlines about user grooming proposal [TangoTickets#63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63#note_1110688846)

**Anton (MaxIV)**:
- Create proposal for Integration tests for Tango cross-platform, cross-system (JTango/cppTango/PyTango).  
The goal is to find incompatibilities.  
  - Step 1:  create proposal on TangoTickets. ==> Anton 
  - Step 2:  get someone to start building it - Thomas Ives is keen.

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure (Patrick Madela)
- Try to reproduce the observed event issues

**Lorenzo (Elettra)**:
- Gitlab Runners: Follow up with Elettra IT Team.
- Co-ordinate hysteresis change infos in TangoTickets

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- Follow up on ESRF Gitlab Windows runner (Will work on it in January 2024)
- Kill dockerhub organization (tango-cs?!) of tango-controls (after T-J will have sent the e-mail)
- Move UniversalTest Tango Device Server from ESRF Gitlab to gitlab.com/tango-controls/device-servers
- Invite Guillaume and Damien to show/share their work on the command line tool

**Sergio (ALBA)**:
- push test code fixing event subscription performance issues as draft

**Thomas B. (byte physics)**:
- Look at TangoDatabase#26 (missing default value for "date" field of "property_device_hist" table)
- Task: Ask every tango-controls member to add their institute/company in their profile under "Organization" and make it public. We will start removing members without organization in 8 weeks.
- Test DESY Gitlab runners and check with Yury that all required software is installed.
- Ask Freexian to Backport omniORB 4.3 package to Bookworm (current stable) so that cppTango can be compiled on the latest stable release.

**Thomas J. (SKAO)**:
- Send email to tango mailing list and Slack asking who is using tango-cs and which SKAO images are used.
- Implement gitlab user grooming proposal TangoTickets#63 (comment 1110688846) and comment in the thread that you finished it.  
  From the input that is now available in the issue, TJ compiles the list of owners, adds it to the issue and assigns it to Reynald to make those on the list owners. After that TJ will tag each owner to apply maintainer role to the people they proposed. 
  Then the maintainers can apply developer role to the remaining people.

**Yury (DESY)**:
- Gitlab Runners: Follow up with TB (try DESY runner with cppTango)


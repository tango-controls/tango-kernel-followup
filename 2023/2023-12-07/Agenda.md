# Tango Kernel Follow-up Meeting - 2023/12/07

To be held on 2023/12/07 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-meeting-2023-12-07-a4s9?lang=en

# Agenda

## Status of [Actions defined in the previous meetings](../2023-11-23/Minutes.md#summary-of-remaining-action)
## High priority issues
## IPV6 Support
## Issues requiring discussion
## AOB

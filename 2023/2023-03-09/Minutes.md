# Tango Kernel Follow-up Meeting
Held on 2023/03/09 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-teleconf-2023-03-09-9zq9?lang=en

**Participants**:
- Anton Joubert (MAX IV)
- Becky Auger-Williams (OSL)
- Benjamin Bertrand (MAX IV)
- Damien Lacoste (ESRF)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Thomas Ives (OSL)
- Vincent Hardion (MAX IV)

## Status of [Actions defined in the previous meetings](../2023-02-23/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with
  your facility until next kernel meeting. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)

Test project to test the runners:
https://gitlab.com/tango-controls/test-institute-runners

> Runners added from ALBA, DESY, MAX IV and SKAO.  PyTango has updated to use this.
Do we need Windows runners?  AppVeyor is being used at the moment for cppTango and PyTango.
TangoSourceDistribution does have a new job that will use a gitlab Windows runner.
Gitlab runners have Windows 10, Visual Studio 17, chocolatey.  Sergi will ask for some new Windows runners from ALBA.

> How to configure runners?  Following Gitlab's docs is fine.  SKAO said they would provide some Ansible files, but we don't have this - probably not critical.

> On cppTango, some issues have been created to improve their pipeline and move to group runners.

**All kernel members**:
- Read Tango DB Refactoring
  [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and
  [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing)
  and comment

> No news.

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure

**Lorenzo, Thomas J**:
- To take the proposal on Gitlab permissions forward,
  write down the details of the proposal with concrete numbers, and then
  communicate with Steering Committee.
  Link to the proposal in [TangoTickets 63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63)

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
- Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM.

**Nicolas (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket

> Reynald encountered it when restarting a device server.  Java client (ATK panel) didn't update after that, and no error reported.  Possibly server (cppTango) Java client did resubscribe (even though client was sending periodic re-subscription messages to DServer, with expected content).  Maybe low level ZMQ subscription wasn't working.  cppTango client was working during this time.  Could also be similar to reaching ZMQ high-level mark, but this particular device had only 1 attribute, and a low rate of events.

**Thomas B. (byte physics)**:
- Get number of participants from https://dud-poll.inf.tu-dresden.de/anonymous/cmake-training/
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

**Thomas I. (OSL)**:
- Get installer into main/9.3-backports branch of TangoSourceDistribution
> Have installer for main branch.  Can we do linking dynamically instead of statically?  Dynamic linking is recommended by Microsoft.  Can we remove static?  Yes.  
Should we include libraries built with debug info?  Yes, this is useful.  
Nice to have:  release (optimised) with debug symbols.  
We can skip this for 9.4.1, if hard. 

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
  Thomas J presented a hands-on course in India.  Lots of code, few slides.
  Seems like a good model for future workshops.
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
   - attendees liked the open nature of the workshop - they could ask about
     areas of interest (e.g., green modes, debugging, working in containers,
     ...) and the workshop would focus on those topics.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

**Ulrik (OSL)**:
- retire Tango-Controls images on Docker Hub, but will need some help.
  Discussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71
  Plan:
      - prepare docker compose file that references correct images.
      - update documentation with the "right" method:
        https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html
     - delete all old images

## High priority issues
None reported.

## Issues requiring discussion

### IDLv6

https://gitlab.com/tango-controls/tango-idl/-/compare?from=main&to=distributed_call_id

This is required to progress on the logging extension development.  We need to be able to distinguish between v6 devices and earlier.
Do we have a formal process for adopting a new IDL version?  No.  Proposal:  do not finalise IDLv6 until after the IDLv6 special interest meeting.

Important that downstream projects work from a tag in the tango-idl repo, so they are not affected by any merges to main.  Proposed tag is the latest one:  5.1.2:  https://gitlab.com/tango-controls/tango-idl/-/tags  

**Actions**:
- [ ] Reynald make cppTango CI use a tag
- [ ] Anton make sure PyTango (builder) CI is using a tag
- [ ] Reynald to ask Gwen to do the same to jTango.
- [ ] Nicolas to create a merge request in tango-idl for v6 changes.

Do we still need to update the major version number of Tango, when IDL updated?  RFC mentions the protocol version.

## AOB

### Debian and Ubuntu packages
Quite old versions for cppTango.  
Freexian have a role here, but that is not clear.  Thomas B probably knows more.  
Which version do we want in Debian/Ubuntu?  Something stable (LTS version), since updates are slow.  
A recent 9.3.x.

### Next SIG Meetings

#### --- Taurus workshop ---
Taurus workshop will be held at the ESRF in hybrid mode on 14th and 15th March 2023.  
More details here: https://indico.esrf.fr/event/76/

#### --- CICD SIG Meeting ---
End April or beginning May.  
Assuming we will have Tango community meeting in June/July.  
Maybe we combine this with the IDLv6 discussions?  
**Action**:  
    - [ ] Sergi finalise the theme for this meeting.

#### --- IDLv6 SIG Meeting ---
Already discussed above.

### ICALEPCS 2023
Discussions about Tango-Controls workshop at ICALEPCS 2023 and papers can take place on icalecps-2023 tango-controls slack channel.

### Next Teleconf Meetings
- The next Tango Kernel Teleconf meeting will take place on Thursday 23rd March 2023 at 15:00 CET.  
- The next cppTango Teleconf meeting will take place on Thursday 16th March 2023 at 14:00 CET.  
- The next PyTango Teleconf meeting will take place on Thursday 16th March 2023 at 15:00 CET.  
- The next HDB++ Teleconf meeting will take place on Thursday 20th March 2023 at 14:00 CET.

## Summary of remaining actions

**All institutes**:
- Ask if your institute can host some Gitlab runners. Please follow up with
  your facility until next kernel meeting. Please add comments on
  [TangoTickets #77](https://gitlab.com/tango-controls/TangoTickets/-/issues/77)

Test project to test the runners:
https://gitlab.com/tango-controls/test-institute-runners

**All kernel members**:
- Read Tango DB Refactoring
  [issue](https://gitlab.com/tango-controls/TangoTickets/-/issues/69) and
  [proposal](https://docs.google.com/document/d/1B2aY5ueMN_YzsA0I4OfEM_kvgUQaJ9kYgf_e8m2wwFw/edit?usp=sharing)
  and comment

<!-- List is sorted starting from here -->

**Andy (ESRF)**:
- Ask those that submitted feedback about their Tango installations for
  permission to publish some information.

**Anton (MaxIV)**:
- Make sure PyTango (builder) CI is using a tag for the Tango IDL

**Gwenaëlle (SOLEIL)**:
- Update GPG Passphrase and keyfile to something more secure

**Lorenzo, Thomas J**:
- To take the proposal on Gitlab permissions forward,
  write down the details of the proposal with concrete numbers, and then
  communicate with Steering Committee.
  Link to the proposal in [TangoTickets 63](https://gitlab.com/tango-controls/TangoTickets/-/issues/63)

**Lorenzo (Elettra)**:
- Provide feedback from Claudio for pogo [pogo!126](https://gitlab.com/tango-controls/pogo/-/merge_requests/126)
- Create a ticket or merge request about the patch on the Tango Database DS to use InnoDB instead of MyISAM.

**Nicolas (ESRF)**:
- Create a merge request in tango-idl for first v6 changes

**Reynald (ESRF)**:
- JTango: Events are not properly reported, investigate issue and create ticket
- make cppTango CI use a tag for the tango IDL.
- Ask Gwen to do the same to jTango.

**Sergi (ALBA)**:
- Finalise the theme for the SIG meeting which will take place in ALBA (CICD?, IDLv6?, Both?).

**Thomas B. (byte physics)**:
- Get number of participants from https://dud-poll.inf.tu-dresden.de/anonymous/cmake-training/
- Look at [TangoDatabase#26](https://gitlab.com/tango-controls/TangoDatabase/-/issues/26)
  (missing default value for "date" field of "property_device_hist" table)

**Thomas I. (OSL)**:
- Get installer into main/9.3-backports branch of TangoSourceDistribution

**Thomas J. (SKAO)**:
- Gather ideas on slack about this new idea: Tutorial independent of
  conferences. Then decide on the format, the date, etc.
  Thomas J presented a hands-on course in India.  Lots of code, few slides.
  Seems like a good model for future workshops.
  Johan V volunteered Thomas J for a Tango workshop at ICALEPCS 2023.  Thomas J is keen.
- As soon as SKAO has reviewed the feedback publish the link to the Miro board
  with the feedback. Give an overview how the workshop went and make material
  available.
   - attendees liked the open nature of the workshop - they could ask about
     areas of interest (e.g., green modes, debugging, working in containers,
     ...) and the workshop would focus on those topics.
- Start tango-controls faq on gitlab.com/tango-controls. See also the website:
  https://www.tango-controls.org/about-us/#faq_9904
- Send email to tango mailing list and Slack asking who is using tango-cs and
  which SKAO images are used.  This action is suspended for the moment. We will
  wait for the outcome of Ulrik's investigation.

**Ulrik (OSL)**:
- retire Tango-Controls images on Docker Hub, but will need some help.
  Discussion on ticket https://gitlab.com/tango-controls/TangoTickets/-/issues/71
  Plan:
      - prepare docker compose file that references correct images.
      - update documentation with the "right" method:
        https://tango-controls.readthedocs.io/en/latest/development/debugging-and-testing/testing-tango-using-docker.html
     - delete all old images

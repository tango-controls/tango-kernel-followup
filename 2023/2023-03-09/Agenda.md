# Tango Kernel Follow-up Meeting - 2023/03/09

To be held on 2023/03/09 at 15:00 CET on Zoom.  
Framapad: https://mensuel.framapad.org/p/tango-kernel-teleconf-2023-03-09-9zq9?lang=en

# Agenda

1. Status of [Actions defined in the previous meetings](../2023-02-23/Minutes.md#summary-of-remaining-actions)
1. High priority issues
1. Issues requiring discussion
1. AOB
    1. Next SIG meetings
    1. ICALEPCS 2023

# Tango Kernel Follow-up Meeting

Held on 2021/06/24 at 15:00 CEST on Zoom.

**Participants**:

- Andy Goetz (ESRF)  
- Gwenaelle Abeillé (Soleil)  
- Jan David Mol (LOFAR)  
- Krystian Kedron (S2Innovation)  
- Lorenzo Pivetta (ELETTRA)  
- Michał Liszcz (S2Innovation)  
- Nicolas Leclercq (ESRF)  
- Piotr Goryl (S2Innovation)  
- Reynald Bourtembourg (ESRF)  
- Thomas Braun (Byte Physics)  
- Thomas Juerges (LOFAR)  

### Status of [Actions defined in the previous meetings](../2021/2021-06-10/Minutes.md#summary-of-remaining-actions)

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Soleil**: Provide a summary of what needs to be done to the Java projects Tango developers not yet registered on sonatype
(Faranguiss Poncet, Jean-Luc Pons, Damien Lacoste, Nicolas Tappret) for the migration from Bintray to Maven Central. **Done**

**Action - Reynald**:
Contact his ESRF colleagues and coordinate the migration of the remaining repositories they are currently
administrating (pogo, Astor). **Done. Pogo and Astor are now on gitlab.com)**

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issues.  
Any good idea in these MRs should be reflected in the Tango 10 RFCs and converted as an rfc issue.

**Action - Sergi**:
Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to be migrated under hdbpp subgroup.

**Action - Sergi**:
https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 (on names on logs) to be reviewed.

**Action - Thomas B.**:
Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side. pytango#409 is a
cppTango bug, needs a proper fix. **Thomas will work on pytango#409 at the end of this week**

**Action - S2Innovation**: Configure rest-api git repo readthedocs for Gitlab. 
**Work in progress.  
See https://gitlab.com/tango-controls/rest-api/-/merge_requests/1.  
Some additional work will be needed if we want to change the theme to be consistent with the other tango projects**

**Action - Reynald**: Create an issue in TangoTickets about the possibility to use a feature like in
https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/8 at device server level
to get a location where to exchange ideas on this topic (how to configure it? Changes required on the tools).  
**Done: See https://gitlab.com/tango-controls/TangoTickets/-/issues/55**

**Action - Lorenzo**: Create issues for server side access control and encryption features in rfc repository with tango-v10 label.

### Next Tango Meeting

The Next Tango Collaboration Meeting will take place on September 14th and 15th 2021.  
It will probably be 2 half days as the last virtual Tango Collaboration Meeting.  
The agenda is open, and you can send ideas of presentations to Nicolas Leclercq (nicolas.leclercq at esrf dot fr).  
If the sanitary conditions allow it, the meeting will be a hybrid meeting (remote and face-to-face for those who can 
come to the ESRF in Grenoble).  
Andy suggested organizing a short face-to-face Tango kernel meeting during this period.  
ESRF no longer hosts an indico website, so it would be great if we could use the indico website of another institute to 
help to organize this meeting. Maybe DESY or ALBA could set up their indico for that?

**Action - Nicolas L. (ESRF)**: Find an indico website before August to host the agenda and registration for the next Tango Meeting.

### Tango RFCs

See https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-06-24.

Emmanuel Taurel had a quick look at some RFCs before he retired. He didn't make any comment.

### High priority issues

Andy mentioned the following issue: https://gitlab.com/tango-controls/TangoTickets/-/issues/39 (Concurrently executing a command and pushing event leads to dead lock).  

Reynald mentioned the following issue: https://gitlab.com/tango-controls/cppTango/-/issues/409 (Potential deadlock when updating attribute configuration?).  

Reynald also mentioned that some other deadlocks have also been observed in some other device servers at the ESRF.

To tackle all these problems, it has been decided to do the following:  
1- Michał Liszc will work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)). 
This might require to compile omniORB with special flags.  
2- Nicolas Leclercq will document/comment the current code to better understand the current usage of the different mutexes 
and will propose some solutions to avoid some potential deadlocks.

**Action - S2Innovation (Michał Liszcz)**:  Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html))

**Action - ESRF (Nicolas L.)**: Document/comment the current cppTango code to better understand the current usage of the different mutexes 
and propose some solutions to avoid some potential deadlocks.

### AOB
#### Kernel Webinars

The 4th Kernel Webinar took take place on Wednesday 23rd June .  
This webinar focused on PyTango and has been presented by Anton Joubert and Geoffrey Mant.

**Action - Reynald**: Edit the 4th Tango kernel Webinar video and upload it on tango-controls youtube channel.

#### Debian packages

Freexian has prepared omniORB 4.2.4 Debian packages.  
They are already uploaded to experimental.  
They have also produced package for buster-backports (not yet uploaded in official repositories it seems?).  
Their work is available on salsa (https://salsa.debian.org/debian/omniorb-dfsg).  
Thomas tested the produced packages on an up to date Debian buster and confirmed it is now compiling successfully with C++17.  
He tried to compile cpptango with C++20 and encountered some compiling issues which are fixed in omniORB 4.3 beta version.  
If bullseye is released before omniorb 4.3.0 is out of beta, Freexian will apply the [gcc-11 patch](https://sourceforge.net/p/omniorb/svn/6530) 
mentioned by Thomas.  

Thomas filled an issue to update Tango in Ubuntu: https://bugs.launchpad.net/ubuntu/+source/tango/+bug/1932290

#### CFT

Andy gave some news about the CFT and said it would be completely official by Friday 25th June at 9:30 CEST. 
By that time the selection of the companies will be official.  
The first contacts with the subcontractors will take place during week #26.

This CFT will allow placing orders easily to a list of pre-selected companies during 3 years (extendable by 2 years).

#### JTango

Krystian reported that CI in JTango is now operational on Gitlab.com.  
He has been doing some refactoring too in JTango.  
The JTango documentation generation on readthedocs is not yet automatic.  

**Action - Krystian**: Update the JTango README which is still showing some links to Travis and Bintray.

#### News from Benjamin Bertrand

Benjamin could not attend the meeting but sent an e-mail to Reynald to give some news.  
Anton reviewed https://gitlab.com/tango-controls/docker/mysql/-/merge_requests/4 (thanks Anton) and Benjamin merged it.  

Benjamin moved tango-cs-docker to https://gitlab.com/tango-controls/docker/tango-cs and wanted to add the same .gitlab-ci.yml as for mysql. 
He then noticed several issues and MR about how to build this image. Maybe something to discuss?
(Reynald: sorry Benjamin, I forgot to mention that during the meeting, but I put it in the minutes. Hopefully this will 
trigger some discussions on the existing pending issues and MR in https://gitlab.com/tango-controls/docker/tango-cs repository).  
Benjamin suggests that an option would be to use conda packages for this image.

#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).

So the next Tango Kernel Teleconf Meeting will take place on Thursday 8th July 2021 at 15:00 CEST.

## Summary of remaining actions

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issues.  
Any good idea in these MRs should be reflected in the Tango 10 RFCs and converted as an rfc issue.

**Action - Reynald**: Edit the 4th Tango kernel Webinar video and upload it on tango-controls youtube channel.

**Action - Sergi**:
Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to be migrated under hdbpp subgroup.

**Action - Sergi**:
https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 (on names on logs) to be reviewed.

**Action - Thomas B.**:
Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side. pytango#409 is a
cppTango bug, needs a proper fix.

**Action - S2Innovation**: Configure rest-api git repo readthedocs for Gitlab.

**Action - S2Innovation (Michał Liszcz)**:  Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html))

**Action - S2Innovation (Krystian)**: Update the JTango README which is still showing some links to Travis and Bintray

**Action - Lorenzo**: Create issues for server side access control and encryption features in rfc repository with tango-v10 label.

**Action - Nicolas L. (ESRF)**: Find an indico website before August to host the agenda and registration for the next Tango Meeting.

**Action - Nicolas L. (ESRF)**: Document/comment the current cppTango code to better understand the current usage of the different mutexes
and propose some solutions to avoid some potential deadlocks.


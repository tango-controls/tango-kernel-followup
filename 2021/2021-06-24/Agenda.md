# Tango Kernel Follow-up Meeting - 2021/06/24

To be held on 2021/06/24 at 15:00 CEST on Zoom.

# Agenda

 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-06-10/Minutes.md#summary-of-remaining-actions)
 2. Next Tango Meeting
 3. Tango RFCs (https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-06-24)
 4. High priority issues
 5. Issues requiring discussion
 6. AOB
    - Freexian work on omniORB Debian package

# Tango Kernel Follow-up Meeting

Held on 2021/11/18 at 15:00 CET on Zoom.

**Participants**:

- Benjamin Bertrand (MAX IV)
- Reynald Bourtembourg (ESRF)
- Andy Goetz (ESRF)
- Vincent Hardion (MAX IV)
- Thomas Juerges (SKAO)
- Damien Lacoste(ESRF)
- Michał Liszcz (S2Innovation)
- Jairo Moldes (ALBA)

## RFC Workshop Organization

S2Innovation will organize the RFC workshop. 
Piotr found a house about 30km from Krakow where 10 to 12 people could stay during the workshop.
This workshop will take place from Tuesday 18th January to Thursday 20th January 2022.  
Participants will be able to arrive on Monday 17th January and can leave on Friday 21st January.

## Status of [Actions defined in the previous meetings](../2021-10-28/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ask java developers to update their projects and remove any reference to bintray. **Reynald will ping the ESRF developers**.
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126.   
**No feedback received yet.  
Thomas Juerges volunteered to participate in the review.
Reynald insisted that these changes will impact all the POGO users so all institutes using POGO are strongly encouraged 
in participating in the review.**
 
**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?
- Activate Gitlab Gold Plan license.   
**Andy made the request for the renewal. This process usually takes time according to Thomas Juerges experience.**
- Fix the issue with the mailing lists no longer working. **This issue has been fixed**

**cppTango developers**:
- Assign milestones (9.4 or 9.3.5) to issues we want to be part of 9.4 or 9.3.5 releases  
**Still work in progress. Michal, Tomasz, Thomas and Reynald went through almost all the cppTango issues 
and assigned milestones to them. There are less than 10 issues awaiting to be assigned to a milestone.
A new teleconf dedicated to this topic will take place on Tuesday 23rd November at 10:30 CET (using the traditional 
Tango Kernel Teleconf Zoom link)**
- Please review and merge cppTango!875. **Still to be done**

**Damien (ESRF)**:
- Merge https://gitlab.com/tango-controls/pogo/-/merge_requests/127 and prepare a new Pogo release. **No progress**

**Łukasz (S2Innovation)**:
- Send a Doodle to find the best dates for the workshop (covering end of 2021 / beginning of 2022) **Done**

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitizer work completed, document/comment the current cppTango
  code to better understand the current usage of the different mutexes
  and propose some solutions to avoid some potential deadlocks.

**Piotr (S2Innovation)**:
- Find a way to remove https://tango-rest-api.readthedocs.io
**S2Innovation contacted Igor who will take care of removing this duplicated project from readthedocs.**

**Reynald (ESRF)**:
- Comment https://gitlab.com/tango-controls/TangoTickets/-/issues/39 to add a link to the related
  cppTango issue. **Done**

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.
- Review cppTango!742 (Function name and line number in logs and exceptions).

**Thomas (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26

**Thomas (SKAO)**:
- Contact ADASS organizers to see whether we could organize a Tango-Controls workshop there.  
**Thomas contacted the ADASS organizers. The procedure to request a time slot for a Tango workshop seems to be simple and straight-forward.
Thomas ~~got~~ volunteered to take care of the organization of such workshop. He will prepare a short description of the
workshop and share it with the kernel team (slack and/or kernel mailing list). 
The next ADASS conference will be held in Victoria, Canada, 11-15 September 2022. 
Vincent said that Max IV can probably send 1 person (max) to help during this workshop.
Andy suggested to present how Tango could be used to control a telescope.  
Thomas will get in touch with LOFAR to see whether they could help too. 
Andy suggested getting in touch with INAF too. Reynald mentioned Virgo which is using Tango too in the astronomy world.
Vincent said that the materials from the previous Tango workshops could be reused.**

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration:
  Drive new repository creation on GitLab with a well known device server.  
**S2Innovation seems to have started working on this topic. Vincent will coordinate with them when they will come to MaxIV.**
- Get videos from ICALEPCS.   
**A video of most of the Tango workshop at ICALEPCS 2021 has been uploaded by the organizers to youtube: 
https://www.youtube.com/watch?v=YkoJI5PyT_Q  
Reynald created a playlist with this video on the [tango-controls youtube channel](https://www.youtube.com/channel/UCezS9cMkektZNItYnPOAQvg) 
after the kernel teleconf to find this video easily.**

## High Priority Issues
Thomas Juerges reminded that [cppTango!852](https://gitlab.com/tango-controls/cppTango/-/merge_requests/852) is the highest priority merge request for SKAO. 

Andy wanted to get some news about https://gitlab.com/tango-controls/TangoTickets/-/issues/39. 
There is a cppTango issue associated to this issue: https://gitlab.com/tango-controls/cppTango/-/issues/762

## Issues requiring discussion

###  POGO: Feedback wanted on https://gitlab.com/tango-controls/pogo/-/merge_requests/126 MR

**Action - Thomas Juerges (SKAO)**: Participate in the review of https://gitlab.com/tango-controls/pogo/-/merge_requests/126

As said before, other institutes using POGO are warmly encouraged to participate in this review too.

### cpptango#300

**Action - Max IV**: get feedback from Johan Forsberg on cppTango#300  **Done just after the meeting**

### Pipe limitation with DevEnum

Andy mentioned that someone encountered the documented limitation with the pipes not yet supporting DevEnum.

**Action - ESRF**: Clarify whether the DevEnum are really not supported. Verify that this limitation is well documented
(It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of 
DevEnum in pipes if it's a real limitation. 

## AOB

### Conda packages

Benjamin announced that a pytango conda package for python 3.10 is now available on conda-forge.  
He also said that the end of life of the python 3.6 version will be at the end of 2021. 

### Next Kernel Teleconf Meeting
It has been decided to do the next Tango Kernel Teleconf Meeting in 2 weeks, so we could have 2 Tango Kernel Teleconf 
in December instead of only 1 if we had kept the 2nd and 4th Thursday of the month.  
As a consequence, the next Tango Kernel Teleconf meetings will take place on Thursday 2nd December and on Thursday 16th 
December 2021 at 15:00 CET.

## Summary of remaining actions

**All institutes**:
- Ask java developers to update their projects and remove any reference to bintray
- **Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126.**   

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?

**cppTango developers**:
- Assign milestones (9.4 or 9.3.5) to issues we want to be part of 9.4 or 9.3.5 releases  
- Please review and merge cppTango!875.

**Damien (ESRF)**:
- Merge https://gitlab.com/tango-controls/pogo/-/merge_requests/127 and prepare a new Pogo release.

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitizer work completed, document/comment the current cppTango
  code to better understand the current usage of the different mutexes
  and propose some solutions to avoid some potential deadlocks.

**Igor (IK)**:
- Find a way to remove https://tango-rest-api.readthedocs.io

**Reynald (ESRF)**:
- Ping Tango java developers to ensure they update their projects after the Bintray closure
- Clarify whether the DevEnum are really not supported. Verify that this limitation is well documented
  (It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of
  DevEnum in pipes if it's a real limitation.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.
- Review cppTango!742 (Function name and line number in logs and exceptions).

**Thomas (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26

**Thomas (SKAO)**:
- Organize a Tango Workshop at the next ADASS conference
   - Get in touch with other institutes using Tango in astronomy to see whether they could help (LOFAR, INAF, VIRGO,...)
   - Prepare a short description of the workshop and share it with the kernel team (slack and/or kernel mailing list).
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration:
  Drive new repository creation on GitLab with a well known device server.

# Tango Kernel Follow-up Meeting

Held on 2021/08/26 at 15:00 CEST on Zoom.

**Participants**:

- Andy Gotz (ESRF)
- Nicolas Leclercq (ESRF)
- Sergi Rubio (ALBA)
- Lorenzo Pivetta (Elettra)
- Gwenaëlle Abeillé (Soleil)
- Thomas Braun (Byte Physics)
- Benjamin Bertrand (MAX IV)
- Vincent Hardion (MAX IV)
- Anton Joubert (SARAO)
- Michał Liszcz (S2Innovation)
- Lukasz Zytniak

Hosted by Vincent Hardion.
Minutes by Benjamin Bertrand.

### Status of [Actions defined in the previous meetings](../2021-08-12/Minutes.md#summary-of-remaining-actions)

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
  - No news.
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  - This task should be delegated to one of the new company.

**Damien (ESRF)**:
- Create some issues in Pogo before implementing some changes which might have an impact
on the community to give a chance to the community to comment the proposed changes.
- Make available to the community (gitlab registry?) a docker image with Pogo.
- pogo#122 branch issues - make the tag and delete the old branch.

Damien not attending. No news.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup.
  - A subgroup should be created under tango-controls for those repositories.
- Review cppTango!742 (Function name and line number in logs and exceptions).
  - Still working on it. Note that a force push was done on the branch.

**S2Innovation (Piotr Goryl)**:
- Review rest-api!2 (readthedocs rest api doc adapted to Gitlab)
  - Action on Reynald to merge. Can someone else help? He had some comments but someone else could check.
    I checked after the meeting and the first comment hadn't been addressed. Action back to S2Innovation.

**S2Innovation (Michał Liszcz)** and **Thomas B (Byte Physics)**:
- Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)).
MR: cppTango!864. Michał made some progress on cppTango!864. we have now 3 new jobs (ASAN, TSAN, UBSAN), which are currently failing.
Michał proposes to fix the issues reported asap in different MRs.
- **Thomas B** review UBSAN fixes in cppTango!865
- **Michał** continue on ASAN, TSAN jobs?

Michal did some fixes. Still working on it.
Thomas proposed to set `allow_failure: true` on the failing jobs and merge cppTango!864, so we can continue
with other MR to fix those. Action on Michal.

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitzer work completed, document/comment the current cppTango
code to better understand the current usage of the different mutexes
and propose some solutions to avoid some potential deadlocks.
  - Still waiting on previous action to be completed

**S2Innovation (Michał Liszcz)**
- Make MRs (main and 9.3 backports) with timestamp precision increased from milliseconds to microseconds (hard-coded). cppTango#858.
  - Andy asked about nanoseconds. This is only for logging. Internally everything is already in nanoseconds.
    Thomas would only do it for the main branch. To check with Stephane if backport to 9.3 is needed.

**Thomas B (Byte Physics)**
- Test cppTango compilation on https://github.com/pypa/manylinux for PyTango binary wheel, pytango#323.
  - No update

**Reynald (ESRF)**:
- Analyze core file obtained after a crash with special cppTango version including
patches solving a deadlock and catching more exceptions in the polling thread.
  - Reynald not attending. No news.

**cppTango developers and other volunteers**:
- Review TangoDatabase!28
  - Could be given to Observatory Sciences

**Vincent (MAX IV)**
- Review RFC-10 Message update:  rfc!124
  - Vincent did a review. WIP. Playground: <https://gitlab.com/hardion/tango-rfc-playground>
- For possible [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration: contact SOLEIL and DESY and ESRF, CC Thomas B.
  - Several propositions were given. No consensus so far. Soleil have their own gitlab instance that they could use.
    The Tango catalogue can point to the git repository on different GitLab instances.
    DESY would prefer an equivalent in gitlab.com.
    Need to decide on ownership and who/what will migrate.
    Would be nice to have a reference used by several institutes.
    Find someone who wants to use it (DESY?).
    Start with modbus? or socket?
    We should start the new repository with a well known device server.

### Call for tender news

Andy gave some news about the call for tender.
Four companies were chosen:

- S2inovation
- Byte Physics (c++ and python + documentation)
- Observatory Sciences
- IK company (java c++ web)

### Tango Community Meeting

Nicolas is working on an agenda:

- project status
- tango ecosystem
- tango roadmap
- feedback from the community

### ICALEPCS 2021

A meeting will take place tomorrow (2021-08-27) about the workshop.

Andy will collect information from the kernel developers for the tango update presentation.
Vincent will start a template on <https://www.overleaf.com>.

### Tango RFCs

RFC-10 already discussed in the actions.

### High priority issues

[High Priority MR](https://gitlab.com/tango-controls/cppTango/-/merge_requests?label_name%5B%5D=High+Priority)
[High Priority issues](https://gitlab.com/tango-controls/cppTango/-/issues?label_name%5B%5D=High+Priority)

The number of MRs is growing too much.

- revise MR process: we should not need to have 2 reviewers for simple MR
- need more people reviewing

Where does the rule of having 2 reviewers come from?
Was it from the sterring committee? Or just from a kernel meeting?
Lorenzo thinks it's the latter.

Thomas: action to define new process.
Lower the number of approval to one and add new label for complex ones

### Issues requiring discussion

ESRF: issues with some devices that hang.
Can't restart them from astor (have to do kill -9).
Lots of occurences with Power supply.

cppTango!863 to solve deadlock didn't fix the pb.
Seems to be related to the events.

At MAX IV, we had a similar issue. We think one of the client was registering itself many times.
Could see that in the logs but couldn't see which client.
The problem disappeared in the end (after restarting different clients and servers).

MAX IV: extractor gauges segfault around midnight
Should provide core dump if available to core dev.

### AOB

#### java projects

Bintray is down. Many java projects haven't been updated and can't be built anymore.
Required changes were communicated by Gwenaëlle to the developers.
Should there be an action to update them?

#### java tools installation

No documentation on how to install java tools.
Installing tools like jive, astor... should be easy for beginners.
Issue on tango-doc created by Vincent: tango-doc#376

#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).

The next Tango Kernel meeting will take place on **Tuesday 14th of September at 10:00 CEST** as a satellite meeting of the Tango Collaboration Virtual Meeting.
As such there will be no meeting in 2 weeks (on Thursday 9th September).

### Summary of remaining actions

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.

**Damien (ESRF)**:
- Create some issues in Pogo before implementing some changes which might have an impact
on the community to give a chance to the community to comment the proposed changes.
- Make available to the community (gitlab registry?) a docker image with Pogo.
- pogo#122 branch issues - make the tag and delete the old branch.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.
- Review cppTango!742 (Function name and line number in logs and exceptions).

**S2Innovation (Piotr Goryl)**:
- Address comments on rest-api!2 (readthedocs rest api doc adapted to Gitlab).

**S2Innovation (Michał Liszcz)** and **Thomas B (Byte Physics)**:
- Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)).
- **Michał** set `allow_failure: true` on the failing jobs and merge cppTango!864
- **Thomas B** review UBSAN fixes in cppTango!865
- **Michał** continue on ASAN, TSAN jobs?

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitzer work completed, document/comment the current cppTango
code to better understand the current usage of the different mutexes
and propose some solutions to avoid some potential deadlocks.

**S2Innovation (Michał Liszcz)**
- Make MRs (main and 9.3 backports) with timestamp precision increased from milliseconds to microseconds (hard-coded). cppTango#858. Check with Stephane if backport to 9.3 is needed?

**Thomas B (Byte Physics)**
- Test cppTango compilation on https://github.com/pypa/manylinux for PyTango binary wheel, pytango#323.
- Define new process for cppTango MR (lower the number of approval to one and add new label for complex ones?)

**Reynald (ESRF)**:
- Analyze core file obtained after a crash with special cppTango version including
patches solving a deadlock and catching more exceptions in the polling thread.

**cppTango developers and other volunteers**:
- Review TangoDatabase!28 (could be given to Observatory Sciences)

**Vincent (MAX IV)**
- Review RFC-10 Message update: rfc!124
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration:
  Drive new repository creation on GitLab with a well known device server.

**All institutes**
- Ask java developers to update their projects and remove any reference to bintray

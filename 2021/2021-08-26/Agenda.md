# Tango Kernel Follow-up Meeting - 2021/08/26

To be held on 2021/08/26 at 15:00 CEST on Zoom.

# Agenda

 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-08-12/Minutes.md#summary-of-remaining-actions)
 2. Tango Collaboration Meeting
 3. Icalepcs 2021
 4. Tango RFCs (https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-08-26)
 5. High priority issues
 6. Issues requiring discussion
 7. AOB

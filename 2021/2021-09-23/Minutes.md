# Tango Kernel Follow-up Meeting

Held on 2021/09/23 at 15:00 CEST on Zoom.

**Participants**:

- Benjamin Bertrand (MAX IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (Byte Physics)
- Vincent Hardion (MAX IV)
- Anton Joubert (SARAO)
- Damien Lacoste(ESRF)
- Nicolas Leclercq (ESRF)
- Michał Liszcz (S2Innovation)
- Lorenzo Pivetta (Elettra)
- Lukasz Zytniak (S2Innovation)

### Status of [Actions defined in the previous meetings](../2021-08-26/Minutes.md#summary-of-remaining-actions)

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message). **No news**
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company. **No news**

**Damien (ESRF)**:
- Create some issues in Pogo before implementing some changes which might have an impact
  on the community to give a chance to the community to comment the proposed changes. **WIP**
- Make available to the community (gitlab registry?) a docker image with Pogo. **Benjamin suggested having a look at the docker-mysql example**.
- pogo#122 branch issues - make the tag and delete the old branch. **done**

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.
- Review cppTango!742 (Function name and line number in logs and exceptions). **Michal will ping Sergi**

**S2Innovation (Piotr Goryl)**:
- Address comments on rest-api!2 (readthedocs rest api doc adapted to Gitlab).
**Benjamin noticed that the online version of the rest-api documentation is still pointing on github.  
S2Innovation will investigate and fix it.**  

**S2Innovation (Michał Liszcz)** and **Thomas B (Byte Physics)**:
- Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)). **WIP**
- **Michał** set `allow_failure: true` on the failing jobs and merge cppTango!864. **In the review phase**
- **Thomas B** review UBSAN fixes in cppTango!865 **In the review phase**
- **Michał** continue on ASAN, TSAN jobs

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitizer work completed, document/comment the current cppTango
  code to better understand the current usage of the different mutexes
  and propose some solutions to avoid some potential deadlocks.

**S2Innovation (Michał Liszcz)**
- Make MRs (main and 9.3 backports) with timestamp precision increased from milliseconds to microseconds (hard-coded). cppTango#858. Check with Stephane if backport to 9.3 is needed?
**Done. cppTango!866 and cppTango!867 have been merged into main and 9.3-backports branches.**

**Thomas B (Byte Physics)**
- Test cppTango compilation on https://github.com/pypa/manylinux for PyTango binary wheel, pytango#323.  
**See https://gitlab.com/tango-controls/pytango/-/issues/323#note_685261033**
- Define new process for cppTango MR (lower the number of approval to one and add new label for complex ones?)  
**Contribute.md file was updated with new process in main branch only so far. See cpptango!874**

**Reynald (ESRF)**:
- Analyze core file obtained after a crash with special cppTango version including
  patches solving a deadlock and catching more exceptions in the polling thread.
**The problem reported during 2021-08-26 Kernel Teleconf with a Power Supply device server was due to a bad polling 
and device configuration which was leading to a situation where the polling thread was monopolizing the Tango Monitor.  
Another core file was analyzed on another device server which seemed to be in a deadlock:**  
- **1 thread was waiting for the event_mutex and holding the Tango Monitor on the admin device while executing ZmqEventSubscriptionChange command**
- **1 polling thread was holding the event_mutex and pushing an event with large data and was waiting on a condition variable to be awakened by ZMQ**
- **another thread was holding the device Tango Monitor and might be trying to lock the attr mutex (in read_attributes_no_except)**

**cppTango developers and other volunteers**:
- Review TangoDatabase!28 (could be given to Observatory Sciences) **Thomas Braun did a review, Lorenzo will see with Alessio to address the review comments**

**Vincent (MAX IV)**
- Review RFC-10 Message update: rfc!124
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration:
  Drive new repository creation on GitLab with a well known device server.

**All institutes**
- Ask java developers to update their projects and remove any reference to bintray
**New Action - Reynald**: Create an issue on all the projects still having references to bintray.

**cppTango developers**  
- Assign milestones (9.4 or 9.3.5) to MR and issues we want to be part of 9.4 or 9.3.5 releases.
**MR have been done during the Tango Community Week. Issues still needs to be associated with milestones.
This will be done during a teleconference on Thursday 30th September at 10:00 CEST.**

**ESRF**
- Ask Freexian for tango Ubuntu packages 
**Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated. What about having a tango-controls ppa (personal package archive)?**

### Tango RFCs

Lorenzo proposed to start thinking about the dates of the foreseen RFC Meeting in Krakow. 
Lorenzo proposed to do this meeting on week 47 (Week from 22nd November to 26th November).  

**Action - Lorenzo:** Propose week 47 (or another) to the community (slack, mailing lists?) for the RFC face to face meeting.

## ICALEPCS 2021

### STATE OF THE TANGO CONTROLS COLLABORATION in 2021 paper

Andy is looking for volunteers to collaborate on the usual Tango Community update ICALEPCS paper.  
Anton will take care of the pytango part of this paper.  
Lorenzo proposed his help too.  
Benjamin will write something about the conda packages available on conda-forge.  
Reynald, Michal and Thomas will contribute on the cppTango status part.  
Damien will contribute on the Pogo news part.  
Vincent will contribute on the web tools part.  
Piotr and Vincent can contribute on the RFC part.  
There could be some sections about HDB++ and the alarm systems too.  

overleaf will be used to collaborate on this paper. The overleaf editable link has been published on "icalepcs-2021" 
tango-controls slack channel.

**Action - All volunteers**: Contribute to ICALEPCS 2021 "STATE OF THE TANGO CONTROLS COLLABORATION in 2021" paper 

### Tango workshop preparation

Vincent needed a title for each session of the Tango workshop before the end of week 38.  

The install sessions (Kubernetes, web, ...) can be done in parallel (Zoom breakout rooms).

### High priority issues

[High Priority MR](https://gitlab.com/tango-controls/cppTango/-/merge_requests?label_name%5B%5D=High+Priority)  
[High Priority issues](https://gitlab.com/tango-controls/cppTango/-/issues?label_name%5B%5D=High+Priority)

### Issues requiring discussion

#### Pytango/cppTango compatibility tests
There was a discussion about how to test pytango compatibility with cpptango 9.3 and 9.4.  
Benjamin will try to add a CI job in cppTango to build a cpptango development conda package which will be available 
in Gitlab registry. He will try to configure some nightly builds. 
No tag will be required in cpptango to identify the version.

#### Tango compatibility with old clients
Vincent asked whether some old Tango clients could speak to new cppTango server.  
Reynald replied that this is indeed possible but limited to the Tango features supported by the Tango IDL interface 
supported by the client.  
Michal said it would be good to get some tests in cppTango with some old clients.  
He also said it would be nice to get some integration tests with some java clients too for instance.

#### Docker 
Benjamin asked whether it could be useful to create some docker images with the Tango Starter or TangoAccessControl.  
For the Starter, since it must be associated with a host, it does not make much sense, except if you want to start 
device servers on this specific docker image instance, which might actually make sense.

Benjamin intends to use conda packages to install Tango services on the target docker images.  
Michal raised a warning about the memory footprint on the resulting docker image induced by the usage of conda. 
He advised using a build step using conda and then to copy the binaries.  
He also proposed to use alpine as base image to reduce the footprint.  

#### 1password 

Benjamin suggested to use 1password services, which gives licenses to open source projects, to easily share the passwords 
used by the Tango developers community on the different service providers (gitlab, docker hub, anaconda, sourceforge, 
pypi, readthedocs, ...).

It was decided to rather list all the different administrators in the Tango community of these different online services in 
[TangoTickets gitlab repository](https://gitlab.com/tango-controls/TangoTickets) README.md file.

**Action - Anton:** Start a MR to update TangoTickets README.md file with list of online services and maintainers.   
**Done after the meeting. See https://gitlab.com/tango-controls/TangoTickets/-/merge_requests/1**

### AOB

#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).
So the next Tango Kernel meeting should take place in 3 weeks on Thursday 14th of October at 10:00 CEST **but as this is 
would be on the same date and time as the ICALEPCS 2021 Tango Workshop**, it is proposed to exceptionally move the **next Tango 
Kernel Teleconf meeting to Thursday 7th October 2021 at 15:00 CEST**.

### Summary of remaining actions

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
    What about having a tango-controls ppa (personal package archive)?

**Damien (ESRF)**:
- Create some issues in Pogo before implementing some changes which might have an impact
  on the community to give a chance to the community to comment the proposed changes.
- Make available to the community (gitlab registry?) a docker image with Pogo.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.
- Review cppTango!742 (Function name and line number in logs and exceptions).

**S2Innovation (Piotr Goryl)**:
- Fix issue with rest-api online documentation

**S2Innovation (Michał Liszcz)** and **Thomas B (Byte Physics)**:
- Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)).
- **Michał** set `allow_failure: true` on the failing jobs and merge cppTango!864.
- **Thomas B** review UBSAN fixes in cppTango!865
- **Michał** continue on ASAN, TSAN jobs

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitizer work completed, document/comment the current cppTango
  code to better understand the current usage of the different mutexes
  and propose some solutions to avoid some potential deadlocks.

**Thomas B (Byte Physics)**:
- Define new process for cppTango MR (lower the number of approval to one and add new label for complex ones?)  
  **Contribute.md file was updated with new process in main branch only so far. Still need to be done on 9.3-backport branch**

**Reynald (ESRF)**:
- Create an issue on all the projects still having references to bintray.

**Lorenzo (Elettra)**:
- Propose week 47 (or another) to the community (slack, mailing lists?) for the RFC face to face meeting.
- Contact Alessio to address review comments on TangoDatabase!28

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration:
  Drive new repository creation on GitLab with a well known device server.

**All institutes**:
- Ask java developers to update their projects and remove any reference to bintray

**cppTango developers**:
- Assign milestones (9.4 or 9.3.5) to issues we want to be part of 9.4 or 9.3.5 releases.

**All volunteers**: 
- Contribute to ICALEPCS 2021 "STATE OF THE TANGO CONTROLS COLLABORATION in 2021" paper 

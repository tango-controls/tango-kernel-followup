# Tango Kernel Follow-up Meeting - 2021/09/23

To be held on 2021/09/23 at 15:00 CEST on Zoom.

# Agenda

 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-08-26/Minutes.md#summary-of-remaining-actions)
 2. Tango RFCs
 3. ICALEPCS 2021
 4. High priority issues
 5. Issues requiring discussion
 6. AOB

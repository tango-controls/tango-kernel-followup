# Tango Kernel Follow-up Meeting - 2021/10/28

To be held on 2021/10/28 at 15:00 CEST on Zoom.

# Agenda

 1. RFC workshop organization
 2. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-10-07/Minutes.md#summary-of-remaining-actions)
 3. High priority issues
 4. Issues requiring discussion  
    4.1 POGO: Feedback wanted on https://gitlab.com/tango-controls/pogo/-/merge_requests/126 MR
 5. AOB

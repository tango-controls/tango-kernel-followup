# Tango Kernel Follow-up Meeting

Held on 2021/10/28 at 15:00 CEST on Zoom.

**Participants**:

- Benjamin Bertrand (MAX IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (byte physics)
- Andy Goetz (ESRF)
- Vincent Hardion (MAX IV)
- Anton Joubert (SARAO)
- Thomas Juerges (SKAO)
- Damien Lacoste(ESRF)
- Michał Liszcz (S2Innovation)
- Tomasz Madej (S2Innovation)
- Lorenzo Pivetta (Elettra)
- Łukasz Żytniak (S2Innovation)

## RFC Workshop Organization

S2Innovation will organize the RFC workshop. 
Piotr found a house about 30km from Krakow where 10 to 12 people could stay during the workshop.
This workshop should be at least 3 full days long.

**Łukasz (S2Innovation)**: Send a Doodle to find the best dates for the workshop (covering end of 2021 / beginning of 2022)

## Status of [Actions defined in the previous meetings](../2021-10-07/Minutes.md#summary-of-remaining-actions)

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?

**Damien (ESRF)**:
- Merge https://gitlab.com/tango-controls/pogo/-/merge_requests/127 and prepare a new Pogo release.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.
- Review cppTango!742 (Function name and line number in logs and exceptions).   

**S2Innovation (Piotr Goryl)**:
- Find a way to remove https://tango-rest-api.readthedocs.io **WIP**

**S2Innovation (Michał Liszcz)** and **Thomas B (Byte Physics)**:
- **Thomas B** review UBSAN fixes in cppTango!865 **Review done by Thomas. This action will be followed from now on on cpptango git repo.**
- **Michał** continue on ASAN, TSAN jobs **No news but there are cppTango issues to track down this action.**

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitizer work completed, document/comment the current cppTango
  code to better understand the current usage of the different mutexes
  and propose some solutions to avoid some potential deadlocks.

**Thomas B (byte physics)**:
- Define new process for cppTango MR (lower the number of approval to one and add new label for complex ones?)  
  Contribute.md file was updated with new process in main branch only so far. Still need to be done on 9.3-backport branch.
**MR ready and will be merged soon. Closing this action**

**Lorenzo (Elettra)**:
- Propose week 47 (or another) to the community (slack, mailing lists?) for the RFC face to face meeting. **Action transfered to Łukasz**
- Contact Alessio to address review comments on TangoDatabase!28 **Done. Thomas Braun will reply to Alessio. 
To be followed on the Tango Database Gitlab repository.**

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration:
  Drive new repository creation on GitLab with a well known device server.

**Soleil**
- Clarify with Gwenaelle and Krystian who will write the Java core library section in
  "STATE OF THE TANGO CONTROLS COLLABORATION in 2021" paper and also in the "MIGRATION OF TANGO-CONTROLS SOURCE CODE REPOSITORIES"
  paper. **Done**

**All institutes**:
- Ask java developers to update their projects and remove any reference to bintray.
**ESRF java developers plan to work on this topic at the beginning of November**
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126. 
**Damien is still waiting for some feedback. Please review the proposal.**
- Prepare some questions for the ICALEPCS Tango workshop Open Space session in case there is not enough questions from the participants.
**Workshop Done**

**cppTango developers**:
- Assign milestones (9.4 or 9.3.5) to issues we want to be part of 9.4 or 9.3.5 releases. **WIP**  

**All volunteers**:
- Contribute to ICALEPCS 2021 "STATE OF THE TANGO CONTROLS COLLABORATION in 2021" paper. The overleaf editable link 
has been published on "icalepcs-2021" tango-controls slack channel. **Done**

## High Priority Issues
Andy wanted to get some news about https://gitlab.com/tango-controls/TangoTickets/-/issues/39.

**Reynald (ESRF)**: Comment https://gitlab.com/tango-controls/TangoTickets/-/issues/39 to add a link to the related 
cppTango issue.

## AOB
### ICALEPCS

Anton got some feedback that the Tango workshop was very difficult to follow for people new to Tango due to the lack of time and the speed of 
the presentations.
Thomas Juerges proposes to organize different workshops for different levels of Tango expertize.   
For complete beginners, 1/2 day might be enough to expose the basics of Tango and to let people play a bit with it.  
For newbies, he is proposing to first start with playing with jupytango to let the participants play with Tango devices 
to better understand the purpose of Tango. In a second stage, we could teach them how easy it is to write simple device 
servers.  
For initiated Tango users/programmers, we could organize a 2 days workshop.  
Thomas Juerges recommended the [ADASS conference](https://www.adass.org) (Astronomical Data Analysis Software & Systems) 
which is allowing the organization of longer workshops than the ones from ICALEPCS.

**Thomas (SKAO)**: Contact ADASS organizers to see whether we could organize a Tango-Controls workshop there.

**Vincent (Max IV)**: Get videos from ICALEPCS

### Conda packages

**cppTango team**: Please review and merge cppTango!875 

### Docker Images

Benjamin worked on common Tango-Controls docker images (like tango-cs). They could be added to the gitlab registry.   
Some of them are already available there: https://gitlab.com/groups/tango-controls/docker/-/container_registries  
The work is done in the repositories located in the [docker tango-controls Gitlab group](https://gitlab.com/tango-controls/docker).

### TangoTest Windows Executable

Anton said he would like to get a TangoTest Windows Executable to run pytango CI tests.  
Mateusz (S2Innovation) already has a TangoTest executable hosted on his personal
GitHub account, [here](https://github.com/spirit1317/tango-test-exe), but there is no CI.
This repo is used for PyTango AppVeyor tests, but is not the correct place to host it.
He asked whether this could be added as an appveyor CI job in TangoTest repository. The answer is yes.   
Merge Request is welcome.   
Thomas Braun is not against the appveyor CI job but also suggested exploring the conda package way.
Could also consider a Gitlab CI Windows build.

### TangoDatabase#26

**Thomas (byte physics)**: Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26

### New cppTango developer at S2Innovation
We welcome Tomasz Madej who started working for S2Innovation and will tackle some cppTango issues.

### Next Kernel Teleconf Meeting
Since the 2nd Thursday of November is the 11th November and this is an official holiday in several European countries, 
it has been decided to move the next meeting to the week after, so on **Thursday 18th November 2021 at 15:00 CET**.

## Summary of remaining actions

**All institutes**:
- Ask java developers to update their projects and remove any reference to bintray
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?
- Activate Gitlab Gold Plan license
- Fix the issue with the mailing lists no longer working

**cppTango developers**:
- Assign milestones (9.4 or 9.3.5) to issues we want to be part of 9.4 or 9.3.5 releases
- Please review and merge cppTango!875

**Damien (ESRF)**:
- Merge https://gitlab.com/tango-controls/pogo/-/merge_requests/127 and prepare a new Pogo release.

**Łukasz (S2Innovation)**: 
- Send a Doodle to find the best dates for the workshop (covering end of 2021 / beginning of 2022)

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitizer work completed, document/comment the current cppTango
  code to better understand the current usage of the different mutexes
  and propose some solutions to avoid some potential deadlocks.

**Piotr (S2Innovation)**:
- Find a way to remove https://tango-rest-api.readthedocs.io

**Reynald (ESRF)**: 
- Comment https://gitlab.com/tango-controls/TangoTickets/-/issues/39 to add a link to the related
cppTango issue.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.
- Review cppTango!742 (Function name and line number in logs and exceptions).

**Thomas (byte physics)**: 
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26

**Thomas (SKAO)**: 
- Contact ADASS organizers to see whether we could organize a Tango-Controls workshop there.

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration:
  Drive new repository creation on GitLab with a well known device server.
- Get videos from ICALEPCS

# Tango Kernel Follow-up Meeting

Held on 2021/06/10 at 15:00 CEST on Zoom.

**Participants**:

- Andy Goetz (ESRF)
- Anton Joubert (SARAO)
- Benjamin Bertrand (MAX IV)
- Damien Lacoste (ESRF)
- Gwenaelle Abeille (Soleil)
- Lorenzo Pivetta (ELETTRA)
- Michał Liszcz (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Thomas Juerges (LOFAR)
- Vincent Hardion (MAX IV)


## Minutes

Reynald who arrived slightly late at the meeting (shame on him!) has been designated (again! ;-) ) to take notes and to write the minutes of this meeting. 

### Status of [Actions defined in the previous meetings](../2021/2021-05-20/Minutes.md#summary-of-remaining-actions)

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Andy (ESRF)**:
Contact freexian about maintaining omniORB packages in debian. Andy asked for a quote from Freexian and will place an order asap after receiving the quote.
**Done. Order is on going. Freexian will start working on this topic as soon as the administrative process is over.**

**Action - Anton (SRAO)**: Contact Geoff and agree on a suitable date for PyTango Kernel Webinar 
and then advertise it on the Tango Mailing list and on tango-controls website.
**Done. Geoff and Anton will present the 4th Tango Kernel Webinar about PyTango on Wednesday 23rd June at 10 am CEST.**

**Action - Benjamin B.**:
Try out gitlab runners on windows for pytango. 
**Done. The main limitation with gitlab runners on windows if using conda on Windows is that it is not possible to use python 2. 
Therefore it has been decided to stick with appveyor for the moment.**

**Action - Benjamin B.**: Update the [docker-mysql](https://gitlab.com/tango-controls/docker/mysql) project to use the gitlab docker registry.
MR(https://gitlab.com/tango-controls/docker/mysql/-/merge_requests/4) waiting for review.
**MR still waiting for review. There has been questions about SKA docker images. Are they similar? 
How to avoid duplication of the work? Shall we make the SKA ones the official ones? 
cpptango CI is using tango-cs docker image which is using this docker-mysql image under the hood. 
Decision was taken to continue the work done initiated by Benjamin on this MR and to use this image from the gitlab docker regitry.  
Anton advertised links to find SKA docker images:  
https://gitlab.com/ska-telescope/ska-tango-images/-/tree/master/docker/tango/tango-db  
https://artefact.skao.int/  
Sergi asks what database is currently used in the different institutes for the Tango Database:  
mariadb is used at ALBA, ESRF, MAX IV, SKA. mysql is used at Soleil and by tango-database conda package.**

**Action - Krystian & Tango Java Projects maintainers**: Adapt mvn release target to deploy new releases on Maven Central instead of Bintray.
**Gwen will send instructions to the Tango Java projects developers**.

**Action - Reynald**:
Contact his ESRF colleagues and coordinate the migration of the remaining repositories they are currently
administrating (pogo, Astor, jive). **jive has been migrated**

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issues. 
**Any good idea in these MRs should be reflected in the Tango 10 RFCs and converted as an rfc issue**

**Action - Sergi**:
Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to be migrated under hdbpp subgroup.

**Action - Sergi**:
https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 (on names on logs) to be reviewed.  
**Michal rebased this MR. Sergi will test it and comment/approve depending on the tests results.**

**Action - Thomas B.**:
Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side. pytango#409 is a
cppTango bug, needs a proper fix.

**Action - Vincent**: Ensure rest-api repository can be migrated smoothly and coordinate the migration with the Gitlab
migration team (Benjamin could help). **Done. Benjamin migrated the rest-api repository. There is still some work to be done to 
configure properly readthedocs. Piotr will take care of that.**

**Action - S2Innovation**: Provide a summary of what needs to be done for the migration from Bintray to Maven Central 
and an estimation of the time required to do the migration per project. 
Is the migration to Maven Central an adequate solution for all the Tango Java projects? Would Gitlab's package registry be a valid alternative?
** Gwenaelle will take care of this action**

**Action - All Tango ICALEPCS participants**: Share the Tango-related ICALEPCS 2021 submitted abstracts titles on Icalpecs-2021 tango-controls slack channel.
**Done. Thanks to the ones who commented on the Icalepcs-2021 slack channel.**


### Tango RFCs

RFC-10 branch is now available on RtD:
- https://tango-controls.readthedocs.io/projects/rfc/en/raw-rfc-10-request-reply/
- https://tango-controls.readthedocs.io/_/downloads/rfc/en/raw-rfc-10-request-reply/pdf/

Reynald asked Emmanuel Taurel to review the RFCs before he retires (Last day of work: 17th June). 
Emmanuel will do his best to review the RFCs. He probably won't be able to review all of them.  
He will try to detect the forgotten features.

### High priority issues 

#### cppTango issues

ESRF would like to raise the priority for cppTango#845.

#### BinTray closure

Krystian Kedron prepared the migration from Bintray to Maven Central for JTango.  
All the remaining Tango Java projects need to do similar migrations.  
Developers need to follow a procedure with sonatype (create an issue) in order to get permissions to push 
artifacts into tango-controls maven organization. Gwen will send instructions to 
the Tango Java projects developers who are not yet registered (Faranguiss Poncet, Jean-Luc Pons, Damien Lacoste, Nicolas Tappret).  

**Action - Soleil**: Provide a summary of what needs to be done to the Java projects Tango developers not yet registered on sonatype 
(Faranguiss Poncet, Jean-Luc Pons, Damien Lacoste, Nicolas Tappret) for the migration from Bintray to Maven Central.


### Issues requiring discussion

#### TangoDatabase #8 MR status

Lorenzo wanted to discuss about the status of https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/8 TangoDatabase MR 
which is a stalled MR since a while.  

This MR should get more focus. Lorenzo will check with Alessio whether a rebase would be required. 
Something will have to be added on tango-doc to advertize this feature.  
Michal suggested to implement this feature at the cppTango level so any device server could benefit from it.  
According to him, it should not be too complicated to implement at the cpptango level. 
Before doing that in cppTango, we will have to figure out what is the best way to configure 
this feature (env. variable, command line argument, ...?) and to ensure it could be used easily with the Starter.

**Action - Reynald**: Create an issue in TangoTickets about the possibility to use a feature like in 
https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/8 at device server level 
to get a location where to exchange ideas on this topic (how to configure it? Changes required on the tools).

Tango is currently missing a proper server side access control. 
Encryption would be a useful feature too.  

**Action - Lorenzo**: Create issues for server side access control and encryption features in rfc repository with tango-v10 label.

### AOB
#### Kernel Webinars

The 4th Kernel Webinar will take place on Wednesday 23rd June at 10:00 CEST.  
This webinar will focus on PyTango and will be presented by Geoffrey Mant and Anton Joubert.

**Action - Anton**: Create a news on tango-controls.org to advertise the webinar and send an e-mail on tango-controls info mailing list.
**Done at the time of writing these minutes**

#### Tango Database Backend with Yaml files

ESRF Beamline Control Unit developed an implementation of the Tango Database server using Beacon, stored using yaml files.  
This version currently does not work with some Tango devices and is tightly coupled with BLISS but if this project is of any interest to 
the Tango community, they might do the work to remove the dependency to BLISS and make it available to the community.  
The benefits to the community appear clearly to ease the setup of some CI tests or to configure easily a small Tango control system.

**Action - Reynald**: Contact his ESRF colleagues to see whether it would be possible to 
make the Tango Database implementation based on yaml files available and easily usable by the community.


#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).

So the next Tango Kernel Teleconf Meeting will take place on Thursday 24th June 2021 at 15:00 CEST.

## Summary of remaining actions

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Soleil**: Provide a summary of what needs to be done to the Java projects Tango developers not yet registered on sonatype 
(Faranguiss Poncet, Jean-Luc Pons, Damien Lacoste, Nicolas Tappret) for the migration from Bintray to Maven Central.

**Action - Reynald**:
Contact his ESRF colleagues and coordinate the migration of the remaining repositories they are currently
administrating (pogo, Astor).

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issues.  
Any good idea in these MRs should be reflected in the Tango 10 RFCs and converted as an rfc issue.

**Action - Sergi**:
Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to be migrated under hdbpp subgroup.

**Action - Sergi**:
https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 (on names on logs) to be reviewed.  

**Action - Thomas B.**:
Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side. pytango#409 is a
cppTango bug, needs a proper fix.

**Action - S2Innovation**: Configure rest-api git repo readthedocs for Gitlab.

**Action - Reynald**: Create an issue in TangoTickets about the possibility to use a feature like in 
https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/8 at device server level 
to get a location where to exchange ideas on this topic (how to configure it? Changes required on the tools).

**Action - Lorenzo**: Create issues for server side access control and encryption features in rfc repository with tango-v10 label.

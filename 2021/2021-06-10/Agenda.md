# Tango Kernel Follow-up Meeting - 2021/06/10

To be held on 2021/06/10 at 15:00 CEST on Zoom.

# Agenda

 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-05-20/Minutes.md#summary-of-remaining-actions)
 2. Tango RFCs
 3. High priority issues
 4. Issues requiring discussion
 6. AOB
    - Databaseds merge request status: https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/8

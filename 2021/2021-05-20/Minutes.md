# Tango Kernel Follow-up Meeting

Held on 2021/05/20 at 15:00 CEST on Zoom.

**Participants**:

- Anton Joubert (SARAO)
- Benjamin Bertrand (MaxIV)
- Jairo Moldes (ALBA)
- Lorenzo Pivetta (ELETTRA)
- Michał Liszcz (S2Innovation)
- Piotr Goryl (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Thomas Braun (byte physics)
- Thomas Juerges (LOFAR)
- Stephane Poirier (Soleil)

## Minutes

Reynald has been "randomly" designated volunteer to take notes and write the minutes of this meeting. 

### Status of [Actions defined in the previous meetings](../2021/2021-04-08/Minutes.md#summary-of-remaining-actions)

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Andy (ESRF)**:
Contact freexian about maintaining omniORB packages in debian. **Done: Andy asked for a quote from Freexian and will place an order asap aftr receiving the quote.**

**Action - Anton (SRAO)**: Contact Geoff and agree on a suitable date for PyTango Kernel Webinar 
and then advertise it on the Tango Mailing list and on tango-controls website.

**Action - Benjamin B.**:
Investigate on whether it would be possible to create conda packages for the Tango java tools.
Need to check whether the JacORB license and java CORBA classes allow to redistribute freely the software.
JacORB is under LGPL: <https://www.jacorb.org/lgpl-explained.html>.
Might require proprietary license as idl/omg/CosBridgeAdmin.idl is not under LGPL-v2.
**Done. JTango conda package is now available on conda-forge thanks to Benjamin.**

**Action - Benjamin B.**:
Try out gitlab runners on windows for pytango

**Action - Benjamin B.**: Update the [docker-mysql](https://gitlab.com/tango-controls/docker/mysql) project to use the gitlab docker registry.
Create docker subgroup and the docker-mysql will be renamed mysql and tangocs-docker to tangocs.
**Docker subgroup has been created and docker-mysql has been renamed mysql**

**Action - Krystian & Tango Java Projects maintainers**: Adapt mvn release target to deploy new releases on Maven Central instead of Bintray.

**Action - Reynald**:
Contact his ESRF colleagues and coordinate the migration of the remaining repositories they are currently
administrating (pogo, Astor, jive)

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issue

**Action - Sergi**:
Organize the migration of the repositories he's administrating (fandango, PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to be migrated under hdbpp subgroup.
**Fandango has been migrated. PANIC and simulatorDS will be migrated soon**

**Action - Sergi**:
https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 (on names on logs) to be reviewed. Michał is reworking the MR in response to Sergi's feedback.
**The MR is ready for review and is still mising 1 approval. Sergi volunteered to review it. Michal warned that this is using some C++14 features**

**Action - Thomas B.**:
Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side. pytango#409 is a
cppTango bug, needs a proper fix.

**Action - Thomas B.**:
Try out the gitlab runners on windows which are in beta phase from gitlab.  
**The results of the investigations are available in https://gitlab.com/tango-controls/cppTango/-/issues/731 with a proposal 
from Thomas to use gitlab runners on windows and to drop appveyor builds for the main branch (we still keep it for 9.3-backports branch. 
Reynald agreed on the proposed solution.  Thomas implemented the changed on https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 which needs review**

**Action - Vincent**: Ensure rest-api repository can be migrated smoothly and coordinate the migration with the Gitlab
migration team (Benjamin could help).

**Action - S2Innovation**: Organize the migration of the repositories they are administrating
(dsc, tangobox, benchmarks). **Done**


### Tango RFCs

Tango RFCs are now available on tango-controls read-the-doc: https://tango-controls.readthedocs.io/projects/rfc/en/latest/  

Piotr submitted an abstract for ICALEPCS 2021 to present the work done on the RFCs. He was looking for co-authors for this paper.  

Piotr proposed that someone asks Emmanuel Taurel to review the RFCs before he retires (mid-june).  

### High priority issues 
#### BinTray closure

Krystian Kedron prepared the migration from Bintray to Maven Central for JTango.  
All the remaining Tango Java projects need to do similar migrations.  
It would be good to get an idea of what needs to be done in order to help deciding who will do the job for the remaining Tango Java projects.  
Krystian got quite busy recently for private reasons (Congratulations Krystian!) and could not wwork full time on this.  

**Action - S2Innovation**: Provide a summary of what needs to be done for the migration from Bintray to Maven Central 
and an estimation of the time required to do the migration per project. 
Is the migration to Maven Central an adequate solution for all the Tango Java projects? Would Gitlab's package registry be a valid alternative?

While waiting for the migration away from Bintray to be over, the artifacts which were hosted on Bintray have been made available 
to the Tango community by S2Innovation on the following link: https://s2innovation.sharepoint.com/:f:/s/Developers/Eg2gQYrxpbpJp3dgb-b-f50BP-pN6RtfcPz70SXsbZ3HyA?e=uYHPiH


### Issues requiring discussion

Benjamin asked for volunteers to review the tango-database conda recipe: https://github.com/conda-forge/staged-recipes/pull/14865 and is looking for co-maintainers.  
Reynald volunteered to be listed as co-maintainer for this tango-database conda package and for doing the review.

**Action - Reynald**: Review tango-database conda recipe PR (https://github.com/conda-forge/staged-recipes/pull/14865). **Done at the time of writing these minutes**

Benjamin asked how he should choose the version number for a new release of the mysql-docker image. It has been suggested to use the Tango Database version number.  

Michal reported that tango_admin needs to be updated to be able to compile in C++14. He created this MR: https://gitlab.com/tango-controls/tango_admin/-/merge_requests/5  

Benjamin asked how to defined version numbers for the conda development packages. 
Thomas braun suggested to include the git commit hash as version string in the CMakefile. 
This string could then be used as the version for the development conda package. 
Please refer to https://gitlab.com/tango-controls/cppTango/-/merge_requests/850 to get more details.

### AOB
#### ICALEPCS 2021

Piotr said it would be good to submit an abstract for a paper presenting the status of the Tango-Controls 
Collaboration for ICALEPCS 2021. 
Reynald submitted an abstract for that after the Kernel meeting and Andy Goetz agreed to be main author and doesn't mind presenting it if required.  

S2Innovation submitted 2 papers with reference to Tango and 7 papers in total.  

LOFAR has submitted 2 abstracts:  
- Tango Controls Attribute extension in Python3 (poster, ID 1645)  
- LOFAR2.0: Station Control Upgrade (talk, ID 1614)  

MaxIV submitted an abstract about the deployment of Control System both with Conda and RPM.  

From the SKAO project (including SARAO and others), these abstract have been submitted:

- From SKA to SKAO: Early Progress in the SKAO Construction (Juande Santander Vela, Nick Rees, M. Miccolis, Marco Bartolini)
- Implementing an Event Tracing Solution with Consistently Formatted Logs for the SKA Telescope Control System (S. Twum, K. Madisa, Johan Venter, A. Joubert, P. Swart, W. Bode, Alan Bridger)
- Design Patterns for the SKA Control System (Sonja Vrcic et al.)

SARAO submitted an abstract about the python 2 to python 3 migration on MeerKAT.  

ALBA submitted an abstract about the general status of their control system.  

It was suggested to create an Icalepcs2021 channel on tango-controls slack.  
It has been created after the Kernel Meeting. Please feel free to use it to exchange on ICALEPCS2021 related topics.  

**Action - All Tango ICALEPCS participants**: Share the Tango-related ICALEPCS 2021 submitted abstracts titles on Icalpecs-2021 tango-controls slack channel

#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).

So the next Tango Kernel Teleconf Meeting will take place on Thursday 10th June 2021 at 15:00 CEST.

## Summary of remaining actions

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Andy (ESRF)**:
Contact freexian about maintaining omniORB packages in debian. Andy asked for a quote from Freexian and will place an order asap aftr receiving the quote.

**Action - Anton (SRAO)**: Contact Geoff and agree on a suitable date for PyTango Kernel Webinar 
and then advertise it on the Tango Mailing list and on tango-controls website.

**Action - Benjamin B.**:
Try out gitlab runners on windows for pytango

**Action - Benjamin B.**: Update the [docker-mysql](https://gitlab.com/tango-controls/docker/mysql) project to use the gitlab docker registry.
MR(https://gitlab.com/tango-controls/docker/mysql/-/merge_requests/4) waiting for review.

**Action - Krystian & Tango Java Projects maintainers**: Adapt mvn release target to deploy new releases on Maven Central instead of Bintray.

**Action - Reynald**:
Contact his ESRF colleagues and coordinate the migration of the remaining repositories they are currently
administrating (pogo, Astor, jive)

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issue

**Action - Sergi**:
Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to be migrated under hdbpp subgroup.

**Action - Sergi**:
https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 (on names on logs) to be reviewed.

**Action - Thomas B.**:
Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side. pytango#409 is a
cppTango bug, needs a proper fix.

**Action - Vincent**: Ensure rest-api repository can be migrated smoothly and coordinate the migration with the Gitlab
migration team (Benjamin could help).

**Action - S2Innovation**: Provide a summary of what needs to be done for the migration from Bintray to Maven Central 
and an estimation of the time required to do the migration per project. 
Is the migration to Maven Central an adequate solution for all the Tango Java projects? Would Gitlab's package registry be a valid alternative?

**Action - All Tango ICALEPCS participants**: Share the Tango-related ICALEPCS 2021 submitted abstracts titles on Icalpecs-2021 tango-controls slack channel

# Tango Kernel Follow-up Meeting - 2021/05/20

To be held on 2021/05/20 at 15:00 CEST on Zoom.

# Agenda

 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-04-08/Minutes.md#summary-of-remaining-actions)
 2. [Tango RFCs](https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-05-20)
 3. High priority issues (BinTray closure)
 4. Issues requiring discussion
 6. AOB (ICALEPCS 2021, ...)

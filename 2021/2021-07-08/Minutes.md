# Tango Kernel Follow-up Meeting

Held on 2021/07/08 at 15:00 CEST on Zoom.

**Participants**:

- Damien Lacoste (ESRF)
- Gwenaelle Abeillé (Soleil)
- Jairo Moldes (ALBA)
- Krystian Kędroń (S2Innovation)    
- Lorenzo Pivetta (ELETTRA)
- Łukasz Żytniak (S2Innovation)
- Michał Liszcz (S2Innovation)   
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Thomas Braun (Byte Physics)    

### Status of [Actions defined in the previous meetings](../2021/2021-06-24/Minutes.md#summary-of-remaining-actions)

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issues.  
Any good idea in these MRs should be reflected in the Tango 10 RFCs and converted as a rfc issue.  
**Reynald mentioned that it is impossible to exploit those MRs as they currently are because they are hundreds of commits 
behind their target branch. One solution to exploit them would be to change the target branch to a branch where the last 
commit would correspond to a commit from the ex-master branch close to the MR creation/update date. Thomas suggested renaming the MR branches in a way 
such as the name includes the MR number as well as a special keyword (e.g.: v10-prototype). Then we could close these MRs.
If we need to implement something similar to what was done in these MRs, we could still refer to them at that time.**

**Action - Reynald**: Edit the 4th Tango kernel Webinar video and upload it on tango-controls youtube channel.  
**Done. Video available at https://www.youtube.com/watch?v=cnyJsNV1dBw**

**Action - Sergi**:
Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to be migrated under hdbpp subgroup.

**Action - Sergi**:
https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 (on names on logs) to be reviewed.

**Action - Thomas B.**:
Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side. pytango#409 is a
cppTango bug, needs a proper fix.  
**Thomas proposed a bug fix for pytango#409 in cppTango#852. He's waiting for some feedback from 
the bug reporter Carlos Pascual.  
Thomas was not able to reproduce pytango#371 and is waiting for some feedback from Carlos Pascual on that one too.**

**Action - S2Innovation**: Configure rest-api git repo readthedocs for Gitlab.

**Action - S2Innovation (Michał Liszcz)**:  Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)).
**Michał started to work on this topic in cppTango#864**.

**Action - S2Innovation (Krystian)**: Update the JTango README which is still showing some links to Travis and Bintray. 
**MR JTango#125 ready for review. Merged just after the meeting.  
Krystian also updated the JTango wiki**

**Action - Lorenzo**: Create issues for server side access control and encryption features in rfc repository with tango-v10 label.
**Done. See rfc#176 and rfc #177.**

**Action - Nicolas L. (ESRF)**: Find an indico website before August to host the agenda and registration for the next Tango Meeting.
**Sergi will ask at ALBA and contact Nicolas.**

**Action - Nicolas L. (ESRF)**: Document/comment the current cppTango code to better understand the current usage of the different mutexes
and propose some solutions to avoid some potential deadlocks.
**Michał and Nicolas had a discussion about https://gitlab.com/tango-controls/cppTango/-/issues/409 and proposed a fix 
for this issue.  
Reynald implemented the proposed fix in https://gitlab.com/tango-controls/cppTango/-/merge_requests/863 
and installed it on Monday at the ESRF for 2 device servers where deadlock issues had been observed. 
This issue was happening more often during the accelerator shutdown periods. The next shutdown period 
at the ESRF will start at the end of July and will last about 3 weeks.**

### Tango RFCs

No news.

### High priority issues

No new high priority issue reported.

### Issues requiring discussions

SonarCloud badges on cppTango Readme are showing some errors since we migrated to Gitlab.com.

Thomas Braun has looked into getting sonarcloud support for gitlab.
He got stuck at creating an organization in sonarcould, see https://sonarcloud.io/documentation/getting-started/gitlab/. It requires a machine-user on gitlab with owner rights on tango-controls and an access token with api scope. See https://docs.gitlab.com/ee/user/permissions.html for the user model. 
Owner is the highest level. According to Thomas, this basically hands out the master key to sonarcloud.
Thomas would not recommend it due to security concerns.  
For this reason and the fact that nobody in the Tango Kernel team seems to rely heavily on SonarCloud, it has been decided in this Kernel Meeting to drop SonarCloud in cppTango.
We already have static code analysis enabled using clang tools.  
We will just miss the summary metrics, but we are already informed of all the things we could improve in the code, which is the most important.

### AOB
#### Next Tango Meeting

The Next Tango Collaboration Meeting will take place on September 14th and 15th 2021.  
It will probably be 2 half days as the last virtual Tango Collaboration Meeting.  
The agenda is open, and you can send ideas of presentations to Nicolas Leclercq (nicolas.leclercq at esrf dot fr).  
If the sanitary conditions allow it, the meeting will be a hybrid meeting (remote and face-to-face for those who can
come to the ESRF in Grenoble).  
Andy suggested at the previous kernel meeting organizing a short face-to-face Tango kernel meeting during this period.  
ESRF no longer hosts an indico website, so it would be great if we could use the indico website of another institute to
help to organize this meeting. Maybe ALBA could set up their indico for that?

**Action - Sergi (ALBA)**: See whether it is possible to use ALBA indico website to host the agenda and registration for 
the next Tango Meeting and contact Nicolas Leclercq before August.

#### Pogo

Damien reported that there is some work in progress improving the CMakefiles generated from Pogo.  
The changes might have an impact on the directory structure of the C++ projects generated by Pogo.
Reynald suggested to Damien to create an issue as soon as it is clear what the changes might be, so 
the community could comment on these new ideas asap. 

**Action - Damien (ESRF)**: Create some issues in Pogo before implementing some changes which might have an impact 
on the community to give a chance to the community to comment the proposed changes.

**Action - Damien (ESRF)**: Create an issue in cppTango to provide a Findlibtango.cmake for cmake integration.
**Done just after the meeting: see https://gitlab.com/tango-controls/cppTango/-/issues/857**

Damien shared an idea to create an additional library which could provide some convenient functions, objects to 
help the device server programmers to solve common problems.  
Damien also shared the idea to develop a library having the same philosophy as the Boost library which could 
be used as a playground to test some new high level API. 
As for Boost with the C++ standard library, the most interesting features could then integrate cppTango.

Michał mentioned that we could for instance add some new overloading methods to simplify device server programmers' lifes when dealing with strings.

#### Next HDB++ Teleconf

The next HDB++ teleconf meeting is foreseen on 23rd July at 11:00 CEST.

#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).

So the next Tango Kernel Teleconf Meeting will take place on Thursday 22nd July 2021 at 15:00 CEST.

## Summary of remaining actions

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Damien (ESRF)**: Create some issues in Pogo before implementing some changes which might have an impact
on the community to give a chance to the community to comment the proposed changes.

**Action - Reynald (ESRF)**: Look at old cppTango PRs targeting `tango-v10`.
Rename the MR branches in a way such as the name includes the MR number as well as a special keyword (e.g.: v10-prototype). Then we could close these MRs.
If we need to implement something similar to what was done in these MRs, we could still refer to them at that time.

**Action - Sergi (ALBA)**:
Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to be migrated under hdbpp subgroup.

**Action - Sergi (ALBA)**:
https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 (on names on logs) to be reviewed.

**Action - Sergi (ALBA)**: See whether it is possible to use ALBA indico website to host the agenda and registration for
the next Tango Meeting and contact Nicolas Leclercq before August.

**Action - Carlos Pascual (ALBA)**:
Thomas Braun proposed a bug fix for pytango#409 in cppTango!852. He's waiting for some feedback from
the bug reporter Carlos Pascual.  
Thomas was not able to reproduce pytango#371 and is waiting for some feedback from Carlos Pascual on that one too.

**Action - S2Innovation**: Configure rest-api git repo readthedocs for Gitlab.

**Action - S2Innovation (Michał Liszcz)**:  Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)).
MR: cppTango#864.

**Action - Nicolas L. (ESRF)/Michał Liszcz (S2Innovation)**: Document/comment the current cppTango code to better understand the current usage of the different mutexes
and propose some solutions to avoid some potential deadlocks.

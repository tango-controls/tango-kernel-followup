# Tango Kernel Follow-up Meeting - 2021/07/08

To be held on 2021/07/08 at 15:00 CEST on Zoom.

# Agenda

 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-06-24/Minutes.md#summary-of-remaining-actions)
 2. Tango RFCs (https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-07-08)
 3. High priority issues
 4. Issues requiring discussion
 5. AOB
     - Next Tango Meeting (14th & 15th September afternoons)
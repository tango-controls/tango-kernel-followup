# Tango Kernel Follow-up Meeting - 2021/09/14

To be held on 2021/09/14 at 10:00 CEST on Zoom.

# Agenda

 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-08-26/Minutes.md#summary-of-remaining-actions)
 2. Tango RFCs (https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-09-14)
 3. Tango V10 (Please see the slides prepared by Vincent Hardion and shared on slack Kernel channel on 9th September at 15:19 CEST in a thread)
 4. Scope of the coming cpptango releases 9.3.5 and 9.4 (MR and issue fixes that should be included in these releases).
 5. High priority issues
 6. Issues requiring discussion
 7. AOB

# Tango Kernel Follow-up Meeting - 2021/07/22

To be held on 2021/07/22 at 15:00 CEST on Zoom.

# Agenda

 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-07-22/Minutes.md#summary-of-remaining-actions)
 2. Tango RFCs (https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-07-22)
 3. High priority issues
 4. Supported C++ versions in cppTango  
 5. Pogo news
 6. Issues requiring discussion
 7. AOB
     - SEI remote training course (https://www.tango-controls.org/community/forum/post/4668/)
     - Next Tango Meeting (14th & 15th September afternoons)

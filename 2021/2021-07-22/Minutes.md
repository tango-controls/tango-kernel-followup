# Tango Kernel Follow-up Meeting

Held on 2021/07/22 at 15:00 CEST on Zoom.

**Participants**:

- Anton Joubert (SARAO)
- Damien Lacoste (ESRF)
- Lorenzo Pivetta (ELETTRA)
- Łukasz Żytniak (S2Innovation)
- Nicolas Leclercq (ESRF)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Thomas Braun (Byte Physics)
- Thomas Juerges (LOFAR)  

### Status of [Actions defined in the previous meetings](../2021/2021-07-08/Minutes.md#summary-of-remaining-actions)

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Damien (ESRF)**: Create some issues in Pogo before implementing some changes which might have an impact
on the community to give a chance to the community to comment the proposed changes.

**Action - Reynald (ESRF)**: Look at old cppTango PRs targeting `tango-v10`.
Rename the MR branches in a way such as the name includes the MR number as well as a special keyword (e.g.: v10-prototype). Then we could close these MRs.
If we need to implement something similar to what was done in these MRs, we could still refer to them at that time.
**Done**.

**Action - Sergi (ALBA)**:
Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to be migrated under hdbpp subgroup.

**Action - Sergi (ALBA)**:
https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 (on names on logs) to be reviewed.

**Action - Sergi (ALBA)**: See whether it is possible to use ALBA indico website to host the agenda and registration for
the next Tango Meeting and contact Nicolas Leclercq before August. **Done**

**Action - Carlos Pascual (ALBA)**:
Thomas Braun proposed a bug fix for pytango#409 in cppTango!852. He's waiting for some feedback from
the bug reporter Carlos Pascual.  
Thomas was not able to reproduce pytango#371 and is waiting for some feedback from Carlos Pascual on that one too.
**Done. Carlos provided all the needed feedback**

**Action - S2Innovation**: Configure rest-api git repo readthedocs for Gitlab. **Michał Gandor created rest-api!2. To be reviewed**

**Action - S2Innovation (Michał Liszcz)**:  Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)).
MR: cppTango!864. **Michał made some progress on cppTango!864. we have now 3 new jobs (ASAN, TSAN, UBSAN), which are currently failing.   
Michał proposes to fix the issues reported asap in different MRs. 
He started fixing UBSAN issues in cppTango!865 (3 failed test cases are left).**

**Action - Nicolas L. (ESRF)/Michał Liszcz (S2Innovation)**: Document/comment the current cppTango code to better understand the current usage of the different mutexes
and propose some solutions to avoid some potential deadlocks.  
**Reynald reported that he compiled a special version of cpptango with a patch suggested by Michał and Nicolas to reduce the risk 
of one potential deadlock and also with a patch from Thomas Braun catching more exceptions in the polling thread. 
With this version, there are fewer crashes, but still one crash occurred, and the generated core file still need to be analyzed.**

### Tango RFCs

No news.

### High priority issues

No new high priority issue reported.

### Supported C++ versions in cppTango

It has been decided to maintain the decision to support C++14 in the main branch (future 9.4 version).  
As a consequence, we will drop Debian 8 support (which is already EOL) in the main branch.

### POGO

Damien proposed pogo!117 to allow multiple attributes to use the same enum.

It has been decided that coming versions of Pogo could use C++11 in the C++ generated code. 
It should be clearly stated in the release notes. Users who still want to generate code for old device servers still requiring 
to be compiled on old compilers should be able to easily identify the version where this change occurred.
For the device servers needing to be compiled with very old compilers, an older version of Pogo will have to be used.

**Action - Damien (ESRF)**: Make available to the community (gitlab registry?) a docker image with Pogo. 
Damien already provided such image to Thomas Braun who found it very convenient.

Anton asked whether CI could be added to Pogo.  
As a minimum CI should compile it and ensure there is no compilation error.  
As a next step, Pogo CI could regenerate code for TangoTest device server and ensure it still compiles (and eventually still behaves correctly).

### Issues requiring discussions

Anton asked whether pytango should throw exceptions when trying to set non-existing attributes (See pytango#422).  
We agreed that the current behaviour is confusing and that throwing an exception in this use case is an acceptable 
solution for the next pytango release.

Lorenzo reported that following Michał Liszcz's review on TangoDatabase!8, Alessio Bogani created TangoDatabase!28 to add 
ACL feature to the Tango Database DS. This MR needs to be reviewed.

**Action - cppTango developers and other volunteers**: Review TangoDatabase!28

### AOB
#### Next Tango Meeting

The Next Tango Collaboration Meeting will take place on September 14th and 15th 2021.  
It will be 2 half days as the last virtual Tango Collaboration Meeting.  
The agenda is open, and you can send ideas of presentations to Nicolas Leclercq (nicolas.leclercq at esrf dot fr).  

Thanks to ALBA, we have an Indico page for this meeting: https://indico.cells.es/event/619/

If the sanitary conditions allow it, the meeting might be a hybrid meeting (remote and face-to-face for those who can
come to the ESRF in Grenoble).  
Andy suggested at a previous kernel meeting organizing a short face-to-face Tango kernel meeting during this period.  

#### SEI remote training course 

Nick Rees from SKAO advertised an SEI remote software architecture training on slack tango-controls general channel and 
on the Tango forum: https://www.tango-controls.org/community/forum/post/4668/.

The meeting will take place on 21-24 September. A new SKA Architect will be attending the meeting. 
If anyone else is interested in doing the course, it would be good to have people with common cause also attend it.  
If enough people are interested, we could think about setting up a dedicated course (or get it held in a reasonable time-zone).

Please refer to the forum post to get more details or feel free to contact Nick if you're interested.

#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).

So the next Tango Kernel Teleconf Meeting will take place **in 3 weeks** on Thursday 12th August 2021 at 15:00 CEST.

## Summary of remaining actions

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Damien (ESRF)**: Create some issues in Pogo before implementing some changes which might have an impact
on the community to give a chance to the community to comment the proposed changes.

**Action - Damien (ESRF)**: Make available to the community (gitlab registry?) a docker image with Pogo.

**Action - Sergi (ALBA)**:
Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to be migrated under hdbpp subgroup.

**Action - Sergi (ALBA)**:
https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 (on names on logs) to be reviewed.

**Action - rest-api lovers, S2Innovation**: Review rest-api!2 (readthedocs rest api doc adapted to Gitlab)

**Action - S2Innovation (Michał Liszcz)**:  Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)).
MR: cppTango!864. **Michał made some progress on cppTango!864. we have now 3 new jobs (ASAN, TSAN, UBSAN), which are currently failing.   
Michał proposes to fix the issues reported asap in different MRs.
He started fixing UBSAN issues in cppTango!865 (3 failed test cases are left).**

**Action - Nicolas L. (ESRF)/Michał Liszcz (S2Innovation)**: Document/comment the current cppTango code to better understand the current usage of the different mutexes
and propose some solutions to avoid some potential deadlocks.  

**Action - Reynald (ESRF)**: Analyze core file obtained after a crash with special cpTango version including patches solving a deadlock and 
catching more exceptions in the polling thread.

**Action - cppTango developers and other volunteers**: Review TangoDatabase!28
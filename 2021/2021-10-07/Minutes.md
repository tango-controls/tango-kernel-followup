# Tango Kernel Follow-up Meeting

Held on 2021/10/07 at 15:00 CEST on Zoom.

**Participants**:

- Benjamin Bertrand (MAX IV)
- Reynald Bourtembourg (ESRF)
- Andy Goetz (ESRF)
- Vincent Hardion (MAX IV)
- Thomas Juerges (SKAO)
- Damien Lacoste(ESRF)
- Nicolas Leclercq (ESRF)
- Michał Liszcz (S2Innovation)
- Stephane Poirier (Soleil)
- Lukasz Zytniak (S2Innovation)

### Status of [Actions defined in the previous meetings](../2021-08-26/Minutes.md#summary-of-remaining-actions)

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?

**Damien (ESRF)**:
- Create some issues in Pogo before implementing some changes which might have an impact
  on the community to give a chance to the community to comment the proposed changes.  
**Damien prepared a Proof of Concept MR (https://gitlab.com/tango-controls/pogo/-/merge_requests/126). Comments are welcome!**
- Make available to the community (gitlab registry?) a docker image with Pogo.  
**See https://gitlab.com/tango-controls/pogo/-/merge_requests/127 . Damien will merge it and prepare a new Pogo release** 

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.
- Review cppTango!742 (Function name and line number in logs and exceptions).   
**Michal exchanged with Sergi on that topic and pointed Sergi to some pytango MR fixing the pytango incompatibilities with cpptango main branch.**

**S2Innovation (Piotr Goryl)**:
- Fix issue with rest-api online documentation  
**Benjamin noticed there are currently 2 readthedocs websites for the tango REST API.  
Action - S2Innovation: Find a way to remove https://tango-rest-api.readthedocs.io**

**S2Innovation (Michał Liszcz)** and **Thomas B (Byte Physics)**:
- Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)). **Done**
- **Michał** set `allow_failure: true` on the failing jobs and merge cppTango!864. **Done**
- **Thomas B** review UBSAN fixes in cppTango!865
- **Michał** continue on ASAN, TSAN jobs

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitizer work completed, document/comment the current cppTango
  code to better understand the current usage of the different mutexes
  and propose some solutions to avoid some potential deadlocks.

**Thomas B (Byte Physics)**:
- Define new process for cppTango MR (lower the number of approval to one and add new label for complex ones?)  
  **Contribute.md file was updated with new process in main branch only so far. Still need to be done on 9.3-backport branch**

**Reynald (ESRF)**:
- Create an issue on all the projects still having references to bintray. **Done**

**Lorenzo (Elettra)**:
- Propose week 47 (or another) to the community (slack, mailing lists?) for the RFC face to face meeting.
- Contact Alessio to address review comments on TangoDatabase!28

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration:
  Drive new repository creation on GitLab with a well known device server.

**All institutes**:
- Ask java developers to update their projects and remove any reference to bintray

**cppTango developers**:
- Assign milestones (9.4 or 9.3.5) to issues we want to be part of 9.4 or 9.3.5 releases.  
**cpptango developers met on Thursday 7th October morning for that. Still some very old issues to review**

**All volunteers**:
- Contribute to ICALEPCS 2021 "STATE OF THE TANGO CONTROLS COLLABORATION in 2021" paper

## ICALEPCS 2021

### Tango workshop

Vincent presented the [Tango workshop program](https://indico.maxiv.lu.se/event/4952/).
On Andy's request, the slot for the "common patterns and anti-patterns" presentation has been swapped with the Sardana one. 

**Action - Kernel team**: Prepare some questions for the Open Space session in case there is not enough questions from 
the participants.

### STATE OF THE TANGO CONTROLS COLLABORATION in 2021 paper

Papers submission closes on October 10th.

Reynald will take care of the C++ core library section and Michal and Thomas will help in reviewing it.  
Nicolas will update the Community Workshops section.  
Tango V10 part is still mainly a copy/paste from a previous paper. It needs to be adapted.

Java Core library section needs someone to work on it.  
**Action - Soleil**: Clarify with Gwenaelle and Krystian who will write the Java core library section in
"STATE OF THE TANGO CONTROLS COLLABORATION in 2021" paper and also in the "MIGRATION OF TANGO-CONTROLS SOURCE CODE REPOSITORIES" 
paper.

**Action - All volunteers**: Contribute to ICALEPCS 2021 "STATE OF THE TANGO CONTROLS COLLABORATION in 2021" paper.
The overleaf editable link has been published on "icalepcs-2021" tango-controls slack channel.

### ICALEPCS TANGO related papers

Vincent said there is about 20 ICALEPCS 2021 abstracts about TANGO (excluding the ones mentioning both EPICS and TANGO).

### High priority issues

[High Priority MR](https://gitlab.com/tango-controls/cppTango/-/merge_requests?label_name%5B%5D=High+Priority)  
[High Priority issues](https://gitlab.com/tango-controls/cppTango/-/issues?label_name%5B%5D=High+Priority)

Vincent mentioned https://gitlab.com/tango-controls/cppTango/-/merge_requests/870 (Device Level Dynamic Attributes) but 
also saying that Henrik will be busy like most of us with ICALEPCS in the next weeks. 

#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).
So the next Tango Kernel meeting should take place **in 3 weeks** on Thursday 28th of October at 10:00 CEST.

### Summary of remaining actions


**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?

**Damien (ESRF)**:
- Merge https://gitlab.com/tango-controls/pogo/-/merge_requests/127 and prepare a new Pogo release.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.
- Review cppTango!742 (Function name and line number in logs and exceptions).   

**S2Innovation (Piotr Goryl)**:
- Find a way to remove https://tango-rest-api.readthedocs.io

**S2Innovation (Michał Liszcz)** and **Thomas B (Byte Physics)**:
- **Thomas B** review UBSAN fixes in cppTango!865
- **Michał** continue on ASAN, TSAN jobs

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitizer work completed, document/comment the current cppTango
  code to better understand the current usage of the different mutexes
  and propose some solutions to avoid some potential deadlocks.

**Thomas B (Byte Physics)**:
- Define new process for cppTango MR (lower the number of approval to one and add new label for complex ones?)  
  **Contribute.md file was updated with new process in main branch only so far. Still need to be done on 9.3-backport branch**

**Lorenzo (Elettra)**:
- Propose week 47 (or another) to the community (slack, mailing lists?) for the RFC face to face meeting.
- Contact Alessio to address review comments on TangoDatabase!28

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration:
  Drive new repository creation on GitLab with a well known device server.

**Soleil**
- Clarify with Gwenaelle and Krystian who will write the Java core library section in
  "STATE OF THE TANGO CONTROLS COLLABORATION in 2021" paper and also in the "MIGRATION OF TANGO-CONTROLS SOURCE CODE REPOSITORIES"
  paper.

**All institutes**:
- Ask java developers to update their projects and remove any reference to bintray
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126
- Prepare some questions for the ICALEPCS Tango workshop Open Space session in case there is not enough questions from the participants.

**cppTango developers**:
- Assign milestones (9.4 or 9.3.5) to issues we want to be part of 9.4 or 9.3.5 releases.  

**All volunteers**:
- Contribute to ICALEPCS 2021 "STATE OF THE TANGO CONTROLS COLLABORATION in 2021" paper. The overleaf editable link 
has been published on "icalepcs-2021" tango-controls slack channel.

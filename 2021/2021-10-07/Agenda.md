# Tango Kernel Follow-up Meeting - 2021/10/07

To be held on 2021/10/07 at 15:00 CEST on Zoom.

# Agenda

 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-09-23/Minutes.md#summary-of-remaining-actions)
 2. ICALEPCS 2021
 3. High priority issues
 4. Issues requiring discussion
 5. AOB

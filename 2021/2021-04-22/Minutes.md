# Tango Kernel Follow-up Meeting - 2021/04/22

To be held on 2021/04/22 at 15:00 CEST on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-04-08/Minutes.md#summary-of-remaining-actions)
 2. [Tango RFCs](https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-04-22)
 3. High priority issues
 4. Issues requiring discussion
 5. Next kernel teleconf meeting date. Suggestion to move it to Thursday 6th May.
 6. AOB

OmniORB

PyTango kernel webminar

Conda package for java tools: wip. copy the binary generated, instead of compiling, next steps is to collect the licence of all dependencies.

New action: Benjamin to update the database project to use the gitlab docker registry.
Create docker subgroup and the docker-mysql will be rename mysql and tangocs-docker to tangocs

Tango java on maven central: Piotr will ask Krystian
Note that other java tool will have to move before the shutdown of bintray

Tango log pattern or not. 
MR 742: Decision to not pattern layout and only use the source code location only in DEBUG. No change in the exception format.
Another MR will be open to include PatternLayout. Need a discussion the different possibility to change the pattern i.e local, entire CS... 
Note changing timestamp format of the log pattern 

Migration of Java tool, discuss internal

Pogo character set encoding Reynald reported Issue 119

Migration of Fandango, Sergi already tried and ready. Working on the excluded authors file. 
panic next item, maybe minor changes

Thomas B could reproduce pytango#409 (moved to cpptango). Other one in progress

Runner on windows: Thomas B. has a prototype. Seen some drawback like boot time of 10 mn. Only VS2019, under investigation maybe toolkits can be used for 2015 and 2017., maybe 2019 is universally binary compatible till 2015

REst api








# Tango Kernel Follow-up Meeting - 2021/01/28

To be held on 2021/01/28 at 15:00 CET on Zoom.

# Agenda
 
 1. Kernel Teleconf Minutes Edition Rotation
 2. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2021/2021-01-14/Minutes.md#summary-of-remaining-actions)
 3. Gitlab migration ([cppTango#830](https://github.com/tango-controls/cppTango/pull/830), [TangoTickets#47](https://github.com/tango-controls/TangoTickets/issues/47))
 4. [Tango RFCs](https://github.com/tango-controls/rfc/wiki/Meeting-2021-01-28)
 5. High priority issues
 6. Tango Kernel Webinars
 7. AOB

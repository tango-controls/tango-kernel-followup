# Tango Kernel Follow-up Meeting - 2021/04/08

To be held on 2021/04/08 at 15:00 CEST on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-03-25/Minutes.md#summary-of-remaining-actions)
 2. [Tango RFCs](https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-04-08)
 3. High priority issues
 4. Gitlab migration ([TangoTickets#47](https://github.com/tango-controls/TangoTickets/issues/47))
 5. Tango Kernel Webinars
 6. AOB

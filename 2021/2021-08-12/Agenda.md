# Tango Kernel Follow-up Meeting - 2021/08/12

To be held on 2021/08/12 at 15:00 CEST on Zoom.

# Agenda

 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-07-22/Minutes.md#summary-of-remaining-actions)
 2. Tango RFCs (https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-08-12)
 3. High priority issues
 4. Should we move Tango classes from [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) to gitlab?
 5. Issues requiring discussion
 6. AOB

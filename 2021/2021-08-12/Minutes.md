# Tango Kernel Follow-up Meeting

Held on 2021/08/12 at 15:00 CEST on Zoom.

**Participants**:

- Anton Joubert (SARAO)
- Damien Lacoste (ESRF)
- Michał Liszcz (S2Innovation)
- Piotr Goryl (S2Innovation)
- Thomas Braun (Byte Physics)
- Thomas Juerges (LOFAR)
- Benjamin Bertrand (MAX IV)
- Vincent Hardion (MAX IV)
- Sonja Vrcic (SKAO)
- ?

Hosted by Thomas Braun, he welcomed us to the “holiday edition” :-)

Minutes by Anton Joubert.

### Status of [Actions defined in the previous meetings](../2021-07-22/Minutes.md#summary-of-remaining-actions)


**Andy (ESRF)**:
- Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>
    - No news (Andy not attending)
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu
    - No news

**Damien (ESRF)**:
- Create some issues in Pogo before implementing some changes which might have an impact
on the community to give a chance to the community to comment the proposed changes.
    - Find cmake module issue was created:  cppTango#857
- Make available to the community (gitlab registry?) a docker image with Pogo.
    - Not done, but plan to start on CI in September.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup.
    - No news (Sergi not attending)
- cppTango!742 (Function name and line number in logs and exceptions) to be reviewed.
    - No news

**rest-api lovers, S2Innovation**:
- Review rest-api!2 (readthedocs rest api doc adapted to Gitlab)
    - MR will be reviewed by **Piotr**.

**S2Innovation (Michał Liszcz)**:
- Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)).
MR: cppTango!864. Michał made some progress on cppTango!864. we have now 3 new jobs (ASAN, TSAN, UBSAN), which are currently failing.
Michał proposes to fix the issues reported asap in different MRs.
He started fixing UBSAN issues in cppTango!865 (3 failed test cases are left).
    - Michał got UBSAN tests working, ready for review.  **Thomas B** will review.
  There were some edge cases fixed, and some false positives.  There are notes in the MR.

**Nicolas L. (ESRF)/Michał Liszcz (S2Innovation)**:
- Document/comment the current cppTango code to better understand the current usage of the different mutexes
and propose some solutions to avoid some potential deadlocks.
    - Michał suggest we get thread sanitiser working first before working on this.

**Reynald (ESRF)**:
- Analyze core file obtained after a crash with special cppTango version including
patches solving a deadlock and catching more exceptions in the polling thread.
    - No news (Reynald not attending)

**cppTango developers and other volunteers**:
- Review TangoDatabase!28
    - No news.  Please review.

### Tango RFCs

Piotr and Vincent working on RFC-10 Message - 1st draft ready.  Expect to create MR soon that **Vincent** will review.  rfc!124

### High priority issues

No new high priority issue reported.

### Should we move Tango classes from [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) to gitlab?

SOLEIL and DESY are main users, looking at recent commit history.  Damien said ESRF as also using it.
Suggest we contact them all for input.  The repo has a large number of device servers, so some thought
should go into new structure.  Could have one repo per institute, maybe on a different gitlab organisation - not tango-controls?
Would be good to have CI, but many devices may no longer be used, so waste of time to test things
that are not longer used.

Conclusion:  **Vincent** Contact SOLEIL and DESY and ESRF, CC Thomas B.

### Issues requiring discussions


#### PyTango binary wheels
Anton asked about a binary wheel for PyTango under Linux (see pytango#323).
We have one for Windows, but what about Linux?  Must dependencies (.so files) get included
well, or can they be linked to dynamically?  What about debugging issues - will we get bug reports that don't have debug symbols?

Under Conda, we already have a precompiled package: pytango on conda-forge.

Can we compile cppTango on https://github.com/pypa/manylinux ? **Thomas B** offered to investigate.

Michał asked why the compilation is so slow.  Maybe we should investigate that as well.  E.g.,
cppTango compiles in 3 minutes on his 8 core machine, so why does PyTango take 5 minutes?  Anton
said PyTango used to take 10 minutes, but recent change to use more cores reduced time to 5
minutes.  There is a Makefile that mentions precompiled headers for tango.h, but not sure that
gets used by setuptools when building the extension.

#### Logging timestamp precision
Michał discussed suggested timestamp precision increased from milliseconds to microseconds (hard-coded). cppTango#858.  No-one opposed to moving from 3 to 6 digits.
Will target main branch and 9.3 backport.  **Michał** will make MRs.

#### POGO branches
Pogo branch issues, as per issue: pogo#122
Some old history (10 years old) is not in the main branch.  The suggestion was to create
a tag at the head of the old branch, and then branch can be deleted.
History will still be reachable, if someone needs it.  **Damien** make the tag and delete the branch

### AOB

#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).

So the next Tango Kernel Teleconf Meeting will take place **in 2 weeks** on Thursday 26th August 2021 at 15:00 CEST.

## Summary of remaining actions


**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.

**Damien (ESRF)**:
- Create some issues in Pogo before implementing some changes which might have an impact
on the community to give a chance to the community to comment the proposed changes.
- Make available to the community (gitlab registry?) a docker image with Pogo.
- pogo#122 branch issues - make the tag and delete the old branch.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup.
- Review cppTango!742 (Function name and line number in logs and exceptions).

**S2Innovation (Piotr Goryl)**:
- Review rest-api!2 (readthedocs rest api doc adapted to Gitlab)

**S2Innovation (Michał Liszcz)** and **Thomas B (Byte Physics)**:
- Work on https://gitlab.com/tango-controls/cppTango/-/issues/797 (Integration with [sanitizers](https://clang.llvm.org/docs/ThreadSanitizer.html)).
MR: cppTango!864. Michał made some progress on cppTango!864. we have now 3 new jobs (ASAN, TSAN, UBSAN), which are currently failing.
Michał proposes to fix the issues reported asap in different MRs.
- **Thomas B** review UBSAN fixes in cppTango!865
- **Michał** continue on ASAN, TSAN jobs?

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitzer work completed, document/comment the current cppTango
code to better understand the current usage of the different mutexes
and propose some solutions to avoid some potential deadlocks.

**S2Innovation (Michał Liszcz)**
- Make MRs (main and 9.3 backports) with timestamp precision increased from milliseconds to microseconds (hard-coded). cppTango#858.

**Thomas B (Byte Physics)**
- Test cppTango compilation on https://github.com/pypa/manylinux for PyTango binary wheel, pytango#323.

**Reynald (ESRF)**:
- Analyze core file obtained after a crash with special cppTango version including
patches solving a deadlock and catching more exceptions in the polling thread.

**cppTango developers and other volunteers**:
- Review TangoDatabase!28

**Vincent (MAX IV)**
- Review RFC-10 Message update:  rfc!12
- For possible [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration: contact SOLEIL and DESY and ESRF, CC Thomas B.

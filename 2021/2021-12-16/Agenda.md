# Tango Kernel Follow-up Meeting - 2021/12/16

To be held on 2021/12/16 at 15:00 CET on Zoom.

# Agenda

 1. Vulnerability in [Apache's Log4j (CVE-2021-44228)](https://www.cve.org/CVERecord?id=CVE-2021-44228):
    - What is its impact on Tango Controls?
    - What needs to be changed/fixed in Tango Controls?
    - What are the recommendations for Tango Controls users until a changed/fixed Tango version is released?
 2. RFC workshop organization (virtual/face to face meeting?)
 3. Status of the move-away from bintray for the java related Tango programs? We can currently not build the Tango Source Distribution due to that.
 4. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-12-02/Minutes.md#summary-of-remaining-actions)
 5. High priority issues
 6. Issues requiring discussion  
 7. AOB

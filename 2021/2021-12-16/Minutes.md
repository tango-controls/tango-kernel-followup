# Tango Kernel Follow-up Meeting

Held on 2021/12/16 at 15:00 CET on Zoom.

**Participants**:

- Benjamin Bertrand (MAX IV)
- Reynald Bourtembourg (ESRF)
- Thomas Braun (Byte Physics)
- Andy Goetz (ESRF)
- Piotr Goryl (S2Innovation)
- Vincent Hardion (MAX IV)
- Anton Joubert (MAX IV)
- Thomas Juerges (SKAO)
- Damien Lacoste (ESRF)
- Michał Liszcz (S2Innovation)
- Lorenzo Pivetta (ELETTRA)
- Faranguiss Poncet (ESRF)
- Jean-Luc Pons (ESRF)
- Sergi Rubio (ALBA)
- Łukasz Żytniak (S2Innovation)

## RFC Workshop Organization

Due to uncertainties linked to COVID-19 pandemic, it has been decided to postpone the RFC workshop initially foreseen on Week#3 2022.  
We will re-assess the international sanitary situation during the next Tango Kernel Meeting on 13th January 2022. 

## Vulnerability in [Apache's Log4j (CVE-2021-44228)](https://www.cve.org/CVERecord?id=CVE-2021-44228):

### What is its impact on Tango Controls?
The impact is not yet fully clear.  
Gwenaelle on slack:
"Concerning jtango, log4j is only used in deprecated server api (package fr.esrf.TangoDs, with IDL2 version). 
So the fix is easy: removing this code and the log4j dependencies in the pom.xml."

Modern Tango java tools and device servers are normally using logback for the logs, not log4j.  
Damien mentioned that there was a recent update of logback to fix a vulnerability similar to the one from log4j.
Benjamin posted this link on the Zoom chat where you can get more details about this fix and the vulnerability discovered 
in logback (much lower risk than the log4j2 one): http://logback.qos.ch/news.html

The problem is that some dependencies might depend on log4j too.
Benjamin Bertrand provided a link to the following project on slack which could be used to help to detect whether some 
code includes log4j2 or uses log4j2: https://github.com/jfrog/log4j-tools

Any application using atk could be impacted.
Some atk dependencies using log4j have already been identified by Jean-Luc Pons: jep, jogl and imagej.  
The jep dependency could probably be removed completely (need to double-check with Soleil).

We need to clarify whether JacORB is using log4j.

Jean-Luc thinks it should not be possible to exploit this log4j2 vulnerability when using the Tango tools.

### What needs to be changed/fixed in Tango Controls?

Jean-Luc Pons and Faranguiss Poncet said that log4j is currently used by some atk dependencies.
It seems to be used by some specific Viewers (a 3-D viewer and some others used by Soleil) provided by ATKWidget but these viewers are not used by Jive, atk-panel, astor.
These viewers were used at some point by Soleil. If they are no longer used, they should  be removed from ATKWidget.

The logback version might need to be updated and if log4j still needs to be used, the latest versions should be used.

### What are the recommendations for Tango Controls users until a changed/fixed Tango version is released?

One recommendation is to forbid ldap protocol on external communications at your institute firewall level.

**Action - Tango Java experts**:  Send an e-mail on the Tango Mailing list informing users about the potential 
impact of the log4j vulnerability and the recommendations from the experts.

## Status of the move-away from bintray for the java related Tango programs?

The jar files corresponding to the different releases of the Tango Java projects previously hosted on BinTray are no longer available on BinTray.

We can currently not build the Tango Source Distribution due to that.

JTango already migrated to sonatype and maven central.

Faranguiss said that the main difference with the Bintray solution is that with SonaType, the process is heavier and involves signing the jar files.

Damien said that there is a risk that what happened with Bintray might happen as well at some point with sonatype. 
He also said that if we confirm the decision to use sonatype, we should clarify the strategy on the usage of the GPG keys (one global key for tango-controls organization or 1 per developer?).

**Action - Tango Java experts**: Organize a teleconf meeting to discuss the strategy to make the jar files available to the community.

In the meantime, it would be good to put the jar files required to build a new Tango Source Distribution on an FTP server.

Thomas Braun proposed to host them temporarily on his ftp server. 
Faranguiss proposed to make them available on an ESRF FTP server as it was done before Bintray.  

Reynald mentioned that S2Innovation made a copy of the BinTray backup before Bintray shutdown.  
Here is a link to this copy: https://s2innovation.sharepoint.com/:f:/s/Developers/Eg2gQYrxpbpJp3dgb-b-f50BP-pN6RtfcPz70SXsbZ3HyA?e=uYHPiH

On Friday 17th Dec: Reynald provided some more jar files as well 7 months ago on the ESRF cloud: https://cloud.esrf.fr/s/BXxnYxAbJEZQtQ6

## Status of [Actions defined in the previous meetings](../2021-12-02/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- if you're willing to help in any way for the Tango workshop at the next [ADASS conference](https://www.adass.org) which
  will take place in Victoria, Canada from 11-15 September 2022, please contact Thomas Juerges (SKAO) before end of January 2022.
**No one contacted Thomas so far**.
- Ask java developers to update their projects and remove any reference to bintray
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126.

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?

**Anton (Max IV)**:
- Try out cppTango conda package from [cppTango!875](https://gitlab.com/tango-controls/cpptango/-/merge_requests/875). 
**Done. The Merge Request has been merged. 
cppTango 9.4 development conda package is now available on tango-controls conda channel (https://anaconda.org/tango-controls/).**

**Damien (ESRF)**:
- Prepare a new Pogo release **WIP**

**Łukasz (S2Innovation)**:
- Follow up with IK the removal of https://tango-rest-api.readthedocs.io **Łukasz pinged IK but got not reply so far**.
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration **The first device servers requested by Soleil will be moved on Friday 17th December**. 
- Check the IDL versions used by the device servers migrated/to be migrated from tango-ds Sourceforge project (are there any IDL v1 or v2 servers?).

**Reynald (ESRF)**:
- Ping Tango java developers to ensure they update their projects after the Bintray closure
- Clarify whether the DevEnum are really not supported. Verify that this limitation is well documented
  (It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of
  DevEnum in pipes if it's a real limitation.
- Communicate decision to remove support for IDLv1 and v2 in cppTango to the users via the Tango mailing list and
  give them 2 weeks to give their feedback. **Done. Deadline for the feedback was set to Thursday 13th January 2022.**

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.

**Thomas (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26

**Thomas (SKAO)**:
- Organize a Tango Workshop at the next ADASS conference. **Thomas said he will ask for 2\*2 hours or 2\*3 hours tutorial slots.**
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...)
- Prepare a short description of the workshop and share it with the kernel team (slack and/or kernel mailing list). **Thomas's wife ready to come to the workshop after seeing the first draft(!)**
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126 **Done. Other reviewers are welcome of course. 
If you don't like the new organization of the generated files, this is the moment to complain. If you like it, it is nice to say it too!**

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124

## High Priority Issues

SKAO is interested in having a way to specify the tango events ZMQ ports used by cppTango and pytango.  
They already worked on a proof of concept and they will create a MR in cppTango repository.
The MR was created after the meeting: https://gitlab.com/tango-controls/cppTango/-/merge_requests/893

They would like to have this improvement merged into cppTango before 22nd February.  
The cppTango developers team proposed to work on releasing a cppTango 9.3.5 version as soon as this MR will be merged.

## Issues requiring discussion

### TangoTest 9.4 compatible conda package
Anton would be interested in having a TangoTest 9.4 compatible conda package.  
This conda package might be created automatically in a TangoTest CI job.

Anton said he would be interested in using a TangoTest python implementation version for pytango tests.
He said that Geoff Mant has developed such a python version but the behaviour was a bit different than the C++ TangoTest version.
Not everything was implemented yet.

### IDLv1/v2 code removal and impact on pytango

As written in an e-mail from the cppTango developers team, code related to IDLv1/v2 will be removed in cppTango 9.4. 
Anton asked whether similar work would have to be done in pytango.  
Michal replied that the methods related to IDLv1/v2 will still be available in cppTango, but they will simply send exceptions 
saying that they are no longer supported. This means that the interface with pytango concerning these methods will stay untouched.
Michal thinks pytango will still compile without issue after the change.

## AOB

### cppTango weekly meetings

The cppTango team is meeting once a week to discuss cppTango issues/MR and to prioritize them. 
If you want to be regularly informed about the date and time of these meetings (they are not always on the same day and at the 
same time), please contact Reynald.

The next cppTango weekly meeting will probably take place on Wednesday 5th January 2022 at 13:00 CET, using the same zoom link as the 
Kernel Teleconf Meetings.

### Next Kernel Teleconf Meeting
The next Tango Kernel Teleconf meeting will take place on Thursday 13th January 2022 at 15:00 CET.

## Summary of remaining actions

**All institutes**:
- if you're willing to help in any way for the Tango workshop at the next [ADASS conference](https://www.adass.org) which
  will take place in Victoria, Canada from 11-15 September 2022, please contact Thomas Juerges (SKAO) before end of January 2022.
- Ask java developers to update their projects and remove any reference to bintray
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126.

**Tango Java experts**:  
- Send an e-mail on the Tango Mailing list informing users about the potential
impact of the log4j vulnerability and the recommendations from the experts.
- Organize a teleconf meeting to discuss the strategy to make the jar files available to the community.

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?

**Damien (ESRF)**:
- Prepare a new Pogo release

**Łukasz (S2Innovation)**:
- Follow up with IK the removal of https://tango-rest-api.readthedocs.io
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration
- Check the IDL versions used by the device servers migrated/to be migrated from tango-ds Sourceforge project (are there any IDL v1 or v2 servers?).

**Reynald (ESRF)**:
- Clarify whether the DevEnum are really not supported. Verify that this limitation is well documented
  (It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of
  DevEnum in pipes if it's a real limitation.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.

**Thomas (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26

**Thomas (SKAO)**:
- Organize a Tango Workshop at the next ADASS conference.
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...)
- Prepare a short description of the workshop and share it with the kernel team (slack and/or kernel mailing list).

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124

# Tango Kernel Follow-up Meeting

Held on 2021/12/02 at 15:00 CET on Zoom.

**Participants**:

- Gwenaelle Abeille (Soleil)
- Benjamin Bertrand (MAX IV)
- Reynald Bourtembourg (ESRF)
- Andy Goetz (ESRF)
- Piotr Goryl (S2Innovation)
- Vincent Hardion (MAX IV)
- Anton Joubert (MAX IV)
- Thomas Juerges (SKAO)
- Michał Liszcz (S2Innovation)
- Sergi Rubio (ALBA)
- Łukasz Żytniak (S2Innovation)

## Shall we still continue supporting IDL v1 or IDL v2 devices in cppTango?

While investigating [cppTango#511](https://gitlab.com/tango-controls/cppTango/-/issues/511), Michał Liszcz discovered a 
[bug](https://gitlab.com/tango-controls/cppTango/-/issues/636) in a part of the code which is executed only in a device 
server inherits directly from DeviceImpl or Device_2Impl or when a device server is talking to an old client using 
Tango IDL v1 or v2 to communicate.

To ease the maintenance work (avoid fixing bugs/ improving code in not used code, ...), Michał suggests removing 
the code parts which are used only by clients and servers using IDL v1 or v2 to communicate. 
An exception will be thrown instead.

This will be done only starting from cppTango 9.4, meaning that cppTango 9.4 will no longer be able to support IDL v1 and
v2 communication protocol.
Users could still use cppTango 9.3 versions if they have some use cases (clients or servers) where IDL v1 or v2 communication is required.

**Action: Reynald (ESRF):** Communicate this decision to the users via the Tango mailing list and give them 2 weeks to give their feedback.

**Action - Łukasz (S2Innovation):** Check the IDL versions used by the device servers migrated/to be migrated from tango-ds Sourceforge project.

## RFC Workshop Organization

S2Innovation will organize the RFC workshop. 
Piotr found a house about 30km from Krakow where 10 to 12 people could stay during the workshop.
This workshop will take place from Tuesday 18th January to Thursday 20th January 2022.  

SKAO has suspended international business travel. The situation will be reassessed around the 7th January 2022.  
There are also currently travel restrictions at ALBA.  

Łukasz said it should be possible to organize a hybrid meeting (face to face + zoom) to allow people who could not travel 
to participate in the RFC workshop via zoom.

## Status of [Actions defined in the previous meetings](../2021-11-18/Minutes.md#summary-of-remaining-actions)

**All institutes**:
- Ask java developers to update their projects and remove any reference to bintray
- **Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126.**
Thomas Juerges will start his review on Friday 3rd December. Others are also welcome of course.

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?

**cppTango developers**:
- Assign milestones (9.4 or 9.3.5) to issues we want to be part of 9.4 or 9.3.5 releases **Done**
- Please review and merge cppTango!875. **Code Review done. Nobody tried the resulting conda package yet. Anton will give it a try.**

**Damien (ESRF)**:
- Merge https://gitlab.com/tango-controls/pogo/-/merge_requests/127 and prepare a new Pogo release.**Merged. New release 
still needs to be done** 

**Nicolas L. (ESRF)** and **Michał Liszcz (S2Innovation)**:
- After thread sanitizer work completed, document/comment the current cppTango
  code to better understand the current usage of the different mutexes
  and propose some solutions to avoid some potential deadlocks. **Michał proposed to remove this action from the Kernel 
Meetings since deadlocks issues are already tracked as cppTango issues, if Nicolas agrees.**

**Igor (IK)**:
- Find a way to remove https://tango-rest-api.readthedocs.io. **Łukasz contacted Igor and is following this action.**  

**Reynald (ESRF)**:
- Ping Tango java developers to ensure they update their projects after the Bintray closure
- Clarify whether the DevEnum are really not supported. Verify that this limitation is well documented
  (It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of
  DevEnum in pipes if it's a real limitation.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.
- Review cppTango!742 (Function name and line number in logs and exceptions). **MR Approved by Sergi. Thanks!**

**Thomas (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26

**Thomas (SKAO)**:
- Organize a Tango Workshop at the next ADASS conference **Thomas said it is too early to send a proposal because the 
ADASS organization committee has not yet been created. SKAO supports the idea of organizing this workshop.**
  - Get in touch with other institutes using Tango in astronomy to see whether they could help (LOFAR, INAF, VIRGO,...) 
**Thomas J. already contacted his former colleagues at LOFAR. 1 system administrator is willing to help in presenting 
how to run Tango in docker containers. Jan David Mol is also willing to help. 
If you're willing to help in any way before and during this workshop, please contact Thomas Juerges before end of January 2022.**
  - Prepare a short description of the workshop and share it with the kernel team (slack and/or kernel mailing list).
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126 
**Thomas will start his review on 3rd December**

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration:
  Drive new repository creation on GitLab with a well known device server. **This action is followed by Łukasz now**

## High Priority Issues

Vincent mentioned some use cases of device server not being properly killed with Starter.  
Some other sites have already experienced the same issue on some device servers (ESRF, Soleil).  
Reynald said that this problem might come from a deadlock occurring during the Device Server shutdown sequence due to a 
bug in cppTango but can also happen due to a bug in the Device Server itself, for instance, when the main thread is 
waiting forever for a device server thread to join (in delete_device()).  

Anton mentioned [pytango!418](https://gitlab.com/tango-controls/pytango/-/merge_requests/418) and would like some help 
from Mateusz Celary.
Łukasz said that Mateusz will come back from holidays during the 2nd half of next week (Week 49). 

## Issues requiring discussion

Anton mentioned a problem with the pipeline from a recent MR on pytango [pytango!433](https://gitlab.com/tango-controls/pytango/-/merge_requests/433).  
The pipeline cannot be started. Michał noticed that the error reported seems to be "failed due to user not being verified". 
So this looks like a security feature of the gitlab pipelines.  (After the meeting, Benjamin provided a link with more [info](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/))

## AOB

### cppTango weekly meetings

The cppTango team is meeting once a week to discuss cppTango issues/MR and to prioritize them. 
If you want to be regularly informed about the date and time of these meetings (they are not always on the same day and at the 
same time), please contact Reynald.

The next cppTango weekly meeting will take place on Thursday 9th December at 13:00 CET, using the same zoom link as the 
Kernel Teleconf Meetings.

### Next Kernel Teleconf Meeting
It has been decided to do the next Tango Kernel Teleconf Meeting in 2 weeks, so we could have 2 Tango Kernel Teleconf 
in December instead of only 1 if we had kept the 2nd and 4th Thursday of the month.  
As a consequence, the next Tango Kernel Teleconf meeting will take place on Thursday 16th December 2021 at 15:00 CET.

## Summary of remaining actions

**All institutes**:
- if you're willing to help in any way for the Tango workshop at the next [ADASS conference](https://www.adass.org) which 
will take place in Victoria, Canada from 11-15 September 2022, please contact Thomas Juerges (SKAO) before end of January 2022.
- Ask java developers to update their projects and remove any reference to bintray
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126.   

**Andy (ESRF)**:
- Update the description of issue cppTango#822 (CORBA transient timeout message).
- Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu.
  Delegate task to one of the new company.
- Ask Freexian for tango Ubuntu packages. Recent Tango versions packages for Ubuntu 18.04 and 20.04 LTS would be appreciated.  
  What about having a tango-controls ppa (personal package archive)?

**Anton (Max IV)**:
- Try out cppTango conda package from [cppTango!875](https://gitlab.com/tango-controls/cpptango/-/merge_requests/875)

**Damien (ESRF)**:
- Prepare a new Pogo release

**Łukasz (S2Innovation)**:
- Follow up with IK the removal of https://tango-rest-api.readthedocs.io
- [tango-ds Sourceforge project](https://sourceforge.net/projects/tango-ds) migration
- Check the IDL versions used by the device servers migrated/to be migrated from tango-ds Sourceforge project (are there any IDL v1 or v2 servers?).

**Reynald (ESRF)**:
- Ping Tango java developers to ensure they update their projects after the Bintray closure
- Clarify whether the DevEnum are really not supported. Verify that this limitation is well documented
  (It seems, according to the user who discussed with Andy) and ask the user to create an issue to ask for the support of
  DevEnum in pipes if it's a real limitation.
- Communicate decision to remove support for IDLv1 and v2 in cppTango to the users via the Tango mailing list and 
give them 2 weeks to give their feedback.

**Sergi (ALBA)**:
- Organize the migration of the repositories he's administrating (PANIC, simulatorDS, VACCA).  PyTangoArchiving will have to be migrated under hdbpp subgroup. Create a subgroup under tango-controls.

**Thomas (byte physics)**:
- Look at https://gitlab.com/tango-controls/TangoDatabase/-/issues/26

**Thomas (SKAO)**:
- Organize a Tango Workshop at the next ADASS conference
- Get in touch with other institutes using Tango in astronomy to see whether they could help (INAF, VIRGO,...)
- Prepare a short description of the workshop and share it with the kernel team (slack and/or kernel mailing list).
- Review and comment Damien's proposal on Pogo: https://gitlab.com/tango-controls/pogo/-/merge_requests/126 

**Vincent (MAX IV)**:
- Review RFC-10 Message update: rfc!124

# Tango Kernel Follow-up Meeting - 2021/12/02

To be held on 2021/12/02 at 15:00 CET on Zoom.

# Agenda

 1. Shall we still continue supporting IDL v1 or IDL v2 devices in cppTango?
 2. RFC workshop organization
 3. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-11-18/Minutes.md#summary-of-remaining-actions)
 4. High priority issues
 5. Issues requiring discussion  
 6. AOB
